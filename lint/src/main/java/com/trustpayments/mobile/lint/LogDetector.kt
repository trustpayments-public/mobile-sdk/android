package com.trustpayments.mobile.lint

import com.android.tools.lint.detector.api.*
import com.intellij.psi.PsiMethod
import org.jetbrains.uast.UCallExpression

@Suppress("UnstableApiUsage")
class LogDetector : Detector(), SourceCodeScanner {
    companion object {
        private val briefDescription = "Direct Log usage is not allowed."
        private val explanation = "Please use :core:log() method instead."
        private val description = "$briefDescription $explanation"

        val ISSUE = Issue.create(
            id = "LogDetector",
            briefDescription = briefDescription,
            explanation = explanation,
            category = Category.CORRECTNESS,
            priority = 9,
            severity = Severity.ERROR,
            androidSpecific = true,
            implementation = Implementation(LogDetector::class.java, Scope.JAVA_FILE_SCOPE)
        )
    }

    override fun getApplicableMethodNames(): List<String> =
        listOf("tag", "format", "v", "d", "i", "w", "e", "wtf")

    override fun visitMethodCall(context: JavaContext, node: UCallExpression, method: PsiMethod) {
        super.visitMethodCall(context, node, method)

        val evaluator = context.evaluator
        if (evaluator.isMemberInClass(method, "android.util.Log")) {
            context.report(
                issue = ISSUE,
                scope = node,
                location = context.getCallLocation(
                    call = node,
                    includeReceiver = true,
                    includeArguments = true
                ),
                message = description
            )
        }
    }
}