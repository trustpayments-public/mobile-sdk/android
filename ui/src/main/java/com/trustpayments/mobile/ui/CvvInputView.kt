package com.trustpayments.mobile.ui

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import com.trustpayments.mobile.ui.card.CardValidator
import com.trustpayments.mobile.ui.card.model.CardType
import com.trustpayments.mobile.ui.common.BaseInputView
import com.trustpayments.mobile.ui.model.PaymentInputType

class CvvInputView(context: Context, attrs: AttributeSet?) : BaseInputView(context, attrs),
    CvvInputViewModel.CvvInputCallbacks {

    private val viewModel = CvvInputViewModel(CardValidator(), this)

    init {
        initView()
    }

    private var beforeText = ""

    private fun initView() {
        setInputMaxLength(3)
        binding.apply {
            input.inputType = InputType.TYPE_CLASS_NUMBER or
                    InputType.TYPE_NUMBER_VARIATION_PASSWORD or
                    InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS

            input.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {}

                override fun beforeTextChanged(
                    text: CharSequence?, start: Int, count: Int, after: Int
                ) {
                    beforeText = text.toString()
                }

                override fun onTextChanged(
                    text: CharSequence?, start: Int, before: Int, count: Int
                ) {
                    viewModel.checkForMaxLengthAndValidate(input.text.toString())
                }
            })

            input.setOnFocusChangeListener { _, hasFocus ->
                if (!hasFocus) {
                    viewModel.validateCvv(input.text.toString())
                }
            }
        }
    }

    override fun getLabelText(): String =
        resources.getString(R.string.pvc_cvv_label)

    override fun getHintText(): String =
        resources.getString(R.string.pvc_cvv_hint)

    override fun getStartIcon(): Drawable? =
        ContextCompat.getDrawable(context, R.drawable.ic_padlock)

    /**
     * Setter for a card type connected with this CVV input.
     * Required for the correct view initialization (text length, hint or enabling/disabling)
     */
    fun setCardType(cardType: CardType) {
        viewModel.handleCardType(cardType)
    }

    override fun onCvvValid(input: String) {
        resetError()
        // pass valid input data to the drop-in view
        inputValidationListener?.onInputValid(PaymentInputType.CVV, input)
    }

    override fun onCvvInvalid(showError: Boolean) {
        if (showError) {
            setError(R.string.pvc_input_error_text)
        } else {
            resetError()
        }
        // notify a drop-in view about validation state change
        inputValidationListener?.onInputInvalid(PaymentInputType.CVV)
    }

    override fun onCvvLengthChanged(maxLength: Int) {
        binding.apply {
            if (maxLength != getInputMaxLength()) {
                setInputMaxLength(maxLength)

                input.hint = when (maxLength) {
                    4 -> resources.getString(R.string.pvc_cvv_4_digits_hint)
                    else -> resources.getString(R.string.pvc_cvv_hint)
                }

                if (input.text.toString().isNotEmpty()) {
                    viewModel.validateCvv(input.text.toString())
                } else {
                    onCvvInvalid(false)
                }
            }
        }
    }
}