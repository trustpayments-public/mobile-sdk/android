package com.trustpayments.mobile.core.testutils

import android.view.View
import androidx.test.espresso.Espresso
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.PaymentSession
import com.trustpayments.mobile.core.models.JwtResponse
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.models.api.response.ResponseErrorCode
import com.trustpayments.mobile.core.models.api.response.TransactionResponse
import com.trustpayments.mobile.core.services.api.TrustPaymentsGatewayType
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.util.OldTransactionResponse
import com.trustpayments.mobile.core.util.PaymentSessionResponse
import com.trustpayments.mobile.core.util.ResponseParser
import org.hamcrest.Matcher
import com.trustpayments.mobile.utils.core.waitForCondition
import java.util.*

fun PaymentTransactionManager.createSessionFor(
    jwtToken: String,
    cardNumber: String? = null,
    cvv: String? = null
): PaymentSession {
    // according to the info stated on the Cardinal wiki, an expiry date should be calculated in the
    // following manner:
    // "The expiration year used in the Test Value should reflect the current year +3.
    // For Example: If the current year is 2019, the Expiration Date Test Value would be 01/2022."
    // resource: https://cardinaldocs.atlassian.net/wiki/spaces/CCen/pages/903577725/EMV+3DS+2.0+Test+Cases
    val calendar = Calendar.getInstance()
    val expiryDateYear = calendar.get(Calendar.YEAR) + 3
    val expiryDate = "01/$expiryDateYear"
    return this.createSession({ jwtToken }, cardNumber, expiryDate, cvv)
}

@Deprecated("This function converts new SDK response format to the old one and should be treated as a temporary solution used during migration. It should no longer be used when writing new tests or fixing old ones.")
fun PaymentSessionResponse.getAsSuccess(requestIndex: Int): OldTransactionResponse.PaymentSuccess = when (this) {
    is PaymentSessionResponse.Success -> this.allResponses[requestIndex]
    is PaymentSessionResponse.Failure.TransactionFailure -> this.allResponses[requestIndex] as OldTransactionResponse.PaymentSuccess
    else -> throw IllegalStateException()
}

@Deprecated("This function converts new SDK response format to the old one and should be treated as a temporary solution used during migration. It should no longer be used when writing new tests or fixing old ones.")
fun PaymentSessionResponse.getAsFailure(requestIndex: Int): OldTransactionResponse.Error.TransactionError =
    (this as PaymentSessionResponse.Failure.TransactionFailure).allResponses[requestIndex] as OldTransactionResponse.Error.TransactionError

@Deprecated("This function converts new SDK response format to the old one and should be treated as a temporary solution used during migration. It should no longer be used when writing new tests or fixing old ones.")
fun PaymentSessionResponse.getAsParsingError(requestIndex: Int): OldTransactionResponse.Error.ParsingError =
    (this as PaymentSessionResponse.Failure.TransactionFailure).allResponses[requestIndex] as OldTransactionResponse.Error.ParsingError

@Deprecated("This function converts new SDK response format to the old one and should be treated as a temporary solution used during migration. It should no longer be used when writing new tests or fixing old ones.")
fun PaymentSessionResponse.getAs3DError(requestIndex: Int): OldTransactionResponse.Error.ThreeDSecureError =
    (this as PaymentSessionResponse.Failure.TransactionFailure).allResponses[requestIndex] as OldTransactionResponse.Error.ThreeDSecureError

@Deprecated("This function converts new SDK response format to the old one and should be treated as a temporary solution used during migration. It should no longer be used when writing new tests or fixing old ones.")
fun PaymentSessionResponse.getResultAsSafetyError(): PaymentSessionResponse.Failure.SafetyError =
    this as PaymentSessionResponse.Failure.SafetyError

fun PaymentTransactionManager.Response.getRiskDecTransactionResponse(): TransactionResponse =
    ResponseParser.parse(this.responseJwtList)!![0].responses.first {
        it.requestTypeDescription == RequestType.RiskDec
    }

fun PaymentTransactionManager.Response.getRiskDecTransactionResponseWithThreeDQuery(): TransactionResponse =
        ResponseParser.parse(this.responseJwtList)!!.first {
            it.responses.find { it.requestTypeDescription == RequestType.RiskDec } != null
        }.responses.find { it.requestTypeDescription == RequestType.RiskDec }!!

fun PaymentTransactionManager.Response.getThreeDQueryTransactionResponse(): TransactionResponse =
    ResponseParser.parse(this.responseJwtList)!!.flatMap { r: JwtResponse -> r.responses }.first {
        it.requestTypeDescription == RequestType.ThreeDQuery
    }

fun PaymentTransactionManager.Response.getAuthTransactionResponse(): TransactionResponse =
    ResponseParser.parse(this.responseJwtList)!!.flatMap { r: JwtResponse -> r.responses }.first {
        it.requestTypeDescription == RequestType.Auth
    }

fun PaymentTransactionManager.Response.areAllResponsesSuccessful(): Boolean {
    val parsedResponse = ResponseParser.parse(this.responseJwtList)
        ?: return false

    val allResponses: List<TransactionResponse> =
        parsedResponse.flatMap { r: JwtResponse -> r.responses }
    val errorResponse = allResponses.find { it.errorCode != ResponseErrorCode.Ok }

    return if (errorResponse != null) {
        return false
    } else true
}

data class ThreeDQueryResponse(
    val enrolled: String?,
    val paresStatus: String?,
    val eciFlag: String?
)

fun createThreeDQueryResponse(
    enrolled: String? = null,
    paresStatus: String? = null,
    eciFlag: String? = null
): ThreeDQueryResponse =
    ThreeDQueryResponse(
        enrolled,
        paresStatus,
        eciFlag
    )

data class RiskDecResponse(
    val fraudControlShieldStatusCode: String,
    val fraudControlResponseCodes: List<String>
)

fun waitUntilElementIsDisplayed(
    viewMatcher: Matcher<View>,
    timeout: Int = 8000
) {
    waitForCondition({
        try {
            Espresso.onView(viewMatcher).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            true
        } catch (ex: NoMatchingViewException) {
            false
        }
    }, timeout.toLong())
}

fun getGatewayType(): TrustPaymentsGatewayType = if(BuildConfig.GATEWAY == "US") {
    TrustPaymentsGatewayType.US
} else {
    TrustPaymentsGatewayType.EU
}

fun PaymentTransactionManager.Response.getTransactionResponseWithTypeOrNull(responseListIndex: Int, index: Int, type: RequestType): TransactionResponse? {
    val responses = ResponseParser.parse(this.responseJwtList)!![responseListIndex].responses
    return if(responses[index].requestTypeDescription == type) {
        responses[index]
    } else {
        null
    }
}

fun PaymentTransactionManager.Response.getResponseCount(): Int =
        ResponseParser.parse(this.responseJwtList)!!.sumBy { it.responses.size }