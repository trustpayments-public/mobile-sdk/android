package com.trustpayments.mobile.core.testcases.eu

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.testutils.createSessionFor
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.util.PaymentSessionResponse
import com.trustpayments.mobile.core.util.parse
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class PaymentTransactionManagerResponseTypesTestCases: BaseTestCase() {

    private lateinit var paymentTransactionManager: PaymentTransactionManager

    private val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    private lateinit var jwtToken: String

    @Before
    override fun setUp() {
        super.setUp()

        jwtToken = jwtBuilder.getStandard(listOf(RequestType.Auth))

        val app = ApplicationProvider.getApplicationContext<Context>().applicationContext
        paymentTransactionManager =
            PaymentTransactionManager(app, getGatewayType(), false, BuildConfig.MERCHANT_USERNAME, isLocationDataConsentGiven = false)
    }

    @Test
    fun test_positive_successfulPaymentSessionResponse() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "4000000000001026", "123")

        // WHEN
        val sessionResult = paymentTransactionManager.executeSession(session).parse()

        // THEN
        Assert.assertTrue(sessionResult is PaymentSessionResponse.Success)
    }

    @Test
    fun test_negative_failedPaymentSessionResponse() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "4242424242424242", "123")

        // WHEN
        val sessionResult = paymentTransactionManager.executeSession(session).parse()

        // THEN
        Assert.assertTrue(sessionResult is PaymentSessionResponse.Failure.TransactionFailure)
    }
}