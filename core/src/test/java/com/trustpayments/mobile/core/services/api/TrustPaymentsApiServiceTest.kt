package com.trustpayments.mobile.core.services.api

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.models.api.jwt.JwtToken
import com.trustpayments.mobile.core.models.api.request.TransactionRequest
import com.trustpayments.mobile.core.models.api.request.WebserviceRequest
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.models.api.response.WebserviceResponse
import com.trustpayments.mobile.core.services.transaction.LoggingManager
import com.trustpayments.mobile.core.util.JWTBuilder
import io.mockk.MockKAnnotations
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.just
import io.mockk.mockk
import io.mockk.mockkClass
import io.mockk.mockkObject
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import retrofit2.Response
import retrofit2.Retrofit
import java.net.ConnectException
import java.net.SocketTimeoutException
import kotlin.reflect.full.declaredMemberFunctions
import kotlin.reflect.jvm.isAccessible

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.M])
class TrustPaymentsApiServiceTest {

    @MockK
    private lateinit var mockContext: Context

    @MockK
    private lateinit var mockLoggingManager: LoggingManager

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        mockkObject(LoggingManager.Companion)
        every { LoggingManager.Companion.getInstance(any()) } returns mockLoggingManager
    }


    @Test
    fun test_initialize_session_generic_failure_flow() = runBlocking {
        // Arrange
        val mockApi: TrustPaymentsApi = mockk()
        val responseBody: ResponseBody = "test".toResponseBody()
        val mockRetrofit = mockkClass(Retrofit::class)
        every { mockRetrofit.create(TrustPaymentsApi::class.java) } returns mockApi
        coEvery {
            mockApi.sendJwtRequest(ofType(WebserviceRequest::class))
        } returns Response.error(
            404,
            responseBody
        )

        val packageManager: PackageManager = mockk()
        every { mockContext.packageManager } returns packageManager
        every { mockContext.packageName } returns "TEST"
        every { packageManager.getPackageInfo("TEST", 0) } returns mockk()

        // Act
        val service: TrustPaymentsApiService = TrustPaymentsApiService.getInstance(
            context = mockContext,
            gatewayType = TrustPaymentsGatewayType.EU,
            loggingManager = mockLoggingManager
        )
        val result = service.initializeSession("test", "test", "test")

        // Assert
        assertTrue(result is TrustPaymentsApiService.Result.Failure)
    }

    @Test
    fun test_initialize_session_failure_flow_on_socket_timeout() = runBlocking {
        // Arrange
        val mockApi: TrustPaymentsApi = mockk()
        val mockRetrofit = mockkClass(Retrofit::class)
        every { mockRetrofit.create(TrustPaymentsApi::class.java) } returns mockApi
        coEvery {
            mockApi.sendJwtRequest(ofType(WebserviceRequest::class))
        } throws SocketTimeoutException()

        val packageManager: PackageManager = mockk()
        every { mockContext.packageManager } returns packageManager
        every { mockContext.packageName } returns "TEST"
        every { packageManager.getPackageInfo("TEST", 0) } returns mockk()

        // Act
        val service: TrustPaymentsApiService = TrustPaymentsApiService.getInstance(
            context = mockContext,
            gatewayType = TrustPaymentsGatewayType.EU,
            loggingManager = mockLoggingManager
        )
        val result = service.initializeSession("test", "test", "test")

        // Assert
        assertTrue(result is TrustPaymentsApiService.Result.Failure)
    }

    @Test
    fun test_initialize_session_failure_flow_on_connection_exception() = runBlocking {
        // Arrange
        val mockApi: TrustPaymentsApi = mockk()
        val mockRetrofit = mockkClass(Retrofit::class)
        every { mockRetrofit.create(TrustPaymentsApi::class.java) } returns mockApi
        coEvery {
            mockApi.sendJwtRequest(ofType(WebserviceRequest::class))
        } throws ConnectException()

        val packageManager: PackageManager = mockk()
        every { mockContext.packageManager } returns packageManager
        every { mockContext.packageName } returns "TEST"
        every { packageManager.getPackageInfo("TEST", 0) } returns mockk()

        // Act
        val service: TrustPaymentsApiService = TrustPaymentsApiService.getInstance(
            context = mockContext,
            gatewayType = TrustPaymentsGatewayType.EU,
            loggingManager = mockLoggingManager
        )
        val result = service.initializeSession("test", "test", "test")

        // Assert
        assertTrue(result is TrustPaymentsApiService.Result.Failure)
    }

    @Test
    fun test_initialize_session_failure_flow_on_generic_exception() = runBlocking {
        // Arrange
        val mockApi: TrustPaymentsApi = mockk()
        val mockRetrofit = mockkClass(Retrofit::class)
        every { mockRetrofit.create(TrustPaymentsApi::class.java) } returns mockApi
        coEvery {
            mockApi.sendJwtRequest(ofType(WebserviceRequest::class))
        } throws ConnectException()

        val packageManager: PackageManager = mockk()
        every { mockContext.packageManager } returns packageManager
        every { mockContext.packageName } returns "TEST"
        every { packageManager.getPackageInfo("TEST", 0) } returns mockk()

        // Act
        val service: TrustPaymentsApiService = TrustPaymentsApiService.getInstance(
            context = mockContext,
            gatewayType = TrustPaymentsGatewayType.EU,
            loggingManager = mockLoggingManager
        )
        val result = service.initializeSession("test", "test", "test")

        // Assert
        assertTrue(result is TrustPaymentsApiService.Result.Failure)
    }

    @Test
    fun test_perform_transaction_generic_failure_flow() = runBlocking {
        // Arrange
        val mockApi: TrustPaymentsApi = mockk()
        val responseBody: ResponseBody = "test".toResponseBody()
        val mockRetrofit = mockkClass(Retrofit::class)
        every { mockRetrofit.create(TrustPaymentsApi::class.java) } returns mockApi
        coEvery {
            mockApi.sendJwtRequest(ofType(WebserviceRequest::class))
        } returns Response.error(
            404,
            responseBody
        )

        val packageManager: PackageManager = mockk()
        every { mockContext.packageManager } returns packageManager
        every { mockContext.packageName } returns "TEST"
        every { packageManager.getPackageInfo("TEST", 0) } returns mockk()

        // Act
        val service: TrustPaymentsApiService = TrustPaymentsApiService.getInstance(
            context = mockContext,
            gatewayType = TrustPaymentsGatewayType.EU,
            loggingManager = mockLoggingManager
        )
        val result = service.performTransaction("test", "test", "test")
        // Assert
        assertTrue(result is TrustPaymentsApiService.Result.Failure)
    }

    @Test
    fun test_perform_transaction_failure_flow_on_socket_timeout() = runBlocking {
        // Arrange
        val mockApi: TrustPaymentsApi = mockk()
        val mockRetrofit = mockkClass(Retrofit::class)
        every { mockRetrofit.create(TrustPaymentsApi::class.java) } returns mockApi
        coEvery {
            mockApi.sendJwtRequest(ofType(WebserviceRequest::class))
        } throws SocketTimeoutException()

        val packageManager: PackageManager = mockk()
        every { mockContext.packageManager } returns packageManager
        every { mockContext.packageName } returns "TEST"
        every { packageManager.getPackageInfo("TEST", 0) } returns mockk()
        // Act
        val service: TrustPaymentsApiService = TrustPaymentsApiService.getInstance(
            context = mockContext,
            gatewayType = TrustPaymentsGatewayType.EU,
            loggingManager = mockLoggingManager
        )
        val result = service.performTransaction(
            sessionId = "test",
            username = "test",
            jwt = "test",
            cacheToken = "test",
            cardNumber = "4111111111111111",
            cardExpiryDate = "12/30",
            cardSecurityCode = "123",
            threeDResponse = "test",
            pares = "test",
            walletToken = "test",
            walletSource = "test",
            returnUrl = "",
            apmCode = "test",
        )
        // Assert
        assertTrue(result is TrustPaymentsApiService.Result.Failure)
    }

    @Test
    fun test_transaction_request_body() = runBlocking {
        // Arrange
        val requestTypes = arrayOf(RequestType.Auth)
        val transactionRequest = TransactionRequest(
            requestId = "requestId",
            threeDResponse = "threeDResponse",
            pares = "pares",
            cacheToken = "cacheToken",
            walletToken = "walletToken",
            walletSource = "walletSource",
            requestTypes = requestTypes,
            cardNumber = "41111111111111111",
            cardSecurityCode = "123",
            cardExpiryDate = "04/30",
            returnUrl = "returnUrl",
            apmCode = "apmCode",
        )


        val requests = arrayOf(transactionRequest)
        val webserviceRequest = WebserviceRequest(
            alias = "alias",
            jwt = "jwt",
            version = "version",
            versionInfo = "versionInfo",
            acceptCustomerOutput = "acceptCustomerOutput",
            requests = requests,
        )

        // Assert
        assertEquals("requestId", transactionRequest.requestId)
        assertEquals("threeDResponse", transactionRequest.threeDResponse)
        assertEquals("pares", transactionRequest.pares)
        assertEquals("cacheToken", transactionRequest.cacheToken)
        assertEquals("walletToken", transactionRequest.walletToken)
        assertEquals("walletSource", transactionRequest.walletSource)
        assertEquals("41111111111111111", transactionRequest.cardNumber)
        assertEquals("123", transactionRequest.cardSecurityCode)
        assertEquals("04/30", transactionRequest.cardExpiryDate)
        assertEquals("returnUrl", transactionRequest.returnUrl)
        assertEquals("apmCode", transactionRequest.apmCode)
        assertEquals(requestTypes.hashCode(), transactionRequest.requestTypes.hashCode())

        assertEquals("alias", webserviceRequest.alias)
        assertEquals("jwt", webserviceRequest.jwt)
        assertEquals("version", webserviceRequest.version)
        assertEquals("versionInfo", webserviceRequest.versionInfo)
        assertEquals("acceptCustomerOutput", webserviceRequest.acceptCustomerOutput)
        assertEquals(requests.hashCode(), webserviceRequest.requests.hashCode())
    }

    @Test
    fun test_validateResponse_when_response_failure() = runBlocking {
        // Arrange
        val service: TrustPaymentsApiService = TrustPaymentsApiService.getInstance(
            context = mockContext,
            gatewayType = TrustPaymentsGatewayType.EU,
            loggingManager = mockLoggingManager
        )
        val method =
            TrustPaymentsApiService::class.declaredMemberFunctions.single {
                it.name == "validateResponse"
            }
        method.isAccessible = true
        val response = mockk<Response<WebserviceResponse>>()
        val webserviceResponse = mockk<WebserviceResponse>()

        every { response.isSuccessful } returns false
        every { response.body() } returns webserviceResponse
        every { response.code() } returns 400
        every { response.message() } returns "error"
        every {
            mockLoggingManager.sendErrorEvent(
                any(), any(), any(), any(), any(),
            )
        } just Runs

        // Act
        val result = method.call(service, response) as TrustPaymentsApiService.Result

        // Assert
        assertTrue(result is TrustPaymentsApiService.Result.Failure)
        verify(exactly = 1) {
            mockLoggingManager.sendErrorEvent(
                any(), any(), any(), any(), any(),
            )
        }
    }

    @Test
    fun test_validateResponse_when_response_body_null() = runBlocking {
        // Arrange
        val service: TrustPaymentsApiService = TrustPaymentsApiService.getInstance(
            context = mockContext,
            gatewayType = TrustPaymentsGatewayType.EU,
            loggingManager = mockLoggingManager
        )
        val method =
            TrustPaymentsApiService::class.declaredMemberFunctions.single {
                it.name == "validateResponse"
            }
        method.isAccessible = true
        val response = mockk<Response<WebserviceResponse>>()

        every { response.isSuccessful } returns true
        every { response.body() } returns null
        every { response.code() } returns 400
        every { response.message() } returns "error"
        every {
            mockLoggingManager.sendErrorEvent(
                any(), any(), any()
            )
        } just Runs

        // Act
        val result = method.call(service, response) as TrustPaymentsApiService.Result

        // Assert
        assertTrue(result is TrustPaymentsApiService.Result.Failure)
        verify(exactly = 1) {
            mockLoggingManager.sendErrorEvent(
                any(), any(), any()
            )
        }
    }

    @Test
    fun test_validateResponse_when_response_jwt_invalid() = runBlocking {
        // Arrange
        val service: TrustPaymentsApiService = TrustPaymentsApiService.getInstance(
            context = mockContext,
            gatewayType = TrustPaymentsGatewayType.EU,
            loggingManager = mockLoggingManager
        )
        val method =
            TrustPaymentsApiService::class.declaredMemberFunctions.single {
                it.name == "validateResponse"
            }
        method.isAccessible = true
        val response = mockk<Response<WebserviceResponse>>()
        val webserviceResponse = mockk<WebserviceResponse>()
        val webserviceBody = mockk<WebserviceResponse.Body>()
        val webservicePayload = mockk<WebserviceResponse.Body.Payload>()
        val token = "aaaa-.aaaa_.bbbb"

        every { response.isSuccessful } returns true
        every { response.body() } returns webserviceResponse
        every { webserviceResponse.body } returns webserviceBody
        every { webserviceBody.payload } returns webservicePayload
        every { webservicePayload.jwt } returns token
        every {
            mockLoggingManager.sendErrorEvent(
                any(), any(), any()
            )
        } just Runs

        // Act
        val result = method.call(service, response) as TrustPaymentsApiService.Result

        // Assert
        assertTrue(result is TrustPaymentsApiService.Result.Failure)
        verify(exactly = 1) {
            mockLoggingManager.sendErrorEvent(
                any(), any(), any()
            )
        }
    }

    @Test
    fun test_validateResponse_when_response_jwt_valid() = runBlocking {
        // Arrange
        val service: TrustPaymentsApiService = TrustPaymentsApiService.getInstance(
            context = mockContext,
            gatewayType = TrustPaymentsGatewayType.EU,
            loggingManager = mockLoggingManager
        )
        val method =
            TrustPaymentsApiService::class.declaredMemberFunctions.single {
                it.name == "validateResponse"
            }
        method.isAccessible = true
        val response = mockk<Response<WebserviceResponse>>()
        val webserviceResponse = mockk<WebserviceResponse>()
        val webserviceBody = mockk<WebserviceResponse.Body>()
        val jwtToken = mockk<JwtToken>()
        val webservicePayload = mockk<WebserviceResponse.Body.Payload>()

        val jwtBuilder = JWTBuilder(
            BuildConfig.MERCHANT_USERNAME,
            BuildConfig.SITE_REFERENCE,
            BuildConfig.JWT_KEY
        )
        val newJwt = jwtBuilder.getStandard(emptyList())

        every { response.isSuccessful } returns true
        every { response.body() } returns webserviceResponse
        every { webserviceResponse.body } returns webserviceBody
        every { webserviceResponse.jwt } returns jwtToken
        every { webserviceBody.payload } returns webservicePayload
        every { webservicePayload.jwt } returns newJwt
        every { webservicePayload.transactionResponses } returns listOf()

        // Act
        val result = method.call(service, response) as TrustPaymentsApiService.Result

        // Assert
        assertTrue(result is TrustPaymentsApiService.Result.Success)
    }
}