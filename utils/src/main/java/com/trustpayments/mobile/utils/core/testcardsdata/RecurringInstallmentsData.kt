package com.trustpayments.mobile.utils.core.testcardsdata

object RecurringInstallmentsData {
    val subscriptionType_Recurring = "Recurring"
    val subscriptionType_Installments = "Installments"
    val numberOfPayments_Three = "3"
    val numberOfPayments_Six = "6"
    val numberOfPayments_twelve = "12"
    val frequency_SevenDays = "7 days"
    val frequency_OneMonth = "1 month"
    val frequency_TwoMonths = "2 months"

}
