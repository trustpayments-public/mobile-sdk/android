package com.trustpayments.mobile.ui.card

import com.trustpayments.mobile.ui.card.model.CardType
import org.junit.Assert.assertEquals
import org.junit.Test

class CardInputMaskTest {

    @Test
    fun test_CorrectInputMaskForVisa() {
        // GIVEN
        val expectedInputMask = "#### #### #### #### ###"

        // WHEN
        val actualInputMask = CardInfo.getInputMask(CardType.Visa)

        // THEN
        assertEquals(expectedInputMask, actualInputMask)
    }

    @Test
    fun test_CorrectInputMaskForMastercard() {
        // GIVEN
        val expectedInputMask = "#### #### #### ####"

        // WHEN
        val actualInputMask = CardInfo.getInputMask(CardType.Mastercard)

        // THEN
        assertEquals(expectedInputMask, actualInputMask)
    }

    @Test
    fun test_CorrectInputMaskForAmex() {
        // GIVEN
        val expectedInputMask = "#### ###### #####"

        // WHEN
        val actualInputMask = CardInfo.getInputMask(CardType.Amex)

        // THEN
        assertEquals(expectedInputMask, actualInputMask)
    }

    @Test
    fun test_CorrectInputMaskForMaestro() {
        // GIVEN
        val expectedInputMask = "#### #### #### #### ###"

        // WHEN
        val actualInputMask = CardInfo.getInputMask(CardType.Maestro)

        // THEN
        assertEquals(expectedInputMask, actualInputMask)
    }

    @Test
    fun test_CorrectInputMaskForDiscover() {
        // GIVEN
        val expectedInputMask = "#### #### #### #### ###"

        // WHEN
        val actualInputMask = CardInfo.getInputMask(CardType.Discover)

        // THEN
        assertEquals(expectedInputMask, actualInputMask)
    }

    @Test
    fun test_CorrectInputMaskForDiners() {
        // GIVEN
        val expectedInputMask = "#### ###### ####"

        // WHEN
        val actualInputMask = CardInfo.getInputMask(CardType.Diners)

        // THEN
        assertEquals(expectedInputMask, actualInputMask)
    }

    @Test
    fun test_CorrectInputMaskForJCB() {
        // GIVEN
        val expectedInputMask = "#### #### #### #### ###"

        // WHEN
        val actualInputMask = CardInfo.getInputMask(CardType.JCB)

        // THEN
        assertEquals(expectedInputMask, actualInputMask)
    }

    @Test
    fun test_CorrectInputMaskForUnknown() {
        // GIVEN
        val expectedInputMask = "#### #### #### ####"

        // WHEN
        val actualInputMask  = CardInfo.getInputMask(CardType.Unknown)

        // THEN
        assertEquals(expectedInputMask, actualInputMask)
    }

    @Test
    fun test_CorrectInputMaskLengthForVisa() {
        // GIVEN
        val expectedInputMaskLength = 23

        // WHEN
        val actualInputMaskLength = CardInfo.getMaxMaskLength(CardType.Visa)

        // THEN
        assertEquals(expectedInputMaskLength, actualInputMaskLength)
    }

    @Test
    fun test_CorrectInputMaskLengthForMastercard() {
        // GIVEN
        val expectedInputMaskLength = 19

        // WHEN
        val actualInputMaskLength = CardInfo.getMaxMaskLength(CardType.Mastercard)

        // THEN
        assertEquals(expectedInputMaskLength, actualInputMaskLength)
    }

    @Test
    fun test_CorrectInputMaskLengthForAmex() {
        // GIVEN
        val expectedInputMaskLength = 17

        // WHEN
        val actualInputMaskLength = CardInfo.getMaxMaskLength(CardType.Amex)

        // THEN
        assertEquals(expectedInputMaskLength, actualInputMaskLength)
    }

    @Test
    fun test_CorrectInputMaskLengthForMaestro() {
        // GIVEN
        val expectedInputMaskLength = 23

        // WHEN
        val actualInputMaskLength = CardInfo.getMaxMaskLength(CardType.Maestro)

        // THEN
        assertEquals(expectedInputMaskLength, actualInputMaskLength)
    }

    @Test
    fun test_CorrectInputMaskLengthForDiscover() {
        // GIVEN
        val expectedInputMaskLength = 23

        // WHEN
        val actualInputMaskLength = CardInfo.getMaxMaskLength(CardType.Discover)

        // THEN
        assertEquals(expectedInputMaskLength, actualInputMaskLength)
    }

    @Test
    fun test_CorrectInputMaskLengthForDiners() {
        // GIVEN
        val expectedInputMaskLength = 16

        // WHEN
        val actualInputMaskLength = CardInfo.getMaxMaskLength(CardType.Diners)

        // THEN
        assertEquals(expectedInputMaskLength, actualInputMaskLength)
    }

    @Test
    fun test_CorrectInputMaskLengthForJCB() {
        // GIVEN
        val expectedInputMaskLength = 23

        // WHEN
        val actualInputMaskLength = CardInfo.getMaxMaskLength(CardType.JCB)

        // THEN
        assertEquals(expectedInputMaskLength, actualInputMaskLength)
    }
}