package com.trustpayments.mobile.utils.core.testcardsdata

object CardsDataWith3DSecureV1 {
    val masterCardNumber = "5200000000000007"
    val visaCardNumber = "4000000000000036"
    val discoverCardNumber = "6011000000000004"
    val unauthenticatedErrorVisaCardNumber = "4000000000000028"
    val passiveAuthencticationAmexCardNumber = "340000000003391"
    val passiveAuthenticationJCBCardNumber = "3528000000000411"
    val bankSystemErrorDiscoverCardNumber = "6011000000000079"
    val passwordOnWebView = "1234"
}