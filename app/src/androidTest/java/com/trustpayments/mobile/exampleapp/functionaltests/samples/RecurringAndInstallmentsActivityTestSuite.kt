package com.trustpayments.mobile.exampleapp.functionaltests.samples

import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.trustpayments.mobile.exampleapp.BuildConfig
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.common.jwt.RecurringAndInstallmentsPayload
import com.trustpayments.mobile.exampleapp.samples.recurringinstallments.RecurringAndInstallmentsSampleActivity
import com.trustpayments.mobile.exampleapp.utils.TestUtils
import com.trustpayments.mobile.ui.model.RequestType
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class RecurringAndInstallmentsActivityTestSuite {
    @get:Rule
    val activityScenarioRule =
        ActivityScenarioRule(RecurringAndInstallmentsSampleActivity::class.java)
    private lateinit var json: Json

    @Before
    fun setUp() {
        json = Json { encodeDefaults = false }
    }

    @After
    fun tearDown() {
        activityScenarioRule.scenario?.close()
    }

    @Test
    fun testRecurringRequestSuccess(): Unit = runBlocking {
        // Move the activity to the RESUMED state
        activityScenarioRule.scenario.moveToState(Lifecycle.State.RESUMED)

        // Perform actions inside the activity
        activityScenarioRule.scenario?.onActivity { activity ->
            // Modify the RecurringAndInstallmentsPayload to simulate an error scenario
            activity.payload = json.encodeToString(
                RecurringAndInstallmentsPayload.serializer(), RecurringAndInstallmentsPayload(
                    siteReference = BuildConfig.SITE_REFERENCE,
                    currencyISO3a = "GBP",
                    baseAmount = 1050,
                    requestTypeDescriptions = listOf(
                        RequestType.AccountCheck.serializedName,
                        RequestType.Subscription.serializedName
                    ),
                    subscriptionUnit = "MONTH",
                    subscriptionFrequency = 1,
                    subscriptionNumber = 3,
                    subscriptionFinalNumber = 12,
                    subscriptionType = "RECURRING"
                )
            )
        }

        // Click the "Run" button
        onView(withId(R.id.btn_run)).perform(click())

        // Wait for the loading indicator to become invisible within 30 seconds
        TestUtils.waitForVisibleView(
            R.id.loading_indicator,
            30000,
            false,
            ViewMatchers.withText(R.string.hs_transaction_success)
        )

        onView(ViewMatchers.withText(R.string.hs_transaction_success))
            .inRoot(RootMatchers.isDialog())
            .check(matches(isDisplayed()))
            .perform(click())
    }

    @Test
    fun testInstallmentsRequestSuccess(): Unit = runBlocking {
        // Move the activity to the RESUMED state
        activityScenarioRule.scenario.moveToState(Lifecycle.State.RESUMED)

        // Perform actions inside the activity
        activityScenarioRule.scenario?.onActivity { activity ->
            // Modify the RecurringAndInstallmentsPayload to simulate an error scenario
            activity.payload = json.encodeToString(
                RecurringAndInstallmentsPayload.serializer(), RecurringAndInstallmentsPayload(
                    siteReference = BuildConfig.SITE_REFERENCE,
                    currencyISO3a = "GBP",
                    baseAmount = 1050,
                    requestTypeDescriptions = listOf(
                        RequestType.AccountCheck.serializedName,
                        RequestType.Subscription.serializedName
                    ),
                    subscriptionUnit = "MONTH",
                    subscriptionFrequency = 1,
                    subscriptionNumber = 3,
                    subscriptionFinalNumber = 12,
                    subscriptionType = "INSTALLMENT"
                )
            )
        }

        // Click the "Run" button
        onView(withId(R.id.btn_run)).perform(click())

        // Wait for the loading indicator to become invisible within 30 seconds
        TestUtils.waitForVisibleView(
            R.id.loading_indicator,
            30000,
            false,
            ViewMatchers.withText(R.string.hs_transaction_success)
        )

        onView(ViewMatchers.withText(R.string.hs_transaction_success))
            .inRoot(RootMatchers.isDialog())
            .check(matches(isDisplayed()))
            .perform(click())
    }

    @Test
    fun testRequestError(): Unit = runBlocking {
        // Move the activity to the RESUMED state
        activityScenarioRule.scenario.moveToState(Lifecycle.State.RESUMED)

        // Perform actions inside the activity
        activityScenarioRule.scenario?.onActivity { activity ->
            // Modify the RecurringAndInstallmentsPayload to simulate an error scenario
            activity.payload = json.encodeToString(
                RecurringAndInstallmentsPayload.serializer(), RecurringAndInstallmentsPayload(
                    siteReference = BuildConfig.SITE_REFERENCE,
                    currencyISO3a = "GBP",
                    baseAmount = 1050,
                    requestTypeDescriptions = listOf(
                        RequestType.AccountCheck.serializedName,
                        RequestType.Subscription.serializedName
                    ),
                    subscriptionUnit = "NONE",
                    subscriptionFrequency = 1,
                    subscriptionNumber = 3,
                    subscriptionFinalNumber = 12,
                    subscriptionType = "INSTALLMENT"
                )
            )
        }

        // Click the "Run" button
        onView(withId(R.id.btn_run)).perform(click())

        // Wait for the loading indicator to become invisible within 30 seconds
        TestUtils.waitForVisibleView(
            R.id.loading_indicator,
            30000,
            false,
            ViewMatchers.withText(R.string.hs_transaction_failed)
        )

        onView(ViewMatchers.withText(R.string.hs_transaction_failed))
            .inRoot(RootMatchers.isDialog())
            .check(matches(isDisplayed()))
            .perform(click())
    }

    // Function to test the behavior of Subscription Type RadioButtons
    private fun testSubscriptionTypeRadioButton(radioButtonId: Int, expectedValueType: String) {
        onView(withId(radioButtonId)).perform(click())
        activityScenarioRule.scenario?.onActivity { activity ->
            val RecurringAndInstallmentsPayload = json.decodeFromString(RecurringAndInstallmentsPayload.serializer(), activity.payload)
            Assert.assertTrue(RecurringAndInstallmentsPayload.subscriptionType == expectedValueType)
        }
    }

    @Test
    fun testSubscriptionTypeOptions() {
        // Test Subscription Type RadioButtons for Recurring
        testSubscriptionTypeRadioButton(R.id.recurring_option, "RECURRING")

        // Test Subscription Type RadioButtons for Installment
        testSubscriptionTypeRadioButton(R.id.installment_option, "INSTALLMENT")
    }

    // Function to test the behavior of Number of Payments RadioButtons
    private fun testNumberPaymentsRadioButtonAction(
        radioButtonId: Int,
        expectedNumberPayments: Int
    ) {
        onView(withId(radioButtonId)).perform(click())
        activityScenarioRule.scenario?.onActivity { activity ->
            val RecurringAndInstallmentsPayload = json.decodeFromString(RecurringAndInstallmentsPayload.serializer(), activity.payload)
            Assert.assertTrue(RecurringAndInstallmentsPayload.subscriptionNumber == expectedNumberPayments)
        }
    }

    @Test
    fun testNumberOfPaymentsOptions() {
        // Test Number of Payments RadioButtons for 3 payments
        testNumberPaymentsRadioButtonAction(R.id.three_payments, 3)

        // Test Number of Payments RadioButtons for 6 payments
        testNumberPaymentsRadioButtonAction(R.id.six_payments, 6)

        // Test Number of Payments RadioButtons for 12 payments
        testNumberPaymentsRadioButtonAction(R.id.twelve_payments, 12)
    }

    // Function to test the behavior of Frequency RadioButtons
    private fun testFrequencyRadioButtonAction(
        radioButtonId: Int,
        expectedFrequencyNumber: Int,
        expectedFrequencyUnit: String = "MONTH"
    ) {
        onView(withId(radioButtonId)).perform(click())
        activityScenarioRule.scenario?.onActivity { activity ->
            val RecurringAndInstallmentsPayload = json.decodeFromString(RecurringAndInstallmentsPayload.serializer(), activity.payload)
            Assert.assertTrue(RecurringAndInstallmentsPayload.subscriptionFrequency == expectedFrequencyNumber)
            Assert.assertTrue(RecurringAndInstallmentsPayload.subscriptionUnit == expectedFrequencyUnit)
        }
    }

    @Test
    fun testFrequencyOptions() {
        // Test Frequency RadioButtons for 7 days
        testFrequencyRadioButtonAction(R.id.seven_days, 7, "DAY")

        // Test Frequency RadioButtons for 1 month
        testFrequencyRadioButtonAction(R.id.one_month, 1)

        // Test Frequency RadioButtons for 2 months
        testFrequencyRadioButtonAction(R.id.two_months, 2)
    }


    // Test to check if the toolbar is visible
    @Test
    fun testToolbarVisibility() {
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()))
    }

    // Test to check if the loading indicator is visible after button click
    @Test
    fun testLoadingIndicatorVisibility() {
        onView(withId(R.id.btn_run)).perform(click())
        onView(withId(R.id.loading_indicator)).check(matches(isDisplayed()))
    }

    // Test to check if the JWTExplorerView is visible after button click
    @Test
    fun testJWTExplorerViewVisibility() {
        onView(withId(R.id.btn_run)).perform(click())
        onView(withId(R.id.jwt_explorer_view)).check(matches(isDisplayed()))
    }

    // Test to check if the progress bar is visible after button click
    @Test
    fun testProgressBarVisibility() {
        onView(withId(R.id.btn_run)).perform(click())
        onView(withId(R.id.loading_indicator)).check(matches(isDisplayed()))
    }
}
