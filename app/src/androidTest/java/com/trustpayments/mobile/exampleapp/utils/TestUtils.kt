package com.trustpayments.mobile.exampleapp.utils

import android.content.Context
import android.view.View
import androidx.preference.PreferenceManager
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.trustpayments.mobile.exampleapp.R
import junit.framework.AssertionFailedError
import kotlinx.coroutines.delay
import org.hamcrest.Matcher
import org.hamcrest.Matchers.not
import java.util.concurrent.TimeoutException

object TestUtils {
    fun clearSharedPreferences(context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.commit()
    }

    suspend fun execute3DSV2Popup() {
        val success = waitForVisibleView(
            R.id.codeEditTextField, 30000,
            throws = false
        )
        if (success) {
            val viewMatcher = withId(R.id.codeEditTextField)
            onView(viewMatcher).perform(ViewActions.typeText("1234"))
            onView(withId(R.id.submitAuthenticationButton))
                .perform(ViewActions.closeSoftKeyboard(), ViewActions.click())
        }
        delay(1000)
    }

    suspend fun waitForVisibleView(
        viewId: Int,
        timeoutMillis: Long,
        throws: Boolean = true,
        optionalViewMatchers: Matcher<View>? = null
    ): Boolean {
        val endTime = System.currentTimeMillis() + timeoutMillis
        val viewMatcher = optionalViewMatchers ?: withId(viewId)

        while (System.currentTimeMillis() < endTime) {
            try {
                onView(viewMatcher).check(matches(isDisplayed()))
                return true
            } catch (e: Exception) {
                delay(500)
            }
        }

        if (throws) {
            // If the element is still not visible after the timeout, throw an exception
            throw TimeoutException("Timed out waiting for view with ID $viewId to be visible.")
        }

        return false
    }

    suspend fun waitForInvisibleView(
        viewId: Int,
        timeoutMillis: Long,
        throws: Boolean = true
    ): Boolean {
        val endTime = System.currentTimeMillis() + timeoutMillis
        val viewMatcher = withId(viewId)

        while (System.currentTimeMillis() < endTime) {
            try {
                onView(viewMatcher).check(matches(not(isDisplayed())))
                return true
            } catch (e: AssertionFailedError) {
                delay(500)
            } catch (e: Exception) {
                delay(500)
            }
        }

        if (throws) {
            // If the element is still not visible after the timeout, throw an exception
            throw TimeoutException("Timed out waiting for view with ID $viewId to be visible.")
        }

        return false
    }
}