package com.trustpayments.mobile.exampleapp.functionaltests.samples

import android.content.Context
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.isEnabled
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.pageobjects.AuthWith3DSecureFormPage
import com.trustpayments.mobile.exampleapp.samples.tokenizedPayment.TokenizedPaymentActivity
import com.trustpayments.mobile.exampleapp.samples.tokenizedPayment.TokenizedPaymentTriggerActivity
import com.trustpayments.mobile.ui.model.PaymentInputType
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4::class)
@LargeTest
class TokenizePaymentLaunchUITestSuite {
    private val authWith3DSecureFormPage = AuthWith3DSecureFormPage()

    @get:Rule
    val activityScenarioRule = ActivityScenarioRule(TokenizedPaymentTriggerActivity::class.java)
    private lateinit var context: Context

    @Before
    fun setUp() {
        context = InstrumentationRegistry.getInstrumentation().targetContext
    }

    @After
    fun after() {
        activityScenarioRule.scenario.close()
    }

    private fun launchPaymentChargeActivity(): Unit = runBlocking {
        val activityScenario = ActivityScenario.launch(TokenizedPaymentActivity::class.java)
        var activity: TokenizedPaymentActivity? = null
        activityScenario.onActivity { activityInstance ->
            activity = activityInstance
        }
        // Provide card details for authorization to be proceeded.
        activity?.onInputValid(PaymentInputType.PAN, "4111111111111111")
        activity?.onInputValid(PaymentInputType.ExpiryDate, "12/2028")
        activity?.onInputValid(PaymentInputType.CVV, "123")
        // Trigger Add card button click.
        Espresso.onView(withId(R.id.btnAddCard))
            .perform(ViewActions.click())
        // Wait for authorization to be executed.
        delay(5000)
        // Verify Authorization success message is displayed in dialog.
        Espresso.onView(withText(context.getString(R.string.hs_transaction_success)))
            .check(matches(isDisplayed()))
        // Dismiss success dialog.
        Espresso.onView(withText(context.getString(R.string.ok_text))).perform(ViewActions.click())
        delay(100)
        // Verify Authorize button is enabled and can proceed to next screen.
        Espresso.onView(withId(R.id.btnAuthorize)).check(matches(isEnabled()))
        // Proceed to payment success screen
        Espresso.onView(withId(R.id.btnAuthorize)).perform(ViewActions.click())
    }

    /**
     * Verify that Payment Charge button is enabled.
     * */
    @Test
    fun testA_PaymentChargeScreenInitialBehaviour(): Unit = runBlocking {
        launchPaymentChargeActivity()
        Espresso.onView(withId(R.id.tvStatus)).check(matches(not(withText(""))))
        Espresso.onView(withId(R.id.btnAuthorize)).check(matches(isEnabled()))
    }

    /**
     * Verify that Payment Charge flow finish successfully.
     * */
    @Test
    fun testA_PaymentChargeScreenSuccessBehaviour(): Unit = runBlocking {
        launchPaymentChargeActivity()
        Espresso.onView(withId(R.id.tvStatus)).check(matches(not(withText(""))))
        Espresso.onView(withId(R.id.btnAuthorize)).check(matches(isEnabled()))
        Espresso.onView(withId(R.id.btnAuthorize)).perform(ViewActions.click())
        delay(10000)
        authWith3DSecureFormPage.fillSecureCode(
            CardsDataWith3DSecureV2.threeDSecureCode
        )
            .tapOnSubmitButton()
        delay(10000)
        Espresso.onView(withText(context.getString(R.string.hs_transaction_success)))
            .check(matches(isDisplayed()))
        Espresso.onView(withText(context.getString(R.string.ok_text))).perform(ViewActions.click())
    }
}
