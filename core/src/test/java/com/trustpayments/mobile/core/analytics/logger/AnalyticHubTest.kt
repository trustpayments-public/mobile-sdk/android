package com.trustpayments.mobile.core.analytics.logger

import com.trustpayments.mobile.core.analytics.logger.protocol.AnalyticClientSettings
import com.trustpayments.mobile.core.analytics.logger.protocol.AnalyticEvent
import com.trustpayments.mobile.core.analytics.logger.protocol.AnalyticEventInfo
import com.trustpayments.mobile.core.analytics.logger.protocol.LogLevel
import com.trustpayments.mobile.core.analytics.logger.protocol.Message
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.UUID

class AnalyticHubTest {

    @Test
    fun test_capture_error_event() {
        // Arrange
        val expectedEventId = UUID.randomUUID()
        val eventInfo = AnalyticEventInfo().apply {
            logLevel = LogLevel.ERROR
            message = "test error"
            data = mutableMapOf()
        }
        val tags = mutableMapOf("priority" to "low")
        val settings = AnalyticClientSettings(
            dsn = "TEST SERVER",
            merchantUsername = "TP"
        )
        val event = AnalyticEvent(
            eventId = expectedEventId,
            eventInfo = listOf(eventInfo),
            message = Message().apply { formatted = "Test Event" },
            tags = tags
        )
        val hub = AnalyticHub(settings)
        // Act
        hub.initialize()
        val result = hub.captureErrorEvent(event)
        // Assert
        assertEquals(expectedEventId, result)

        assertEquals(1, event.tags?.size)

        assertEquals("TEST SERVER", settings.dsn)
        assertEquals("TP", settings.merchantUsername)

        assertEquals(LogLevel.ERROR, eventInfo.logLevel)
        assertEquals("test error", eventInfo.message)
        assertEquals(0, eventInfo.data.size)
    }
}