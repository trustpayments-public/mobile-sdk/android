package com.trustpayments.mobile.utils.core.assertions

import android.view.View
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAssertion

abstract class CustomViewAssertion<T : View, V : Any>(private val clazz: Class<T>) : ViewAssertion {
    private var value: V? = null

    /**
     * After calling check on ViewInteraction this method can be called to get extracted value
     *
     * @return extracted value
     */
    fun getValue(): V {
        checkNotNull(value) { "No matching views or view assertion was not called before!" }
        return value as V
    }

    override fun check(view: View, noViewFoundException: NoMatchingViewException?) {
        val typedView = clazz.cast(view) as T
        value = extractValue(typedView)
    }

    /**
     * Called internally by CustomViewAssertion
     *
     *
     * Provide implementation that will return extracted value from provided argument
     *
     * @param typedView view that provides value for extraction
     * @return extracted value
     */
    protected abstract fun extractValue(typedView: T): V
}