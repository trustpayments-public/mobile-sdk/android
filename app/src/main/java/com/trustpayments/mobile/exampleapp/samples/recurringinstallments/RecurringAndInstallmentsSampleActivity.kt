package com.trustpayments.mobile.exampleapp.samples.recurringinstallments

import android.os.Bundle
import android.widget.RadioGroup
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.trustpayments.mobile.exampleapp.BuildConfig
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.common.base.BaseSampleActivity
import com.trustpayments.mobile.exampleapp.common.jwt.RecurringAndInstallmentsPayload
import com.trustpayments.mobile.exampleapp.databinding.ActivityRecurringAndInstallmentsBinding
import com.trustpayments.mobile.exampleapp.utils.Constants
import com.trustpayments.mobile.exampleapp.utils.DateHelper
import com.trustpayments.mobile.exampleapp.utils.Utils
import com.trustpayments.mobile.exampleapp.utils.Utils.hideKeyboard
import com.trustpayments.mobile.ui.model.RequestType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RecurringAndInstallmentsSampleActivity : BaseSampleActivity() {

    private val viewBinding by lazy {
        ActivityRecurringAndInstallmentsBinding.inflate(layoutInflater)
    }

    private var subscriptionUnit = "MONTH"
    private var subscriptionFrequency = 1
    private var subscriptionFinalNumber = 12
    private var subscriptionType = "RECURRING"
    private var subscriptionNumber = 3

    override var payload = getStringPayload()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(viewBinding.root)
        setJWTExplorerView(viewBinding.jwtExplorerView)
        setSupportActionBar(viewBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewBinding.toolbar.setNavigationOnClickListener { finish() }
        viewBinding.btnRun.setOnClickListener { runButtonClick() }
        setOnCheckedChangeListener()
    }

    private fun getStringPayload(): String {
        return json.encodeToString(
            RecurringAndInstallmentsPayload.serializer(),
            RecurringAndInstallmentsPayload(
                siteReference = BuildConfig.SITE_REFERENCE,
                currencyISO3a = "GBP",
                baseAmount = 1050,
                requestTypeDescriptions = listOf(
                    RequestType.ThreeDQuery.serializedName,
                    RequestType.AccountCheck.serializedName,
                    RequestType.Subscription.serializedName
                ),
                subscriptionUnit = this.subscriptionUnit,
                subscriptionFrequency = subscriptionFrequency,
                subscriptionNumber = subscriptionNumber,
                subscriptionFinalNumber = subscriptionFinalNumber,
                subscriptionType = subscriptionType,
            )
        )
    }

    override fun getPayload() = try {
        json.decodeFromString(RecurringAndInstallmentsPayload.serializer(), payload)
    } catch (e: Exception) {
        null
    }

    private fun setOnCheckedChangeListener() {
        val onCheckedChangeListener: RadioGroup.OnCheckedChangeListener =
            RadioGroup.OnCheckedChangeListener { _, checkedId ->
                subscriptionUnit = "MONTH"

                when (checkedId) {
                    R.id.recurring_option -> subscriptionType = "RECURRING"
                    R.id.installment_option -> subscriptionType = "INSTALLMENT"
                    R.id.three_payments -> subscriptionNumber = 3
                    R.id.six_payments -> subscriptionNumber = 6
                    R.id.twelve_payments -> subscriptionNumber = 12
                    R.id.one_month -> subscriptionFrequency = 1
                    R.id.two_months -> subscriptionFrequency = 2
                    R.id.seven_days -> {
                        subscriptionFrequency = 7
                        subscriptionUnit = "DAY"
                    }
                }

                payload = getStringPayload()
                viewBinding.jwtExplorerView.setRequestJwtToken(payload)
            }

        viewBinding.numberOfPayments.setOnCheckedChangeListener(onCheckedChangeListener)
        viewBinding.frequencyOptions.setOnCheckedChangeListener(onCheckedChangeListener)
        viewBinding.selectionType.setOnCheckedChangeListener(onCheckedChangeListener)
    }

    private fun setExecutingSessionUIState() {
        viewBinding.btnRun.isEnabled = false
        viewBinding.btnRun.text = ""
        viewBinding.loadingIndicator.isVisible = true
    }

    private fun runButtonClick() {
        hideKeyboard(viewBinding.jwtExplorerView)
        val token = getToken() ?: return
        setExecutingSessionUIState()
        executeSession(token)
    }

    /**
     * Executes a payment session with the provided token.
     *
     * @param token The authentication token used to create the payment session.
     */
    private fun executeSession(token: String) {
        // Create a payment session with the provided token and additional parameters
        val session = paymentTransactionManager.createSession(
            { token },
            Constants.TEST_CARD_NUM,
            DateHelper.getValidCardExpiryDate(),
            Constants.TEST_CARD_CVV
        )

        // Perform the execution of the payment session in the background using coroutines
        lifecycleScope.launch(Dispatchers.IO) {
            // Execute the payment session with relevant parameters
            val result = paymentTransactionManager.executeSession(
                session
            ) { this@RecurringAndInstallmentsSampleActivity }

            // Check if there is an error message in the result
            val errorMessage =
                Utils.findExecuteSessionErrorMessage(
                    result,
                    this@RecurringAndInstallmentsSampleActivity
                )

            // Initialize jwtResponseData with an empty JSON object
            val jwtResponseData = Utils.findExecuteSessionErrorData(
                result,
                this@RecurringAndInstallmentsSampleActivity
            ) ?: Utils.jwtStringListToDecodedJsonArrayString(result.responseJwtList)
            var alertMessage = errorMessage ?: getString(R.string.hs_transaction_failed)
            var alertTitle = getString(R.string.hs_transaction_failed)

            // Check if there are response JWT tokens in the result
            if (errorMessage.isNullOrBlank() && result.responseJwtList.isNotEmpty()) {
                alertTitle = getString(R.string.hs_transaction_success)
                alertMessage = getString(R.string.hs_transaction_successfully_completed)
            }

            // Update the UI state based on the execution result
            lifecycleScope.launch {
                setFinishedExecuteSessionUIState(jwtResponseData, alertMessage, alertTitle)
            }
        }
    }

    private fun setFinishedExecuteSessionUIState(
        jwtViewData: String,
        alertMessage: String,
        alertTitle: String
    ) {
        viewBinding.jwtExplorerView.setResponseJwtToken(jwtViewData)
        viewBinding.jwtExplorerView.moveTokenTab(1)
        viewBinding.btnRun.text =
            getString(R.string.recurring_and_installments_perform_subscription)
        viewBinding.btnRun.isEnabled = true
        viewBinding.loadingIndicator.isVisible = false
        showDialog(alertTitle, alertMessage)
    }
}
