package com.trustpayments.mobile.exampleapp.samples.advancedfeatures

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.common.recyclerview.CommonAdapter
import com.trustpayments.mobile.exampleapp.common.recyclerview.ListItem
import com.trustpayments.mobile.exampleapp.databinding.ActivityAdvancedFeaturesBinding
import com.trustpayments.mobile.exampleapp.samples.accountverificationwithlaterauth.AccountVerificationWithLaterAuthSampleActivity
import com.trustpayments.mobile.exampleapp.samples.backgroundrequest.BackgroundRequestSampleActivity

class AdvancedFeaturesActivity : AppCompatActivity() {
    private val viewBinding by lazy {
        ActivityAdvancedFeaturesBinding.inflate(layoutInflater)
    }

    private val items: List<ListItem> by lazy {
        listOf(
            ListItem.SampleItem(R.string.background_request, BackgroundRequestSampleActivity::class.java),
            ListItem.SampleItem(R.string.account_verification_with_later_auth, AccountVerificationWithLaterAuthSampleActivity::class.java),
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(viewBinding.root)
        setSupportActionBar(viewBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewBinding.toolbar.setNavigationOnClickListener { finish() }
        viewBinding.samplesList.apply {
            val linearLayoutManager = LinearLayoutManager(this@AdvancedFeaturesActivity)
            layoutManager = linearLayoutManager
            adapter = CommonAdapter(items)
            addItemDecoration(DividerItemDecoration(this@AdvancedFeaturesActivity, linearLayoutManager.orientation))
        }
    }
}