package com.trustpayments.mobile.utils.core.actions

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.util.concurrent.TimeUnit

/**
 * For a given phone number, fetches a ZIP verification codes list from the REST API and parses
 * the response to get the last of them.
 */
object VerificationCodeProvider {

    private val loggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    private val okHttpClient = OkHttpClient.Builder()
        .connectTimeout(10, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        .addInterceptor(loggingInterceptor)
        .build()

    private val retrofit = Retrofit.Builder()
        .client(okHttpClient)
        .baseUrl(VerificationCodeApi.BASE_API_URL)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .build()

    private val retrofitApi = retrofit.create(VerificationCodeApi::class.java)

    suspend fun getLastCode(): String {
        val codeObj = retrofitApi.getCodes().first()
        return codeObj.body.replace("\\s".toRegex(), "").split(":")[1]
    }

}

private interface VerificationCodeApi {

    companion object {
        const val BASE_API_URL = "https://0hw6hlmlvj.execute-api.us-east-1.amazonaws.com/dev/" +
                "ecd8b8af-81ae-42b5-a448-a63c97b4f6b5/numbers/"
    }

    @GET("+447897035699/")
    suspend fun getCodes(): List<VerificationCode>
}

data class VerificationCode(
    val body: String,
    val created: String,
    val sent: String
)