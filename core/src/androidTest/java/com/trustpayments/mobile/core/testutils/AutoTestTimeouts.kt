package com.trustpayments.mobile.core.testutils

object AutoTestTimeouts {
    const val TIME_TO_WAIT_ON_ELEMENT = 60 * 1000
    const val UI_AUTOMATOR_TIMEOUT = 60 * 1000L
}