package com.trustpayments.mobile.core.ui

import android.os.Bundle

data class WebActivityResult(
    val requestCode: Int,
    val resultCode: Int,
    val bundle: Bundle?
)