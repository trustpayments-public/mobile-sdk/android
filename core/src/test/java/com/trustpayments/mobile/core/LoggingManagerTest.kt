package com.trustpayments.mobile.core

import com.trustpayments.mobile.core.analytics.logger.AnalyticHub
import com.trustpayments.mobile.core.analytics.logger.protocol.AnalyticEvent
import com.trustpayments.mobile.core.services.api.TrustPaymentsGatewayType
import com.trustpayments.mobile.core.services.transaction.ErrorMessages
import com.trustpayments.mobile.core.services.transaction.LoggingManager
import com.trustpayments.mobile.core.services.transaction.LoggingManagerDataSource
import io.mockk.MockKAnnotations
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockkConstructor
import io.mockk.slot
import io.mockk.unmockkAll
import io.mockk.verify
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNull
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.UUID

class LoggingManagerTest {

    private lateinit var loggingManager: LoggingManager

    @Before
    fun setup() {
        val loggingManagerDataSource =
            LoggingManagerDataSource(true, TrustPaymentsGatewayType.EU, "testUsername")
        MockKAnnotations.init(this)

        mockkConstructor(AnalyticHub::class)
        every { anyConstructed<AnalyticHub>().initialize() } just Runs

        loggingManager = LoggingManager.getInstance(loggingManagerDataSource).also { it.init() }

    }

    @After
    fun after() {
        unmockkAll()
    }

    @Test
    fun messageWithPanNumberFormattedTest() {
        //GIVEN
        val panCardNumber = "4000000000000000"
        val expectedMessage = "################"
        val captureEvent = slot<AnalyticEvent>()
        every { anyConstructed<AnalyticHub>().captureErrorEvent(capture(captureEvent)) } returns UUID.randomUUID()

        //WHEN
        loggingManager.sendErrorEvent(
            ErrorMessages.EmptyBundle,
            ErrorMessages.EmptyBundle,
            errorMessage = panCardNumber
        )

        //THEN
        val eventInfo = captureEvent.captured.eventInfo?.get(0)
        assertEquals(eventInfo?.getData("errorMessage"), expectedMessage)
    }

    @Test
    fun messageWithCVVNumberFormattedTest() {
        //GIVEN
        val cvv = "123"
        val expectedMessage = "###"
        val captureEvent = slot<AnalyticEvent>()
        every { anyConstructed<AnalyticHub>().captureErrorEvent(capture(captureEvent)) } returns UUID.randomUUID()

        //WHEN
        loggingManager.sendErrorEvent(
            ErrorMessages.EmptyBundle,
            ErrorMessages.EmptyBundle,
            errorMessage = cvv
        )

        //THEN
        val eventInfo = captureEvent.captured.eventInfo?.get(0)
        assertEquals(eventInfo?.getData("errorMessage"), expectedMessage)
    }

    @Test
    fun messageWithExpirationDateNumberFormattedTest() {
        //GIVEN
        val date = "12/23"
        val expectedMessage = "##/##"
        val captureEvent = slot<AnalyticEvent>()
        every { anyConstructed<AnalyticHub>().captureErrorEvent(capture(captureEvent)) } returns UUID.randomUUID()

        //WHEN
        loggingManager.sendErrorEvent(
            ErrorMessages.EmptyBundle,
            ErrorMessages.EmptyBundle,
            errorMessage = date
        )

        //THEN
        val eventInfo = captureEvent.captured.eventInfo?.get(0)
        assertEquals(eventInfo?.getData("errorMessage"), expectedMessage)
    }

    @Test
    fun messageWithExceptionMessageCVVNumberFormattedTest() {
        //GIVEN
        val panCardNumber = "12/23"
        val expectedMessage = "class java.lang.RuntimeException"
        val exception = RuntimeException(panCardNumber)
        val captureEvent = slot<AnalyticEvent>()
        every { anyConstructed<AnalyticHub>().captureErrorEvent(capture(captureEvent)) } returns UUID.randomUUID()

        //WHEN
        loggingManager.sendExceptionEvent(exception)

        //THEN
        val eventInfo = captureEvent.captured.eventInfo?.get(0)
        val message = captureEvent.captured.message?.formatted
        assertNull(eventInfo?.getData("errorMessage"))
        assertEquals(message, expectedMessage)

    }

    @Test
    fun messageAfterHubReleaseNotLoggedTest() {
        //GIVEN
        val panCardNumber = "12/23"
        val captureEvent = slot<AnalyticEvent>()
        every { anyConstructed<AnalyticHub>().captureErrorEvent(capture(captureEvent)) } returns UUID.randomUUID()

        //WHEN
        loggingManager.release()
        loggingManager.sendErrorEvent(
            ErrorMessages.EmptyBundle,
            ErrorMessages.EmptyBundle,
            errorMessage = panCardNumber
        )

        //THEN
        verify(exactly = 0) { anyConstructed<AnalyticHub>().captureErrorEvent(any()) }
    }
}