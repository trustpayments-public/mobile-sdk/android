package com.trustpayments.mobile.core.testcases.customeroutput

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.rule.ActivityTestRule
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.R
import com.trustpayments.mobile.core.models.api.response.CustomerOutput
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.models.api.response.ResponseErrorCode
import com.trustpayments.mobile.core.services.transaction.Error
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.testutils.createSessionFor
import com.trustpayments.mobile.core.testutils.waitUntilElementIsDisplayed
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.util.ResponseParser
import com.trustpayments.mobile.core.utils.ThreeDQueryTestActivity
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2.bankSystemErrorVisaCardNumber
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2.frictionlessMasterCardNumber
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2.frictionlessVisaCardNumber
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2.nonFrictionlessMasterCardNumber
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2.nonFrictionlessVisaCardNumber
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2.threeDSecureCode
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2.unauthenticatedErrorVisaCardNumber
import com.trustpayments.mobile.utils.core.testcardsdata.GeneralCardsData.correctVisaCardNumber
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class CustomerOutputTestCases: BaseTestCase() {

    private lateinit var paymentTransactionManager: PaymentTransactionManager

    private lateinit var activity: ThreeDQueryTestActivity

    @get:Rule
    val initialActivityTestRule = ActivityTestRule(ThreeDQueryTestActivity::class.java)

    private val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    @Before
    override fun setUp() {
        super.setUp()

        val app = ApplicationProvider.getApplicationContext<Context>().applicationContext
        paymentTransactionManager =
            PaymentTransactionManager(app, getGatewayType(), false, BuildConfig.MERCHANT_USERNAME, isLocationDataConsentGiven = false)

        activity = initialActivityTestRule.activity
    }

    @Test
    fun test_positive_3DQv2_frictionless() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getFor3DSecure(listOf(
            RequestType.ThreeDQuery
        ))

        val session = paymentTransactionManager.createSessionFor(jwtToken, frictionlessMasterCardNumber, "123")
        val sessionResult = paymentTransactionManager.executeSession(session) { activity }

        assertEquals(1, sessionResult.responseJwtList.size)
        assertNull(sessionResult.additionalTransactionResult)
        assertNull(sessionResult.error)

        val parsedResponse = ResponseParser.parse(sessionResult.responseJwtList.first())

        assertEquals(1, parsedResponse!!.responses.size)
        assertEquals(parsedResponse.customerOutput, parsedResponse.responses.first())
        assertEquals(RequestType.ThreeDQuery, parsedResponse.customerOutput!!.requestTypeDescription!!)
        assertEquals(CustomerOutput.Result, parsedResponse.customerOutput!!.customerOutput)
    }

    @Test
    fun test_positive_3DQv2_auth_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getFor3DSecure(listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        ))

        val session = paymentTransactionManager.createSessionFor(jwtToken, nonFrictionlessVisaCardNumber, "123")

        // WHEN
        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, threeDSecureCode)

        job.join()

        assertEquals(2, sessionResult.responseJwtList.size)
        assertNotNull(sessionResult.additionalTransactionResult!!.threeDResponse)
        assertNull(sessionResult.additionalTransactionResult!!.pares)
        assertNull(sessionResult.error)

        val parsedResponse = ResponseParser.parse(sessionResult.responseJwtList)

        assertEquals(2, parsedResponse!!.size)
        assertEquals(1, parsedResponse[0].responses.size)
        assertEquals(1, parsedResponse[1].responses.size)

        assertEquals(parsedResponse[0].customerOutput, parsedResponse[0].responses.first())
        assertEquals(RequestType.ThreeDQuery, parsedResponse[0].customerOutput!!.requestTypeDescription!!)
        assertEquals(CustomerOutput.ThreeDRedirect, parsedResponse[0].customerOutput!!.customerOutput)

        assertEquals(parsedResponse[1].customerOutput, parsedResponse[1].responses.first())
        assertEquals(RequestType.Auth, parsedResponse[1].customerOutput!!.requestTypeDescription!!)
        assertEquals(CustomerOutput.Result, parsedResponse[1].customerOutput!!.customerOutput)
    }

    @Test
    fun test_positive_3DQv2_nonfrictionless() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getFor3DSecure(listOf(
            RequestType.ThreeDQuery
        ))

        val nonFrictionlessVisaCard = "4000000000001091"
        val session = paymentTransactionManager.createSessionFor(jwtToken, nonFrictionlessVisaCard, "123")

        // WHEN
        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, threeDSecureCode)

        job.join()

        assertEquals(1, sessionResult.responseJwtList.size)
        assertNotNull(sessionResult.additionalTransactionResult!!.threeDResponse)
        assertNull(sessionResult.additionalTransactionResult!!.pares)
        assertNull(sessionResult.error)

        val parsedResponse = ResponseParser.parse(sessionResult.responseJwtList.first())

        assertEquals(1, parsedResponse!!.responses.size)
        assertEquals(parsedResponse.customerOutput, parsedResponse.responses.first())
        assertEquals(RequestType.ThreeDQuery, parsedResponse.customerOutput!!.requestTypeDescription!!)
        assertEquals(CustomerOutput.ThreeDRedirect, parsedResponse.customerOutput!!.customerOutput)
    }

    @Test
    fun test_negative_3DQ_auth_unauthenticatedCard() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getFor3DSecure(listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        ))

        val session = paymentTransactionManager.createSessionFor(jwtToken, unauthenticatedErrorVisaCardNumber, "123")
        val sessionResult = paymentTransactionManager.executeSession(session) { activity }

        assertEquals(1, sessionResult.responseJwtList.size)
        assertNull(sessionResult.additionalTransactionResult)
        assertNull(sessionResult.error)

        val parsedResponse = ResponseParser.parse(sessionResult.responseJwtList.first())

        assertEquals(2, parsedResponse!!.responses.size)
        assertEquals(parsedResponse.customerOutput, parsedResponse.responses.last())
        assertEquals(RequestType.Error, parsedResponse.customerOutput!!.requestTypeDescription!!)
        assertEquals(CustomerOutput.TryAgain, parsedResponse.customerOutput!!.customerOutput)
        assertNull(parsedResponse.responses.first().customerOutput) //No customer output for 3DQ
    }

    @Test
    fun test_negative_accountCheck_3DQ_auth_bankErrorCard() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getFor3DSecure(listOf(
            RequestType.AccountCheck,
            RequestType.ThreeDQuery,
            RequestType.Auth
        ))

        val session = paymentTransactionManager.createSessionFor(jwtToken, bankSystemErrorVisaCardNumber, "123")
        val sessionResult = paymentTransactionManager.executeSession(session) { activity }

        assertEquals(1, sessionResult.responseJwtList.size)
        assertNull(sessionResult.additionalTransactionResult)
        assertNull(sessionResult.error)

        val parsedResponse = ResponseParser.parse(sessionResult.responseJwtList.first())

        assertEquals(3, parsedResponse!!.responses.size)
        assertEquals(parsedResponse.customerOutput, parsedResponse.responses.last())
        assertEquals(RequestType.Auth, parsedResponse.customerOutput!!.requestTypeDescription!!)
        assertEquals(CustomerOutput.Result, parsedResponse.customerOutput!!.customerOutput)

        assertNull(parsedResponse.responses[0].customerOutput) //No customer output for account check
        assertNull(parsedResponse.responses[1].customerOutput) //No customer output for 3DQ
    }

    @Test
    fun test_positive_accountCheck_3DQ_auth_subscription_frictionless() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForSubscription(listOf(
            RequestType.AccountCheck,
            RequestType.ThreeDQuery,
            RequestType.Auth,
            RequestType.Subscription
        ))

        val session = paymentTransactionManager.createSessionFor(jwtToken, frictionlessVisaCardNumber, "123")
        val sessionResult = paymentTransactionManager.executeSession(session) { activity }

        assertEquals(1, sessionResult.responseJwtList.size)
        assertNull(sessionResult.additionalTransactionResult)
        assertNull(sessionResult.error)

        val parsedResponse = ResponseParser.parse(sessionResult.responseJwtList.first())

        assertEquals(4, parsedResponse!!.responses.size)

        assertEquals(parsedResponse.customerOutput, parsedResponse.responses[2])
        assertEquals(RequestType.Auth, parsedResponse.customerOutput!!.requestTypeDescription!!)
        assertEquals(CustomerOutput.Result, parsedResponse.customerOutput!!.customerOutput) //Customer output set for Auth

        assertNull(parsedResponse.responses[0].customerOutput)  //No customer output set for account check
        assertNull(parsedResponse.responses[1].customerOutput)  //No customer output set for 3DQ
        assertNull(parsedResponse.responses[3].customerOutput)  //No customer output set for subscription
    }

    @Test
    fun test_positive_accountCheck_3DQ_auth_subscription_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForSubscription(listOf(
            RequestType.AccountCheck,
            RequestType.ThreeDQuery,
            RequestType.Auth,
            RequestType.Subscription
        ))

        val session = paymentTransactionManager.createSessionFor(jwtToken, nonFrictionlessMasterCardNumber, "123")

        // WHEN
        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, threeDSecureCode)

        job.join()

        assertEquals(2, sessionResult.responseJwtList.size)
        assertNotNull(sessionResult.additionalTransactionResult!!.threeDResponse)
        assertNull((sessionResult.additionalTransactionResult!!).pares)
        assertNull(sessionResult.error)

        val parsedResponse = ResponseParser.parse(sessionResult.responseJwtList)

        assertEquals(2, parsedResponse!!.size)
        assertEquals(2, parsedResponse[0].responses.size)
        assertEquals(2, parsedResponse[1].responses.size)

        assertEquals(parsedResponse[0].customerOutput, parsedResponse[0].responses.last())
        assertEquals(RequestType.ThreeDQuery, parsedResponse[0].customerOutput!!.requestTypeDescription!!)
        assertEquals(CustomerOutput.ThreeDRedirect, parsedResponse[0].customerOutput!!.customerOutput)
        assertNull(parsedResponse[0].responses[0].customerOutput)   //No customer output set for account check

        assertEquals(parsedResponse[1].customerOutput, parsedResponse[1].responses.first())
        assertEquals(RequestType.Auth, parsedResponse[1].customerOutput!!.requestTypeDescription!!)
        assertEquals(CustomerOutput.Result, parsedResponse[1].customerOutput!!.customerOutput)
        assertNull(parsedResponse[1].responses[1].customerOutput)   //No customer output set for subscription
    }

    @Test
    fun test_negative_accountCheck_3DQ_auth_subscription_failedChallenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForSubscription(listOf(
            RequestType.AccountCheck,
            RequestType.ThreeDQuery,
            RequestType.Auth,
            RequestType.Subscription
        ))

        val session = paymentTransactionManager.createSessionFor(jwtToken, nonFrictionlessMasterCardNumber, "123")

        // WHEN
        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }
        }

        waitUntilACSPopupIsDisplayed()
        Espresso.pressBack()

        job.join()

        assertEquals(1, sessionResult.responseJwtList.size)
        assertNull(sessionResult.additionalTransactionResult)
        assertTrue(sessionResult.error is Error.ThreeDSFailure)

        val parsedResponse = ResponseParser.parse(sessionResult.responseJwtList.first())

        assertEquals(2, parsedResponse!!.responses.size)
        assertEquals(parsedResponse.customerOutput, parsedResponse.responses.last())
        assertEquals(RequestType.ThreeDQuery, parsedResponse.customerOutput!!.requestTypeDescription!!)
        assertEquals(CustomerOutput.ThreeDRedirect, parsedResponse.customerOutput!!.customerOutput)

        assertNull(parsedResponse.responses[0].customerOutput)  //No customer output set for account check
    }

    @Test
    fun test_negative_auth_subscription_failedSubscription() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForSubscription(listOf(
            RequestType.Auth,
            RequestType.Subscription
        ), currency = "JPY")

        val session = paymentTransactionManager.createSessionFor(jwtToken, correctVisaCardNumber, "123")
        val sessionResult = paymentTransactionManager.executeSession(session) { activity }

        assertEquals(1, sessionResult.responseJwtList.size)
        assertNull(sessionResult.additionalTransactionResult)
        assertNull(sessionResult.error)

        val parsedResponse = ResponseParser.parse(sessionResult.responseJwtList.first())

        assertEquals(2, parsedResponse!!.responses.size)

        assertEquals(parsedResponse.customerOutput, parsedResponse.responses[0])
        assertEquals(RequestType.Auth, parsedResponse.customerOutput!!.requestTypeDescription!!)
        assertEquals(CustomerOutput.Result, parsedResponse.customerOutput!!.customerOutput) //Customer output set for Auth

        assertNotEquals(ResponseErrorCode.Ok, parsedResponse.responses[1].errorCode)
        assertNull(parsedResponse.responses[1].customerOutput)  //No customer output set for subscription
    }

    @Test
    fun test_negative_auth_subscription_noAccountFound() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForSubscription(listOf(
            RequestType.Auth,
            RequestType.Subscription
        ), currency = "CAD")

        val session = paymentTransactionManager.createSessionFor(jwtToken, correctVisaCardNumber, "123")
        val sessionResult = paymentTransactionManager.executeSession(session) { activity }

        assertEquals(1, sessionResult.responseJwtList.size)
        assertNull(sessionResult.additionalTransactionResult)
        assertNull(sessionResult.error)

        val parsedResponse = ResponseParser.parse(sessionResult.responseJwtList.first())

        assertEquals(1, parsedResponse!!.responses.size)

        assertEquals(parsedResponse.customerOutput, parsedResponse.responses[0])
        assertEquals(RequestType.Error, parsedResponse.customerOutput!!.requestTypeDescription!!)
        assertEquals(CustomerOutput.TryAgain, parsedResponse.customerOutput!!.customerOutput) //Customer output set for Auth
    }

    private fun waitUntilACSPopupIsDisplayed() =
        waitUntilElementIsDisplayed(
            ViewMatchers.withId(R.id.codeEditTextField)
        )
}