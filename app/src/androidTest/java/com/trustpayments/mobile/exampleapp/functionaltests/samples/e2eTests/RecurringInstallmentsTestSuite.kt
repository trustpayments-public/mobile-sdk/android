package com.trustpayments.mobile.exampleapp.functionaltests.samples.e2eTests

import android.content.Context
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.trustpayments.mobile.exampleapp.MainActivity
import com.trustpayments.mobile.exampleapp.pageobjects.AuthWith3DSecureFormPage
import com.trustpayments.mobile.exampleapp.pageobjects.HomePage
import com.trustpayments.mobile.exampleapp.pageobjects.PayByCardFormPage
import com.trustpayments.mobile.exampleapp.pageobjects.RecurringInstallmentsPage
import com.trustpayments.mobile.exampleapp.utils.TestUtils
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2
import com.trustpayments.mobile.utils.core.testcardsdata.RecurringInstallmentsData
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
class RecurringInstallmentsTestSuite {

    @get:Rule
    val activityScenarioRule = ActivityScenarioRule(MainActivity::class.java)

    private lateinit var context: Context
    private val homePage = HomePage()
    private val payByCardFormPage = PayByCardFormPage()
    private val authWith3DSecureFormPage = AuthWith3DSecureFormPage()
    private val recurringInstallmentsPage = RecurringInstallmentsPage()

    @Before
    fun setUp() {
        context = InstrumentationRegistry.getInstrumentation().targetContext
        TestUtils.clearSharedPreferences(context)
    }
    @After
    fun after() {
        TestUtils.clearSharedPreferences(context)
    }

     private fun fillRecurringInstallmentsForm(subscriptionType: String, numberOfPayments: String, frequency: String){
        homePage.tapRecurringInstallments()
        recurringInstallmentsPage.selectSubscriptionType(subscriptionType)
         recurringInstallmentsPage.selectNumberOfPayments(numberOfPayments)
         recurringInstallmentsPage.selectFrequency(frequency)
         recurringInstallmentsPage.clickPerformSubscriptionButton()
    }

    @Test
    fun successfulRecurringPayment() {
        fillRecurringInstallmentsForm(
            RecurringInstallmentsData.subscriptionType_Recurring,
            RecurringInstallmentsData.numberOfPayments_twelve,
            RecurringInstallmentsData.frequency_SevenDays
        )
        Thread.sleep(10000)
        authWith3DSecureFormPage.fillSecureCode(
            CardsDataWith3DSecureV2.threeDSecureCode
        )
            .tapOnSubmitButton()
        Thread.sleep(10000)
        Assert.assertTrue(
            "Alert with successful payment processing message was not displayed.",
            payByCardFormPage.isSuccessPaymentCompletedAlertDisplayed()
        )


    }

    @Test
    fun successfulInstllmentsPayment() {
        fillRecurringInstallmentsForm(
            RecurringInstallmentsData.subscriptionType_Installments,
            RecurringInstallmentsData.numberOfPayments_Six,
            RecurringInstallmentsData.frequency_TwoMonths
        )
        Thread.sleep(10000)
        authWith3DSecureFormPage.fillSecureCode(
            CardsDataWith3DSecureV2.threeDSecureCode
        )
            .tapOnSubmitButton()
        Thread.sleep(10000)
        Assert.assertTrue(
            "Alert with successful payment processing message was not displayed.",
            payByCardFormPage.isSuccessPaymentCompletedAlertDisplayed()
        )

    }
    @Test
    fun instllmentsPaymentWithInvalidOtp() {
        fillRecurringInstallmentsForm(
            RecurringInstallmentsData.subscriptionType_Installments,
            RecurringInstallmentsData.numberOfPayments_Six,
            RecurringInstallmentsData.frequency_TwoMonths
        )
        Thread.sleep(10000)
        authWith3DSecureFormPage.fillSecureCode(
            CardsDataWith3DSecureV2.invalidThreeDSecureCode
        )
            .tapOnSubmitButton()
        Thread.sleep(5000)
        authWith3DSecureFormPage.isSubmitButtonEnabled()


    }
}