package com.trustpayments.mobile.core.testcases.bypass

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.rule.ActivityTestRule
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.models.api.response.ResponseErrorCode
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.testutils.CardType
import com.trustpayments.mobile.core.testutils.createSessionFor
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.util.OldTransactionResponse
import com.trustpayments.mobile.core.util.PaymentSessionResponse
import com.trustpayments.mobile.core.util.parse
import com.trustpayments.mobile.core.utils.ThreeDQueryTestActivity
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class BypassCardsTestCases: BaseTestCase() {

    private lateinit var paymentTransactionManager: PaymentTransactionManager
    
    private val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    private val cardsToBypass = listOf(
        CardType.Visa,
        CardType.Amex,
        CardType.Mastercard,
        CardType.Maestro,
        CardType.Discover,
        CardType.JCB,
        CardType.Diners
    ).map { it.serializedName }

    lateinit var basicJwtToken: String

    private lateinit var activity: ThreeDQueryTestActivity

    @get:Rule
    val initialActivityTestRule = ActivityTestRule(ThreeDQueryTestActivity::class.java)

    @Before
    override fun setUp() {
        super.setUp()

        val app = ApplicationProvider.getApplicationContext<Context>().applicationContext
        paymentTransactionManager =
            PaymentTransactionManager(app, getGatewayType(), false, BuildConfig.MERCHANT_USERNAME, isLocationDataConsentGiven = false)

        basicJwtToken = jwtBuilder.getStandard(listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        ), bypassCards = cardsToBypass)

        activity = initialActivityTestRule.activity
    }

    //region Bypassed frictionless cards
    @Test
    fun test_positive_bypassVisaFrictionless() {
        test_positive_bypassedFrictionlessCard("4000000000001026", "123")
    }

    @Test
    fun test_positive_bypassMastercardFrictionless1() = runBlocking {
        test_positive_bypassedFrictionlessCard("5200000000001005", "123")
    }

    @Test
    fun test_positive_bypassMastercardFrictionless2() = runBlocking {
        test_positive_bypassedFrictionlessCard("2221000000000801", "123")
    }

    @Test
    fun test_positive_bypassDiscoverFrictionless() = runBlocking {
        test_positive_bypassedFrictionlessCard("6011000000000004", "123")
    }

    @Test
    fun test_positive_bypassAmexFrictionless() = runBlocking {
        test_positive_bypassedFrictionlessCard("340000000001007", "1234")
    }

    @Test
    fun test_positive_bypassJCBFrictionless() = runBlocking {
        test_positive_bypassedFrictionlessCard("3528000000000411", "123")
    }

    @Test
    fun test_positive_bypassDinersFrictionless() = runBlocking {
        test_positive_bypassedFrictionlessCard("3005000000006246", "123")
    }
    //endregion

    @Test
    fun test_fail_bypassForMaestro() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(basicJwtToken, "5000000000000611", "123")

        // WHEN
        val sessionResult = paymentTransactionManager.executeSession(session) { activity }.parse()

        // THEN
        Assert.assertTrue(sessionResult is PaymentSessionResponse.Failure.TransactionFailure)

        sessionResult as PaymentSessionResponse.Failure.TransactionFailure
        Assert.assertEquals(1, sessionResult.responseParts.size)
        Assert.assertEquals(1, sessionResult.allResponses.size)
        Assert.assertEquals(ResponseErrorCode.MaestroMustUseSecureCode, (sessionResult.allResponses[0] as OldTransactionResponse.Error.TransactionError).errorCode)
    }

    private fun test_positive_bypassedFrictionlessCard(cardPan: String, cvv: String) = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(basicJwtToken, cardPan, cvv)

        // WHEN
        val sessionResult = paymentTransactionManager.executeSession(session) { activity }.parse()

        // THEN
        Assert.assertTrue(sessionResult is PaymentSessionResponse.Success)

        sessionResult as PaymentSessionResponse.Success
        Assert.assertEquals(1, sessionResult.responseParts.size)
        Assert.assertEquals(1, sessionResult.allResponses.size)
        Assert.assertEquals(RequestType.Auth, sessionResult.allResponses[0].requestTypeDescription)
    }
}