package com.trustpayments.mobile.exampleapp.common.base

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.trustpayments.mobile.core.services.api.TrustPaymentsGatewayType
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.exampleapp.BuildConfig
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.common.jwt.BasePayload
import com.trustpayments.mobile.exampleapp.common.jwtexplorer.JWTEditorToken
import com.trustpayments.mobile.exampleapp.common.jwtexplorer.JWTExplorerView
import com.trustpayments.mobile.exampleapp.common.jwtexplorer.OnTokenChangedListener
import com.trustpayments.mobile.exampleapp.utils.Utils
import kotlinx.serialization.json.Json

abstract class BaseSampleActivity : AppCompatActivity() {
    lateinit var paymentTransactionManager: PaymentTransactionManager
    open var payload = ""
    protected val json = Json { encodeDefaults = false }
    abstract fun getPayload(): BasePayload?

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        paymentTransactionManager =
            PaymentTransactionManager(
                this,
                TrustPaymentsGatewayType.EU,
                false,
                BuildConfig.MERCHANT_USERNAME,
                isLocationDataConsentGiven = false
            )
    }

    protected fun setJWTExplorerView(jwt: JWTExplorerView) {
        jwt.setRequestJwtToken(payload)
        jwt.setOnTokenChangedListener(object : OnTokenChangedListener {
            override fun onTokenChanged(token: JWTEditorToken) {
                payload = token.token
            }
        })
    }

    protected fun getToken(payload: BasePayload? = null): String? {
        val payloadObject = payload ?: getPayload()
        return if (payloadObject == null) {
            showDialog(getString(R.string.error), getString(R.string.invalid_payload))
            null
        } else {
            val requestPayload = Utils.createRequestPayload(payloadObject)
            Utils.buildJWT(
                BuildConfig.MERCHANT_USERNAME,
                requestPayload
            )
        }
    }

    protected fun showDialog(title: String, message: String) {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle(title)
        alertDialogBuilder.setMessage(message)
        alertDialogBuilder.setPositiveButton(R.string.ok_text) { dialog, _ ->
            dialog.dismiss()
        }

        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }
}