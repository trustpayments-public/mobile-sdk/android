package com.trustpayments.mobile.ui.dropin

import com.trustpayments.mobile.ui.model.PaymentInputType


internal class DropInPaymentViewModel(private val dropInPaymentCallbacks: DropInPaymentCallbacks) {

    private var isPnValid = false
    private var isExpiryDateValid = false
    private var isCvvValid = false

    private var requiredViews: Set<PaymentInputType>? = null

    @JvmSynthetic
    fun updatePaymentForm(paymentInputType: PaymentInputType, isValid: Boolean) {
        if (requiredViews?.contains(paymentInputType) == false) return

        when (paymentInputType) {
            PaymentInputType.PAN -> isPnValid = isValid
            PaymentInputType.ExpiryDate -> isExpiryDateValid = isValid
            PaymentInputType.CVV -> isCvvValid = isValid
        }

        if (validatePaymentForm()) {
            dropInPaymentCallbacks.onPaymentFormValid()
        } else {
            dropInPaymentCallbacks.onPaymentFormInvalid()
        }
    }

    private fun validatePaymentForm(): Boolean =
        isPnValid && isExpiryDateValid && isCvvValid

    @JvmSynthetic
    fun setupForTokenizedPayment(requiredViews: Set<PaymentInputType>) {
        this.requiredViews = requiredViews

        isPnValid = requiredViews.none { it == PaymentInputType.PAN }
        isExpiryDateValid = requiredViews.none { it == PaymentInputType.ExpiryDate }
        isCvvValid = requiredViews.none { it == PaymentInputType.CVV }

        if (requiredViews.isEmpty()) {
            dropInPaymentCallbacks.onPaymentFormValid()
        }
    }

    /**
     * VM callbacks used by [DropInPaymentView]
     */
    interface DropInPaymentCallbacks {
        fun onPaymentFormValid()
        fun onPaymentFormInvalid()
    }
}