package com.trustpayments.mobile.utils.core

import java.util.concurrent.TimeoutException

fun waitForCondition(condition: () -> Boolean, timeoutMillis: Long) {
    var totalTime = 0L

    do {
        if(condition()) {
            break
        }

        val step = 250L
        Thread.sleep(step)
        totalTime += step

        if(totalTime > timeoutMillis) {
            throw TimeoutException("Condition timed out")
        }
    } while (true)
}