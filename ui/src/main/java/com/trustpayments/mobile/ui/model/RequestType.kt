package com.trustpayments.mobile.ui.model

enum class RequestType(val serializedName: String) {
    JsInit("JSINIT"),
    Error("ERROR"),
    AccountCheck("ACCOUNTCHECK"),
    Auth("AUTH"),
    Subscription("SUBSCRIPTION"),
    ThreeDQuery("THREEDQUERY"),
    RiskDec("RISKDEC"),
    CacheTokenise("CACHETOKENISE");

    companion object {
        fun createForString(name: String): RequestType =
            when (name) {
                JsInit.serializedName -> JsInit
                Error.serializedName -> Error
                AccountCheck.serializedName -> AccountCheck
                Auth.serializedName -> Auth
                Subscription.serializedName -> Subscription
                ThreeDQuery.serializedName -> ThreeDQuery
                RiskDec.serializedName -> RiskDec
                CacheTokenise.serializedName -> CacheTokenise
                else -> throw Exception("Unsupported request type provided in the JWT")
            }
    }
}