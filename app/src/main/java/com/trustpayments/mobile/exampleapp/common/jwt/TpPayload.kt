package com.trustpayments.mobile.exampleapp.common.jwt

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
sealed class BasePayload(
    @SerialName("accounttypedescription") val accountTypeDescription: String = "ECOM",
    @SerialName("termurl") val termUrl: String =
        "https://payments.securetrading.net/process/payments/mobilesdklistener",
)

@Serializable
data class StandardPayload(
    @SerialName("sitereference") val siteReference: String,
    @SerialName("currencyiso3a") val currencyISO3a: String,
    @SerialName("baseamount") val baseAmount: Int,
    @SerialName("requesttypedescriptions") val requestTypeDescriptions: List<String>
) : BasePayload()

@Serializable
data class TokenizationPayload(
    @SerialName("sitereference") val siteReference: String,
    @SerialName("currencyiso3a") val currencyISO3a: String,
    @SerialName("baseamount") val baseAmount: Int,
    @SerialName("requesttypedescriptions") val requestTypeDescriptions: List<String>,
    @SerialName("credentialsonfile") val credentialsOnFile: CredentialsOnFile,
    @SerialName("parenttransactionreference") var parentTransactionReference: String?,
) : BasePayload()

@Serializable
data class RecurringAndInstallmentsPayload(
    @SerialName("sitereference") val siteReference: String,
    @SerialName("currencyiso3a") val currencyISO3a: String,
    @SerialName("baseamount") val baseAmount: Int,
    @SerialName("requesttypedescriptions") val requestTypeDescriptions: List<String>,
    @SerialName("subscriptionunit") val subscriptionUnit: String,
    @SerialName("subscriptionfrequency") val subscriptionFrequency: Int,
    @SerialName("subscriptionnumber") val subscriptionNumber: Int,
    @SerialName("subscriptionfinalnumber") val subscriptionFinalNumber: Int,
    @SerialName("subscriptiontype") val subscriptionType: String
) : BasePayload()

@Serializable
enum class CredentialsOnFile {
    @SerialName("1")
    SaveForFutureUse,

    @SerialName("2")
    UsePreviouslySaved
}