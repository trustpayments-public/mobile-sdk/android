package com.trustpayments.mobile.ui.card

import com.trustpayments.mobile.ui.card.model.CardType
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class CardCvvTest {

    private lateinit var cardValidator: CardValidator

    @Before
    fun setUp() {
        cardValidator = CardValidator()
    }

    @Test
    fun test_InvalidEmptyCvvForVisa() {
        // GIVEN
        val cvv = ""
        val expectedResult = false

        // WHEN
        val actualResult = cardValidator.isCvvValid(cvv, CardType.Visa)

        // THEN
        Assert.assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_ValidThreeDigitCvvForVisa() {
        // GIVEN
        val cvv = "123"
        val expectedResult = true

        // WHEN
        val actualResult = cardValidator.isCvvValid(cvv, CardType.Visa)

        // THEN
        Assert.assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_InvalidFourDigitCvvForVisa() {
        // GIVEN
        val cvv = "1234"
        val expectedResult = false

        // WHEN
        val actualResult = cardValidator.isCvvValid(cvv, CardType.Visa)

        // THEN
        Assert.assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_ValidFourDigitCvvForAmex() {
        // GIVEN
        val cvv = "1234"
        val expectedResult = true

        // WHEN
        val actualResult = cardValidator.isCvvValid(cvv, CardType.Amex)

        // THEN
        Assert.assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_InvalidThreeDigitCvvForAmex() {
        // GIVEN
        val cvv = "123"
        val expectedResult = false

        // WHEN
        val actualResult = cardValidator.isCvvValid(cvv, CardType.Amex)

        // THEN
        Assert.assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CorrectCvvLengthForVisa() {
        // GIVEN
        val expectedResult = 3

        // WHEN
        val actualResult = CardInfo.getCvvLength(CardType.Visa)

        // THEN
        Assert.assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CorrectCvvLengthForAmex() {
        // GIVEN
        val expectedResult = 4

        // WHEN
        val actualResult = CardInfo.getCvvLength(CardType.Amex)

        // THEN
        Assert.assertEquals(expectedResult, actualResult)
    }
}