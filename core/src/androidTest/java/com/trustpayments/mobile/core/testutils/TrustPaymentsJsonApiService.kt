package com.trustpayments.mobile.core.testutils

import com.google.gson.annotations.SerializedName
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.models.api.response.ResponseErrorCode
import com.trustpayments.mobile.core.services.getGson
import com.trustpayments.mobile.core.utils.logJson
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit

class TrustPaymentsJsonApiService {

    private val retrofitApi: TrustPaymentsApi

    private val MAX_RETRY_NUMBER = 20
    private val MAX_RETRY_TIMEOUT_SEC = 40
    private val CONNECT_TIMEOUT_SEC = 5L
    private val READ_WRITE_TIMEOUT_SEC = 60L

    private val contentFormatVersion = "1.00"

    private val gson = getGson()

    init {
        val headerInterceptor = object: Interceptor {
            override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
                val newRequest = chain.request().newBuilder().addHeader("Accept", "application/json").build()
                return chain.proceed(newRequest)
            }
        }

        val okHttpClient = OkHttpClient.Builder()
            .apply {
                addInterceptor(HttpLoggingInterceptor(object: HttpLoggingInterceptor.Logger {
                    override fun log(message: String) {
                        logJson(message)
                    }
                }).apply { level = HttpLoggingInterceptor.Level.BODY })
            }
            .apply {
                addInterceptor(Interceptor { chain ->
                    var request = chain.request()
                    request = request.newBuilder()
                        .addHeader("Authorization",
                            Credentials.basic(BuildConfig.MERCHANT_USERNAME_JSON, BuildConfig.MERCHANT_PASSWORD_JSON))
                        .build()

                    chain.proceed(request)
                })
            }
            .readTimeout(READ_WRITE_TIMEOUT_SEC, TimeUnit.SECONDS)
            .writeTimeout(READ_WRITE_TIMEOUT_SEC, TimeUnit.SECONDS)
            .connectTimeout(CONNECT_TIMEOUT_SEC, TimeUnit.SECONDS)
            .addInterceptor(headerInterceptor)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(TrustPaymentsGatewayType.EU.url)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        retrofitApi = retrofit.create(TrustPaymentsApi::class.java)
    }

    @JvmSynthetic
    internal suspend fun performRefund(
        cacheToken: String?, requestTypes: Array<String>,
        currency: String, baseAmount: String,
        parentTransactionReference: String? = null
    ): Result {
        val refundRequest =
            JsonTransactionRequest(
                "ECOM",
                baseAmount,
                currency,
                BuildConfig.SITE_REFERENCE_JSON,
                cacheToken,
                requestTypes,
                parentTransactionReference = parentTransactionReference,
                null
            )

        val webRequest =
            JsonWebserviceRequest(
                BuildConfig.MERCHANT_USERNAME_JSON,
                contentFormatVersion,
                arrayOf(refundRequest)
            )

        return performRequest(webRequest)
    }

    @JvmSynthetic
    suspend fun performTransactionQuery(
            requestTypes: Array<String>,
            currency: String, baseAmount: String,
            transactionReferences: Array<String>? = null
    ): Result {
        val refundRequest =
                JsonTransactionRequest(
                    "ECOM",
                    baseAmount,
                    currency,
                    BuildConfig.SITE_REFERENCE_JSON,
                    null,
                    requestTypes,
                    null,
                    JsonFilterRequest(
                        arrayOf(JsonReferenceRequest(currency)),
                        arrayOf(JsonReferenceRequest(BuildConfig.SITE_REFERENCE)),
                        transactionReferences?.map { JsonReferenceRequest(it) }?.toTypedArray()
                    )
                )

        val webRequest =
                JsonWebserviceRequest(
                    BuildConfig.MERCHANT_USERNAME_JSON,
                    contentFormatVersion,
                    arrayOf(refundRequest)
                )

        return performRequest(webRequest)
    }

    private suspend fun performRequest(request: JsonWebserviceRequest): Result {
        val startTimeoutMillis = System.currentTimeMillis()
        var numberOfRetries = 0

        while(numberOfRetries < MAX_RETRY_NUMBER && (System.currentTimeMillis() + CONNECT_TIMEOUT_SEC * 1000 < startTimeoutMillis + MAX_RETRY_TIMEOUT_SEC * 1000)) {
            val response = try {
                retrofitApi.sendJsonRequest(request)
            } catch (ex: Exception) {
                when(ex) {
                    is SocketTimeoutException,
                    is ConnectException -> {
                        //this can happen if there is networking error (connect)
                        numberOfRetries++
                        continue
                    }
                    //this can happen if there is networking error (not connect) or parsing error (no jwt found, or response structure is incorrect)
                    else -> return Result.Failure
                }
            }

            //at this point either there is a HTTP status code error (in which case response was not yet deserialized)
            //or HTTP status code is ok, and response was successfully deserialized
            //we are treating single transaction deserialization errors as 'correct' responses, returning TransactionResponse.ParsingError
            return validateResponse(response)
        }

        //maximum number of retries has been reached
        return Result.Failure
    }

    private fun validateResponse(response: Response<JsonWebserviceResponse>): Result {
        if(!response.isSuccessful) {
            return Result.Failure
        }

        val body = response.body()

        body ?: return Result.Failure

        return Result.Success(body.transactionResponses.toList())
    }

    sealed class Result {
        data class Success(
            val transactionResponses: List<JsonTransactionResponse>): Result()
        object Failure: Result()
    }
}

private interface TrustPaymentsApi {
    @POST("json/")
    suspend fun sendJsonRequest(@Body request: JsonWebserviceRequest): Response<JsonWebserviceResponse>
}

private enum class TrustPaymentsGatewayType(val url: String) {
    EU("https://webservices.securetrading.net/"),
    EU_BACKUP("https://webservices2.securetrading.net/"),
    US("https://webservices.securetrading.us/")
}

private data class JsonWebserviceRequest(
    val alias: String,
    val version: String,
    @SerializedName("request") val requests: Array<JsonTransactionRequest>
)

private data class JsonTransactionRequest(
    val accountTypeDescription: String,
    val baseAmount: String,
    val currencyiso3a: String,
    val siteReference: String,
    val cacheToken: String?,
    @SerializedName("requesttypedescriptions") val requestTypes: Array<String>,
    val parentTransactionReference: String?,
    val filter: JsonFilterRequest?
)

private data class JsonFilterRequest(
    val currencyiso3a: Array<JsonReferenceRequest>?,
    val siteReference: Array<JsonReferenceRequest>?,
    val transactionReference: Array<JsonReferenceRequest>?,
)

private data class JsonReferenceRequest(
    val value: String
)

data class JsonWebserviceResponse(
    @SerializedName("response") val transactionResponses: List<JsonTransactionResponse>
)

data class JsonTransactionResponse(
    val errorCode: ResponseErrorCode,
    val errorMessage: String, //always returned, "Ok" if successful
    val errorData: Array<String>? = null, //may be empty if no error
    val records: Array<JsonRecordResponse>,
    val requestTypeDescription: String? = null
)

data class JsonRecordResponse(
    val customerpostcode: String?,
    val billingfirstname: String?,
    val customerfirstname: String?,
    val billingpostcode: String?
)