package com.trustpayments.mobile.core.services.transaction

sealed class ThreeDSecureError {
    object IncorrectVersion: ThreeDSecureError()
    object ChallengeCanceled: ThreeDSecureError()
    object EmptyChallengeResult: ThreeDSecureError()
    object IncorrectTransactionId: ThreeDSecureError()

    /**
     * Error returned when the transaction process was interrupted by a user (e.g. by taping the
     * back button). This error is not applicable to the transaction cancelation that is
     * indicated by other errors types.
     */
    data class InterruptedByUser(val errorMessage: String = "") : ThreeDSecureError()

    /**
     * Error returned when required resource couldn't be loaded into the web view
     */
    object WebResourceLoadingError : ThreeDSecureError()

    object Unknown: ThreeDSecureError()
    data class GeneralError(val errorCode: Int): ThreeDSecureError()
    object UncaughtExceptionError: ThreeDSecureError()
}