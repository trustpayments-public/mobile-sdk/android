package com.trustpayments.mobile.exampleapp.samples.backgroundrequest

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.trustpayments.mobile.exampleapp.BuildConfig
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.common.base.BaseSampleActivity
import com.trustpayments.mobile.exampleapp.common.jwt.StandardPayload
import com.trustpayments.mobile.exampleapp.databinding.ActivityBackgroundRequestBinding
import com.trustpayments.mobile.exampleapp.utils.Constants
import com.trustpayments.mobile.exampleapp.utils.DateHelper
import com.trustpayments.mobile.exampleapp.utils.Utils
import com.trustpayments.mobile.exampleapp.utils.Utils.hideKeyboard
import com.trustpayments.mobile.ui.model.RequestType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class BackgroundRequestSampleActivity : BaseSampleActivity() {

    private val viewBinding by lazy {
        ActivityBackgroundRequestBinding.inflate(layoutInflater)
    }

    override var payload = json.encodeToString(
        StandardPayload.serializer(), StandardPayload(
            BuildConfig.SITE_REFERENCE,
            "GBP",
            1050,
            listOf(RequestType.ThreeDQuery.serializedName, RequestType.Auth.serializedName),
        )
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(viewBinding.root)
        setJWTExplorerView(viewBinding.jwtExplorerView)
        setSupportActionBar(viewBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewBinding.toolbar.setNavigationOnClickListener { finish() }
        viewBinding.btnRun.setOnClickListener { runButtonClick() }
        setRequestTypeAndStatusText(getString(R.string.background_request_ready))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.background_request_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val itemId = item.itemId
        return if (itemId == R.id.info) {
            showDialog(
                getString(R.string.background_request_available_request_types_title),
                resources.getStringArray(R.array.background_request_available_request_types)
                    .joinToString("\n\n")
            )
            true
        } else super.onOptionsItemSelected(item)
    }

    override fun getPayload() = try {
        json.decodeFromString(StandardPayload.serializer(), payload)
    } catch (e: Exception) {
        null
    }

    private fun setRequestTypeAndStatusText(status: String) {
        viewBinding.currentRequestType.text =
            getString(R.string.background_request_current_request_type_s,
                getPayload()?.requestTypeDescriptions?.reduce { acc, s -> "$acc + $s" })
        viewBinding.status.text = getString(R.string.background_request_status, status)
    }

    private fun setExecutingSessionUIState() {
        setRequestTypeAndStatusText(getString(R.string.background_request_runnig))
        viewBinding.btnRun.isEnabled = false
        viewBinding.btnRun.text = ""
        viewBinding.loadingIndicator.isVisible = true
    }

    private fun runButtonClick() {
        hideKeyboard(viewBinding.jwtExplorerView)
        val token = getToken() ?: return
        setExecutingSessionUIState()
        executeSession(token)
    }

    /**
     * Executes a payment session with the provided token.
     *
     * @param token The authentication token used to create the payment session.
     */
    private fun executeSession(token: String) {
        // Create a payment session with the provided token and additional parameters
        val session = paymentTransactionManager.createSession(
            { token },
            Constants.TEST_CARD_NUM,
            DateHelper.getValidCardExpiryDate(),
            Constants.TEST_CARD_CVV
        )

        // Perform the execution of the payment session in the background using coroutines
        lifecycleScope.launch(Dispatchers.IO) {
            // Execute the payment session with relevant parameters
            val result = paymentTransactionManager.executeSession(
                session
            ) { this@BackgroundRequestSampleActivity }

            // Check if there is an error message in the result
            val errorMessage =
                Utils.findExecuteSessionErrorMessage(
                    result,
                    this@BackgroundRequestSampleActivity
                )

            // Determine the status message based on the presence of an error message
            val statusMessage = errorMessage ?: getString(R.string.hs_transaction_success)

            // Get jwtResponseData
            val jwtResponseData =
                Utils.findExecuteSessionErrorData(result, this@BackgroundRequestSampleActivity)
                    ?: Utils.jwtStringListToDecodedJsonArrayString(result.responseJwtList)

            var alertMessage = errorMessage ?: getString(R.string.hs_transaction_failed)
            var alertTitle = getString(R.string.hs_transaction_failed)

            // Check if there are response JWT tokens in the result
            if (errorMessage.isNullOrBlank() && result.responseJwtList.isNotEmpty()) {
                alertTitle = getString(R.string.hs_transaction_success)
                alertMessage = getString(R.string.hs_transaction_successfully_completed)
            }

            // Update the UI state based on the execution result
            lifecycleScope.launch {
                setFinishedExecuteSessionUIState(
                    jwtResponseData,
                    statusMessage,
                    alertMessage,
                    alertTitle
                )
            }
        }
    }

    private fun setFinishedExecuteSessionUIState(
        jwtViewData: String,
        responseStatus: String,
        alertMessage: String,
        alertTitle: String
    ) {
        viewBinding.jwtExplorerView.setResponseJwtToken(jwtViewData)
        viewBinding.jwtExplorerView.moveTokenTab(1)
        setRequestTypeAndStatusText(responseStatus)
        viewBinding.btnRun.text = getString(R.string.run)
        viewBinding.btnRun.isEnabled = true
        viewBinding.loadingIndicator.isVisible = false
        showDialog(alertTitle, alertMessage)
    }
}
