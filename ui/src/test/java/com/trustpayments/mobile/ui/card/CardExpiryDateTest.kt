package com.trustpayments.mobile.ui.card

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class CardExpiryDateTest {

    private lateinit var cardValidator: CardValidator

    @Before
    fun setUp() {
        cardValidator = CardValidator()
    }

    @Test
    fun test_ValidExpirationDateFromTheFuture() {
        // GIVEN
        val month = "05"
        val year = "33"
        val expectedResult = true

        // WHEN
        val actualResult = cardValidator.isExpiryDateValid(month, year)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_InvalidExpirationDateFromThePast() {
        // GIVEN
        val month = "05"
        val year = "03"
        val expectedResult = false

        // WHEN
        val actualResult = cardValidator.isExpiryDateValid(month, year)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_InvalidExpirationDateForMissingMonth() {
        // GIVEN
        val month = ""
        val year = "03"
        val expectedResult = false

        // WHEN
        val actualResult = cardValidator.isExpiryDateValid(month, year)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_InvalidExpirationDateForMissingYear() {
        // GIVEN
        val month = "05"
        val year = ""
        val expectedResult = false

        // WHEN
        val actualResult = cardValidator.isExpiryDateValid(month, year)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_InvalidExpirationDateForMontGreaterTahDecember() {
        // GIVEN
        val month = "14"
        val year = "33"
        val expectedResult = false

        // WHEN
        val actualResult = cardValidator.isExpiryDateValid(month, year)

        // THEN
        assertEquals(expectedResult, actualResult)
    }
}