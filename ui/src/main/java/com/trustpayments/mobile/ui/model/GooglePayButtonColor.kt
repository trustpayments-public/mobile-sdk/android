package com.trustpayments.mobile.ui.model

enum class GooglePayButtonColor(val color : Int) {
    BLACK(1),
    WHITE(2)
}