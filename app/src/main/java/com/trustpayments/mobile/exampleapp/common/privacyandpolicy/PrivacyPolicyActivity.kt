package com.trustpayments.mobile.exampleapp.common.privacyandpolicy

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.databinding.ActivityPrivacyPolicyBinding
import com.trustpayments.mobile.exampleapp.utils.Constants.SP_KEY_IS_PRIVACY_POLICY_ACCEPTED

class PrivacyPolicyActivity : AppCompatActivity() {
    private val viewBinding by lazy {
        ActivityPrivacyPolicyBinding.inflate(layoutInflater)
    }

    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, PrivacyPolicyActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(viewBinding.root)
        val html = resources.openRawResource(R.raw.sample_app_privacy_policy).bufferedReader()
            .use { it.readText() }
        val encodedHtml = Base64.encodeToString(html.toByteArray(), Base64.NO_PADDING)
        viewBinding.privacyPolicyWebView.loadData(encodedHtml, "text/html", "base64")
        viewBinding.privacyPolicyButtonAccept.setOnClickListener { showAdditionalConsent() }
        viewBinding.privacyPolicyButtonDecline.setOnClickListener {
            Toast.makeText(
                this, R.string.privacy_policy_decline_message,
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun showAdditionalConsent() {
        val message = getString(R.string.privacy_policy_alert_message)
        val builder = AlertDialog.Builder(this)
        builder.setPositiveButton(R.string.privacy_policy_yes) { dialog, _ ->
            dialog.dismiss()
            saveAccept()
            finish()
        }
        builder.setNegativeButton(R.string.privacy_policy_no, null)
        val alert = builder.create()
        alert.setTitle(R.string.privacy_policy_alert_title)
        alert.setMessage(message)
        alert.show()
    }

    private fun saveAccept() {
        val sharedPreference = PreferenceManager.getDefaultSharedPreferences(this)
        val editor = sharedPreference.edit()
        editor.putBoolean(SP_KEY_IS_PRIVACY_POLICY_ACCEPTED, true)
        editor.apply()
    }
}