package com.trustpayments.mobile.core.services.transaction

import android.os.Build
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.services.api.TrustPaymentsGatewayType
import com.trustpayments.mobile.core.analytics.logger.protocol.AnalyticEventInfo
import com.trustpayments.mobile.core.analytics.logger.protocol.AnalyticEvent
import com.trustpayments.mobile.core.analytics.logger.protocol.LogLevel
import com.trustpayments.mobile.core.analytics.logger.protocol.AnalyticClientSettings
import com.trustpayments.mobile.core.analytics.logger.AnalyticHub
import com.trustpayments.mobile.core.analytics.logger.protocol.Message
import java.util.Date
import java.util.UUID

internal class LoggingManager private constructor(
    private val loggingManagerDataSource: LoggingManagerDataSource
) {

    companion object {
        @JvmSynthetic
        internal fun getInstance(
            loggingManagerDataSource: LoggingManagerDataSource
        ) =
            LoggingManager(loggingManagerDataSource)
    }
    
    private var hub: AnalyticHub? = null
    
    private var tags = mutableMapOf<String, String>()

    @JvmSynthetic
    internal fun sendExceptionEvent(e: Exception) {
        val exception = e.javaClass.toString()
        val eventInfo = AnalyticEventInfo().apply {
            logLevel = LogLevel.FATAL
            message = "Exception caught"
            setData("date", Date())
            setData("username", loggingManagerDataSource.merchantUsername ?: "none")
        }
        val errorMessage = Message().apply { formatted = formatMessage(exception) }
        val event = AnalyticEvent(
            eventId = UUID.randomUUID(),
            eventInfo = arrayListOf((eventInfo)),
            message = errorMessage,
            tags = tags
        )
        hub?.captureErrorEvent(event)
    }

    @JvmSynthetic
    internal fun sendErrorEvent(
        eventMessage: ErrorMessages,
        breadcrumbMessage: ErrorMessages,
        logLevel: LogLevel? = LogLevel.ERROR,
        errorCode: Int? = null,
        errorMessage: String? = null
    ) {
        val eventInfo = AnalyticEventInfo().apply {
            this.logLevel = logLevel ?: LogLevel.INFO
            message = formatMessage(breadcrumbMessage.msg)
            setData("date", Date())
            setData("username", loggingManagerDataSource.merchantUsername ?: "none")
            if(errorMessage != null || errorCode != null){
                setData("errorMessage", formatMessage(errorMessage, errorCode))
            }
        }
        val formattedMessage =
            Message().apply { formatted = formatMessage(eventMessage.msg) }
        val event = AnalyticEvent(
            eventId = UUID.randomUUID(),
            eventInfo = arrayListOf((eventInfo)),
            message = formattedMessage,
            tags = tags
        )
        hub?.captureErrorEvent(event)
    }

    private fun formatMessage(eventMessage: String?, errorCode: Int? = 0): String {
        val message = eventMessage?.replace("[0-9]".toRegex(), "#") ?: ErrorMessages.Unknown.msg
        return if (errorCode != null && errorCode != 0) {
            "$message - error code: $errorCode"
        } else {
            message
        }
    }

    @JvmSynthetic
    internal fun init() {
        if(hub == null) {
            val clientSettings = AnalyticClientSettings().apply {
                dsn = "dummyServer"
                merchantUsername = loggingManagerDataSource.merchantUsername
            }
            this.hub = AnalyticHub(clientSettings).apply {
                initialize()
            }
            createTagsMap(loggingManagerDataSource)
        }
    }

    @JvmSynthetic
    internal fun release() {
        hub = null
    }

    /**
     * Generates data related to the environment used in the app for logging purposes.
     * */
    private fun createTagsMap(loggingManagerDataSource: LoggingManagerDataSource) {
        loggingManagerDataSource.gatewayType?.let {
            tags["gatewayUrl"] = it.url
            tags["gatewayType"] = it.toString()
        }
        loggingManagerDataSource.isCardinalLive.let {
            tags["env"] = if (it) "production" else "staging"
        }
        tags["device"] = "${Build.MANUFACTURER} ${Build.MODEL}"
        tags["OS version"] = Build.VERSION.SDK_INT.toString()
        tags["release"] = BuildConfig.VERSION_CODE
    }
}

data class LoggingManagerDataSource(
    val isCardinalLive: Boolean,
    val gatewayType: TrustPaymentsGatewayType?,
    val merchantUsername: String?
)


internal enum class ErrorMessages(val msg: String) {
    PaymentTransactionManagerEvent("Caught in PaymentTransactionManager"),
    ThreeDSecureManagerEvent("Caught in ThreeDSecureManager"),
    TrustPaymentsApiServiceEvent("Caught in TrustPaymentsApiService"),
    MissingUrlParams("Missing URL parameters"),
    WebResourceLoadingError("Web resource loading error"),
    Unknown("Unknown error"),
    FailedExtractingRequestTypes("Failed to extract request types from given JWT"),
    FailedExtractingReturnUrl("Failed to extract returnUrl from given JWT"),
    JwtEmpty("List of request types in given JWT is empty"),
    NoActivityProvider("No activity provider"),
    NoActivityResultProvider("No activity result provider"),
    CardinalWarning("Cardinal warning"),
    HttpFailure("Http failure"),
    NoBody("Empty body in response"),
    JWTInvalid("Invalid JWT in body"),
    FailedToParseServerResponse("Failed to parse server response"),
    ThreeDInitNull("ThreeDInit null"),
    InitError("Initialization error"),
    CardinalError("Cardinal error"),
    MdNull("Md is null"),
    MdNotTransId("Md is not equal to transactionId"),
    ParesNull("Pares is null"),
    ConsumerSessionIdNull("ConsumerSessionId is null"),
    ValidateResponseNull("validateResponse is null"),
    ValidationResponseErrorCodeNull("validation response errorCode is null"),
    ValidationResponseError("validation response error"),
    InitSessionError("InitializeSession error"),
    EmptyRequestTypes("List of request types in returned JWT is empty"),
    EmptyBundle("Bundle is empty"),
}


