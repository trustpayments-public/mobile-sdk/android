package com.trustpayments.mobile.ui

import com.trustpayments.mobile.ui.card.CardInfo
import com.trustpayments.mobile.ui.card.CardValidator
import com.trustpayments.mobile.ui.card.model.CardType

internal class CvvInputViewModel(
    private val cardValidator: CardValidator,
    private val cvvInputCallbacks: CvvInputCallbacks) {

    private var cardType: CardType = CardType.Unknown

    @JvmSynthetic
    fun handleCardType(cardType: CardType) {
        if (this.cardType != cardType) {
            this.cardType = cardType
            cvvInputCallbacks.onCvvLengthChanged(CardInfo.getCvvLength(cardType))
        }
    }

    /**
     * Perform validation and notify a view to display an error no matter if a full CVV was provided or not
     */
    @JvmSynthetic
    fun validateCvv(input: String) {
        if (cardValidator.isCvvValid(input, cardType)) {
            cvvInputCallbacks.onCvvValid(input)
        } else {
            cvvInputCallbacks.onCvvInvalid(true)
        }
    }

    /**
     * Perform validation and notify a view to display an error only if a full CVV was provided
     */
    @JvmSynthetic
    fun checkForMaxLengthAndValidate(input: String) {
        if (input.length == CardInfo.getCvvLength(cardType)) {
            if (cardValidator.isCvvValid(input, cardType)) {
                cvvInputCallbacks.onCvvValid(input)
            } else {
                cvvInputCallbacks.onCvvInvalid(true)
            }
        } else {
            cvvInputCallbacks.onCvvInvalid(false)
        }
    }

    /**
     * VM callbacks used by [CvvInputView]
     */
    interface CvvInputCallbacks {
        /**
         * Called when a provided CVV was correctly validated
         *
         * @param input the the valid input provided in the CVV component
         */
        fun onCvvValid(input: String)

        /**
         * Called when a provided CVV was invalid - incorrect length
         *
         * @param showError indicates if an error should be displayed in case of invalid input data -
         * there is no need to always show it, e.g when CVV is to short and a user is still typing
         */
        fun onCvvInvalid(showError: Boolean)

        fun onCvvLengthChanged(maxLength: Int)
    }
}