package com.trustpayments.mobile.ui

import com.trustpayments.mobile.ui.card.CardValidator

internal class ExpiryDateInputViewModel(
    private val cardValidator: CardValidator,
    private val expiryDateInputCallbacks: ExpiryDateInputCallbacks) {

    @JvmSynthetic
    fun formatExpiryDate(input: String): String {
        if (input.isEmpty())
            return ""

        // extract date components and init month/year variables
        val dateComponents = decomposeDate(input)
        var month = dateComponents.first
        var year = dateComponents.second

        // if month has more than 2 digits (additional digits added before '/' sign), remove it
        // and add at the beginning of the year component
        if (month.length > 2) {
            year = month.slice(2..month.lastIndex) + year
            month = month.dropLast((2..month.lastIndex).count())
        }

        // if year has more than 2 digits (additional digits added after '/' sign), remove it
        // and add at the end of the month component
        if (year.length > 2) {
            month += year.slice(0..year.lastIndex - 2)
            year = year.drop((0..year.lastIndex - 2).count())
        }

        // append default '0' for January - September or correct if month > 12 or == 0
        if (month.length == 1 && month.toInt() > 1) {
            month = "0$month"
        } else if (month.length == 2 && month == "00") {
            month = "01"
        }

        val expiryDate =
            if (month.length <= 1 && year.isEmpty()) {
                month
            } else {
                "$month/$year"
            }

        validateExpiryDate(month, year)

        return expiryDate
    }

    /**
     * Decompose date provided in format: mm/yy to the separated month/year components
     */
    private fun decomposeDate(date: String): Pair<String, String> {
        val decomposed = date.split("/")
        val month = decomposed[0]
        val year = if (decomposed.size == 2) decomposed[1] else ""
        return Pair(month, year)
    }

    /**
     * Perform validation and notify a view to display an error no matter if a full date was provided or not
     */
    @JvmSynthetic
    fun validateExpiryDate(expiryDate: String) {
        val dateComponents = decomposeDate(expiryDate)
        if (cardValidator.isExpiryDateValid(dateComponents.first, dateComponents.second)) {
            expiryDateInputCallbacks.onExpiryDateValid(dateComponents.first + "/" + dateComponents.second)
        } else {
            expiryDateInputCallbacks.onExpiryDateInvalid(true)
        }
    }

    /**
     * Perform validation and notify a view to display an error only if a full date was provided
     */
    private fun validateExpiryDate(month: String, year: String) {
        if (month.length == 2 && year.length == 2) {
            if (cardValidator.isExpiryDateValid(month, year)) {
                expiryDateInputCallbacks.onExpiryDateValid("$month/$year")
            } else {
                expiryDateInputCallbacks.onExpiryDateInvalid(true)
            }
        } else {
            expiryDateInputCallbacks.onExpiryDateInvalid(false)
        }
    }

    /**
     * VM callbacks used by [ExpiryDateInputView]
     */
    interface ExpiryDateInputCallbacks {
        /**
         * Called when a provided expiry date was correctly validated
         *
         * @param input the the valid input provided in the expiry date component
         */
        fun onExpiryDateValid(input: String)

        /**
         * Called when a provided expiry date was invalid - missing month/year
         * or mont/year from the past
         *
         * @param showError indicates if an error should be displayed in case of invalid input data -
         * there is no need to always show it, e.g when an expiry date is to small and a user is still typing
         */
        fun onExpiryDateInvalid(showError: Boolean)
    }
}