package com.trustpayments.mobile.exampleapp.functionaltests.samples

import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.PressBackAction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.isEnabled
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.pageobjects.AuthWith3DSecureFormPage
import com.trustpayments.mobile.exampleapp.samples.accountverificationwithlaterauth.AccountVerificationWithLaterAuthSampleActivity
import com.trustpayments.mobile.exampleapp.utils.TestUtils
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Rule
import org.junit.Test

class AccountVerificationWithLaterAuthActivityTestSuite {
    private val authWith3DSecureFormPage = AuthWith3DSecureFormPage()
    @get:Rule
    val activityScenarioRule =
        ActivityScenarioRule(AccountVerificationWithLaterAuthSampleActivity::class.java)

    @After
    fun tearDown() {
        activityScenarioRule.scenario?.close()
    }

    @Test
    fun testAccountVerificationWithLaterAuthSuccess(): Unit = runBlocking {
        // Click the "Run" button
        onView(withId(R.id.btn_run)).perform(click())

        // Check that the "Run" button is disabled
        onView(withId(R.id.btn_run)).check(matches(not(isEnabled())))

        // Simulate the execution of a 3DS V2 popup
        delay(10000)
        authWith3DSecureFormPage.fillSecureCode(
            CardsDataWith3DSecureV2.threeDSecureCode
        )
            .tapOnSubmitButton()
        delay(10000)

        // Wait for the loading indicator to become invisible within 30 seconds
        TestUtils.waitForVisibleView(
            0,
            30000,
            false,
            withText(R.string.hs_transaction_success)
        )

        onView(withText(R.string.hs_transaction_success))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
            .perform(click())
    }

    @Test
    fun testAccountVerificationWithLaterAuthRequestError(): Unit = runBlocking {
        // Click the "Run" button
        onView(withId(R.id.btn_run)).perform(click())

        TestUtils.waitForVisibleView(
            0,
            30000,
            false,
            withText(R.string.cancel)
        )
        Espresso.pressBack()
//        onView(withText(R.string.cancel))
//            .perform(ViewActions.closeSoftKeyboard(), click())

        // Wait for the loading indicator to become invisible within 30 seconds
        TestUtils.waitForVisibleView(
            0,
            30000,
            false,
            withText(R.string.hs_transaction_failed)
        )

        onView(withText(R.string.hs_transaction_failed))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
            .perform(click())

    }

    // Test to check if the toolbar is visible
    @Test
    fun testToolbarVisibility() {
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()))
    }

    // Test to check the initial texts in the activity
    @Test
    fun testInitialTexts() {
        // Perform actions inside the activity
        var expectedStepOneRequestTypeText = ""
        var expectedStepTwoOneRequestTypeText = ""
        var expectedStatusText = ""
        activityScenarioRule.scenario?.onActivity { activity ->
            expectedStepOneRequestTypeText =
                activity.getString(
                    R.string.account_verification_with_later_auth_step,
                    1,
                    "THREEDQUERY + ACCOUNTCHECK"
                )
            expectedStepTwoOneRequestTypeText =
                activity.getString(R.string.account_verification_with_later_auth_step, 2, "AUTH")
            val readyText = activity.getString(R.string.background_request_ready)
            expectedStatusText = activity.getString(R.string.background_request_status, readyText)
        }

        onView(withId(R.id.step_one)).check(matches(withText(expectedStepOneRequestTypeText)))
        onView(withId(R.id.step_two)).check(matches(withText(expectedStepTwoOneRequestTypeText)))
        onView(withId(R.id.status)).check(matches(withText(expectedStatusText)))
    }

    // Test to check if the loading indicator is visible after button click
    @Test
    fun testLoadingIndicatorVisibility() {
        onView(withId(R.id.btn_run)).perform(click())
        onView(withId(R.id.loading_indicator)).check(matches(isDisplayed()))
    }

    // Test to check if the JWTExplorerView is visible after button click
    @Test
    fun testJWTExplorerViewVisibility() {
        onView(withId(R.id.btn_run)).perform(click())
        onView(withId(R.id.jwt_explorer_view)).check(matches(isDisplayed()))
    }

    // Test to check if the progress bar is visible after button click
    @Test
    fun testProgressBarVisibility() {
        onView(withId(R.id.btn_run)).perform(click())
        onView(withId(R.id.loading_indicator)).check(matches(isDisplayed()))
    }
}
