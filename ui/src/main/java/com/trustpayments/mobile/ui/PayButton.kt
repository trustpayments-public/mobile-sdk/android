package com.trustpayments.mobile.ui

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatButton
import com.trustpayments.mobile.ui.common.PaymentView


class PayButton(context: Context, attrs: AttributeSet
) : AppCompatButton(context, attrs), PaymentView {

    init {
        setupAttributes(attrs)
        isEnabled = false
        isAllCaps = false
    }

    private fun setupAttributes(attrs: AttributeSet) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.PayButton, 0, 0)

        text = typedArray.getString(R.styleable.PayButton_android_text) ?:
                resources.getString(R.string.pvc_pay_button_text)

        typedArray.recycle()
    }
}