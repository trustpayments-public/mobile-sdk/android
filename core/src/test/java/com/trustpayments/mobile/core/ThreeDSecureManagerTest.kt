package com.trustpayments.mobile.core

import android.app.Activity
import android.content.Context
import android.os.Build
import android.util.Log
import com.cardinalcommerce.cardinalmobilesdk.Cardinal
import com.cardinalcommerce.cardinalmobilesdk.enums.CardinalEnvironment
import com.cardinalcommerce.cardinalmobilesdk.models.CardinalActionCode
import com.cardinalcommerce.cardinalmobilesdk.models.CardinalConfigurationParameters
import com.cardinalcommerce.cardinalmobilesdk.models.ValidateResponse
import com.cardinalcommerce.cardinalmobilesdk.services.CardinalInitService
import com.cardinalcommerce.cardinalmobilesdk.services.CardinalValidateReceiver
import com.trustpayments.mobile.core.services.transaction.LoggingManager
import com.trustpayments.mobile.core.services.transaction.ThreeDSecureError
import com.trustpayments.mobile.core.services.transaction.ThreeDSecureManager
import io.mockk.MockKAnnotations
import io.mockk.Runs
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.just
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.mockkStatic
import io.mockk.slot
import io.mockk.unmockkAll
import io.mockk.verify
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.M])
class ThreeDSecureManagerTest {
    private lateinit var threeDSecureManager: ThreeDSecureManager

    @MockK
    private lateinit var context: Context

    @MockK
    private lateinit var mockCardinal: Cardinal

    @MockK
    private lateinit var mockActivity: Activity

    @MockK
    private lateinit var mockLoggingManager: LoggingManager

    private val REQUEST_VERSION1 = "1.0.0"
    private val REQUEST_VERSION2 = "2.0.0"
    private val REQUEST_TRANSACTIONID = "transactionId"
    private val REQUEST_PAYLOAD = "payload"

    @Before
    fun setup() {
        MockKAnnotations.init(this)

        threeDSecureManager =
            ThreeDSecureManager.getInstance(context, false, null, null, mockLoggingManager, false)

        mockkStatic(Cardinal::class)
        every { Cardinal.getInstance() } returns mockCardinal

        mockkStatic(Log::class)
        every { Log.w(any(), any<String>()) } returns 0

        every { mockCardinal.configure(any(), any()) } just Runs
        every { mockCardinal.cleanup() } just Runs

        every { mockActivity.startActivityForResult(any(), any()) } just Runs
        every { mockActivity.getPackageName() } returns ""

        every { context.resources.configuration } returns mockk()
        every { context.getString(any<Int>()) } returns "MockText"

        mockkObject(LoggingManager.Companion)
        every { LoggingManager.Companion.getInstance(any()) } returns mockLoggingManager

        every { mockLoggingManager.sendExceptionEvent(any()) } returns Unit
        every { mockLoggingManager.sendErrorEvent(any(), any(), any(), any(), any()) } returns Unit

    }

    @After
    fun after() {
        unmockkAll()
    }

    //- create

    @Test
    fun test_positive_creationStaging() = runBlocking {
        //GIVEN
        threeDSecureManager =
            ThreeDSecureManager.getInstance(context, false, null, null, mockLoggingManager, false)
        val oldCardinal = mockk<Cardinal>()
        threeDSecureManager.cardinal = oldCardinal

        val captureConfig = slot<CardinalConfigurationParameters>()
        every { mockCardinal.configure(any(), capture(captureConfig)) } just Runs

        //WHEN
        threeDSecureManager.createSession()

        //THEN
        verifyCardinalConfigureCalled()

        assertEquals(mockCardinal, threeDSecureManager.cardinal)
        assertEquals(CardinalEnvironment.STAGING, captureConfig.captured.environment)
    }

    @Test
    fun test_positive_creationProduction() = runBlocking {
        //GIVEN
        val oldCardinal = mockk<Cardinal>()
        delay(200)

        threeDSecureManager =
            ThreeDSecureManager.getInstance(context, true, null, null, mockLoggingManager, false)
        delay(400)
        threeDSecureManager.cardinal = oldCardinal
        delay(800)

        val captureConfig = slot<CardinalConfigurationParameters>()
        every { mockCardinal.configure(any(), capture(captureConfig)) } just Runs

        //WHEN
        threeDSecureManager.createSession()

        //THEN
        verifyCardinalConfigureCalled()

        assertEquals(mockCardinal, threeDSecureManager.cardinal)
        assertEquals(CardinalEnvironment.PRODUCTION, captureConfig.captured.environment)
    }

    //- init

    //1. succeeds
    @Test
    fun test_positive_initialization_succeeds() = runBlocking {
        //GIVEN
        threeDSecureManager.cardinal = mockCardinal
        cardinalReturnsSuccessfulInitResult()

        //WHEN
        threeDSecureManager.initSession("")

        //THEN
        verifyCardinalInitCalled()
    }

    //2. fails

    //- execution

    //3. version is not int
    @Test
    fun test_negative_execution_versionIsNotAnInt() = runBlocking {
        //GIVEN
        val version = "a"
        //WHEN
        val result = threeDSecureManager.executeSession(
            version,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity
        )

        //THEN
        assertTrue(result is ThreeDSecureManager.ExecutionResult.Failure)
    }

    //4. version is not 1 or 2
    @Test
    fun test_negative_execution_unknownVersion() = runBlocking {
        //GIVEN
        val version = "3.0.0"

        //WHEN
        val result = threeDSecureManager.executeSession(
            version,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity
        )

        //THEN
        assertTrue(result is ThreeDSecureManager.ExecutionResult.Failure)
        val errorResult = result as ThreeDSecureManager.ExecutionResult.Failure
        assertTrue(errorResult.error is ThreeDSecureError.IncorrectVersion)
    }

    //-- version is 1

    //5. wrong request code

    //6. result code not successful
    @Test
    fun test_negative_executionV1_resultCodeNotSuccessful() = runBlocking {
        //WHEN
        val result = threeDSecureManager.executeSession(
            REQUEST_VERSION1,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity
        )

        //THEN
        assertTrue(result is ThreeDSecureManager.ExecutionResult.Failure)
        val errorResult = result as ThreeDSecureManager.ExecutionResult.Failure
        assertTrue(errorResult.error is ThreeDSecureError.IncorrectVersion)
    }

    //7. bundle null
    @Test
    fun test_negative_executionV1_bundleIsNull() = runBlocking {
        //WHEN
        val result = threeDSecureManager.executeSession(
            REQUEST_VERSION1,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity
        )

        //THEN
        assertTrue(result is ThreeDSecureManager.ExecutionResult.Failure)
        val errorResult = result as ThreeDSecureManager.ExecutionResult.Failure
        assertTrue(errorResult.error is ThreeDSecureError.IncorrectVersion)
    }

    //8. md null
    @Test
    fun test_negative_executionV1_mdIsNull() = runBlocking {
        //WHEN
        val result = threeDSecureManager.executeSession(
            REQUEST_VERSION1,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity
        )

        //THEN
        assertTrue(result is ThreeDSecureManager.ExecutionResult.Failure)
        val errorResult = result as ThreeDSecureManager.ExecutionResult.Failure
        assertTrue(errorResult.error is ThreeDSecureError.IncorrectVersion)
    }

    //9. pares null
    @Test
    fun test_negative_executionV1_paresIsNull() = runBlocking {
        //WHEN
        val result = threeDSecureManager.executeSession(
            REQUEST_VERSION1,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity
        )

        //THEN
        assertTrue(result is ThreeDSecureManager.ExecutionResult.Failure)
        val errorResult = result as ThreeDSecureManager.ExecutionResult.Failure
        assertTrue(errorResult.error is ThreeDSecureError.IncorrectVersion)
    }

    //10. md not equal to transactionId
    @Test
    fun test_negative_executionV1_mdNotEqualToTransactionId() = runBlocking {
        //WHEN
        val result = threeDSecureManager.executeSession(
            REQUEST_VERSION1,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity,
        )

        //THEN
        assertTrue(result is ThreeDSecureManager.ExecutionResult.Failure)
        val errorResult = result as ThreeDSecureManager.ExecutionResult.Failure
        assertTrue(errorResult.error is ThreeDSecureError.IncorrectVersion)
    }

    //11. Try successful
    @Test
    fun test_try_positive_executionV1_successful() = runBlocking {
        //WHEN
        val result = threeDSecureManager.executeSession(
            REQUEST_VERSION1,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity
        )

        //THEN
        assertTrue(result is ThreeDSecureManager.ExecutionResult.Failure)
        val errorResult = result as ThreeDSecureManager.ExecutionResult.Failure
        assertTrue(errorResult.error is ThreeDSecureError.IncorrectVersion)
    }

    //-- version is 2

    //12. validation response is empty
    @Test
    fun test_negative_executionV2_validationResponseIsNull() = runBlocking {
        //GIVEN
        cardinalReturnsEmptyContinueResult()
        threeDSecureManager.cardinal = mockCardinal
        //WHEN
        val result = threeDSecureManager.executeSession(
            REQUEST_VERSION2,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity
        )

        //THEN
        assertTrue(result is ThreeDSecureManager.ExecutionResult.Failure)

        verifyCardinalContinueCalled()
    }

    //13. action code is success
    @Test
    fun test_positive_executionV2_actionCodeIsSuccess() = runBlocking {
        //GIVEN
        cardinalReturnsSuccessContinueResult()
        threeDSecureManager.cardinal = mockCardinal

        //WHEN
        val result = threeDSecureManager.executeSession(
            REQUEST_VERSION2,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity
        )

        //THEN
        assertTrue(result is ThreeDSecureManager.ExecutionResult.SuccessV2)

        verifyCardinalContinueCalled()
    }

    //14. action code is noaction
    @Test
    fun test_positive_executionV2_actionCodeIsNoAction() = runBlocking {
        //GIVEN
        cardinalReturnsNoActionContinueResult()
        threeDSecureManager.cardinal = mockCardinal
        //WHEN
        val result = threeDSecureManager.executeSession(
            REQUEST_VERSION2,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity
        )

        //THEN
        assertTrue(result is ThreeDSecureManager.ExecutionResult.SuccessV2)

        verifyCardinalContinueCalled()
    }

    //15. action code is something else
    @Test
    fun test_negative_executionV2_unknownActionCode() = runBlocking {
        //GIVEN
        cardinalReturnsErrorContinueResult()
        threeDSecureManager.cardinal = mockCardinal

        //WHEN
        val result = threeDSecureManager.executeSession(
            REQUEST_VERSION2,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity
        )

        //THEN
        assertTrue(result is ThreeDSecureManager.ExecutionResult.Failure)

        verifyCardinalContinueCalled()
    }

    @Test
    fun test_init_session_validation_failure_with_response() = runBlocking {
        // Arrange
        threeDSecureManager.cardinal = mockCardinal
        val validateResponse: ValidateResponse = mockk()
        every { validateResponse.errorNumber } returns 0
        every { mockCardinal.init(any(), any()) } answers {
            val callback = secondArg<CardinalInitService>()
            callback.onValidated(validateResponse, null)
        }
        // Act
        val failureResult = threeDSecureManager.initSession("")
        // Assert
        verify(exactly = 1) { mockCardinal.init(any(), any()) }
        assertTrue(failureResult is ThreeDSecureManager.InitializationResult.Failure)
        val threeDSecureError =
            (failureResult as? ThreeDSecureManager.InitializationResult.Failure)?.error
        assertNotNull(threeDSecureError)
        assertTrue(threeDSecureError is ThreeDSecureError.GeneralError)
    }

    @Test
    fun test_init_session_validation_failure_without_response() = runBlocking {
        // Arrange
        threeDSecureManager.cardinal = mockCardinal
        every { mockCardinal.init(any(), any()) } answers {
            val callback = secondArg<CardinalInitService>()
            callback.onValidated(null, null)
        }
        // Act
        val failureResult = threeDSecureManager.initSession("")
        // Assert
        verify(exactly = 1) { mockCardinal.init(any(), any()) }
        assertTrue(failureResult is ThreeDSecureManager.InitializationResult.Failure)
        val threeDSecureError =
            (failureResult as? ThreeDSecureManager.InitializationResult.Failure)?.error
        assertNotNull(threeDSecureError)
        assertTrue(threeDSecureError is ThreeDSecureError.Unknown)
    }

    @Test
    fun test_performV2Session_when_cardinal_is_null() = runBlocking {
        // Arrange
        threeDSecureManager.cardinal = null
        // Act
        val result = threeDSecureManager.executeSession(
            REQUEST_VERSION2,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity
        )
        // Assert
        assertTrue(result is ThreeDSecureManager.ExecutionResult.Failure)
    }

    @Test
    fun test_performV2Session_when_cardinal_action_code_success() = runBlocking {
        // Arrange
        cardinalReturnsSuccessContinueResult()
        threeDSecureManager.cardinal = mockCardinal
        // Act
        val result = threeDSecureManager.executeSession(
            REQUEST_VERSION2,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity
        )
        // Assert
        assertTrue(result is ThreeDSecureManager.ExecutionResult.SuccessV2)
    }

    @Test
    fun test_performV2Session_when_cardinal_action_code_no_action() = runBlocking {
        // Arrange
        cardinalReturnsNoActionContinueResult()
        threeDSecureManager.cardinal = mockCardinal
        // Act
        val result = threeDSecureManager.executeSession(
            REQUEST_VERSION2,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity
        )
        // Assert
        assertTrue(result is ThreeDSecureManager.ExecutionResult.SuccessV2)
    }

    @Test
    fun test_performV2Session_when_cardinal_action_code_null() = runBlocking {
        // Arrange
        cardinalReturnsNullActionContinueResult()
        threeDSecureManager.cardinal = mockCardinal
        every {
            mockLoggingManager.sendErrorEvent(
                any(), any()
            )
        } just Runs
        // Act
        val result = threeDSecureManager.executeSession(
            REQUEST_VERSION2,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity
        )
        // Assert
        assertTrue(result is ThreeDSecureManager.ExecutionResult.Failure)
        val failureResult = result as ThreeDSecureManager.ExecutionResult.Failure
        assertTrue(failureResult.error is ThreeDSecureError.Unknown)
        verify(exactly = 1) {
            mockLoggingManager.sendErrorEvent(
                any(), any()
            )
        }
    }

    @Test
    fun test_performV2Session_when_cardinal_action_code_cancel() = runBlocking {
        // Arrange
        cardinalReturnsCancelActionContinueResult()
        threeDSecureManager.cardinal = mockCardinal
        // Act
        val result = threeDSecureManager.executeSession(
            REQUEST_VERSION2,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity
        )
        // Assert
        assertTrue(result is ThreeDSecureManager.ExecutionResult.Failure)
        val failureResult = result as ThreeDSecureManager.ExecutionResult.Failure
        assertTrue(failureResult.error is ThreeDSecureError.ChallengeCanceled)
    }

    @Test
    fun test_performV2Session_when_cardinal_action_code_unknown() = runBlocking {
        // Arrange
        cardinalReturnsErrorActionContinueResult()
        threeDSecureManager.cardinal = mockCardinal
        every {
            mockLoggingManager.sendErrorEvent(
                any(), any(), any(), any(), any()
            )
        } just Runs
        // Act
        val result = threeDSecureManager.executeSession(
            REQUEST_VERSION2,
            REQUEST_TRANSACTIONID,
            REQUEST_PAYLOAD,
            mockActivity
        )
        // Assert
        assertTrue(result is ThreeDSecureManager.ExecutionResult.Failure)
        val failureResult = result as ThreeDSecureManager.ExecutionResult.Failure
        assertTrue(failureResult.error is ThreeDSecureError.GeneralError)
        val generalError = failureResult.error as ThreeDSecureError.GeneralError
        assertEquals(100, generalError.errorCode)
        verify(exactly = 1) {
            mockLoggingManager.sendErrorEvent(
                any(), any(), any(), any(), any()
            )
        }
    }

    //GIVEN helpers

    private fun cardinalReturnsSuccessfulInitResult() {
        every { mockCardinal.init(any(), any()) } answers {
            val callback = secondArg<CardinalInitService>()

            callback.onSetupCompleted("")
        }
    }

    private fun cardinalReturnsErrorInitResult() {
        every { mockCardinal.init(any(), any()) } answers {
            val callback = secondArg<CardinalInitService>()

            callback.onValidated(null, null)
        }
    }

    private fun cardinalReturnsSuccessContinueResult() {
        every { mockCardinal.cca_continue(any(), any(), any(), any()) } answers {
            val callback = lastArg<CardinalValidateReceiver>()

            val response = mockk<ValidateResponse>()
            every { response.actionCode } returns CardinalActionCode.SUCCESS

            callback.onValidated(null, response, "")
        }
    }

    private fun cardinalReturnsNoActionContinueResult() {
        every { mockCardinal.cca_continue(any(), any(), any(), any()) } answers {
            val callback = lastArg<CardinalValidateReceiver>()

            val response = mockk<ValidateResponse>()
            every { response.actionCode } returns CardinalActionCode.NOACTION

            callback.onValidated(null, response, "")
        }
    }

    private fun cardinalReturnsCancelActionContinueResult() {
        every { mockCardinal.cca_continue(any(), any(), any(), any()) } answers {
            val callback = lastArg<CardinalValidateReceiver>()

            val response = mockk<ValidateResponse>()
            every { response.actionCode } returns CardinalActionCode.CANCEL

            callback.onValidated(null, response, "")
        }
    }

    private fun cardinalReturnsErrorActionContinueResult() {
        every { mockCardinal.cca_continue(any(), any(), any(), any()) } answers {
            val callback = lastArg<CardinalValidateReceiver>()

            val response = mockk<ValidateResponse>()
            every { response.actionCode } returns CardinalActionCode.ERROR
            every { response.errorNumber } returns 100
            every { response.errorDescription } returns "error"

            callback.onValidated(null, response, "")
        }
    }

    private fun cardinalReturnsNullActionContinueResult() {
        every { mockCardinal.cca_continue(any(), any(), any(), any()) } answers {
            val callback = lastArg<CardinalValidateReceiver>()

            val response = mockk<ValidateResponse>()
            every { response.actionCode } returns null

            callback.onValidated(null, response, "")
        }
    }

    private fun cardinalReturnsEmptyContinueResult() {
        every { mockCardinal.cca_continue(any(), any(), any(), any()) } answers {
            val callback = lastArg<CardinalValidateReceiver>()

            callback.onValidated(null, null, null)
        }
    }

    private fun cardinalReturnsErrorContinueResult() {
        every { mockCardinal.cca_continue(any(), any(), any(), any()) } answers {
            val callback = lastArg<CardinalValidateReceiver>()

            val response = mockk<ValidateResponse>()
            every { response.actionCode } returns CardinalActionCode.ERROR
            every { response.errorNumber } returns 0

            callback.onValidated(null, response, null)
        }
    }


    //WHEN helpers

    //THEN helpers

    private fun verifyCardinalConfigureCalled() {
        verify(exactly = 1) { mockCardinal.configure(any(), any()) }
    }

    private fun verifyCardinalInitCalled() {
        verify(exactly = 1) { mockCardinal.init(any(), any()) }
    }

    private fun verifyCardinalContinueCalled() {
        verify(exactly = 1) { mockCardinal.cca_continue(any(), any(), any(), any()) }
    }
}