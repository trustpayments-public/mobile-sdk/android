package com.trustpayments.mobile.core.testcases.javasupport;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import com.trustpayments.mobile.core.PaymentSession;
import com.trustpayments.mobile.core.models.JwtResponse;
import com.trustpayments.mobile.core.models.api.response.CustomerOutput;
import com.trustpayments.mobile.core.models.api.response.RequestType;
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager;
import com.trustpayments.mobile.core.test.BuildConfig;
import com.trustpayments.mobile.core.testutils.BaseTestCase;
import com.trustpayments.mobile.core.util.JWTBuilder;
import com.trustpayments.mobile.core.util.ResponseParser;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

public class GeneralIntegrationJavaTestCases extends BaseTestCase {

    private PaymentTransactionManager paymentTransactionManager;

    private JWTBuilder jwtBuilder = new JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY);

    @Before
    @Override
    public void setUp() {
        super.setUp();

        Context app = ApplicationProvider.getApplicationContext();
        paymentTransactionManager = new PaymentTransactionManager(
                app, getGatewayType(), false, BuildConfig.MERCHANT_USERNAME,
                null, null, false
        );
    }

    @Test
    public void test_positive_auth() throws ExecutionException, InterruptedException {
        // GIVEN
        String jwtToken = jwtBuilder.getStandard(new ArrayList<RequestType>() {{
            add(RequestType.Auth);
        }}, null, null, null);
        PaymentSession session =
                paymentTransactionManager.createSession(jwtToken, "4111111111111111", getExpiryDate(), "123",
                        null, null);

        // WHEN
        PaymentTransactionManager.Response sessionResult = paymentTransactionManager.executeSession(session, null, null);
        JwtResponse jwtResponse = ResponseParser.Companion.parse(sessionResult.getResponseJwtList().get(0));

        // THEN
        Assert.assertEquals(CustomerOutput.Result, jwtResponse.getCustomerOutput().getCustomerOutput());
    }

    @Test
    public void test_negative_auth() throws ExecutionException, InterruptedException {
        // GIVEN
        String jwtToken = jwtBuilder.getStandard(new ArrayList<RequestType>() {{
            add(RequestType.Auth);
        }}, null, null, null);
        PaymentSession session =
                paymentTransactionManager.createSession(jwtToken, "411111111111", getExpiryDate(), "123",
                        null, null);

        // WHEN
        PaymentTransactionManager.Response sessionResult = paymentTransactionManager.executeSession(session, null, null);
        JwtResponse jwtResponse = ResponseParser.Companion.parse(sessionResult.getResponseJwtList().get(0));

        // THEN
        Assert.assertEquals(CustomerOutput.TryAgain, jwtResponse.getCustomerOutput().getCustomerOutput());
    }

    @Test
    public void test_positive_accountCheckAndAuth() throws ExecutionException, InterruptedException {
        // GIVEN
        String jwtToken = jwtBuilder.getStandard(new ArrayList<RequestType>() {{
            add(RequestType.AccountCheck);
            add(RequestType.Auth);
        }}, null, null, null);
        PaymentSession session =
                paymentTransactionManager.createSession(jwtToken, "4111111111111111", getExpiryDate(), "123",
                        null, null);

        // WHEN
        PaymentTransactionManager.Response sessionResult = paymentTransactionManager.executeSession(session, null, null);
        JwtResponse jwtResponse = ResponseParser.Companion.parse(sessionResult.getResponseJwtList().get(0));

        // THEN
        Assert.assertEquals(CustomerOutput.Result, jwtResponse.getCustomerOutput().getCustomerOutput());
    }

    @Test
    public void test_negative_accountCheckAndAuth() throws ExecutionException, InterruptedException {
        // GIVEN
        String jwtToken = jwtBuilder.getStandard(new ArrayList<RequestType>() {{
            add(RequestType.AccountCheck);
            add(RequestType.Auth);
        }}, null, null, null);
        PaymentSession session =
                paymentTransactionManager.createSession(jwtToken, "411111111111", getExpiryDate(), "123",
                        null, null);

        // WHEN
        PaymentTransactionManager.Response sessionResult = paymentTransactionManager.executeSession(session, null, null);
        JwtResponse jwtResponse = ResponseParser.Companion.parse(sessionResult.getResponseJwtList().get(0));

        // THEN
        Assert.assertEquals(CustomerOutput.TryAgain, jwtResponse.getCustomerOutput().getCustomerOutput());
    }

    private String getExpiryDate() {
        Calendar calendar = Calendar.getInstance();
        int expiryDateYear = calendar.get(Calendar.YEAR) + 3;
        return "01/" + expiryDateYear;
    }
}
