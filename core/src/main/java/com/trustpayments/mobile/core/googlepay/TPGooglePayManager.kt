package com.trustpayments.mobile.core.googlepay

import android.app.Activity
import android.content.Intent
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.wallet.AutoResolveHelper
import com.google.android.gms.wallet.IsReadyToPayRequest
import com.google.android.gms.wallet.PaymentData
import com.google.android.gms.wallet.PaymentDataRequest
import com.google.android.gms.wallet.PaymentsClient
import com.google.android.gms.wallet.Wallet
import com.trustpayments.mobile.core.utils.log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.math.BigDecimal
import java.math.RoundingMode

class TPGooglePayManager(builder: Builder) {
    val baseRequest = JSONObject().apply {
        put("apiVersion", builder.apiVersion)
        put("apiVersionMinor", builder.apiVersionMinor)
    }

    val allowedCardAuthMethods = JSONArray(builder.allowedCardAuthMethods)
    val allowedCardNetworks = JSONArray(builder.allowedCardNetworks)

    val billingAddressRequired = builder.billingAddressRequired
    val billingAddressParameters = JSONObject().apply {
        put("format", builder.format)
        put("phoneNumberRequired", builder.billingPhoneNumberRequired)
    }

    val shippingAddressRequired = builder.shippingAddressRequired
    val shippingAddressParameters = JSONObject().apply {
        put("phoneNumberRequired", builder.shippingPhoneNumberRequired)
        put("allowedCountryCodes", JSONArray(builder.allowedCountryCodes))
    }

    private val paymentsClient: PaymentsClient = builder.paymentsClient
    val countryCode = builder.countryCode
    val currencyCode = builder.currencyCode
    val gatewayMerchantId = builder.gatewayMerchantId
    val gateway = builder.gateway
    val requestCode = builder.requestCode ?: REQUEST_CODE
    val activity = builder.activity
    val merchantInfo = JSONObject().apply {
        if (!builder.merchantName.isNullOrEmpty()) put("merchantName", builder.merchantName)
    }

    class Builder(
        val activity: Activity,
        environment: Int,
        countryCode: String,
        currencyCode: String,
        gatewayMerchantId: String
    ) {

        internal val paymentsClient: PaymentsClient
        internal var countryCode: String
        internal var currencyCode: String
        internal var gatewayMerchantId: String
        internal var gateway: String = "trustpayments"

        internal var apiVersion: Int = 2
        internal var apiVersionMinor: Int = 0

        internal var billingAddressRequired: Boolean = false
        internal var format: String = "FULL"
        internal var billingPhoneNumberRequired: Boolean = false

        internal var shippingAddressRequired: Boolean = false
        internal var allowedCountryCodes: List<String> = emptyList()
        internal var shippingPhoneNumberRequired: Boolean = false

        internal var requestCode: Int? = null

        internal var allowedCardAuthMethods = listOf(
            "PAN_ONLY", "CRYPTOGRAM_3DS"
        )

        internal var allowedCardNetworks = listOf(
            "AMEX", "DISCOVER", "JCB", "MASTERCARD", "VISA"
        )

        internal var merchantName: String? = null

        init {
            val walletOptions = Wallet.WalletOptions.Builder().setEnvironment(environment).build()
            paymentsClient = Wallet.getPaymentsClient(activity, walletOptions)
            this.countryCode = countryCode
            this.currencyCode = currencyCode
            this.gatewayMerchantId = gatewayMerchantId
        }

        fun setApiVersion(apiVersion: Int): Builder {
            this.apiVersion = apiVersion
            return this
        }

        fun setApiVersionMinor(apiVersionMinor: Int): Builder {
            this.apiVersionMinor = apiVersionMinor
            return this
        }

        fun setAllowedCardAuthMethods(allowedCardAuthMethods: List<String>): Builder {
            this.allowedCardAuthMethods = allowedCardAuthMethods
            return this
        }

        fun setAllowedCardNetworks(supportedNetworks: List<String>): Builder {
            this.allowedCardNetworks = supportedNetworks
            return this
        }

        fun setGateway(gateway: String): Builder {
            this.gateway = gateway
            return this
        }

        fun setBillingAddressRequired(
            billingAddressRequired: Boolean, format: String, phoneNumberRequired: Boolean = false
        ): Builder {
            this.billingAddressRequired = billingAddressRequired
            this.format = format
            this.billingPhoneNumberRequired = phoneNumberRequired
            return this
        }

        fun setShippingAddressRequired(
            shippingAddressRequired: Boolean,
            allowedCountryCodes: List<String>,
            phoneNumberRequired: Boolean = false
        ): Builder {
            this.shippingAddressRequired = shippingAddressRequired
            this.allowedCountryCodes = allowedCountryCodes
            this.shippingPhoneNumberRequired = phoneNumberRequired
            return this
        }

        fun setRequestCode(requestCode: Int): Builder {
            this.requestCode = requestCode
            return this
        }

        fun setMerchantName(merchantName: String): Builder {
            this.merchantName = merchantName
            return this
        }

        fun build(): TPGooglePayManager {
            return TPGooglePayManager(this)
        }

    }

    private fun gatewayTokenizationSpecification(): JSONObject {
        return JSONObject().apply {
            put("type", "PAYMENT_GATEWAY")
            put(
                "parameters", JSONObject(
                    mapOf(
                        "gateway" to gateway, "gatewayMerchantId" to gatewayMerchantId
                    )
                )
            )
        }
    }

    private fun baseCardPaymentMethod(): JSONObject {
        return JSONObject().apply {

            val parameters = JSONObject().apply {
                put("allowedAuthMethods", allowedCardAuthMethods)
                put("allowedCardNetworks", allowedCardNetworks)
                put("billingAddressRequired", billingAddressRequired)
                put("billingAddressParameters", billingAddressParameters)
            }

            put("type", "CARD")
            put("parameters", parameters)
        }
    }

    private fun cardPaymentMethod(): JSONObject {
        val cardPaymentMethod = baseCardPaymentMethod()
        cardPaymentMethod.put("tokenizationSpecification", gatewayTokenizationSpecification())

        return cardPaymentMethod
    }

    @Nullable
    private fun isReadyToPayRequest(): JSONObject? {
        return try {
            baseRequest.apply {
                put("allowedPaymentMethods", JSONArray().put(baseCardPaymentMethod()))
            }

        } catch (e: JSONException) {
            null
        }
    }

    fun possiblyShowGooglePayButton(showGooglePayButtonCallback: ShowGooglePayButtonCallback?) {
        val isReadyToPayJson = isReadyToPayRequest()
        if (isReadyToPayJson == null) {
            showGooglePayButtonCallback?.canShowButton(false)
            return
        }

        val request = IsReadyToPayRequest.fromJson(isReadyToPayJson.toString())

        val task = paymentsClient.isReadyToPay(request)
        task.addOnCompleteListener { completedTask ->
            try {
                completedTask.getResult(ApiException::class.java)?.let {
                    showGooglePayButtonCallback?.canShowButton(it)
                }
            } catch (exception: ApiException) {
                showGooglePayButtonCallback?.canShowButton(false)
            }
        }
    }

    private fun getTransactionInfo(price: String): JSONObject {
        return JSONObject().apply {
            put("totalPrice", price)
            put("totalPriceStatus", "FINAL")
            put("countryCode", countryCode)
            put("currencyCode", currencyCode)
        }
    }

    @Nullable
    private fun getPaymentDataRequest(price: Long): JSONObject? {
        return try {
            baseRequest.apply {
                put("allowedPaymentMethods", JSONArray().put(cardPaymentMethod()))
                put("transactionInfo", getTransactionInfo(price.centsToString()))
                put("merchantInfo", merchantInfo)

                put("shippingAddressParameters", shippingAddressParameters)
                put("shippingAddressRequired", shippingAddressRequired)
            }
        } catch (e: JSONException) {
            null
        }
    }

    fun requestPayment(priceCents: Long) {

        val paymentDataRequestJson = getPaymentDataRequest(priceCents)
        if (paymentDataRequestJson == null) {
            log("RequestPayment", "Can't fetch payment data request")
            return
        }
        val request = PaymentDataRequest.fromJson(paymentDataRequestJson.toString())

        AutoResolveHelper.resolveTask(
            paymentsClient.loadPaymentData(request), activity, requestCode
        )
    }

    fun onActivityResult(
        requestCode: Int, resultCode: Int, data: Intent?
    ): String? {
        if (requestCode == this.requestCode) {
            when (resultCode) {
                AppCompatActivity.RESULT_OK -> data?.let { intent ->
                    PaymentData.getFromIntent(intent)?.let {
                        return handlePaymentSuccess(it)
                    }
                }

                AppCompatActivity.RESULT_CANCELED -> return null

                AutoResolveHelper.RESULT_ERROR -> {
                    AutoResolveHelper.getStatusFromIntent(data)?.let {
                        return it.statusMessage
                    }
                }
            }
        }
        return null
    }

    @Nullable
    private fun handlePaymentSuccess(paymentData: PaymentData): String? {
        return try {
            paymentData.toJson()
        } catch (e: JSONException) {
            log("handlePaymentSuccess", "Error: $e")
            null
        }
    }

    companion object {
        const val REQUEST_CODE = 8686
        private val CENTS = BigDecimal(100)
        fun Long.centsToString() = BigDecimal(this)
            .divide(CENTS)
            .setScale(2, RoundingMode.HALF_EVEN)
            .toString()
    }

    interface ShowGooglePayButtonCallback {
        fun canShowButton(boolean: Boolean)
    }
}