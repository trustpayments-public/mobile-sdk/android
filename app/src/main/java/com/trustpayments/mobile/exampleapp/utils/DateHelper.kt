package com.trustpayments.mobile.exampleapp.utils

import java.text.SimpleDateFormat
import java.util.*

object DateHelper {

    @JvmStatic
    fun getValidCardExpiryDate(): String {
        val formatter = SimpleDateFormat("MM/yyyy", Locale.getDefault())
        val date = Calendar.getInstance()
        date.add(Calendar.MONTH, 1)
        return formatter.format(date.time)
    }
}