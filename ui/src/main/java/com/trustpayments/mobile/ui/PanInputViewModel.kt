package com.trustpayments.mobile.ui

import com.trustpayments.mobile.ui.card.CardInfo
import com.trustpayments.mobile.ui.card.CardValidator
import com.trustpayments.mobile.ui.card.model.CardType


internal class PanInputViewModel(
    private val cardValidator: CardValidator,
    private val panInputCallbacks: PanInputCallbacks) {

    @JvmSynthetic
    fun formatCardNumber(input: String): String {
        // get card type and notify view about possible icon and max input length change
        val cardType = cardValidator.getCardType(input)
        panInputCallbacks.onCardTypeDetected(cardType)
        panInputCallbacks.onIconChanged(CardInfo.getIcon(cardType))
        // Diners cards required a new length for number longer than 14 digits
        if (isNewLengthForDinersRequired(input, cardType)) {
            panInputCallbacks.onPanLengthChanged(CardInfo.DEFAULT_INPUT_MASK.length)
        } else {
            panInputCallbacks.onPanLengthChanged(CardInfo.getMaxMaskLength(cardType))
        }

        // calculate actual mask for the given card type (based on the input)
        val mask =
            // Diners cards required a new mask for number longer than 14 digits
            if (isNewMaskForDinersRequired(input, cardType)) {
                CardInfo.DEFAULT_INPUT_MASK
            } else {
                CardInfo.getInputMask(cardType)
            }

        // perform masking
        var i = 0
        val out = java.lang.StringBuilder()
        for (m in mask.toCharArray()) {
            if (m != '#' && i < input.length) {
                out.append(m)
                continue
            }

            try {
                out.append(input[i])
            } catch (e: Exception) {
                break
            }

            i++
        }

        validateForMaxNumberLength(input, cardType)

        return out.toString()
    }

    @JvmSynthetic
    fun validateCardNumber(input: String) {
        if (cardValidator.isNumberLengthValid(input, cardValidator.getCardType(input)) &&
            cardValidator.checkWithLuhn(input)
        ) {
            panInputCallbacks.onPanValid(input)
        } else {
            panInputCallbacks.onPanInvalid(true)
        }
    }

    /**
     * Checks if the provided number length is in the collection of lengths for the given card type
     * and performs validation followed by a proper callback triggering
     */
    private fun validateForMaxNumberLength(input: String, cardType: CardType) {
        if (cardValidator.isNumberLengthValid(input, cardType)) {
            if (cardValidator.checkWithLuhn(input)) {
                panInputCallbacks.onPanValid(input)
            } else {
                panInputCallbacks.onPanInvalid(true)
            }
        } else {
            panInputCallbacks.onPanInvalid(false)
        }
    }

    private fun isNewLengthForDinersRequired(input: String, cardType: CardType): Boolean =
        cardType == CardType.Diners && input.length >= CardInfo.getMinNumberLength(cardType)

    private fun isNewMaskForDinersRequired(input: String, cardType: CardType): Boolean =
        cardType == CardType.Diners && input.length > CardInfo.getMinNumberLength(cardType)


    /**
     * VM callbacks used by [PanInputViewn]
     */
    interface PanInputCallbacks {
        /**
         * Called on every change inside the pan input and sending back the info about
         * detected card type
         *
         * @param cardType the card type detected for the provided input
         */
        fun onCardTypeDetected(cardType: CardType)

        /**
         * Called when a provided card number was correctly validated
         *
         * @param input the the valid input provided in the card number component
         */
        fun onPanValid(input: String)

        /**
         * Called when a provided card number was invalid - number had incorrect length or
         * was non-compliant with the Luhn algorithm
         *
         * @param showError indicates if an error should be displayed in case of invalid input data -
         * there is no need to always show it, e.g when a card number is to short and a user is still typing
         */
        fun onPanInvalid(showError: Boolean)

        fun onIconChanged(iconRes: Int)
        fun onPanLengthChanged(maxLength: Int)
    }
}