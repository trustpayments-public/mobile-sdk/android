package com.trustpayments.mobile.ui.dropin

import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.trustpayments.mobile.ui.R
import com.trustpayments.mobile.ui.card.model.CardType
import com.trustpayments.mobile.ui.model.PaymentInputType
import com.trustpayments.mobile.ui.testutils.TokenizationDropInPaymentViewTestActivity
import org.junit.Assert
import org.junit.Rule
import org.junit.Test

class TokenizedDropInPaymentViewTestCases {

    @get:Rule
    val activityScenarioRule =
        ActivityScenarioRule(TokenizationDropInPaymentViewTestActivity::class.java)

    @Test
    fun test_allInputsHidden() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView = activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            dropInPaymentView.setupForTokenizedPayment(emptySet(), null)

            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.panInput)?.visibility
            )
            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.expireInput)?.visibility
            )
            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.cvvInput)?.visibility
            )
            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.googlePayButton)?.visibility
            )
        }
    }

    @Test
    fun test_panVisible() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView = activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            dropInPaymentView.setupForTokenizedPayment(setOf(PaymentInputType.PAN), null)

            Assert.assertEquals(
                View.VISIBLE, dropInPaymentView
                    .findViewById<View>(R.id.panInput)?.visibility
            )
            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.expireInput)?.visibility
            )
            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.cvvInput)?.visibility
            )
            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.googlePayButton)?.visibility
            )
        }
    }

    @Test
    fun test_expiryDateVisible() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView = activity
                .findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            dropInPaymentView
                .setupForTokenizedPayment(setOf(PaymentInputType.ExpiryDate), null)

            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.panInput)?.visibility
            )
            Assert.assertEquals(
                View.VISIBLE, dropInPaymentView
                    .findViewById<View>(R.id.expireInput)?.visibility
            )
            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.cvvInput)?.visibility
            )
            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.googlePayButton)?.visibility
            )
        }
    }

    @Test
    fun test_negative_cvvVisible_cardTypeNotProvided() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView = activity
                .findViewById<DropInPaymentView>(R.id.dropInPaymentView)

            try {
                dropInPaymentView
                    .setupForTokenizedPayment(setOf(PaymentInputType.CVV), null)
            } catch (ex: Exception) {
                Assert.assertTrue(ex is IllegalArgumentException)
            }
        }
    }

    @Test
    fun test_positive_cvvVisible_cardTypeProvided() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView = activity
                .findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            dropInPaymentView
                .setupForTokenizedPayment(setOf(PaymentInputType.CVV), CardType.Visa)

            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.panInput)?.visibility
            )
            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.expireInput)?.visibility
            )
            Assert.assertEquals(
                View.VISIBLE, dropInPaymentView
                    .findViewById<View>(R.id.cvvInput)?.visibility
            )
            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.googlePayButton)?.visibility
            )
        }
    }

    @Test
    fun test_googlePayVisible() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView = activity
                .findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            dropInPaymentView.setupForTokenizedPayment(emptySet(), null)
            dropInPaymentView.setupForGooglePayPayment()

            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.panInput)?.visibility
            )
            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.expireInput)?.visibility
            )
            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.cvvInput)?.visibility
            )
            Assert.assertEquals(
                View.VISIBLE, dropInPaymentView
                    .findViewById<View>(R.id.googlePayButton)?.visibility
            )
        }
    }

    @Test
    fun test_negative_panHidden_cardTypeNotProvided() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView = activity
                .findViewById<DropInPaymentView>(R.id.dropInPaymentView)

            try {
                dropInPaymentView.setupForTokenizedPayment(
                    setOf(
                        PaymentInputType.ExpiryDate,
                        PaymentInputType.CVV
                    ),
                    null
                )
            } catch (ex: Exception) {
                Assert.assertTrue(ex is IllegalArgumentException)
            }
        }
    }

    @Test
    fun test_positive_panHidden_cardTypeProvided() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView = activity
                .findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            dropInPaymentView.setupForTokenizedPayment(
                setOf(
                    PaymentInputType.ExpiryDate,
                    PaymentInputType.CVV
                ), CardType.Amex
            )

            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.panInput)?.visibility
            )
            Assert.assertEquals(
                View.VISIBLE, dropInPaymentView
                    .findViewById<View>(R.id.expireInput)?.visibility
            )
            Assert.assertEquals(
                View.VISIBLE, dropInPaymentView
                    .findViewById<View>(R.id.cvvInput)?.visibility
            )
            Assert.assertEquals(
                View.GONE,
                dropInPaymentView.findViewById<View>(R.id.googlePayButton)?.visibility
            )
        }
    }

    @Test
    fun test_expiryDateHidden() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView = activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            dropInPaymentView.setupForTokenizedPayment(
                setOf(
                    PaymentInputType.PAN,
                    PaymentInputType.CVV
                ), CardType.Amex
            )

            Assert.assertEquals(
                View.VISIBLE, dropInPaymentView
                    .findViewById<View>(R.id.panInput)?.visibility
            )
            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.expireInput)?.visibility
            )
            Assert.assertEquals(
                View.VISIBLE, dropInPaymentView
                    .findViewById<View>(R.id.cvvInput)?.visibility
            )
            Assert.assertEquals(
                View.GONE,
                dropInPaymentView.findViewById<View>(R.id.googlePayButton)?.visibility
            )
        }
    }

    @Test
    fun test_cvvHidden() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView = activity
                .findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            dropInPaymentView.setupForTokenizedPayment(
                setOf(
                    PaymentInputType.PAN,
                    PaymentInputType.ExpiryDate
                ), CardType.Amex
            )

            Assert.assertEquals(
                View.VISIBLE, dropInPaymentView
                    .findViewById<View>(R.id.panInput)?.visibility
            )
            Assert.assertEquals(
                View.VISIBLE, dropInPaymentView
                    .findViewById<View>(R.id.expireInput)?.visibility
            )
            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.cvvInput)?.visibility
            )
            Assert.assertEquals(
                View.GONE,
                dropInPaymentView.findViewById<View>(R.id.googlePayButton)?.visibility
            )
        }
    }

    @Test
    fun test_allVisible() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView = activity
                .findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            dropInPaymentView.setupForTokenizedPayment(
                setOf(
                    PaymentInputType.PAN,
                    PaymentInputType.ExpiryDate,
                    PaymentInputType.CVV
                ), null
            )

            dropInPaymentView.setupForGooglePayPayment()

            Assert.assertEquals(
                View.VISIBLE, dropInPaymentView
                    .findViewById<View>(R.id.panInput)?.visibility
            )
            Assert.assertEquals(
                View.VISIBLE, dropInPaymentView
                    .findViewById<View>(R.id.expireInput)?.visibility
            )
            Assert.assertEquals(
                View.VISIBLE, dropInPaymentView
                    .findViewById<View>(R.id.cvvInput)?.visibility
            )
            Assert.assertEquals(
                View.VISIBLE,
                dropInPaymentView.findViewById<View>(R.id.googlePayButton)?.visibility
            )
        }
    }

    @Test
    fun test_payHidden() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView = activity
                .findViewById<DropInPaymentView>(R.id.dropInPaymentView)

            Assert.assertEquals(
                View.VISIBLE, dropInPaymentView
                    .findViewById<View>(R.id.payButton)?.visibility
            )

            dropInPaymentView.setPayButtonVisibility(View.GONE)

            Assert.assertEquals(
                View.GONE, dropInPaymentView
                    .findViewById<View>(R.id.payButton)?.visibility
            )
        }
    }
}