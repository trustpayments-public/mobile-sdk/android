package com.trustpayments.mobile.core.testcases.eu

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.rule.ActivityTestRule
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.models.api.response.ResponseErrorCode
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.testutils.createSessionFor
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.util.OldTransactionResponse
import com.trustpayments.mobile.core.util.PaymentSessionResponse
import com.trustpayments.mobile.core.util.parse
import com.trustpayments.mobile.core.utils.ThreeDQueryTestActivity
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MultipleRequestTypesTestCases: BaseTestCase() {

    private lateinit var paymentTransactionManager: PaymentTransactionManager

    private lateinit var activity: ThreeDQueryTestActivity

    @get:Rule
    val initialActivityTestRule = ActivityTestRule(ThreeDQueryTestActivity::class.java)

    private val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    @Before
    override fun setUp() {
        super.setUp()

        val app = ApplicationProvider.getApplicationContext<Context>().applicationContext
        paymentTransactionManager =
            PaymentTransactionManager(app, getGatewayType(), false,
                BuildConfig.MERCHANT_USERNAME, isLocationDataConsentGiven = false
            )

        activity = initialActivityTestRule.activity
    }


    // .auth]
    @Test
    fun test_positive_auth() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getFor3DSecure(listOf(RequestType.Auth))
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "4111111111111111", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session).parse()
        val result = sessionResult as PaymentSessionResponse.Success

        // THEN
        Assert.assertTrue(result.allResponses.size == 1)
    }

    // .accountcheck]
    @Test
    fun test_positive_accountcheck() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getFor3DSecure(listOf(RequestType.AccountCheck))
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "4111111111111111", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session).parse()
        val result = sessionResult as PaymentSessionResponse.Success

        // THEN
        Assert.assertTrue(result.allResponses.size == 1)
    }

    // .accountcheck, .auth]
    @Test
    fun test_positive_accountcheck_auth() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForSubscription(listOf(
            RequestType.AccountCheck,
            RequestType.Auth)
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "4111111111111111", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session).parse()
        val result = sessionResult as PaymentSessionResponse.Success

        // THEN
        Assert.assertTrue(result.allResponses.size == 2)
    }

    // .accountCheck, .subscription]"subscriptiontype":"RECURRING","subscriptionunit":"MONTH","subscriptionfrequency":"1","subscriptionnumber":"1","subscriptionfinalnumber":"12",
    @Test
    fun test_positive_accountcheck_subscription() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForSubscription(listOf(
            RequestType.AccountCheck,
            RequestType.Subscription)
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val cardinalResult = sessionResult as PaymentSessionResponse.Success

        // THEN
        Assert.assertTrue(cardinalResult.allResponses.size == 2)
    }

    // .auth, .subscription]"subscriptiontype":"RECURRING","subscriptionunit":"MONTH","subscriptionfrequency":"1","subscriptionnumber":"1","subscriptionfinalnumber":"12",
    @Test
    fun test_positive_auth_subscription() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForSubscription(listOf(
            RequestType.Auth,
            RequestType.Subscription)
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val cardinalResult = sessionResult as PaymentSessionResponse.Success

        // THEN
        Assert.assertTrue(cardinalResult.allResponses.size == 2)
    }

    // .accountCheck, .auth, .subscription]"subscriptiontype":"RECURRING","subscriptionunit":"MONTH","subscriptionfrequency":"1","subscriptionnumber":"1","subscriptionfinalnumber":"12",
    @Test
    fun test_positive_accountcheck_auth_subscription() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForSubscription(listOf(
            RequestType.AccountCheck,
            RequestType.Auth,
            RequestType.Subscription)
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val cardinalResult = sessionResult as PaymentSessionResponse.Success

        // THEN
        Assert.assertTrue(cardinalResult.allResponses.size == 3)
    }

    // .threeDQuery]
    @Test
    fun test_positive_threedquery() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getFor3DSecure(listOf(RequestType.ThreeDQuery))
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val cardinalResult = sessionResult as PaymentSessionResponse.Success

        // THEN
        Assert.assertTrue(cardinalResult.allResponses.size == 1)
    }

    // .threeDQuery, .auth]
    @Test
    fun test_positive_threedquery_auth() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getFor3DSecure(listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        ))
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val cardinalResult = sessionResult as PaymentSessionResponse.Success

        // THEN
        Assert.assertTrue(cardinalResult.allResponses.size == 2)
    }

    // .threeDQuery, .accountcheck]
    @Test
    fun test_positive_threedquery_accountcheck() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getFor3DSecure(listOf(
            RequestType.ThreeDQuery,
            RequestType.AccountCheck
        ))
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val cardinalResult = sessionResult as PaymentSessionResponse.Success

        // THEN
        Assert.assertTrue(cardinalResult.allResponses.size == 2)
    }

    // .threeDQuery, .accountCheck, .subscription]"subscriptiontype":"RECURRING","subscriptionunit":"MONTH","subscriptionfrequency":"1","subscriptionnumber":"1","subscriptionfinalnumber":"12",
    @Test
    fun test_positive_threedquery_accountcheck_subscription() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForSubscription(listOf(
            RequestType.ThreeDQuery,
            RequestType.AccountCheck,
            RequestType.Subscription
        ))
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val cardinalResult = sessionResult as PaymentSessionResponse.Success

        // THEN
        Assert.assertTrue(cardinalResult.allResponses.size == 3)
    }

    // .threeDQuery, .auth, .subscription]"subscriptiontype":"RECURRING","subscriptionunit":"MONTH","subscriptionfrequency":"1","subscriptionnumber":"1","subscriptionfinalnumber":"12",
    @Test
    fun test_positive_threedquery_auth_subscription() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForSubscription(listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth,
            RequestType.Subscription)
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val cardinalResult = sessionResult as PaymentSessionResponse.Success

        // THEN
        Assert.assertTrue(cardinalResult.allResponses.size == 3)
    }

    // .accountCheck, .threeDQuery, .auth]
    @Test
    fun test_positive_accountcheck_threedquery_auth() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getFor3DSecure(listOf(
            RequestType.AccountCheck,
            RequestType.ThreeDQuery,
            RequestType.Auth
        ))
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val cardinalResult = sessionResult as PaymentSessionResponse.Success

        // THEN
        Assert.assertTrue(cardinalResult.allResponses.size == 3)
    }

    // .accounCheck, .threeDQuery, .auth, .subscription]"subscriptiontype":"RECURRING","subscriptionunit":"MONTH","subscriptionfrequency":"1","subscriptionnumber":"1","subscriptionfinalnumber":"12",
    @Test
    fun test_positive_accountcheck_threedquery_auth_subscription() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForSubscription(listOf(
            RequestType.AccountCheck,
            RequestType.ThreeDQuery,
            RequestType.Auth,
            RequestType.Subscription
        ))

        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val cardinalResult = sessionResult as PaymentSessionResponse.Success

        // THEN
        Assert.assertTrue(cardinalResult.allResponses.size == 4)
    }

    // .accountCheck, .threeDQuery]
    @Test
    fun test_positive_accountcheck_threedquery() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getFor3DSecure(listOf(
            RequestType.AccountCheck,
            RequestType.ThreeDQuery
        ))

        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val cardinalResult = sessionResult as PaymentSessionResponse.Success

        // THEN
        Assert.assertTrue(cardinalResult.allResponses.size == 2)
    }

    @Test
    fun test_negative_auth_threedquery() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getFor3DSecure(listOf(
            RequestType.Auth,
            RequestType.ThreeDQuery
        ))

        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "4000000000001026", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
            as PaymentSessionResponse.Failure.TransactionFailure

        // THEN
        Assert.assertEquals(1, sessionResult.allResponses.size)

        val error = sessionResult.allResponses[0] as OldTransactionResponse.Error.TransactionError
        Assert.assertEquals(ResponseErrorCode.InvalidField, error.errorCode)
    }
}