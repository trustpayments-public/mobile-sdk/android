#!/usr/bin/env bash
source .cicd_scripts/other/utils.sh

execute_browserstack_tests_in_parallel() {
    set -u -o pipefail -E

    local tests_path=$1
    local tests_package=$2
    local branch_type=$3

    log_this INFO "Checking if all files are named correctly..."

    ENABLED_TESTS=($(find ${tests_path}${tests_package} -type f | { grep -E 'TestCases.(kt|java)$' || test $? = 1; } | sed s~${tests_path}~~g | sed s~/~.~g | sort))
    len_ENABLED_TESTS=${#ENABLED_TESTS[@]}

    log_this INFO "Enabled tests found: ${len_ENABLED_TESTS}"

    DISABLED_TESTS_BOTH=($(find ${tests_path}${tests_package} -type f | { grep -E 'Ignored.(kt|java)$' || test $? = 1; } | sed s~${tests_path}~~g | sed s~/~.~g | sort))
    len_DISABLED_TESTS_BOTH=${#DISABLED_TESTS_BOTH[@]}

    log_this INFO "Completely disabled tests found: ${len_DISABLED_TESTS_BOTH}"

    DISABLED_TESTS_FEATURE=($(find ${tests_path}${tests_package} -type f | { grep -E 'IgnoredFeature.(kt|java)$' || test $? = 1; } | sed s~${tests_path}~~g | sed s~/~.~g | sort))
    len_DISABLED_TESTS_FEATURE=${#DISABLED_TESTS_FEATURE[@]}

    log_this INFO "Feature branch disabled tests found: ${len_DISABLED_TESTS_FEATURE}"

    len_ENABLED_DISABLED_TESTS=$((len_ENABLED_TESTS + len_DISABLED_TESTS_BOTH + len_DISABLED_TESTS_FEATURE))

    ALL_FILES=($(find ${tests_path}${tests_package} -type f | sed s~${tests_path}~~g | sed s~/~.~g | sort))
    len_ALL_FILES=${#ALL_FILES[@]}

    if [ $len_ALL_FILES -ne $len_ENABLED_DISABLED_TESTS ]; then
      log_this ERROR "Not all files found in given package have correct names. Please make sure all enabled test files end with <TestCases> and all disabled test files end with <Ignored> or <IgnoredFeature>"
      exit 1
    fi

    log_this INFO "Running Browserstack parallel tests: ${tests_path}${tests_package}. Actual node: ${CI_NODE_INDEX}/${CI_NODE_TOTAL}"

    if [ "$branch_type" = "master" ]; then
      ENABLED_TESTS+=(${DISABLED_TESTS_FEATURE[@]})
      len_ENABLED_TESTS=${#ENABLED_TESTS[@]}
    fi

    if [ $CI_NODE_TOTAL -ne $len_ENABLED_TESTS ]; then
      log_this ERROR "Number of nodes must be equal to the number of files that should be run in parallel. Nodes: ${CI_NODE_TOTAL}. Feature files: ${len_ENABLED_TESTS}"
      exit 1
    fi

    index=$(($CI_NODE_INDEX-1))
    TEST=${ENABLED_TESTS[$index]%.java}
    TEST=${ENABLED_TESTS%.kt}

    log_this INFO "Executing test class: ${TEST}"

    ${PY_VERSION} scripts/tests/browserstack_espresso_build.py \
        --browserstack-device "${BROWERSTACK_DEVICE}" \
        --browserstack-user "${BROWSERSTACK_USERNAME}" \
        --browserstack-access-key "${BROWSERSTACK_ACCESS_KEY}" \
        --app-file "${ARTIFACTS_APP}" \
        --tests-file "${ARTIFACTS_TESTS}" \
        --google-username "${GOOGLE_USERNAME}" \
        --google-password "${GOOGLE_PASSWORD}" \
        --tests-class "${TEST}"

    log_this INFO "Execution finished"
}
