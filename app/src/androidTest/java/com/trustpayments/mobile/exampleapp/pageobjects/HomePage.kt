package com.trustpayments.mobile.exampleapp.pageobjects

import android.content.Context
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.trustpayments.mobile.exampleapp.R
import androidx.test.espresso.intent.Intents
import androidx.test.uiautomator.UiObject
import androidx.test.uiautomator.UiSelector
import com.trustpayments.mobile.exampleapp.samples.tokenizedPayment.TokenizedPaymentActivity

class HomePage : BaseAppPage() {


    //region Elements
    private val okButtonOnPopUpMessage = withText("OK")
    private val recyclerView = withId(R.id.samples_list)
    private lateinit var context: Context
    private val okButtonOnPopUpMessageUiDevice = Button::class.java
    fun tapPayByCardPayment() {
        Intents.init()
        performPrivacyPolicyAcceptance()
        onView(recyclerView)
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))

    }

    fun tapAdvancedFeatures() {
        Intents.init()
        performPrivacyPolicyAcceptance()
        onView(recyclerView)
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(7, click()))

    }

    fun tapRecurringInstallments() {
        Intents.init()
        performPrivacyPolicyAcceptance()
        onView(recyclerView)
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(3, click()))

    }

    fun tapGooglePay() {
        Intents.init()
        performPrivacyPolicyAcceptance()
        onView(recyclerView)
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(5, click()))
}
}