package com.trustpayments.mobile.core.testcases.eu

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.rule.ActivityTestRule
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.R
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.models.api.response.ResponseErrorCode
import com.trustpayments.mobile.core.services.transaction.Error
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.testutils.createSessionFor
import com.trustpayments.mobile.core.testutils.getAuthTransactionResponse
import com.trustpayments.mobile.core.testutils.getThreeDQueryTransactionResponse
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.utils.ThreeDQueryTestActivity
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class RetryTestCases: BaseTestCase() {

    private lateinit var paymentTransactionManager: PaymentTransactionManager

    private lateinit var activity: ThreeDQueryTestActivity

    @get:Rule
    val initialActivityTestRule = ActivityTestRule(ThreeDQueryTestActivity::class.java)

    private val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    @Before
    override fun setUp() {
        super.setUp()

        val app = ApplicationProvider.getApplicationContext<Context>().applicationContext
        paymentTransactionManager =
            PaymentTransactionManager(app, getGatewayType(), false,
                BuildConfig.MERCHANT_USERNAME, isLocationDataConsentGiven = false
            )

        activity = initialActivityTestRule.activity
    }


    // .threeDQuery, .auth
    @Test
    fun test_negative_threedquery_auth_failedThreedquery() = runBlocking {
        //step 1 - original request

        // GIVEN
        val jwtToken = jwtBuilder.getFor3DSecure(listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        ), 14492)
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "4000000000001091", "123")

        // WHEN
        lateinit var sessionResult: PaymentTransactionManager.Response
        var job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(
            R.id.codeEditTextField, R.id.submitAuthenticationButton,
            CardsDataWith3DSecureV2.threeDSecureCode
        )

        job.join()

        var actualThreeDQueryTransactionResponse = sessionResult.getThreeDQueryTransactionResponse()
        var actualAuthTransactionResponse = sessionResult.getAuthTransactionResponse()

        // THEN
        Assert.assertEquals(
            actualThreeDQueryTransactionResponse.errorCode,
            ResponseErrorCode.Ok
        )
        Assert.assertNotEquals(
            actualAuthTransactionResponse.errorCode,
            ResponseErrorCode.Ok
        )
        Assert.assertTrue(session.intermediateJwt.isEmpty())

        //step 2 - retried request

        // WHEN
        job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(
            R.id.codeEditTextField, R.id.submitAuthenticationButton,
            CardsDataWith3DSecureV2.threeDSecureCode
        )

        job.join()

        actualThreeDQueryTransactionResponse = sessionResult.getThreeDQueryTransactionResponse()
        actualAuthTransactionResponse = sessionResult.getAuthTransactionResponse()

        // THEN
        Assert.assertEquals(
            actualThreeDQueryTransactionResponse.errorCode,
            ResponseErrorCode.Ok
        )
        Assert.assertNotEquals(
            actualAuthTransactionResponse.errorCode,
            ResponseErrorCode.Ok
        )
        Assert.assertTrue(session.intermediateJwt.isEmpty())
    }

    // .threeDQuery, .auth
    @Test
    fun test_negative_threedquery_auth_failedCardinalProcessing() = runBlocking {
        //step 1 - original request

        // GIVEN
        val jwtToken = jwtBuilder.getFor3DSecure(listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        ), 14492)
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001104", "123")

        // WHEN
        lateinit var sessionResult: PaymentTransactionManager.Response
        var job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(
            R.id.codeEditTextField, R.id.submitAuthenticationButton,
            CardsDataWith3DSecureV2.threeDSecureCode
        )

        job.join()

        var actualThreeDQueryTransactionResponse = sessionResult.getThreeDQueryTransactionResponse()

        // THEN
        Assert.assertEquals(
            actualThreeDQueryTransactionResponse.errorCode,
            ResponseErrorCode.Ok
        )
        Assert.assertTrue(
            sessionResult.error is Error.ThreeDSFailure
        )
        Assert.assertTrue(session.intermediateJwt.isEmpty())

        //step 2 - retried request

        // WHEN
        job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(
            R.id.codeEditTextField, R.id.submitAuthenticationButton,
            CardsDataWith3DSecureV2.threeDSecureCode
        )

        job.join()

        actualThreeDQueryTransactionResponse = sessionResult.getThreeDQueryTransactionResponse()

        // THEN
        Assert.assertEquals(
            actualThreeDQueryTransactionResponse.errorCode,
            ResponseErrorCode.Ok
        )
        Assert.assertTrue(
            sessionResult.error is Error.ThreeDSFailure
        )
        Assert.assertTrue(session.intermediateJwt.isEmpty())
    }
}