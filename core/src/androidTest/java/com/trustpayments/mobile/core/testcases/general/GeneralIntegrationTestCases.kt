package com.trustpayments.mobile.core.testcases.general

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.testutils.createSessionFor
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.util.PaymentSessionResponse
import com.trustpayments.mobile.core.util.parse
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GeneralIntegrationTestCases: BaseTestCase() {

    private lateinit var paymentTransactionManager: PaymentTransactionManager
    
    private val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    @Before
    override fun setUp() {
        super.setUp()

        val app = ApplicationProvider.getApplicationContext<Context>().applicationContext
        paymentTransactionManager =
            PaymentTransactionManager(app, getGatewayType(), false, BuildConfig.MERCHANT_USERNAME, isLocationDataConsentGiven = false)
    }

    //region General: AUTH
    @Test
    fun test_positive_auth() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getStandard(listOf(RequestType.Auth))
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "4111111111111111", "123")

        // WHEN
        val sessionResult = paymentTransactionManager.executeSession(session).parse()

        // THEN
        Assert.assertTrue(sessionResult is PaymentSessionResponse.Success)
    }

    @Test
    fun test_positive_nonDeferInitAuth() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getStandard(listOf(RequestType.Auth))
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "4111111111111111", "123")

        // WHEN
        val sessionResult = paymentTransactionManager.executeSession(session).parse()

        // THEN
        Assert.assertTrue(sessionResult is PaymentSessionResponse.Success)
    }

    @Test
    fun test_negative_auth() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getStandard(listOf(RequestType.Auth))
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "411111111111", "123")

        // WHEN
        val sessionResult = paymentTransactionManager.executeSession(session).parse()

        // THEN
        Assert.assertTrue(sessionResult is PaymentSessionResponse.Failure.TransactionFailure)
    }
    //endregion

    //region General: ACCOUNTCHECK
    @Test
    fun test_positive_accountCheck() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getStandard(listOf(RequestType.AccountCheck))
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "4111111111111111", "123")

        // WHEN
        val sessionResult = paymentTransactionManager.executeSession(session).parse()

        // THEN
        Assert.assertTrue(sessionResult is PaymentSessionResponse.Success)
    }

    @Test
    fun test_negative_accountCheck() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getStandard(listOf(RequestType.AccountCheck))
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "411111111111", "123")

        // WHEN
        val sessionResult = paymentTransactionManager.executeSession(session).parse()

        // THEN
        Assert.assertTrue(sessionResult is PaymentSessionResponse.Failure.TransactionFailure)
    }
    //endregion

    //region General: ACCOUNTCHECK and AUTH
    @Test
    fun test_positive_accountCheckAndAuth() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getStandard(listOf(
            RequestType.AccountCheck,
            RequestType.Auth))

        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "4111111111111111", "123")

        // WHEN
        val sessionResult = paymentTransactionManager.executeSession(session).parse()

        // THEN
        Assert.assertTrue(sessionResult is PaymentSessionResponse.Success)
    }

    @Test
    fun test_negative_accountCheckAndAuth() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getStandard(listOf(
            RequestType.AccountCheck,
            RequestType.Auth))

        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "411111111111", "123")

        // WHEN
        val sessionResult = paymentTransactionManager.executeSession(session).parse()

        // THEN
        Assert.assertTrue(sessionResult is PaymentSessionResponse.Failure.TransactionFailure)
    }
    //endregion

    //region Subscription: ACCOUNTCHECK and SUBSCRIPTION
    @Test
    fun test_positive_subscription_accountCheckAndSubscription() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForSubscription(listOf(
            RequestType.AccountCheck,
            RequestType.Subscription))

        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "4111111111111111", "123")

        // WHEN
        val sessionResult = paymentTransactionManager.executeSession(session).parse()

        // THEN
        Assert.assertTrue(sessionResult is PaymentSessionResponse.Success)
    }

    @Test
    fun test_negative_subscription_accountCheckAndSubscription() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForSubscription(listOf(
            RequestType.AccountCheck,
            RequestType.Subscription))

        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "411111111111", "123")

        // WHEN
        val sessionResult = paymentTransactionManager.executeSession(session).parse()

        // THEN
        Assert.assertTrue(sessionResult is PaymentSessionResponse.Failure.TransactionFailure)
    }
    //endregion

    //region Subscription: AUTH and SUBSCRIPTION
    @Test
    fun test_positive_subscription_authAndSubscription() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForSubscription(listOf(
            RequestType.Auth,
            RequestType.Subscription
        ))
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "4111111111111111", "123")

        // WHEN
        val sessionResult = paymentTransactionManager.executeSession(session).parse()

        // THEN
        Assert.assertTrue(sessionResult is PaymentSessionResponse.Success)
    }

    @Test
    fun test_negative_subscription_authAndSubscription() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForSubscription(listOf(
            RequestType.Auth,
            RequestType.Subscription
        ))
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "411111111111", "123")

        // WHEN
        val sessionResult = paymentTransactionManager.executeSession(session).parse()

        // THEN
        Assert.assertTrue(sessionResult is PaymentSessionResponse.Failure.TransactionFailure)
    }
    //endregion

    //region Recurring payments: ACCOUNTCHECK and AUTH
    @Test
    fun test_positive_recurringPayments_accountCheckAndAuth() = runBlocking {
        // GIVEN
        val initialJwtToken = jwtBuilder.getForInitialRecurringPayment(listOf(RequestType.AccountCheck))
        val initialSession =
            paymentTransactionManager.createSessionFor(initialJwtToken, "4111111111111111", "123")

        // WHEN
        val initialSessionResult = paymentTransactionManager.executeSession(initialSession).parse()

        // THEN
        Assert.assertTrue(initialSessionResult is PaymentSessionResponse.Success)

        // GIVEN
        val nextJwtToken = jwtBuilder.getForNextRecurringPayment(
            initialSessionResult.getTransactionReferenceFromSuccessfulResponse(),
            listOf(RequestType.Auth)
        )
        val nextSession =
            paymentTransactionManager.createSessionFor(nextJwtToken, "4111111111111111", "123")

        // WHEN
        val nextSessionResult = paymentTransactionManager.executeSession(nextSession).parse()

        // THEN
        Assert.assertTrue(nextSessionResult is PaymentSessionResponse.Success)
    }

    @Test
    fun test_negative_recurringPayments_failedInitial_accountCheckAndAuth() = runBlocking {
        // GIVEN
        val initialJwtToken = jwtBuilder.getForInitialRecurringPayment(listOf(RequestType.AccountCheck))
        val initialSession =
            paymentTransactionManager.createSessionFor(initialJwtToken, "411111111111", "123")

        // WHEN
        val initialSessionResult = paymentTransactionManager.executeSession(initialSession).parse()

        // THEN
        Assert.assertTrue(initialSessionResult is PaymentSessionResponse.Failure.TransactionFailure)
    }

    @Test
    fun test_positive_recurringPayments_failedNext_accountCheckAndAuth() = runBlocking {
        // GIVEN
        val initialJwtToken = jwtBuilder.getForInitialRecurringPayment(listOf(RequestType.AccountCheck))
        val initialSession =
            paymentTransactionManager.createSessionFor(initialJwtToken, "4111111111111111", "123")

        // WHEN
        val initialSessionResult = paymentTransactionManager.executeSession(initialSession).parse()

        // THEN
        Assert.assertTrue(initialSessionResult is PaymentSessionResponse.Success)

        // GIVEN
        val nextJwtToken = jwtBuilder.getForNextRecurringPayment(
            initialSessionResult.getTransactionReferenceFromSuccessfulResponse(),
            listOf(RequestType.Auth)
        )
        val nextSession =
            paymentTransactionManager.createSessionFor(nextJwtToken, "411111111111", "123")

        // WHEN
        val nextSessionResult = paymentTransactionManager.executeSession(nextSession).parse()

        // THEN
        Assert.assertTrue(nextSessionResult is PaymentSessionResponse.Failure.TransactionFailure)
    }
    //endregion

    //region Recurring payments: ACCOUNTCHECK and AUTH
    @Test
    fun test_positive_recurringPayments_authAndAuth() = runBlocking {
        // GIVEN
        val initialJwtToken = jwtBuilder.getForInitialRecurringPayment(listOf(RequestType.Auth))
        val initialSession =
            paymentTransactionManager.createSessionFor(initialJwtToken, "4111111111111111", "123")

        // WHEN
        val initialSessionResult = paymentTransactionManager.executeSession(initialSession).parse()

        // THEN
        Assert.assertTrue(initialSessionResult is PaymentSessionResponse.Success)

        // GIVEN
        val nextJwtToken = jwtBuilder.getForNextRecurringPayment(
            initialSessionResult.getTransactionReferenceFromSuccessfulResponse(),
            listOf(RequestType.Auth)
        )
        val nextSession =
            paymentTransactionManager.createSessionFor(nextJwtToken, "4111111111111111", "123")

        // WHEN
        val nextSessionResult = paymentTransactionManager.executeSession(nextSession).parse()

        // THEN
        Assert.assertTrue(nextSessionResult is PaymentSessionResponse.Success)
    }

    @Test
    fun test_negative_recurringPayments_failedInitial_authAndAuth() = runBlocking {
        // GIVEN
        val initialJwtToken = jwtBuilder.getForInitialRecurringPayment(listOf(RequestType.Auth))
        val initialSession =
            paymentTransactionManager.createSessionFor(initialJwtToken, "411111111111", "123")

        // WHEN
        val initialSessionResult = paymentTransactionManager.executeSession(initialSession).parse()

        // THEN
        Assert.assertTrue(initialSessionResult is PaymentSessionResponse.Failure.TransactionFailure)
    }

    @Test
    fun test_positive_recurringPayments_failedNext_authAndAuth() = runBlocking {
        // GIVEN
        val initialJwtToken = jwtBuilder.getForInitialRecurringPayment(listOf(RequestType.Auth))
        val initialSession =
            paymentTransactionManager.createSessionFor(initialJwtToken, "4111111111111111", "123")

        // WHEN
        val initialSessionResult = paymentTransactionManager.executeSession(initialSession).parse()

        // THEN
        Assert.assertTrue(initialSessionResult is PaymentSessionResponse.Success)

        // GIVEN
        val nextJwtToken = jwtBuilder.getForNextRecurringPayment(
            initialSessionResult.getTransactionReferenceFromSuccessfulResponse(),
            listOf(RequestType.Auth)
        )
        val nextSession =
            paymentTransactionManager.createSessionFor(nextJwtToken, "411111111111", "123")

        // WHEN
        val nextSessionResult = paymentTransactionManager.executeSession(nextSession).parse()

        // THEN
        Assert.assertTrue(nextSessionResult is PaymentSessionResponse.Failure.TransactionFailure)
    }
    //endregion

    private fun PaymentSessionResponse.getTransactionReferenceFromSuccessfulResponse(): String =
        (this as PaymentSessionResponse.Success).allResponses.first().transactionReference
}