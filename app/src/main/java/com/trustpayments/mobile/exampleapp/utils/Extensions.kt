package com.trustpayments.mobile.exampleapp.utils

import android.content.Context
import android.view.View
import android.widget.ProgressBar
import com.trustpayments.mobile.core.models.DeviceSafetyWarning
import com.trustpayments.mobile.core.services.transaction.Error
import com.trustpayments.mobile.exampleapp.R
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.booleanOrNull
import kotlinx.serialization.json.contentOrNull
import kotlinx.serialization.json.doubleOrNull
import kotlinx.serialization.json.floatOrNull
import kotlinx.serialization.json.intOrNull
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive
import kotlinx.serialization.json.longOrNull

fun Error.SafetyError.errorMessage(context: Context) = this.errors.joinToString {
    context.getString(
        when (it) {
            DeviceSafetyWarning.DeviceRooted -> R.string.safetyWarning_message_rootedDevice
            DeviceSafetyWarning.SdkIntegrityTampered -> R.string.safetyWarning_message_sdkTampered
            DeviceSafetyWarning.EmulatorIsUsed -> R.string.safetyWarning_message_emulatorInUse
            DeviceSafetyWarning.DebuggerIsAttached -> R.string.safetyWarning_message_debuggerAttached
            DeviceSafetyWarning.AppInstalledFromUntrustedSource -> R.string.safetyWarning_message_appFromUntrustedSource
        }
    )
}

val JsonElement.extractedContent: Any?
    get() {
        if (this is JsonPrimitive) {
            if (this.jsonPrimitive.isString) {
                return this.jsonPrimitive.content
            }
            return this.jsonPrimitive.booleanOrNull ?: this.jsonPrimitive.intOrNull
            ?: this.jsonPrimitive.longOrNull ?: this.jsonPrimitive.floatOrNull
            ?: this.jsonPrimitive.doubleOrNull ?: this.jsonPrimitive.contentOrNull
        }
        if (this is JsonArray) {
            return this.jsonArray.map {
                it.extractedContent
            }
        }
        if (this is JsonObject) {
            return this.jsonObject.entries.associate {
                it.key to it.value.extractedContent
            }
        }
        return null
    }