package com.trustpayments.mobile.exampleapp.utils

object Constants {
    const val SP_KEY_IS_PRIVACY_POLICY_ACCEPTED = "isPrivacyPolicyAccepted"
    const val TEST_CARD_NUM = "4111111111111111"
    const val TEST_CARD_CVV = "123"
    const val WEB_ACTIVITY_REQ_CODE = 666
}