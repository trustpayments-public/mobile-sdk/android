package com.trustpayments.mobile.ui.dropin

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.core.view.setPadding
import com.trustpayments.mobile.ui.PanInputView
import com.trustpayments.mobile.ui.R
import com.trustpayments.mobile.ui.card.model.CardType
import com.trustpayments.mobile.ui.common.BaseInputView
import com.trustpayments.mobile.ui.databinding.ViewDropInPaymentBinding
import com.trustpayments.mobile.ui.model.GooglePayButtonColor
import com.trustpayments.mobile.ui.model.PaymentInputType
import com.trustpayments.mobile.ui.utils.findActivity
import com.trustpayments.mobile.ui.utils.logWarning
import com.trustpayments.mobile.ui.utils.setOnSingleClickListener
import com.trustpayments.mobile.ui.utils.setSecureWindowFlags

/**
 * Drop-in view for grouping all ui components and providing general payment form
 */
class DropInPaymentView(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs),
    BaseInputView.InputValidationListener, DropInPaymentViewModel.DropInPaymentCallbacks,
    PanInputView.CardTypeListener {

    constructor(context: Context) : this(context, null)

    private val viewModel: DropInPaymentViewModel = DropInPaymentViewModel(this)

    var dropInPaymentViewListener: DropInPaymentViewListener? = null
    var dropInInvalidInputListener: DropInInvalidInputListener? = null
    var dropInGooglePayPaymentViewListener: DropInGooglePayPaymentViewListener? = null

    private var binding: ViewDropInPaymentBinding

    init {
        context.findActivity()?.setSecureWindowFlags()
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = ViewDropInPaymentBinding.inflate(inflater, this)

        orientation = VERTICAL

        binding.panInput.inputValidationListener = this
        binding.panInput.cardTypeListener = this
        binding.expireInput.inputValidationListener = this
        binding.cvvInput.inputValidationListener = this

        binding.payButton.setOnSingleClickListener {
            dropInPaymentViewListener?.onPayButtonClicked()
        }

        binding.googlePayButton.setOnSingleClickListener {
            dropInGooglePayPaymentViewListener?.onGooglePayClicked()
        }

        attrs?.let {
            setupAttributes(it)
        }
    }

    private fun setupAttributes(attrs: AttributeSet) {
        val typedArray = context.obtainStyledAttributes(
            attrs, R.styleable.DropInPaymentView,
            0, 0
        )

        val inputs = binding.run {
            listOf(panInput, expireInput, cvvInput)
        }

        val styleManager = StyleManager(context, typedArray)

        inputs.forEach { input ->
            input.setInputBackgroundColor(styleManager.attributes.inputBackgroundColor)
            input.setInputBorderColor(styleManager.attributes.inputBorderColor)
            input.setInputBorderWidth(styleManager.attributes.inputBorderWidth)
            input.setInputCornerRadius(styleManager.attributes.inputCornerRadius)

            input.setTextColor(styleManager.attributes.inputTextColor)
            input.setTextSize(styleManager.attributes.inputTextSize)
            input.setHintTextColor(styleManager.attributes.inputHintTextColor)
            input.setSpacing(styleManager.attributes.inputSpacing)
            input.setLabelTextColor(styleManager.attributes.labelTextColor)
            input.setLabelTextSize(styleManager.attributes.labelTextSize)
            input.setLabelTextSpacing(styleManager.attributes.labelTextSpacing)
            input.setErrorTextColor(styleManager.attributes.errorTextColor)
        }

        binding.payButton.apply {
            val shape = styleManager.createRippleDrawable(
                styleManager.attributes.payButtonBackgroundColor,
                styleManager.attributes.payButtonBorderColor,
                styleManager.attributes.payButtonBorderWidth,
                styleManager.attributes.payButtonCornerRadius,
                styleManager.attributes.payButtonDisabledBackgroundColor,
                styleManager.attributes.payButtonPressedBackgroundColor
            )
            background = shape
            setTextColor(
                styleManager.createPayButtonTextColors(
                    styleManager.attributes.payButtonTextColor
                )
            )
            setTextSize(TypedValue.COMPLEX_UNIT_PX, styleManager.attributes.payButtonTextSize)
            setPadding(styleManager.attributes.payButtonPadding)
        }

        typedArray.recycle()
    }

    override fun onCardTypeDetected(cardType: CardType) {
        binding.cvvInput.setCardType(cardType)
    }

    override fun onInputValid(paymentInputType: PaymentInputType, input: String) {
        viewModel.updatePaymentForm(paymentInputType, true)
        dropInPaymentViewListener?.onInputValid(paymentInputType, input)
    }

    override fun onInputInvalid(paymentInputType: PaymentInputType) {
        viewModel.updatePaymentForm(paymentInputType, false)
        dropInInvalidInputListener?.onInputInvalid(paymentInputType)
    }

    override fun onPaymentFormValid() {
        binding.payButton.isEnabled = true
    }

    override fun onPaymentFormInvalid() {
        binding.payButton.isEnabled = false
    }

    fun setupForGooglePayPayment(
        isEnabled: Boolean = true,
        buttonColor: GooglePayButtonColor = GooglePayButtonColor.BLACK,
        withText: Boolean = true,
        withShadow: Boolean = true,
    ) {
        binding.googlePayButton.apply {
            visibility = if (isEnabled) View.VISIBLE else View.GONE
            setupButtonView(buttonColor, withText, withShadow)
        }
    }

    fun setupForTokenizedPayment(requiredFields: Set<PaymentInputType>, cardType: CardType?) {
        val requiresPAN = requiredFields.contains(PaymentInputType.PAN)
        val requiresExpiryDate = requiredFields.contains(PaymentInputType.ExpiryDate)
        val requiresCVV = requiredFields.contains(PaymentInputType.CVV)

        if (requiresCVV && !requiresPAN && cardType == null) {
            throw IllegalArgumentException("cardType param is required when CVV field is required and card PAN input is not")
        }

        binding.panInput.apply {
            inputValidationListener = if (requiresPAN) this@DropInPaymentView else null
            cardTypeListener = if (requiresPAN) this@DropInPaymentView else null
            visibility = if (requiresPAN) View.VISIBLE else View.GONE
        }

        binding.expireInput.apply {
            inputValidationListener = if (requiresExpiryDate) this@DropInPaymentView else null
            visibility = if (requiresExpiryDate) View.VISIBLE else View.GONE
        }

        binding.cvvInput.apply {
            inputValidationListener = if (requiresCVV) this@DropInPaymentView else null
            visibility = if (requiresCVV) View.VISIBLE else View.GONE
        }

        viewModel.setupForTokenizedPayment(requiredFields)

        if (requiresCVV) {
            if (requiresPAN) {
                logWarning(
                    DropInPaymentView::class.java.simpleName,
                    "cardType param is ignored when card PAN is required"
                )
            } else {
                binding.cvvInput.setCardType(cardType!!)
            }
        }
    }

    fun setCustomView(
        layoutRes: Int,
        customViewPosition: CustomViewPosition
    ) {
        val customView = LayoutInflater.from(context).inflate(layoutRes, null)
        setCustomView(customView, customViewPosition)
    }

    fun setCustomView(
        customView: View,
        customViewPosition: CustomViewPosition
    ) {
        binding.apply {
            val view = when (customViewPosition) {
                CustomViewPosition.UnderPan -> underPanView
                CustomViewPosition.UnderExpiryDate -> underDateView
                CustomViewPosition.UnderCvv -> underCvvView
                CustomViewPosition.UnderPayButton -> underPayButton
            }
            view.apply {
                removeAllViews()
                addView(customView)
                visibility = View.VISIBLE
            }
        }
    }

    fun setPayButtonVisibility(visibility: Int) {
        binding.payButton.visibility = visibility
    }

    /**
     * Interface for using in the component that will be hosting
     * the drop-in payment view, e.g Activity or Fragment.
     */
    interface DropInPaymentViewListener {
        fun onInputValid(paymentInputType: PaymentInputType, input: String)
        fun onPayButtonClicked()
    }

    /**
     * Interface for using in the component that will be hosting
     * the drop-in payment view, e.g Activity or Fragment.
     * To track invalid input submitted to the drop-in payment view.
     */
    interface DropInInvalidInputListener {
        fun onInputInvalid(paymentInputType: PaymentInputType)
    }

    /**
     * Interface to be used in the component that will be hosting
     * the drop-in payment view and allow google pay, e.g Activity or Fragment.
     * created separately for backward compatibility.
     */
    interface DropInGooglePayPaymentViewListener {
        fun onGooglePayClicked()
    }
}