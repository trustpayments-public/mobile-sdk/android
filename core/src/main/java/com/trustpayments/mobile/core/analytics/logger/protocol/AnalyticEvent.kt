package com.trustpayments.mobile.core.analytics.logger.protocol

import java.util.UUID

/**
 * Contains data related to the analytic/error incident.
 * @param eventInfo holds meta data related to the incidents.
 * @param message holds simplified message related to the incident.
 * @param tags holds data related to the environment used in the app.
 * */
data class AnalyticEvent(
    val eventId: UUID,
    val eventInfo: List<AnalyticEventInfo>? = null,
    val message: Message? = null,
    val tags: Map<String, String>? = null
)
