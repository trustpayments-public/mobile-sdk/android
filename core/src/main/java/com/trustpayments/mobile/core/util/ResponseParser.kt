package com.trustpayments.mobile.core.util

import com.trustpayments.mobile.core.models.JwtResponse
import com.trustpayments.mobile.core.models.api.jwt.JwtToken
import com.trustpayments.mobile.core.models.api.response.WebserviceResponse
import com.trustpayments.mobile.core.services.getGson
import com.trustpayments.mobile.core.utils.log

class ResponseParser {
    companion object {
        private val tag = "ResponseParser"

        private val gson by lazy { getGson() }

        fun parse(jwt: String?): JwtResponse? {
            if (jwt == null) {
                log(tag, "Empty JWT provided")
                return null
            }
            val jwtToken = JwtToken(jwt)
            if (!jwtToken.isValid) {
                log(tag, "Invalid JWT provided")
                return null
            }

            val responses = gson.fromJson(jwtToken.body, WebserviceResponse.Body::class.java).payload.transactionResponses
            return JwtResponse(responses.find { it.customerOutput != null }, responses)
        }

        fun parse(jwtList: List<String>): List<JwtResponse>? = jwtList.mapNotNull { parse(it) }.takeIf { it.size == jwtList.size }
    }
}