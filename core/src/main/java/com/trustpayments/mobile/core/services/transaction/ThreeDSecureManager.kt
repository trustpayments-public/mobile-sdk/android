package com.trustpayments.mobile.core.services.transaction

import android.app.Activity
import android.content.Context
import com.cardinalcommerce.cardinalmobilesdk.Cardinal
import com.cardinalcommerce.cardinalmobilesdk.enums.CardinalEnvironment
import com.cardinalcommerce.cardinalmobilesdk.enums.CardinalRenderType
import com.cardinalcommerce.cardinalmobilesdk.enums.CardinalUiType
import com.cardinalcommerce.cardinalmobilesdk.models.CardinalActionCode
import com.cardinalcommerce.cardinalmobilesdk.models.CardinalConfigurationParameters
import com.cardinalcommerce.cardinalmobilesdk.models.ValidateResponse
import com.cardinalcommerce.cardinalmobilesdk.services.CardinalInitService
import com.cardinalcommerce.cardinalmobilesdk.services.CardinalValidateReceiver
import com.trustpayments.mobile.core.analytics.logger.protocol.LogLevel
import com.trustpayments.mobile.core.models.DeviceSafetyWarning
import com.trustpayments.mobile.core.services.transaction.ErrorMessages.*
import com.trustpayments.mobile.core.ui.*
import com.trustpayments.mobile.core.utils.log
import org.json.JSONArray
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

internal class ThreeDSecureManager private constructor(
    private val applicationContext: Context,
    private val isLive: Boolean,
    private val cardinalStyleManager: CardinalStyleManager?,
    private val cardinalDarkThemeStyleManager: CardinalStyleManager?,
    private val loggingManager: LoggingManager,
    private val isLocationDataConsentGiven: Boolean
) {
    internal var cardinal: Cardinal? = null
        @JvmSynthetic get
        @JvmSynthetic set

    internal var isDarkThemeForced: Boolean? = null
        @JvmSynthetic get
        @JvmSynthetic set

    companion object {
        @JvmSynthetic
        internal fun getInstance(
            context: Context,
            isLive: Boolean,
            cardinalStyleManager: CardinalStyleManager?,
            cardinalDarkModeStyleManager: CardinalStyleManager?,
            loggingManager: LoggingManager,
            isLocationDataConsentGiven: Boolean
        ) = ThreeDSecureManager(
            context,
            isLive,
            cardinalStyleManager,
            cardinalDarkModeStyleManager,
            loggingManager,
            isLocationDataConsentGiven
        )
    }

    internal val deviceSafetyWarnings: List<DeviceSafetyWarning>
        @JvmSynthetic
        get() = cardinal?.warnings?.mapNotNull {
            when (it.id) {
                "SW01" -> DeviceSafetyWarning.DeviceRooted
                "SW02" -> DeviceSafetyWarning.SdkIntegrityTampered
                "SW03" -> DeviceSafetyWarning.EmulatorIsUsed
                "SW04" -> DeviceSafetyWarning.DebuggerIsAttached
                "SW06" -> DeviceSafetyWarning.AppInstalledFromUntrustedSource
                else -> null
            }
        } ?: emptyList()

    @JvmSynthetic
    internal fun createSession() {
        if (cardinal != null) {
            log(
                this::class.java.simpleName,
                "Initiating new Cardinal session before previous is finished!"
            )
        }

        val newCardinal = Cardinal.getInstance().apply {
            val cardinalConfigurationParameters = CardinalConfigurationParameters()
            cardinalConfigurationParameters.environment =
                if (isLive) CardinalEnvironment.PRODUCTION else CardinalEnvironment.STAGING
            cardinalConfigurationParameters.requestTimeout = 8000
            cardinalConfigurationParameters.challengeTimeout = 8
            cardinalConfigurationParameters.isEnableLogging = false

            val renderType = JSONArray()
            renderType.put(CardinalRenderType.OTP)
            renderType.put(CardinalRenderType.SINGLE_SELECT)
            renderType.put(CardinalRenderType.MULTI_SELECT)
            renderType.put(CardinalRenderType.OOB)
            renderType.put(CardinalRenderType.HTML)
            cardinalConfigurationParameters.renderType = renderType

            cardinalConfigurationParameters.uiType = CardinalUiType.BOTH
            cardinalConfigurationParameters.isLocationDataConsentGiven = isLocationDataConsentGiven

            isDarkThemeForced?.let {
                if (it) {
                    CardinalStyleManager.applyDarkTheme(
                        applicationContext,
                        cardinalDarkThemeStyleManager,
                        cardinalConfigurationParameters
                    )
                } else {
                    CardinalStyleManager.applyLightTheme(
                        applicationContext,
                        cardinalStyleManager,
                        cardinalConfigurationParameters
                    )
                }
            } ?: run {
                if (CardinalStyleManager.isDarkThemeSetOnDevice(applicationContext)) {
                    CardinalStyleManager.applyDarkTheme(
                        applicationContext,
                        cardinalDarkThemeStyleManager,
                        cardinalConfigurationParameters
                    )
                } else {
                    CardinalStyleManager.applyLightTheme(
                        applicationContext,
                        cardinalStyleManager,
                        cardinalConfigurationParameters
                    )
                }
            }

            configure(applicationContext, cardinalConfigurationParameters)
        }

        cardinal = newCardinal
    }

    @JvmSynthetic
    internal suspend fun initSession(jwtToken: String) =
        suspendCoroutine<InitializationResult> { continuation ->
            val localCardinal = this.cardinal
            if (localCardinal == null) {
                continuation.resume(InitializationResult.Failure(ThreeDSecureError.Unknown))
                return@suspendCoroutine
            }

            log("Initializing Cardinal with token: $jwtToken")

            localCardinal.init(jwtToken, object : CardinalInitService {
                override fun onSetupCompleted(consumerSessionId: String?) {
                    //this method will be called when init succeeds
                    if (consumerSessionId != null) {
                        continuation.resume(InitializationResult.Success(consumerSessionId))
                    } else {
                        loggingManager.sendErrorEvent(
                            ConsumerSessionIdNull,
                            ThreeDSecureManagerEvent
                        )
                        continuation.resume(InitializationResult.Failure(ThreeDSecureError.Unknown))
                    }
                }

                override fun onValidated(validateResponse: ValidateResponse?, serverJwt: String?) {
                    //this method will be called when init fails
                    if (validateResponse != null) {
                        loggingManager.sendErrorEvent(
                            InitError,
                            ThreeDSecureManagerEvent,
                            errorCode = validateResponse.errorNumber
                        )
                        continuation.resume(
                            InitializationResult.Failure(
                                ThreeDSecureError.GeneralError(validateResponse.errorNumber)
                            )
                        )
                    } else {
                        loggingManager.sendErrorEvent(
                            ValidateResponseNull,
                            ThreeDSecureManagerEvent
                        )
                        continuation.resume(InitializationResult.Failure(ThreeDSecureError.Unknown))
                    }
                }
            })
        }

    @JvmSynthetic
    internal suspend fun executeSession(
        version: String,
        transactionId: String,
        payload: String,
        activity: Activity
    ): ExecutionResult {

        val versionParts = version.split(".")

        val versionInt = try {
            versionParts[0].toInt()
        } catch (ex: Exception) {
            loggingManager.sendExceptionEvent(ex)
            return ExecutionResult.Failure(ThreeDSecureError.IncorrectVersion)
        }

        return if (versionInt == 2) {
            executeV2Session(transactionId, payload, activity)
        } else {
            ExecutionResult.Failure(ThreeDSecureError.IncorrectVersion)
        }
    }

    private suspend fun executeV2Session(
        transactionId: String, payload: String,
        activity: Activity
    ): ExecutionResult {
        return try {
            performV2Session(transactionId, payload, activity)
        } catch (ex: Exception) {
            loggingManager.sendExceptionEvent(ex)
            ExecutionResult.Failure(ThreeDSecureError.UncaughtExceptionError)
        }
    }

    private suspend fun performV2Session(
        transactionId: String,
        payload: String,
        activity: Activity
    ) = suspendCoroutine<ExecutionResult> { continuation ->
        val localCcardinal = this.cardinal
        if (localCcardinal == null) {
            continuation.resume(ExecutionResult.Failure(ThreeDSecureError.Unknown))
            return@suspendCoroutine
        }

        localCcardinal.cca_continue(
            transactionId,
            payload,
            activity,
            object : CardinalValidateReceiver {
                override fun onValidated(
                    context: Context?,
                    validationResponse: ValidateResponse?,
                    responseJwt: String?
                ) {
                    when (validationResponse?.actionCode) {
                        CardinalActionCode.SUCCESS -> {
                            releaseCardinal()
                            continuation.resume(ExecutionResult.SuccessV2(responseJwt!!))
                        }
                        CardinalActionCode.NOACTION -> {
                            releaseCardinal()
                            continuation.resume(ExecutionResult.SuccessV2(responseJwt!!))
                        }
                        null -> {
                            loggingManager.sendErrorEvent(
                                ValidationResponseErrorCodeNull,
                                CardinalError
                            )
                            continuation.resume(ExecutionResult.Failure(ThreeDSecureError.Unknown))
                        }
                        CardinalActionCode.CANCEL -> continuation.resume(
                            ExecutionResult.Failure(
                                ThreeDSecureError.ChallengeCanceled
                            )
                        )

                        else -> {
                            loggingManager.sendErrorEvent(
                                ValidationResponseError,
                                CardinalError,
                                LogLevel.ERROR,
                                validationResponse.errorNumber,
                                "${validationResponse.errorDescription} - actionCode: ${validationResponse.actionCode}"
                            )
                            continuation.resume(
                                ExecutionResult.Failure(
                                    ThreeDSecureError.GeneralError(validationResponse.errorNumber)
                                )
                            )
                        }
                    }
                }
            })
    }

    private fun releaseCardinal() {
        cardinal?.cleanup()
        cardinal = null
    }

    internal sealed class InitializationResult {
        data class Success(val result: String) : InitializationResult()
        data class Failure(val error: ThreeDSecureError) : InitializationResult()
    }

    internal sealed class ExecutionResult {
        data class SuccessV1(val result: String) : ExecutionResult()
        data class SuccessV2(val result: String) : ExecutionResult()
        data class Failure(val error: ThreeDSecureError) : ExecutionResult()
    }
}