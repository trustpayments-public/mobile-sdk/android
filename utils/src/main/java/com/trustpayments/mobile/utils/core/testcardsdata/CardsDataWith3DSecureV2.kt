package com.trustpayments.mobile.utils.core.testcardsdata

object CardsDataWith3DSecureV2 {
    val threeDSecureCode = "1234"
    val invalidThreeDSecureCode = "123456"
    val frictionlessVisaCardNumber = "4000000000001026"
    val frictionlessMasterCardNumber = "5200000000001005"
    val frictionlessAmexCardNumber = "340000000000611"
    val nonFrictionlessVisaCardNumber = "4000000000001091"
    val nonFrictionlessMasterCardNumber = "5200000000001096"
    val nonFrictionlessAmexCardNumber = "340000000001098"
    val nonFrictionlessMaestroCardNumber = "5000000000000611"
    val bankSystemErrorVisaCardNumber = "4000000000001067"
    val unauthenticatedErrorVisaCardNumber = "4000000000001018"

}
