package com.trustpayments.mobile.ui

import com.trustpayments.mobile.ui.card.CardInfo
import com.trustpayments.mobile.ui.card.CardValidator
import com.trustpayments.mobile.ui.card.model.CardType
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.slot
import io.mockk.verify
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class CvvInputViewModelTest {

    @MockK
    private lateinit var cardValidator: CardValidator

    @MockK
    private lateinit var cvvInputCallbacks: CvvInputViewModel.CvvInputCallbacks

    private lateinit var viewModel: CvvInputViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        viewModel = CvvInputViewModel(cardValidator, cvvInputCallbacks)
    }

    @Test
    fun test_handleCardType() {
        // GIVEN
        val cvvLengthSlot = slot<Int>()
        val actualCvvLengths = mutableListOf<Int>()
        every { cvvInputCallbacks.onCvvLengthChanged(capture(cvvLengthSlot)) } answers {
            actualCvvLengths.add(cvvLengthSlot.captured)
        }

        val expectedCvvLengths = listOf(
            CardInfo.getCvvLength(CardType.Visa),
            CardInfo.getCvvLength(CardType.Amex)
        )

        // WHEN
        // first invocation - change: CardType.Unknown -> CardType.Visa
        // callback triggered: Yes
        viewModel.handleCardType(CardType.Visa)

        // second invocation - no change: CardType.Visa -> CardType.Visa
        // callback triggered: No
        viewModel.handleCardType(CardType.Visa)

        // third invocation - change: CardType.Visa -> CardType.Amex
        // callback triggered: Yes
        viewModel.handleCardType(CardType.Amex)

        // THEN
        assertEquals(expectedCvvLengths, actualCvvLengths)
        verify(exactly = 2) { cvvInputCallbacks.onCvvLengthChanged(any()) }
    }

    @Test
    fun test_validateCvvFroCorrectInput() {
        // GIVEN
        val correctCvv = "123"
        every { cardValidator.isCvvValid(any(), any()) } returns true
        every { cvvInputCallbacks.onCvvValid(any()) } returns Unit

        // WHEN
        viewModel.validateCvv(correctCvv)

        // THEN
        verify(exactly = 1) { cvvInputCallbacks.onCvvValid(any()) }
    }


    @Test
    fun test_validateCvvFroIncorrectInput() {
        // GIVEN
        val incorrectCvv = "12"
        every { cardValidator.isCvvValid(any(), any()) } returns false
        every { cvvInputCallbacks.onCvvInvalid(any()) } returns Unit

        // WHEN
        viewModel.validateCvv(incorrectCvv)

        // THEN
        verify(exactly = 1) { cvvInputCallbacks.onCvvInvalid(any()) }
    }

    @Test
    fun test_checkCorrectCvvLengthForVisaAndValidate() {
        // GIVEN
        val correctCvv = "123"
        every { cardValidator.isCvvValid(any(), any()) } returns true
        every { cvvInputCallbacks.onCvvValid(any()) } returns Unit
        every { cvvInputCallbacks.onCvvLengthChanged(any()) } returns Unit

        // WHEN
        viewModel.handleCardType(CardType.Visa)
        viewModel.checkForMaxLengthAndValidate(correctCvv)

        // THEN
        verify(exactly = 1) { cvvInputCallbacks.onCvvValid(any()) }
    }

    @Test
    fun test_checkIncorrectCvvLengthForAmexAndValidate() {
        // GIVEN
        val incorrectCvv = "123"
        every { cvvInputCallbacks.onCvvInvalid(any()) } returns Unit
        every { cvvInputCallbacks.onCvvLengthChanged(any()) } returns Unit

        // WHEN
        viewModel.handleCardType(CardType.Amex)
        viewModel.checkForMaxLengthAndValidate(incorrectCvv)

        // THEN
        verify(exactly = 1) { cvvInputCallbacks.onCvvInvalid(any()) }
    }
}