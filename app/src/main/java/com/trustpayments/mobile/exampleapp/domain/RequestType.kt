package com.trustpayments.mobile.exampleapp.domain

/**
 * Consists of services that are available via Trust Payments SDK.
 * */
enum class RequestType(val serializedName: String) {
    JsInit("JSINIT"),
    AccountCheck("ACCOUNTCHECK"),
    Auth("AUTH"),
    Subscription("SUBSCRIPTION"),
    ThreeDQuery("THREEDQUERY"),
}