package com.trustpayments.mobile.core.services

import android.app.Activity
import android.os.Build
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParseException
import com.google.gson.JsonPrimitive
import com.trustpayments.mobile.core.models.api.response.CustomerOutput
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.models.api.response.ResponseErrorCode
import com.trustpayments.mobile.core.models.api.response.ResponseLiveStatus
import com.trustpayments.mobile.core.models.api.response.ResponseSettlementStatus
import com.trustpayments.mobile.core.models.api.response.WebserviceResponse
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThrows
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.android.controller.ActivityController
import org.robolectric.annotation.Config
import java.lang.reflect.Type

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.M])
class ResponseSerializerTest {

    private var activity: Activity? = null

    @Before
    fun setup() {
        val activityController: ActivityController<Activity> = Robolectric.buildActivity(
            Activity::class.java
        )
        activity = activityController.get()
    }

    @Test
    fun test_LiveStatusTypeSerializer_with_valid_JSON() {
        // Arrange
        val jsonElement = mockk<JsonElement>()
        val typeOfT = mockk<Type>()
        val context = mockk<JsonDeserializationContext>()
        val value = 1
        every { jsonElement.asInt } returns value
        // Act
        val serializer = LiveStatusTypeSerializer()
        val result = serializer.deserialize(jsonElement, typeOfT, context)
        // Assert
        assertEquals(ResponseLiveStatus.Live, result)
    }

    @Test
    fun test_LiveStatusTypeSerializer_with_unknown_value() {
        // Arrange
        val jsonElement = mockk<JsonElement>()
        val typeOfT = mockk<Type>()
        val context = mockk<JsonDeserializationContext>()
        val value = 999 // An unknown value
        every { jsonElement.asInt } returns value
        // Act
        val serializer = LiveStatusTypeSerializer()
        // Assert
        assertThrows(JsonParseException::class.java) {
            serializer.deserialize(jsonElement, typeOfT, context)
        }
    }

    @Test
    fun test_ErrorCodeSerialize_with_unknown_code() {
        // Arrange
        val jsonElement = mockk<JsonElement>()
        val typeOfT = mockk<Type>()
        val context = mockk<JsonDeserializationContext>()
        val code = 999 // An unknown code
        every { jsonElement.asInt } returns code
        // Act
        val serializer = ErrorCodeSerializer()
        // Assert
        val exception = assertThrows(JsonParseException::class.java) {
            serializer.deserialize(jsonElement, typeOfT, context)
        }
        assertEquals("", exception.message)
    }

    @Test
    fun test_ErrorCodeSerializer_deserialize_valid_serialized_name() {
        // Arrange
        val serializer = ErrorCodeSerializer()
        val code = 25001
        val json = JsonPrimitive(code)
        val expectedOutput = ResponseErrorCode.CodingError

        // Act
        val result = serializer.deserialize(json, null, null)
        // Assert
        assertEquals(expectedOutput, result)
    }

    @Test
    fun test_ErrorCodeSerializer_deserialize_missing_serialized_name() {
        // Arrange
        val serializer = ErrorCodeSerializer()
        val json = JsonPrimitive(-369)
        // Act, Assert
        assertThrows(JsonParseException::class.java) {
            serializer.deserialize(json, null, null)
        }
    }

    @Test
    fun test_ErrorCodeSerializer_deserialize_unknown_payload() {
        // Arrange
        val serializer = ErrorCodeSerializer()
        val json = JsonPrimitive("")
        // Act, Assert
        assertThrows(NumberFormatException::class.java) {
            serializer.deserialize(json, null, null)
        }
    }

    @Test
    fun test_SettlementStatusTypeSerializer_with_valid_JSON() {
        // Arrange
        val jsonElement = mockk<JsonElement>()
        val typeOfT = mockk<Type>()
        val context = mockk<JsonDeserializationContext>()
        val code = 1
        every { jsonElement.asInt } returns code
        // Act
        val serializer = SettlementStatusTypeSerializer()
        val result = serializer.deserialize(jsonElement, typeOfT, context)
        // Assert
        assertEquals(ResponseSettlementStatus.PendingManualOverride, result)
    }

    @Test
    fun test_SettlementStatusTypeSerializer_with_unknown_code() {
        // Arrange
        val jsonElement = mockk<JsonElement>()
        val typeOfT = mockk<Type>()
        val context = mockk<JsonDeserializationContext>()
        val code = 999 // An unknown code
        every { jsonElement.asInt } returns code
        // Act
        val serializer = SettlementStatusTypeSerializer()
        // Assert
        val exception = assertThrows(JsonParseException::class.java) {
            serializer.deserialize(jsonElement, typeOfT, context)
        }
        assertEquals("", exception.message)
    }

    @Test
    fun test_WebserviceResponseSerializer_with_valid_JSON() {
        // Arrange
        val jsonElement = mockk<JsonElement>()
        val typeOfT = mockk<Type>()
        val context = mockk<JsonDeserializationContext>()
        val mockBody = mockk<WebserviceResponse.Body>()
        val jwt =
            "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtcGdzbW9iaWxlc2RrYW5kcm9pZCIsImlhdCI6MTcwOTEyMzgzNSwicGF5bG9hZCI6eyJ0eXBlIjoiY29tLnRydXN0cGF5bWVudHMubW9iaWxlLmV4YW1wbGVhcHAuY29tbW9uLmp3dC5Ub2tlbml6YXRpb25QYXlsb2FkIiwiYWNjb3VudHR5cGVkZXNjcmlwdGlvbiI6IkVDT00iLCJ0ZXJtdXJsIjoiaHR0cHM6XC9cL3BheW1lbnRzLnNlY3VyZXRyYWRpbmcubmV0XC9wcm9jZXNzXC9wYXltZW50c1wvbW9iaWxlc2RrbGlzdGVuZXIiLCJzaXRlcmVmZXJlbmNlIjoidGVzdF9wZ3Ntb2JpbGVzZGthbmRyb2lkODA2NjYiLCJjdXJyZW5jeWlzbzNhIjoiR0JQIiwiYmFzZWFtb3VudCI6MTA1MCwicmVxdWVzdHR5cGVkZXNjcmlwdGlvbnMiOlsiVEhSRUVEUVVFUlkiLCJBVVRIIl0sImNyZWRlbnRpYWxzb25maWxlIjoiMSIsInBhcmVudHRyYW5zYWN0aW9ucmVmZXJlbmNlIjpudWxsfX0.45o-AK9ND-gHGzxEhjmvhdVHlDeS8vR3pe4j_JgqlfA"
        val jsonObject = mockk<JsonObject>()
        every { jsonElement.asJsonObject } returns jsonObject
        every { jsonObject.get("jwt") } returns mockk {
            every { asString } returns jwt
        }
        every {
            context.deserialize<WebserviceResponse.Body>(
                any(),
                WebserviceResponse.Body::class.java
            )
        } returns mockBody
        // Act
        val serializer = WebserviceResponseSerializer()
        val result = serializer.deserialize(jsonElement, typeOfT, context)
        // Assert
        assertEquals(mockBody, result.body)
    }

    @Test
    fun test_WebserviceResponseSerializer_with_invalid_JSON() {
        // Arrange
        val jsonElement = mockk<JsonElement>()
        val typeOfT = mockk<Type>()
        val context = mockk<JsonDeserializationContext>()
        every { jsonElement.asJsonObject } returns null
        // Act
        val serializer = WebserviceResponseSerializer()
        // Assert
        val exception = assertThrows(JsonParseException::class.java) {
            serializer.deserialize(jsonElement, typeOfT, context)
        }
        assertEquals("", exception.message)
    }

    @Test
    fun test_WebserviceResponseSerializer_with_invalid_token() {
        // Arrange
        val jsonElement = mockk<JsonElement>()
        val typeOfT = mockk<Type>()
        val context = mockk<JsonDeserializationContext>()
        val jwt = "invalid_jwt"
        val jsonObject = mockk<JsonObject>()
        every { jsonElement.asJsonObject } returns jsonObject
        every { jsonObject.get("jwt") } returns mockk {
            every { asString } returns jwt
        }
        // Act
        val serializer = WebserviceResponseSerializer()
        // Assert
        val exception = assertThrows(JsonParseException::class.java) {
            serializer.deserialize(jsonElement, typeOfT, context)
        }
        assertEquals("", exception.message)
    }

    @Test
    fun test_RequestTypeSerializer_serialize_with_valid_request_type() {
        // Arrange
        val serializer = RequestTypeSerializer()
        val requestType = RequestType.JsInit
        val expectedJson = JsonPrimitive(requestType.serializedName)
        // Act
        val result = serializer.serialize(requestType, null, null)
        // Assert
        assertEquals(expectedJson, result)
    }

    @Test
    fun test_RequestTypeSerializer_deserialize_valid_serialized_name() {
        // Arrange
        val serializer = RequestTypeSerializer()
        val serializedName = "ACCOUNTCHECK"
        val json = JsonPrimitive(serializedName)
        val expectedRequestType = RequestType.AccountCheck
        // Act
        val result = serializer.deserialize(json, null, null)
        // Assert
        assertEquals(expectedRequestType, result)
    }

    @Test
    fun test_RequestTypeSerializer_deserialize_missing_serialized_name() {
        // Arrange
        val serializer = RequestTypeSerializer()
        val json = JsonPrimitive("")
        // Act, Assert
        assertThrows(JsonParseException::class.java) {
            serializer.deserialize(json, null, null)
        }
    }
    @Test
    fun test_CustomerOutputTypeSerializer_deserialize_valid_serialized_name() {
        // Arrange
        val serializer = CustomerOutputTypeSerializer()
        val code = "RESULT"
        val json = JsonPrimitive(code)
        val expectedOutput = CustomerOutput.Result

        // Act
        val result = serializer.deserialize(json, null, null)
        // Assert
        assertEquals(expectedOutput, result)
    }

    @Test
    fun test_CustomerOutputTypeSerializer_deserialize_missing_serialized_name() {
        // Arrange
        val serializer = CustomerOutputTypeSerializer()
        val json = JsonPrimitive("")
        // Act, Assert
        assertThrows(JsonParseException::class.java) {
            serializer.deserialize(json, null, null)
        }
    }

    @Test
    fun test_SettlementStatusTypeSerializer_deserialize_valid_serialized_name() {
        // Arrange
        val serializer = SettlementStatusTypeSerializer()
        val serializedCode = 2
        val json = JsonPrimitive(serializedCode)
        val expectedRequestType = ResponseSettlementStatus.Suspended
        // Act
        val result = serializer.deserialize(json, null, null)
        // Assert
        assertEquals(expectedRequestType, result)
    }

    @Test
    fun test_SettlementStatusTypeSerializer_deserialize_missing_serialized_name() {
        // Arrange
        val serializer = SettlementStatusTypeSerializer()
        val json = JsonPrimitive(-444)
        // Act, Assert
        assertThrows(JsonParseException::class.java) {
            serializer.deserialize(json, null, null)
        }
    }

    @Test
    fun test_LiveStatusTypeSerializer_deserialize_valid_serialized_name() {
        // Arrange
        val serializer = LiveStatusTypeSerializer()
        val serializedCode = 1
        val json = JsonPrimitive(serializedCode)
        val expectedRequestType = ResponseLiveStatus.Live
        // Act
        val result = serializer.deserialize(json, null, null)
        // Assert
        assertEquals(expectedRequestType, result)
    }

    @Test
    fun test_LiveStatusTypeSerializer_deserialize_missing_serialized_name() {
        // Arrange
        val serializer = LiveStatusTypeSerializer()
        val json = JsonPrimitive(888)
        // Act, Assert
        assertThrows(JsonParseException::class.java) {
            serializer.deserialize(json, null, null)
        }
    }
}