package com.trustpayments.mobile.core.utils

import android.app.Activity
import android.content.Intent
import com.trustpayments.mobile.core.ui.WebActivityResult

class ThreeDQueryTestActivity : Activity() {

    lateinit var resultListener: (result: WebActivityResult) -> Unit

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        resultListener(WebActivityResult(requestCode, resultCode, data?.extras))
    }
}