package com.trustpayments.mobile.exampleapp.functionaltests.samples.e2eTests

import android.content.Context
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.trustpayments.mobile.exampleapp.MainActivity
import com.trustpayments.mobile.exampleapp.pageobjects.AuthWith3DSecureFormPage
import com.trustpayments.mobile.exampleapp.pageobjects.GooglePayPage
import com.trustpayments.mobile.exampleapp.pageobjects.HomePage
import com.trustpayments.mobile.exampleapp.pageobjects.PayByCardFormPage
import com.trustpayments.mobile.exampleapp.utils.TestUtils
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2
import com.trustpayments.mobile.utils.core.testcardsdata.GeneralCardsData
import org.junit.Assert
import org.junit.Test
import org.junit.After
import org.junit.Before
import org.junit.Rule
class GooglePayTestSuite {

    @get:Rule
    val activityScenarioRule = ActivityScenarioRule(MainActivity::class.java)

    private lateinit var context: Context
    private val homePage = HomePage()
    private val payByCardFormPage = PayByCardFormPage()
    private val authWith3DSecureFormPage = AuthWith3DSecureFormPage()
    private  val googlePayPage = GooglePayPage()

    @Before
    fun setUp() {
        context = InstrumentationRegistry.getInstrumentation().targetContext
        TestUtils.clearSharedPreferences(context)
    }
    @After
    fun after() {
        TestUtils.clearSharedPreferences(context)
    }
    private fun fillAndSubmitCardPaymentForm(cardNumber: String, date: String, cvv: String) {
        homePage.tapGooglePay()
        payByCardFormPage.fillPaymentFormWith(cardNumber, date, cvv)
            .tapOnPayButton()
        Thread.sleep(10000)
    }

    @Test
    fun successfulCardPaymentByVisaInGooglePayScreen() {
        fillAndSubmitCardPaymentForm(
            CardsDataWith3DSecureV2.nonFrictionlessVisaCardNumber,
            GeneralCardsData.correctExpiryDate,
            GeneralCardsData.correct3digitsCVV
        )
        authWith3DSecureFormPage.fillSecureCode(
            CardsDataWith3DSecureV2.threeDSecureCode
        )
            .tapOnSubmitButton()
        Assert.assertTrue(
            "Alert with successful payment processing message was not displayed.",
            payByCardFormPage.isSuccessCardPaymentCompletedAlertDisplayed()
        )
    }
}