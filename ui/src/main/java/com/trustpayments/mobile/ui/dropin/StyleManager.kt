package com.trustpayments.mobile.ui.dropin

import android.content.Context
import android.content.res.ColorStateList
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.RippleDrawable
import android.graphics.drawable.StateListDrawable
import android.util.StateSet
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import com.trustpayments.mobile.ui.R
import com.trustpayments.mobile.ui.model.PaymentViewStyleAttributes
import com.trustpayments.mobile.ui.utils.fromDpToPx
import com.trustpayments.mobile.ui.utils.fromPxToDp
import com.trustpayments.mobile.ui.utils.fromSpToPx
import kotlin.math.roundToInt


internal class StyleManager(private val context: Context, typedArray: TypedArray) {

    private companion object {
        private const val DEFAULT_INPUT_TEXT_SIZE = 16.0f
        private const val DEFAULT_INPUT_SPACING = 10.0f
        private const val DEFAULT_LABEL_SPACING = 5.0f
        private const val DEFAULT_PAY_BUTTON_TEXT_SIZE = 14.0f
        private const val DEFAULT_PAY_BUTTON_PADDING = 20.0f
        private const val DEFAULT_BORDER_WIDTH = 1.0f
        private const val DEFAULT_CORNER_RADIUS = 4.0f
    }

    @JvmSynthetic
    val attributes = PaymentViewStyleAttributes(
        inputTextColor = typedArray.getColor(
            R.styleable.DropInPaymentView_inputTextColor,
            ContextCompat.getColor(context, R.color.colorInputText)
        ),
        inputTextSize = typedArray.getDimension(
            R.styleable.DropInPaymentView_inputTextSize,
            DEFAULT_INPUT_TEXT_SIZE.fromSpToPx()
        ),
        inputBackgroundColor = typedArray.getColor(
            R.styleable.DropInPaymentView_inputBackgroundColor,
            ContextCompat.getColor(context, android.R.color.transparent)
        ),
        inputBorderColor = typedArray.getColor(
            R.styleable.DropInPaymentView_inputBorderColor,
            ContextCompat.getColor(context, R.color.colorInputBorder)
        ),
        inputBorderWidth = typedArray.getDimension(
            R.styleable.DropInPaymentView_inputBorderWidth,
            DEFAULT_BORDER_WIDTH.fromDpToPx()
        ),
        inputCornerRadius = typedArray.getDimension(
            R.styleable.DropInPaymentView_inputCornerRadius,
            DEFAULT_CORNER_RADIUS.fromDpToPx()
        ),
        inputHintTextColor = typedArray.getColor(
            R.styleable.DropInPaymentView_inputHintTextColor,
            ContextCompat.getColor(context, R.color.colorInputHint)
        ),
        inputSpacing = typedArray.getDimension(
            R.styleable.DropInPaymentView_inputSpacing,
            DEFAULT_INPUT_SPACING.fromDpToPx()
        ).toInt(),

        labelTextColor = typedArray.getColor(
            R.styleable.DropInPaymentView_labelTextColor,
            ContextCompat.getColor(context, R.color.colorLabelText)
        ),
        labelTextSize = typedArray.getDimension(
            R.styleable.DropInPaymentView_labelTextSize,
            DEFAULT_INPUT_TEXT_SIZE.fromSpToPx()
        ),
        labelTextSpacing = typedArray.getDimension(
            R.styleable.DropInPaymentView_labelSpacing,
            DEFAULT_LABEL_SPACING.fromDpToPx()
        ).toInt(),

        errorTextColor = typedArray.getColor(
            R.styleable.DropInPaymentView_errorTextColor,
            ContextCompat.getColor(context, R.color.colorInputError)
        ),

        payButtonTextColor = typedArray.getColor(
            R.styleable.DropInPaymentView_payButtonTextColor,
            ContextCompat.getColor(context, R.color.colorButtonText)
        ),
        payButtonTextSize = typedArray.getDimension(
            R.styleable.DropInPaymentView_payButtonTextSize,
            DEFAULT_PAY_BUTTON_TEXT_SIZE.fromSpToPx()
        ),
        payButtonBackgroundColor = typedArray.getColor(
            R.styleable.DropInPaymentView_payButtonBackgroundColor,
            ContextCompat.getColor(context, R.color.colorButtonBackground)
        ),
        payButtonDisabledBackgroundColor = typedArray.getColor(
            R.styleable.DropInPaymentView_payButtonDisabledBackgroundColor,
            ContextCompat.getColor(context, R.color.colorButtonBackgroundDisabled)
        ),
        payButtonPressedBackgroundColor = typedArray.getColor(
            R.styleable.DropInPaymentView_payButtonPressedBackgroundColor,
            ContextCompat.getColor(context, R.color.colorButtonPressed)
        ),
        payButtonBorderColor = typedArray.getColor(
            R.styleable.DropInPaymentView_payButtonBorderColor,
            ContextCompat.getColor(context, R.color.colorButtonBorder)
        ),
        payButtonBorderWidth = typedArray.getDimension(
            R.styleable.DropInPaymentView_payButtonBorderWidth,
            DEFAULT_BORDER_WIDTH.fromDpToPx()
        ),
        payButtonCornerRadius = typedArray.getDimension(
            R.styleable.DropInPaymentView_payButtonCornerRadius,
            DEFAULT_CORNER_RADIUS.fromDpToPx()
        ),
        payButtonPadding = typedArray.getDimension(
            R.styleable.DropInPaymentView_payButtonPadding,
            DEFAULT_PAY_BUTTON_PADDING.fromDpToPx()
        ).toInt()
    )

    @JvmSynthetic
    fun createRippleDrawable(
        backgroundColor: Int, borderColor: Int, borderWidth: Float,
        cornerRadius: Float, disabledBackgroundColor: Int, pressedBackgroundColor: Int
    ): Drawable =
        RippleDrawable(
            ColorStateList.valueOf(pressedBackgroundColor),
            createPayButtonBackground(
                backgroundColor, borderColor, borderWidth, cornerRadius, disabledBackgroundColor
            ),
            null
        )

    private fun createPayButtonBackground(
        backgroundColor: Int, borderColor: Int, borderWidth: Float,
        cornerRadius: Float, disabledBackgroundColor: Int
    ): Drawable {
        val default = GradientDrawable()
        default.shape = GradientDrawable.RECTANGLE;
        default.cornerRadii = FloatArray(8) {
            cornerRadius
        }
        default.setColor(backgroundColor);
        default.setStroke(borderWidth.toInt(), borderColor)

        val disabled = GradientDrawable()
        disabled.shape = GradientDrawable.RECTANGLE;
        disabled.cornerRadii = FloatArray(8) {
            cornerRadius
        }
        disabled.setColor(disabledBackgroundColor);
        disabled.setStroke(borderWidth.toInt(), borderColor)

        val stateList = StateListDrawable()
        stateList.addState(intArrayOf(-android.R.attr.state_enabled), disabled)
        stateList.addState(StateSet.WILD_CARD, default)

        return stateList
    }

    @JvmSynthetic
    fun createPayButtonTextColors(textColor: Int
    ): ColorStateList {
        val states = arrayOf(
            intArrayOf(android.R.attr.state_enabled),
            intArrayOf(-android.R.attr.state_enabled),
        )
        val colors = intArrayOf(
            textColor,
            adjustAlpha(textColor, 0.3f)
        )

        return ColorStateList(states, colors)
    }

    private fun adjustAlpha(@ColorInt color: Int, factor: Float): Int {
        val alpha = (Color.alpha(color) * factor).roundToInt()
        val red = Color.red(color)
        val green = Color.green(color)
        val blue = Color.blue(color)
        return Color.argb(alpha, red, green, blue)
    }
}