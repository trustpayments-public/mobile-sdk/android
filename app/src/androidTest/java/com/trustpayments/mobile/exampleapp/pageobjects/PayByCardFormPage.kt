package com.trustpayments.mobile.exampleapp.pageobjects

import android.os.Build
import android.view.View
import android.widget.Button
import androidx.test.espresso.Espresso
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.PerformException
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.uiautomator.By
import androidx.test.uiautomator.Until
import com.trustpayments.mobile.exampleapp.R
import org.hamcrest.CoreMatchers
import org.hamcrest.Matcher

class PayByCardFormPage : BaseAppPage() {


    //Elements

    private val cardNumberInput = CoreMatchers.allOf(
        ViewMatchers.isDescendantOfA(ViewMatchers.withId(R.id.panInput)),
        ViewMatchers.withId(R.id.input)
    )

    private fun waitUntilAlertIsDisplayedUsingUIAutomator() {
        uiDevice.wait(Until.hasObject(By.text("OK")), 10)
    }

    private val dateInput = CoreMatchers.allOf(
        ViewMatchers.isDescendantOfA(ViewMatchers.withId(R.id.expireInput)),
        ViewMatchers.withId(R.id.input)
    )

    private val cvvInput = CoreMatchers.allOf(
        ViewMatchers.isDescendantOfA(ViewMatchers.withId(R.id.cvvInput)),
        ViewMatchers.withId(R.id.input)
    )

    private val submitButton = CoreMatchers.allOf(
        ViewMatchers.isDescendantOfA(ViewMatchers.withId(R.id.dropInPaymentView)),
        ViewMatchers.withId(R.id.payButton),
        ViewMatchers.withText(R.string.pvc_pay_button_text)
    )

    private val creditCardValidationMessage = CoreMatchers.allOf(
        ViewMatchers.isDescendantOfA(ViewMatchers.withId(R.id.panInput)),
        ViewMatchers.withId(R.id.textinput_error),
        ViewMatchers.withText(R.string.pvc_input_error_text)
    )
    private val expiryDateValidationMessage = CoreMatchers.allOf(
        ViewMatchers.isDescendantOfA(ViewMatchers.withId(R.id.expireInput)),
        ViewMatchers.withId(R.id.textinput_error),
        ViewMatchers.withText(R.string.pvc_input_error_text)
    )
    private val cvvValidationMessage = CoreMatchers.allOf(
        ViewMatchers.isDescendantOfA(ViewMatchers.withId(R.id.cvvInput)),
        ViewMatchers.withId(R.id.textinput_error),
        ViewMatchers.withText(R.string.pvc_input_error_text)
    )

    private val successfulPaymentAlert =
        ViewMatchers.withText(CoreMatchers.startsWith("Payment Executed successfully"))
    private val successfulGooglePaymentAlert =
        ViewMatchers.withText(CoreMatchers.startsWith("Google Payment Executed successfully"))
    private val successfulCardPaymentAlert =
        ViewMatchers.withText(CoreMatchers.startsWith("Card Payment Executed successfully"))
    private val successfulPaymentCompletedAlert =
        ViewMatchers.withText(CoreMatchers.startsWith("The request has been successfully completed"))
    private val unauthenticatedPaymenAlert =
        ViewMatchers.withText(CoreMatchers.startsWith("Unauthenticated:"))
    private val cardDetailsErrorAlert =
        ViewMatchers.withText(CoreMatchers.startsWith("Please provide card details"))
    private val failedPaymentAlert =
        ViewMatchers.withText(CoreMatchers.startsWith("Transaction failed: Bank System Error"))
    private val okButtonOnPopUpMessage = withText("OK")
    private val okButtonOnPopUpMessageUiDevice = Button::class.java

    // Actions

    fun isCreditCardValidationMessageDisplayed(): Boolean {
        return viewExists(
            CoreMatchers.allOf(
                creditCardValidationMessage,
                ViewMatchers.isDisplayed()
            )
        )
    }

    fun isExpireDateValidationMessageDisplayed(): Boolean {
        return viewExists(
            CoreMatchers.allOf(
                expiryDateValidationMessage,
                ViewMatchers.isDisplayed()
            )
        )
    }

    fun isCvvValidationMessageDisplayed(): Boolean {
        return viewExists(
            CoreMatchers.allOf(
                cvvValidationMessage,
                ViewMatchers.isDisplayed()
            )
        )
    }

    fun isSubmitButtonEnabled(): Boolean {
        return viewExists(
            CoreMatchers.allOf(
                submitButton,
                ViewMatchers.isEnabled()
            )
        )
    }
    fun fillCardNumberInputWith(cardNumber: String): PayByCardFormPage {
        Espresso.onView(cardNumberInput).perform(ViewActions.typeText(cardNumber))
        return this;
    }

    fun fillDateInputWith(date: String): PayByCardFormPage {
        Espresso.onView(dateInput).perform(ViewActions.typeText(date))
        return this;
    }

    fun fillCvvInputWith(cvvNumber: String): PayByCardFormPage {
        Espresso.onView(cvvInput).perform(ViewActions.scrollTo(), ViewActions.typeText(cvvNumber))
        return this;
    }
    fun tapOnCardNumberInput(): PayByCardFormPage {
        Espresso.onView(cardNumberInput)
            .perform(ViewActions.click(), ViewActions.closeSoftKeyboard())
        return this
    }

    fun tapOnValidDateInput(): PayByCardFormPage {
        Espresso.onView(dateInput).perform(ViewActions.click())
        return this
    }

    fun tapOnCVVInput(): PayByCardFormPage {
        Espresso.onView(cvvInput).perform(ViewActions.scrollTo(), ViewActions.click())
        return this
    }

    fun fillPaymentFormWith(cardNumber: String, date: String, cvv: String): PayByCardFormPage {
        fillCardNumberInputWith(cardNumber)
        fillDateInputWith(date)
        fillCvvInputWith(cvv)
        return this
    }

    fun waitUntilAlertIsDisplayed(): PayByCardFormPage {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            waitUntilElementIsDisplayed(
                okButtonOnPopUpMessage,
                timeout = 10
            )
        } else {
            waitUntilAlertIsDisplayedUsingUIAutomator()
        }
        return this
    }
    fun isSuccessPaymentAlertDisplayed(): Boolean {
        waitUntilAlertIsDisplayed()
        return viewExists(
            CoreMatchers.allOf(
                successfulPaymentAlert,
                ViewMatchers.isDisplayed()
            )
        )
    }
    fun isSuccessPaymentCompletedAlertDisplayed(): Boolean {
        waitUntilAlertIsDisplayed()
        return viewExists(
            CoreMatchers.allOf(
                successfulPaymentCompletedAlert,
                ViewMatchers.isDisplayed()
            )
        )
    }

    fun isSuccessCardPaymentCompletedAlertDisplayed(): Boolean {
        waitUntilAlertIsDisplayed()
        return viewExists(
            CoreMatchers.allOf(
                successfulCardPaymentAlert,
                ViewMatchers.isDisplayed()
            )
        )
    }

    fun isSuccessGooglePaymentCompletedAlertDisplayed(): Boolean {
        waitUntilAlertIsDisplayed()
        return viewExists(
            CoreMatchers.allOf(
                successfulCardPaymentAlert,
                ViewMatchers.isDisplayed()
            )
        )
    }
    fun isUnauthenticatedPaymentAlertDisplayed(): Boolean {
        waitUntilAlertIsDisplayed()
        return viewExists(
            CoreMatchers.allOf(
                unauthenticatedPaymenAlert,
                ViewMatchers.isDisplayed()
            )
        )
    }

    fun isCardDetailsErrorAlertDisplayed(): Boolean {
        waitUntilAlertIsDisplayed()
        return viewExists(
            CoreMatchers.allOf(
                cardDetailsErrorAlert,
                ViewMatchers.isDisplayed()
            )
        )
    }
    fun tapOnPayButton(): PayByCardFormPage {
        Espresso.onView(submitButton).perform(ViewActions.closeSoftKeyboard())
        // additional wait for the Pay button after closing the keyboard; it happened that
        // a click failed because the button wasn't  displayed in at least 90%
        waitUntilSubmitButtonIsCompletelyDisplayed()
        Espresso.onView(submitButton).perform(ViewActions.click())
        return this
    }

    fun tapOnNuyWithGPayButton(): PayByCardFormPage {
        Espresso.onView(submitButton).perform(ViewActions.closeSoftKeyboard())
        // additional wait for the Pay button after closing the keyboard; it happened that
        // a click failed because the button wasn't  displayed in at least 90%
        waitUntilSubmitButtonIsCompletelyDisplayed()
        Espresso.onView(submitButton).perform(ViewActions.click())
        return this
    }


    fun viewExists(viewMatcher: Matcher<View>): Boolean {
        return try {
            Espresso.onView(viewMatcher).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            true
        } catch (e: PerformException) {
            false
        } catch (e: NoMatchingViewException) {
            false
        }
    }
    private fun waitUntilSubmitButtonIsCompletelyDisplayed() {
        waitUntilElementIsDisplayed(
            CoreMatchers.allOf(
                submitButton,
                ViewMatchers.isCompletelyDisplayed()
            )
        )
    }


}