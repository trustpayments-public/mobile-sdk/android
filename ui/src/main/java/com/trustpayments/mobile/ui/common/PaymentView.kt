package com.trustpayments.mobile.ui.common

/**
 * Common interface for annotating all ui components used by SDK
 */
interface PaymentView