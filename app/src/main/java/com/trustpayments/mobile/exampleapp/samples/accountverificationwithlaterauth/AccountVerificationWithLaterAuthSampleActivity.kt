package com.trustpayments.mobile.exampleapp.samples.accountverificationwithlaterauth

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.util.ResponseParser
import com.trustpayments.mobile.exampleapp.BuildConfig
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.common.base.BaseSampleActivity
import com.trustpayments.mobile.exampleapp.common.jwt.BasePayload
import com.trustpayments.mobile.exampleapp.common.jwt.CredentialsOnFile
import com.trustpayments.mobile.exampleapp.common.jwt.StandardPayload
import com.trustpayments.mobile.exampleapp.common.jwt.TokenizationPayload
import com.trustpayments.mobile.exampleapp.common.jwtexplorer.JWTEditorStep
import com.trustpayments.mobile.exampleapp.common.jwtexplorer.JWTEditorToken
import com.trustpayments.mobile.exampleapp.common.jwtexplorer.OnTokenChangedListener
import com.trustpayments.mobile.exampleapp.databinding.ActivityAccountVerificationWithLaterAuthBinding
import com.trustpayments.mobile.exampleapp.utils.Constants
import com.trustpayments.mobile.exampleapp.utils.DateHelper
import com.trustpayments.mobile.exampleapp.utils.Utils
import com.trustpayments.mobile.ui.model.RequestType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AccountVerificationWithLaterAuthSampleActivity : BaseSampleActivity() {

    private val viewBinding by lazy {
        ActivityAccountVerificationWithLaterAuthBinding.inflate(layoutInflater)
    }

    private var stepOnePayload = StandardPayload(
        siteReference = BuildConfig.SITE_REFERENCE,
        currencyISO3a = "GBP",
        baseAmount = 1050,
        requestTypeDescriptions = listOf(
            RequestType.ThreeDQuery.serializedName,
            RequestType.AccountCheck.serializedName
        )
    )

    private var stepTwoPayload = TokenizationPayload(
        siteReference = BuildConfig.SITE_REFERENCE,
        currencyISO3a = "GBP",
        baseAmount = 1050,
        requestTypeDescriptions = listOf(RequestType.Auth.serializedName),
        credentialsOnFile = CredentialsOnFile.UsePreviouslySaved,
        parentTransactionReference = "[DEFINED ON STEP 1]"
    )

    private var stepOneToken = json.encodeToString(
        StandardPayload.serializer(),
        stepOnePayload
    )
    private var stepTwoToken = json.encodeToString(
        TokenizationPayload.serializer(),
        stepTwoPayload
    )
    private lateinit var jwtEditorStepOne: JWTEditorStep
    private lateinit var jwtEditorStepTwo: JWTEditorStep

    override fun getPayload(): BasePayload? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(viewBinding.root)
        setSupportActionBar(viewBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewBinding.toolbar.setNavigationOnClickListener { finish() }
        viewBinding.btnRun.setOnClickListener { performRunClick() }
        setRequestTypeAndStatusText(getString(R.string.background_request_ready))
        setupJWTExplorerView()
    }

    private fun setupJWTExplorerView() {
        val requestJwt = getString(R.string.request_jwt)
        val responseJwt = getString(R.string.response_jwt)
        jwtEditorStepOne = JWTEditorStep(
            "step1", "STEP 1", listOf(
                JWTEditorToken("request", "step1", requestJwt, stepOneToken, true),
                JWTEditorToken("response", "step1", responseJwt, "[]", false)
            )
        )
        jwtEditorStepTwo = JWTEditorStep(
            "step2", "STEP 2", listOf(
                JWTEditorToken("request", "step2", requestJwt, stepTwoToken, true),
                JWTEditorToken("response", "step2", responseJwt, "[]", false)
            )
        )
        viewBinding.jwtExplorerView.setSteps(listOf(jwtEditorStepOne, jwtEditorStepTwo))
        viewBinding.jwtExplorerView.setOnTokenChangedListener(object : OnTokenChangedListener {
            override fun onTokenChanged(token: JWTEditorToken) {
                if (token.stepId == "step1") {
                    stepOneToken = token.token
                } else {
                    stepTwoToken = token.token
                }
            }
        })
    }

    private fun setRequestTypeAndStatusText(status: String) {
        viewBinding.status.text = getString(R.string.background_request_status, status)
        viewBinding.stepOne.text = getString(
            R.string.account_verification_with_later_auth_step, 1,
            stepOnePayload.requestTypeDescriptions.reduce { acc, s -> "$acc + $s" }
        )
        viewBinding.stepTwo.text = getString(
            R.string.account_verification_with_later_auth_step, 2,
            stepTwoPayload.requestTypeDescriptions.reduce { acc, s -> "$acc + $s" }
        )
    }

    private fun setExecutingSessionUIState() {
        setRequestTypeAndStatusText(getString(R.string.background_request_runnig))
        viewBinding.btnRun.isEnabled = false
        viewBinding.btnRun.text = ""
        viewBinding.loadingIndicator.isVisible = true
    }

    private fun createStepOneFinalPayloadObject(token: String) = try {
        json.decodeFromString(StandardPayload.serializer(), token)
    } catch (e: Exception) {
        null
    }

    private fun createStepTwoFinalPayloadObject(token: String) = try {
        json.decodeFromString(TokenizationPayload.serializer(), token)
    } catch (e: Exception) {
        null
    }

    private fun performRunClick() {
        // Prepare payload object for the first session
        stepOnePayload = createStepOneFinalPayloadObject(stepOneToken) ?: return showDialog(
            getString(R.string.error),
            getString(R.string.payload_error)
        )

        // Prepare payload object for the second session
        stepTwoPayload = createStepTwoFinalPayloadObject(stepTwoToken) ?: return showDialog(
            getString(R.string.error),
            getString(R.string.failed_to_create_step_two_payload)
        )

        // Set UI state before executing the first session
        setExecutingSessionUIState()

        lifecycleScope.launch {
            // Execute first session
            val sessionToken = getToken(stepOnePayload) ?: return@launch
            val result = executeStrepOneSession(sessionToken) ?: return@launch
            // Execute second session
            executeStepTwoSession(result)
        }
    }

    // This method is used to execute the first session to get parent transaction reference
    private suspend fun executeStrepOneSession(sessionToken: String) = executeSession(
        sessionToken, jwtEditorStepOne, 0,
        showEndSessionDialog = false,
        showDialogOnError = true,
        cardNumber = Constants.TEST_CARD_NUM,
        cardExpiryDate = DateHelper.getValidCardExpiryDate(),
        cardCvv = Constants.TEST_CARD_CVV
    )

    // This method is used to execute the second session using the parent transaction reference
    private suspend fun executeStepTwoSession(stepOneResponse: PaymentTransactionManager.Response) {
        // Set parent transaction reference for the second session
        val parsedResponse = ResponseParser.parse(stepOneResponse.responseJwtList) ?: return
        val customerOutput = parsedResponse.firstOrNull()?.customerOutput
        val parentTransaction = customerOutput?.transactionReference
        stepTwoPayload.parentTransactionReference = parentTransaction ?: return

        // Update JWTExplorerView adding new step 2 payload with parent transaction reference
        val jwtPayload = json.encodeToString(TokenizationPayload.serializer(), stepTwoPayload)
        jwtEditorStepTwo.tokens.first { it.id == "request" }.token = jwtPayload

        // Move to the second step to show parent transaction reference
        viewBinding.jwtExplorerView.moveStepTab(1)

        // Get session token for the second session
        val sessionTokenTwo = getToken(stepTwoPayload) ?: return

        // Proceed to execute the second session
        executeSession(
            sessionTokenTwo, jwtEditorStepTwo, 1,
            showEndSessionDialog = true,
            showDialogOnError = true
        )
    }

    private suspend fun executeSession(
        sessionToken: String,
        jwtEditorStep: JWTEditorStep,
        stepIndex: Int,
        showEndSessionDialog: Boolean,
        showDialogOnError: Boolean,
        cardNumber: String? = null,
        cardExpiryDate: String? = null,
        cardCvv: String? = null
    ) = withContext(Dispatchers.IO) {
        // Create a new session with relevant parameters for the session
        val session = paymentTransactionManager.createSession(
            { sessionToken },
            cardNumber,
            cardExpiryDate,
            cardCvv
        )

        // Execute the payment session with relevant parameters for the session
        val result = paymentTransactionManager.executeSession(
            session
        ) { this@AccountVerificationWithLaterAuthSampleActivity }

        // Get error message
        val errorMessage =
            Utils.findExecuteSessionErrorMessage(
                result,
                this@AccountVerificationWithLaterAuthSampleActivity
            )

        // Get status message
        val statusMessage = errorMessage ?: getString(R.string.hs_transaction_success)

        // Get error data or response data
        val jwtResponseData = Utils.findExecuteSessionErrorData(
            result,
            this@AccountVerificationWithLaterAuthSampleActivity
        ) ?: Utils.jwtStringListToDecodedJsonArrayString(result.responseJwtList)

        // Set UI state after completing the session
        launch(Dispatchers.Main) {
            jwtEditorStep.tokens.last { it.id == "response" }.token = jwtResponseData
            viewBinding.jwtExplorerView.moveStepTab(stepIndex)
            viewBinding.jwtExplorerView.moveTokenTab(1)
            setRequestTypeAndStatusText(statusMessage)
            handleDialog(showEndSessionDialog, showDialogOnError, errorMessage)
        }

        // On Error, return null to cancel the execution of the next session
        if (errorMessage != null) return@withContext null

        result
    }

    private fun handleDialog(
        showEndSessionDialog: Boolean,
        showDialogOnError: Boolean,
        errorMessage: String?
    ) {
        if (showEndSessionDialog || (showDialogOnError && errorMessage != null)) {
            val dialogTitle = errorMessage?.let { getString(R.string.hs_transaction_failed) }
                ?: getString(R.string.hs_transaction_success)

            val dialogMessage = errorMessage
                ?: getString(R.string.hs_transaction_successfully_completed)

            showDialog(dialogTitle, dialogMessage)
            viewBinding.btnRun.text = getString(R.string.run)
            viewBinding.btnRun.isEnabled = true
            viewBinding.loadingIndicator.isVisible = false
        }
    }
}
