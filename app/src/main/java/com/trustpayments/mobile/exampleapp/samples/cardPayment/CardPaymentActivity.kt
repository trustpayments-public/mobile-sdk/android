package com.trustpayments.mobile.exampleapp.samples.cardPayment

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.trustpayments.mobile.core.services.api.TrustPaymentsGatewayType
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.exampleapp.BuildConfig
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.common.jwtexplorer.JWTEditorToken
import com.trustpayments.mobile.exampleapp.common.jwtexplorer.OnTokenChangedListener
import com.trustpayments.mobile.exampleapp.databinding.ActivityCardPaymentBinding
import com.trustpayments.mobile.exampleapp.utils.Utils
import com.trustpayments.mobile.exampleapp.utils.Utils.hideKeyboard
import com.trustpayments.mobile.ui.dropin.DropInPaymentView
import com.trustpayments.mobile.ui.model.PaymentInputType

class CardPaymentActivity : AppCompatActivity(), DropInPaymentView.DropInPaymentViewListener {

    private val viewBinding by lazy {
        ActivityCardPaymentBinding.inflate(layoutInflater)
    }

    // PaymentTransactionManager responsible for executing the transaction.
    private val paymentTransactionManager by lazy {
        PaymentTransactionManager(
            context = this@CardPaymentActivity,
            gatewayType = TrustPaymentsGatewayType.EU,
            isCardinalLive = false,
            merchantUsername = BuildConfig.MERCHANT_USERNAME,
            isLocationDataConsentGiven = true,
        )
    }
    private val viewModel: CardPaymentViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(viewBinding.root)
        setSupportActionBar(viewBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewBinding.toolbar.setNavigationOnClickListener { finish() }
        setUiElements()
        setObservers()
    }

    private fun setUiElements() {
        viewBinding.apply {
            jwtExplorer.apply {
                setOnTokenChangedListener(
                    object :
                        OnTokenChangedListener {
                        override fun onTokenChanged(token: JWTEditorToken) {
                            viewModel.onPayloadChanged(token)
                        }
                    },
                )
            }
            dropInPaymentView.apply {
                dropInPaymentViewListener = this@CardPaymentActivity
            }
        }
    }

    private fun setObservers() {
        viewBinding.apply {
            viewModel.requestPayload.observe(this@CardPaymentActivity) {
                jwtExplorer.setRequestJwtToken(it)
            }
            viewModel.responsePayload.observe(this@CardPaymentActivity) {
                jwtExplorer.apply {
                    setResponseJwtToken(it)
                    it.takeIf { it.isNotBlank() }?.also {
                        moveTokenTab(1)
                    }
                }
            }
            viewModel.uiState.observe(this@CardPaymentActivity) {
                when (it) {
                    is CardPaymentViewModel.UIState.Loading -> {
                        progressBar.isVisible = true
                    }

                    is CardPaymentViewModel.UIState.Idle -> {
                        progressBar.isVisible = false
                    }

                    is CardPaymentViewModel.UIState.Success -> {
                        progressBar.isVisible = false
                        Utils.showDialog(
                            context = this@CardPaymentActivity,
                            title = getString(R.string.hs_transaction_success),
                            message = getString(R.string.payment_executed_success),
                        )
                    }

                    is CardPaymentViewModel.UIState.Error -> {
                        progressBar.isVisible = false
                        Utils.showDialog(
                            context = this@CardPaymentActivity,
                            title = getString(R.string.error),
                            message = it.message,
                        )
                    }
                }
            }
        }
    }

    override fun onInputValid(
        paymentInputType: PaymentInputType,
        input: String,
    ) {
        viewModel.onUpdateCardData(paymentInputType, input)
    }

    override fun onPayButtonClicked() {
        hideKeyboard(viewBinding.root)
        viewModel.onPayButtonClicked(
            context = this@CardPaymentActivity,
            paymentTransactionManager = paymentTransactionManager,
            activityProvider = {
                this@CardPaymentActivity
            },
        )
    }
}
