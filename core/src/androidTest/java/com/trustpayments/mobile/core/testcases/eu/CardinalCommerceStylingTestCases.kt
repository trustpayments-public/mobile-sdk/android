package com.trustpayments.mobile.core.testcases.eu

import android.content.Context
import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.rule.ActivityTestRule
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.PaymentSession
import com.trustpayments.mobile.core.R
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.testutils.createSessionFor
import com.trustpayments.mobile.core.testutils.waitUntilElementIsDisplayed
import com.trustpayments.mobile.core.ui.CardinalStyleManager
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.utils.ThreeDQueryTestActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CardinalCommerceStylingTestCases : BaseTestCase() {

    private val cardinalStyleManager = CardinalStyleManager(
        resendButtonStyle = CardinalStyleManager.ButtonStyle(
            textColor = "#FFFFFF",
            backgroundColor = "#5585A2",
            cornerRadius = 150
        ),
        labelStyle = CardinalStyleManager.LabelStyle(
            headingTextColor = "#660000",
            headingTextFontSize = 35,
            textColor = "#0086B3"
        ),
        toolbarStyle = CardinalStyleManager.ToolbarStyle(
            backgroundColor = "#003759",
            buttonText = "CANCEL",
            headerText = "DEMO CHECKOUT",
            cancelButtonTextColor = "#FFFFFF"
        ),
        textBoxStyle = CardinalStyleManager.TextBoxStyle(
            borderColor = "#000000",
            borderWidth = 5,
            cornerRadius = 1,
            textColor = "#00FF2A"
        )
    )

    private val cardinalDarkThemeStyleManager = CardinalStyleManager(
        resendButtonStyle = CardinalStyleManager.ButtonStyle(
            textColor = "#FFFFFF",
            backgroundColor = "#BB86FC",
            cornerRadius = 50
        ),
        labelStyle = CardinalStyleManager.LabelStyle(
            headingTextColor = "#004966",
            headingTextFontSize = 24,
            textColor = "#466A78"
        ),
        toolbarStyle = CardinalStyleManager.ToolbarStyle(
            backgroundColor = "#121212",
            buttonText = "Cancel",
            headerText = "SECURE CHECKOUT",
            cancelButtonTextColor = "#FFFFFF"
        ),
        textBoxStyle = CardinalStyleManager.TextBoxStyle(
            borderColor = "#BB86FC",
            borderWidth = 2,
            textColor = "#000000"
        )
    )

    private lateinit var paymentTransactionManager: PaymentTransactionManager

    private lateinit var jwtToken: String

    private lateinit var activity: ThreeDQueryTestActivity

    private lateinit var session: PaymentSession

    @get:Rule
    val initialActivityTestRule = ActivityTestRule(ThreeDQueryTestActivity::class.java)

    private val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    @Before
    override fun setUp() {
        super.setUp()

        val app = ApplicationProvider.getApplicationContext<Context>().applicationContext

        paymentTransactionManager =
            PaymentTransactionManager(
                app,
                getGatewayType(),
                false,
                BuildConfig.MERCHANT_USERNAME,
                cardinalStyleManager,
                cardinalDarkThemeStyleManager,
                isLocationDataConsentGiven = false
            )

        jwtToken = jwtBuilder.getFor3DSecure(listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        ))

        activity = initialActivityTestRule.activity

        session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001096", "123")
    }

    @Test
    fun test_resendButtonStyle(): Unit = runBlocking {
        // GIVEN
        val expectedBackgroundColor = Color.parseColor("#5585A2")
        val expectedTextColor = Color.parseColor("#FFFFFF")
        val expectedCornerRadius = 150.0f
        var actualBackgroundColor = 0
        var actualTextColor = 0
        var actualCornerRadius = 0.0f

        // WHEN
        paymentTransactionManager.isDarkThemeForced = false
        val job = launch(Dispatchers.Unconfined) {
            paymentTransactionManager.executeSession(session) { activity }
        }

        waitUntilACSPopupIsDisplayed()
        Espresso.onView(ViewMatchers.withId(R.id.resendInfoButton)).check { view, _ ->
            actualBackgroundColor = view.getButtonBackground()
            actualTextColor = (view as Button).textColors.defaultColor
            actualCornerRadius = view.getCornerRadius()
        }
        Espresso.onView(ViewMatchers.withId(R.id.toolbarButton))
            .perform(ViewActions.closeSoftKeyboard(), ViewActions.click())

        job.join()

        // THEN
        assertEquals(expectedBackgroundColor, actualBackgroundColor)
        assertEquals(expectedTextColor, actualTextColor)
        assertEquals(expectedCornerRadius, actualCornerRadius)
    }

    @Test
    fun test_resendButtonStyleInDarkTheme(): Unit = runBlocking {
        // GIVEN
        val expectedBackgroundColor = Color.parseColor("#BB86FC")
        val expectedTextColor = Color.parseColor("#FFFFFF")
        val expectedCornerRadius = 50.0f
        var actualBackgroundColor = 0
        var actualTextColor = 0
        var actualCornerRadius = 0.0f

        // WHEN
        paymentTransactionManager.isDarkThemeForced = true
        val job = launch(Dispatchers.Unconfined) {
            paymentTransactionManager.executeSession(session) { activity }
        }

        waitUntilACSPopupIsDisplayed()
        Espresso.onView(ViewMatchers.withId(R.id.resendInfoButton)).check { view, _ ->
            actualBackgroundColor = view.getButtonBackground()
            actualTextColor = (view as Button).textColors.defaultColor
            actualCornerRadius = view.getCornerRadius()
        }
        Espresso.onView(ViewMatchers.withId(R.id.toolbarButton))
            .perform(ViewActions.closeSoftKeyboard(), ViewActions.click())

        job.join()

        // THEN
        assertEquals(expectedBackgroundColor, actualBackgroundColor)
        assertEquals(expectedTextColor, actualTextColor)
        assertEquals(expectedCornerRadius, actualCornerRadius)
    }

    @Test
    fun test_labelStyle(): Unit = runBlocking {
        // GIVEN
        val expectedHeaderTextColor = Color.parseColor("#660000")
        val expectedHeaderTextSize = 35.0f.fromSpToPx()
        val expectedTextColor = Color.parseColor("#0086B3")
        var actualHeaderTextColor = 0
        var actualHeaderTextSize = 0.0f
        var actualTextColor = 0

        // WHEN
        paymentTransactionManager.isDarkThemeForced = false
        val job = launch(Dispatchers.Unconfined) {
            paymentTransactionManager.executeSession(session) { activity }
        }

        waitUntilACSPopupIsDisplayed()
        Espresso.onView(ViewMatchers.withId(R.id.challengeInfoHeaderTextView)).check { view, _ ->
            actualHeaderTextColor = (view as TextView).textColors.defaultColor
            actualHeaderTextSize = view.textSize
        }
        Espresso.onView(ViewMatchers.withId(R.id.challengeInfoTextView)).check { view, _ ->
            actualTextColor = (view as TextView).textColors.defaultColor
        }
        Espresso.onView(ViewMatchers.withId(R.id.toolbarButton))
            .perform(ViewActions.closeSoftKeyboard(), ViewActions.click())

        job.join()

        // THEN
        assertEquals(expectedHeaderTextColor, actualHeaderTextColor)
        assertEquals(expectedHeaderTextSize, actualHeaderTextSize)
        assertEquals(expectedTextColor, actualTextColor)
    }

    @Test
    fun test_labelStyleInDarkTheme(): Unit = runBlocking {
        // GIVEN
        val expectedHeaderTextColor = Color.parseColor("#004966")
        val expectedHeaderTextSize = 24.0f.fromSpToPx()
        val expectedTextColor = Color.parseColor("#466A78")
        var actualHeaderTextColor = 0
        var actualHeaderTextSize = 0.0f
        var actualTextColor = 0

        // WHEN
        paymentTransactionManager.isDarkThemeForced = true
        val job = launch(Dispatchers.Unconfined) {
            paymentTransactionManager.executeSession(session) { activity }
        }

        waitUntilACSPopupIsDisplayed()
        Espresso.onView(ViewMatchers.withId(R.id.challengeInfoHeaderTextView)).check { view, _ ->
            actualHeaderTextColor = (view as TextView).textColors.defaultColor
            actualHeaderTextSize = view.textSize
        }
        Espresso.onView(ViewMatchers.withId(R.id.challengeInfoTextView)).check { view, _ ->
            actualTextColor = (view as TextView).textColors.defaultColor
        }
        Espresso.onView(ViewMatchers.withId(R.id.toolbarButton))
            .perform(ViewActions.closeSoftKeyboard(), ViewActions.click())

        job.join()

        // THEN
        assertEquals(expectedHeaderTextColor, actualHeaderTextColor)
        assertEquals(expectedHeaderTextSize, actualHeaderTextSize)
        assertEquals(expectedTextColor, actualTextColor)
    }

    @Test
    fun test_toolbarStyle(): Unit = runBlocking {
        // GIVEN
        val expectedBackgroundColor = Color.parseColor("#003759")
        val expectedToolbarHeaderText = "DEMO CHECKOUT"
        val expectedCancelButtonText = ""
        // Cardinal SDK has overridden Cancel Button get text behaviour.
        var actualBackgroundColor = 0
        var actualToolbarHeaderText = ""
        var actualCancelButtonText = ""

        // WHEN
        paymentTransactionManager.isDarkThemeForced = false
        val job = launch(Dispatchers.Unconfined) {
            paymentTransactionManager.executeSession(session) { activity }
        }

        waitUntilACSPopupIsDisplayed()
        Espresso.onView(ViewMatchers.withId(R.id.toolbar)).check { view, _ ->
            actualBackgroundColor = (view.background as ColorDrawable).color
            actualToolbarHeaderText = (view as Toolbar).title.toString()
        }
        Espresso.onView(ViewMatchers.withId(R.id.toolbarButton)).check { view, _ ->
            actualCancelButtonText = (view as TextView).text.toString()
        }.perform(ViewActions.closeSoftKeyboard(), ViewActions.click())

        job.join()

        // THEN
        assertEquals(expectedBackgroundColor, actualBackgroundColor)
        assertEquals(expectedToolbarHeaderText, actualToolbarHeaderText)
        assertEquals(expectedCancelButtonText, actualCancelButtonText)
    }

    @Test
    fun test_toolbarStyleInDarkTheme(): Unit = runBlocking {
        // GIVEN
        val expectedBackgroundColor = Color.parseColor("#121212")
        val expectedToolbarHeaderText = "SECURE CHECKOUT"
        val expectedCancelButtonText = ""
        // Cardinal SDK has overridden Cancel Button get text behaviour.
        var actualBackgroundColor = 0
        var actualToolbarHeaderText = ""
        var actualCancelButtonText = ""

        // WHEN
        paymentTransactionManager.isDarkThemeForced = true
        val job = launch(Dispatchers.Unconfined) {
            paymentTransactionManager.executeSession(session) { activity }
        }

        waitUntilACSPopupIsDisplayed()
        Espresso.onView(ViewMatchers.withId(R.id.toolbar)).check { view, _ ->
            actualBackgroundColor = (view.background as ColorDrawable).color
            actualToolbarHeaderText = (view as Toolbar).title.toString()
        }
        Espresso.onView(ViewMatchers.withId(R.id.toolbarButton)).check { view, _ ->
            actualCancelButtonText = (view as TextView).text.toString()
        }.perform(ViewActions.closeSoftKeyboard(), ViewActions.click())

        job.join()

        // THEN
        assertEquals(expectedBackgroundColor, actualBackgroundColor)
        assertEquals(expectedToolbarHeaderText, actualToolbarHeaderText)
        assertEquals(expectedCancelButtonText, actualCancelButtonText)
    }

    @Test
    fun test_textBoxStyle(): Unit = runBlocking {
        // GIVEN
        val expectedInputTextColor = Color.parseColor("#00FF2A")
        val expectedCornerRadius = 1.0f
        var actualInputTextColor = 0
        var actualCornerRadius = 0.0f

        // WHEN
        paymentTransactionManager.isDarkThemeForced = false
        val job = launch(Dispatchers.Unconfined) {
            paymentTransactionManager.executeSession(session) { activity }
        }

        waitUntilACSPopupIsDisplayed()
        Espresso.onView(ViewMatchers.withId(R.id.codeEditTextField)).check { view, _ ->
            actualInputTextColor = (view as TextView).textColors.defaultColor
            actualCornerRadius = view.getCornerRadius()
        }
        Espresso.onView(ViewMatchers.withId(R.id.toolbarButton))
            .perform(ViewActions.closeSoftKeyboard(), ViewActions.click())

        job.join()

        // THEN
        assertEquals(expectedInputTextColor, actualInputTextColor)
        assertEquals(expectedCornerRadius, actualCornerRadius)
    }

    @Test
    fun test_textBoxStyleInDarkTheme(): Unit = runBlocking {
        // GIVEN
        val expectedInputTextColor = Color.parseColor("#000000")
        var actualInputTextColor = 0

        // WHEN
        paymentTransactionManager.isDarkThemeForced = true
        val job = launch(Dispatchers.Unconfined) {
            paymentTransactionManager.executeSession(session) { activity }
        }

        waitUntilACSPopupIsDisplayed()
        Espresso.onView(ViewMatchers.withId(R.id.codeEditTextField)).check { view, _ ->
            actualInputTextColor = (view as TextView).textColors.defaultColor
        }
        Espresso.onView(ViewMatchers.withId(R.id.toolbarButton))
            .perform(ViewActions.closeSoftKeyboard(), ViewActions.click())

        job.join()

        // THEN
        assertEquals(expectedInputTextColor, actualInputTextColor)
    }

    private fun waitUntilACSPopupIsDisplayed() =
        waitUntilElementIsDisplayed(
            ViewMatchers.withId(R.id.codeEditTextField)
        )

    private fun View.getButtonBackground() =
        ((this.background as GradientDrawable).color as ColorStateList).defaultColor

    private fun View.getCornerRadius() =
        (this.background as GradientDrawable).cornerRadius

    private fun Float.fromSpToPx(): Float {
        return (this * Resources.getSystem().displayMetrics.scaledDensity)
    }
}