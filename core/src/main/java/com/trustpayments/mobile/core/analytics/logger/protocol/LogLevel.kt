package com.trustpayments.mobile.core.analytics.logger.protocol

/**
 * Logging level associated with analytic event.
 * */
enum class LogLevel {
    DEBUG, INFO, WARNING, ERROR, FATAL
}
