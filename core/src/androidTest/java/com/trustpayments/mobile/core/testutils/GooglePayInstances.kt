package com.trustpayments.mobile.core.testutils

import android.app.Activity
import com.google.android.gms.wallet.WalletConstants
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.googlepay.TPGooglePayManager

object GooglePayInstances {

    fun billingAddressMin(activity: Activity) = TPGooglePayManager.Builder(
            activity,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
    )
            .setBillingAddressRequired(true, "MIN", false)
            .setMerchantName(BuildConfig.MERCHANT_USERNAME)
            .build()

    fun billingAddressFull(activity: Activity) = TPGooglePayManager.Builder(
            activity,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
    )
            .setBillingAddressRequired(true, "FULL", false)
            .build()

    fun billingAddressPhone(activity: Activity) = TPGooglePayManager.Builder(
            activity,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
    )
            .setBillingAddressRequired(true, "FULL", true)
            .build()

    fun customMerchantName(activity: Activity, merchantName : String) = TPGooglePayManager.Builder(
            activity,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
    )
            .setMerchantName(merchantName)
            .build()

    fun customRequestCode(activity: Activity, requestCode : Int) = TPGooglePayManager.Builder(
            activity,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
    )
            .setRequestCode(requestCode)
            .build()

    fun shippingAddressGB(activity: Activity) = TPGooglePayManager.Builder(
            activity,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
    )
            .setShippingAddressRequired(true, listOf("GB"), false)
            .build()

    fun shippingAddressPhone(activity: Activity) = TPGooglePayManager.Builder(
            activity,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
    )
            .setShippingAddressRequired(true, listOf("GB"), true)
            .build()

    fun supportedNetworkVisaOnly(activity: Activity) = TPGooglePayManager.Builder(
            activity,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
    )
            .setAllowedCardNetworks(listOf("VISA"))
            .build()

}