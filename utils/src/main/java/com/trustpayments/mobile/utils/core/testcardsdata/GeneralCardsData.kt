package com.trustpayments.mobile.utils.core.testcardsdata

object GeneralCardsData {
    val correctExpiryDate = "12/32"
    val correct3digitsCVV = "123"
    val correct4digitsCVV = "1234"
    val incorrect2digitsCVV = "12"
    val incorrectExpiryDate = "12/19"
    val correctVisaCardNumber = "4111111111111111"
    val correctMastercardCardNumber = "5100000000000511"
    val correctAmexCardNumber = "340000000001080"
    val failedCardNumber = "4111100000000"
    val incorrectCardNumber = "400000000000"
}