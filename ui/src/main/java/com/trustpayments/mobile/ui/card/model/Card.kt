package com.trustpayments.mobile.ui.card.model

data class Card(
    val pan: String,
    val expiryDate: String,
    val cvv: String,
    val cardType: CardType
)