package com.trustpayments.mobile.core.util

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.trustpayments.mobile.core.models.api.response.RequestType
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.security.Keys
import java.util.*
import javax.crypto.SecretKey

class JWTBuilder(
    private val merchantUsername: String,
    private val siteReference: String,
    private val jwtKey: String
) {
    fun getForRawRequestTypes(requestTypes: List<String>) =
        buildJWT(
            merchantUsername,
            StandardTPPayload(
                siteReference,
                "GBP",
                1050,
                requestTypes,
                null,
                null,
                null
            )
        )

    fun getStandard(
        requestTypes: List<RequestType>, credentialsOnFile: CredentialsOnFile? = null,
        parentTransactionReference: String? = null, bypassCards: List<String>? = null
    ) =
        buildJWT(
            merchantUsername,
            StandardTPPayload(
                siteReference,
                "GBP",
                1050,
                requestTypes.map { it.serializedName },
                credentialsOnFile,
                parentTransactionReference,
                locale = null,
                bypassCards
            )
        )

    fun getStandardWithMainAmountAndAddress(
        requestTypes: List<RequestType>,
        credentialsOnFile: CredentialsOnFile? = null,
        parentTransactionReference: String? = null,
        bypassCards: List<String>? = null,
        billingprefixname: String? = null,
        billingfirstname: String? = null,
        billingmiddlename: String? = null,
        billinglastname: String? = null,
        billingsuffixname: String? = null,
        billingstreet: String? = null,
        billingtown: String? = null,
        billingcounty: String? = null,
        billingcountryiso2a: String? = null,
        billingpostcode: String? = null,
        billingemail: String? = null,
        billingtelephone: String? = null,
        customerprefixname: String? = null,
        customerfirstname: String? = null,
        customermiddlename: String? = null,
        customerlastname: String? = null,
        customersuffixname: String? = null,
        customerstreet: String? = null,
        customertown: String? = null,
        customercounty: String? = null,
        customercountryiso2a: String? = null,
        customerpostcode: String? = null,
        customeremail: String? = null,
        customertelephone: String? = null
    ) =
        buildJWT(
            merchantUsername,
            MainAmountAddressTPPayload(
                siteReference,
                "GBP",
                10.50,
                requestTypes.map { it.serializedName },
                credentialsOnFile,
                parentTransactionReference,
                locale = null,
                bypassCards,
                billingprefixname,
                billingfirstname,
                billingmiddlename,
                billinglastname,
                billingsuffixname,
                billingstreet,
                billingtown,
                billingcounty,
                billingcountryiso2a,
                billingpostcode,
                billingemail,
                billingtelephone,
                customerprefixname,
                customerfirstname,
                customermiddlename,
                customerlastname,
                customersuffixname,
                customerstreet,
                customertown,
                customercounty,
                customercountryiso2a,
                customerpostcode,
                customeremail,
                customertelephone
            )
        )

    fun getForSubscription(
        requestTypes: List<RequestType>,
        cardsToBypass: List<String>? = null,
        currency: String = "GBP"
    ) =
        buildJWT(
            merchantUsername,
            SubscriptionTPPayload(
                siteReference,
                currency,
                1050,
                SubscriptionUnit.Month,
                1,
                12,
                SubscriptionType.Recurring,
                requestTypes.map { it.serializedName },
                cardsToBypass = cardsToBypass
            )
        )

    fun getForInitialRecurringPayment(requestTypes: List<RequestType>) =
        buildJWT(
            merchantUsername,
            InitialRecurringPaymentTPPayload(
                siteReference,
                "GBP",
                1000,
                SubscriptionType.Recurring,
                requestTypes.map { it.serializedName }
            )
        )

    fun getForNextRecurringPayment(reference: String, requestTypes: List<RequestType>) =
        buildJWT(
            merchantUsername,
            NextRecurringPaymentTPPayload(
                siteReference,
                2,
                SubscriptionType.Recurring,
                reference,
                requestTypes.map { it.serializedName }
            )
        )

    fun getFor3DSecure(requestTypes: List<RequestType>, baseAmount: Int? = null) =
        buildJWT(
            merchantUsername,
            StandardTPPayload(
                siteReference,
                "GBP",
                baseAmount ?: 1050,
                requestTypes.map { it.serializedName }
            )
        )

    fun getForRiskDec(baseAmount: Int, requestTypes: List<RequestType>) =
        buildJWT(
            merchantUsername,
            StandardTPPayload(
                siteReference,
                "GBP",
                baseAmount,
                requestTypes.map { it.serializedName }
            )
        )

    fun getForRiskDecWithSubscription(baseAmount: Int, requestTypes: List<RequestType>) =
        buildJWT(
            merchantUsername,
            SubscriptionTPPayload(
                siteReference,
                "GBP",
                baseAmount,
                SubscriptionUnit.Month,
                1,
                12,
                SubscriptionType.Recurring,
                requestTypes.map { it.serializedName }
            )
        )

    fun getStandardWithPares(
        parentTransaction: String,
        pares: String,
        requestTypes: List<RequestType>
    ) =
        buildJWT(
            merchantUsername,
            ThreeDQV1TPPayload(
                siteReference,
                "GBP",
                1050,
                parentTransaction,
                pares,
                requestTypes.map { it.serializedName }
            )
        )

    fun getStandardWith3DResponse(
        parentTransaction: String,
        threeDResponse: String,
        requestTypes: List<RequestType>
    ) =
        buildJWT(
            merchantUsername,
            ThreeDQV2TPPayload(
                siteReference,
                "GBP",
                1050,
                parentTransaction,
                threeDResponse,
                requestTypes.map { it.serializedName }
            )
        )


    fun getForLocale(requestTypes: List<RequestType>, locale: String? = null) =
        buildJWT(
            merchantUsername,
            StandardTPPayload(
                siteReference,
                "GBP",
                1050,
                requestTypes.map { it.serializedName },
                locale = locale
            )
        )

    fun getForGooglePay(
        walletToken: String,
        requestTypes: List<RequestType>,
        baseAmount: Int = 1050,
        billingContactDetailsOverride: Int = 0,
        customerContactDetailsOverride: Int = 0,
        billingEmail: String? = null,
        billingFirstName: String? = null,
        billingPostCode: String? = null,
        customerEmail: String? = null,
        customerFirstName: String? = null,
        customerPostCode: String? = null,
    ) =
        buildJWT(
            merchantUsername,
            GooglePayTPPayload(
                siteReference,
                "GBP",
                baseAmount,
                requestTypes.map { it.serializedName },
                walletToken,
                billingContactDetailsOverride = billingContactDetailsOverride,
                customerContactDetailsOverride = customerContactDetailsOverride,
                billingEmail = billingEmail,
                customerEmail = customerEmail,
                billingFirstName = billingFirstName,
                billingPostCode = billingPostCode,
                customerFirstName = customerFirstName,
                customerPostCode = customerPostCode
            )
        )

    fun getForGooglePaySubscription(
        walletToken: String,
        requestTypes: List<RequestType>,
        baseAmount: Int = 1050
    ) =
        buildJWT(
            merchantUsername,
            GooglePaySubscriptionTPPayload(
                siteReference,
                "GBP",
                baseAmount,
                SubscriptionUnit.Month,
                1,
                12,
                SubscriptionType.Recurring,
                requestTypes.map { it.serializedName },
                walletToken
            )
        )

    private fun buildJWT(
        merchantUsername: String,
        payload: TPPayload
    ): String {
        val key: SecretKey = Keys.hmacShaKeyFor(jwtKey.toByteArray())
        val header = Jwts.header().setType("JWT")

        return Jwts.builder()
            .setHeader(header)
            .setIssuer(merchantUsername)
            .setIssuedAt(Date())
            .claim(
                "payload",
                payload.serializeObjectToMap(Gson().newBuilder().setFieldNamingStrategy {
                    it.name.toLowerCase(Locale.ROOT)
                }.create())
            )
            .signWith(key, SignatureAlgorithm.HS256)
            .compact()
    }

    fun buildResponseJWT(payload: BasePayload): String {
        val key: SecretKey = Keys.hmacShaKeyFor(jwtKey.toByteArray())
        val header = Jwts.header().setType("JWT")

        return Jwts.builder()
            .setHeader(header)
            .setIssuer(merchantUsername)
            .setIssuedAt(Date())
            .claim(
                "payload",
                payload.serializeObjectToMap(Gson().newBuilder().setFieldNamingStrategy {
                    it.name.toLowerCase(Locale.ROOT)
                }.create())
            )
            .signWith(key, SignatureAlgorithm.HS256)
            .compact()
    }

    fun <InputType> InputType.serializeObjectToMap(gson: Gson): Map<String, Any> {
        return convert(gson)
    }

    inline fun <InputType, reified OutputType> InputType.convert(gson: Gson): OutputType {
        val json = gson.toJson(this)

        return gson.fromJson(json, object : TypeToken<OutputType>() {}.type)
    }
}