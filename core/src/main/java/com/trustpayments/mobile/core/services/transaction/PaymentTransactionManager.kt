package com.trustpayments.mobile.core.services.transaction

import android.app.Activity
import android.content.Context
import androidx.annotation.WorkerThread
import com.google.gson.Gson
import com.trustpayments.mobile.core.analytics.logger.protocol.LogLevel
import com.trustpayments.mobile.core.PaymentSession
import com.trustpayments.mobile.core.models.DeviceSafetyWarning
import com.trustpayments.mobile.core.models.api.jwt.JwtToken
import com.trustpayments.mobile.core.models.api.response.CustomerOutput
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.models.api.response.ResponseErrorCode
import com.trustpayments.mobile.core.services.api.TrustPaymentsApiService
import com.trustpayments.mobile.core.services.api.TrustPaymentsGatewayType
import com.trustpayments.mobile.core.services.transaction.ErrorMessages.*
import com.trustpayments.mobile.core.ui.CardinalStyleManager
import kotlinx.coroutines.runBlocking


class PaymentTransactionManager(
    context: Context,
    gatewayType: TrustPaymentsGatewayType,
    isCardinalLive: Boolean,
    private val merchantUsername: String,
    cardinalStyleManager: CardinalStyleManager? = null,
    cardinalDarkThemeStyleManager: CardinalStyleManager? = null,
    isLocationDataConsentGiven: Boolean
) {
    internal var isCardinalLive: Boolean = isCardinalLive
        @JvmSynthetic get
        @JvmSynthetic set

    /**
     * Parameter indicating whenever a merchant gives an option inside his app to override a default
     * system theme (e.g. form the settings inside an app). In that case, to have a possibility of
     * applying correct theme to the Cardinal challenge pop-up, this param has to be set to:
     *
     * * _true_ - when the dark theme is set form the local app settings,
     * * _false_ - when the light theme is set,
     * * _null_  - in case of controlling the theme by the system.
     *
     * It's a merchant responsibility to handle correct value assignment based on the changes inside
     * his app settings.
     */
    var isDarkThemeForced: Boolean? = null
        set(value) {
            field = value
            threeDSecureManager.isDarkThemeForced = field
        }

    private val loggingManager = LoggingManager.getInstance(
        LoggingManagerDataSource(
            isCardinalLive,
            gatewayType,
            merchantUsername
        )
    )

    private val apiService: TrustPaymentsApiService =
        TrustPaymentsApiService.getInstance(context, gatewayType, loggingManager)

    private val threeDSecureManager: ThreeDSecureManager = ThreeDSecureManager.getInstance(
        context,
        isCardinalLive,
        cardinalStyleManager,
        cardinalDarkThemeStyleManager,
        loggingManager,
        isLocationDataConsentGiven
    )


    private val gson by lazy { Gson() }


    //TODO can multiple transactions be done or do we need a 'completed' flag on session after performing single transaction?
    @JvmSynthetic
    fun createSession(
        jwtProvider: () -> String,
        cardPan: String? = null,
        cardExpiryDate: String? = null,
        cardSecurityCode: String? = null,
        walletToken: String? = null,
        walletSource: String? = null
    ) = PaymentSession(
        jwtProvider,
        cardPan,
        cardExpiryDate,
        cardSecurityCode,
        walletToken,
        walletSource
    )

    fun createSession(
        jwt: String,
        cardPan: String? = null,
        cardExpiryDate: String? = null,
        cardSecurityCode: String? = null,
        walletToken: String? = null,
        walletSource: String? = null
    ) = PaymentSession(
        { jwt },
        cardPan,
        cardExpiryDate,
        cardSecurityCode,
        walletToken,
        walletSource
    )

    @Deprecated(
        "Use executeSession(PaymentSession, Activity?, ActivityResultProvider?) instead",
        replaceWith = ReplaceWith(
            "executeSession(PaymentSession, Activity?)"
        )
    )
    @WorkerThread
    fun executeSession(
        session: PaymentSession,
        activity: Activity?,
        activityResultProvider: ActivityResultProvider?
    ): Response {
        return runBlocking {
            executeSession(
                session,
                activity?.run { { this } } as (() -> Activity)?)
        }
    }

    @WorkerThread
    fun executeSession(
        session: PaymentSession,
        activity: Activity?,
    ): Response {
        return runBlocking {
            executeSession(
                session,
                activity?.run { { this } } as (() -> Activity)?,
            )
        }
    }

    @Deprecated(
        "Use executeSession(PaymentSession, Activity?) instead",
        replaceWith = ReplaceWith(
            "executeSession(PaymentSession, ActivityProvider?)"
        )
    )
    @JvmSynthetic
    suspend fun executeSession(
        session: PaymentSession,
        activityProvider: (() -> Activity)? = null,
        activityResultProvider: ActivityResultProvider? = null
    ): Response {
        return try {
            loggingManager.init()
            val response = performSession(session, activityProvider)
            loggingManager.release()
            response
        } catch (e: Exception) {
            loggingManager.sendExceptionEvent(e)
            loggingManager.release()
            finishExecution(session, Response(error = Error.UncaughtExceptionError))
        }
    }
    @JvmSynthetic
    suspend fun executeSession(
        session: PaymentSession,
        activityProvider: (() -> Activity)? = null
    ): Response {
        return try {
            loggingManager.init()
            val response = performSession(session, activityProvider)
            loggingManager.release()
            response
        } catch (e: Exception) {
            loggingManager.sendExceptionEvent(e)
            loggingManager.release()
            finishExecution(session, Response(error = Error.UncaughtExceptionError))
        }
    }


    @JvmSynthetic
    internal suspend fun performSession(
        session: PaymentSession,
        activityProvider: (() -> Activity)? = null
    ): Response {
        session.intermediateJwt = session.jwtProvider()

        var requests = try {
            parsePayload(jwt = session.intermediateJwt).requestTypes
        } catch (ex: Exception) {
            loggingManager.sendErrorEvent(
                FailedExtractingRequestTypes,
                PaymentTransactionManagerEvent
            )
            return finishExecution(
                session,
                Response(error = Error.ParsingError("Failed to extract request types from given JWT"))
            )
        }


        if (requests.isEmpty()) {
            loggingManager.sendErrorEvent(
                JwtEmpty,
                PaymentTransactionManagerEvent
            )
            return finishExecution(
                session,
                Response(error = Error.ParsingError("List of request types in given JWT is empty"))
            )
        }

        if (requests.any { it == RequestType.ThreeDQuery }) {
            if (activityProvider == null) {
                loggingManager.sendErrorEvent(
                    NoActivityProvider,
                    PaymentTransactionManagerEvent
                )
                return finishExecution(
                    session,
                    Response(error = Error.DeveloperError.ActivityProviderRequired)
                )
            }
        }

        //Create Cardinal session to be able to check safety warnings
        threeDSecureManager.createSession()
        checkForSafetyWarnings()?.run {
            loggingManager.sendErrorEvent(
                CardinalWarning,
                PaymentTransactionManagerEvent,
                LogLevel.WARNING,
                errorMessage = formatCardinalWarningsMessage(this.errors)
            )
            return finishExecution(
                session,
                Response(error = this)
            )
        }

        //Initialize session only if there's a 3DQuery request on the list
        if (requests.any { it == RequestType.ThreeDQuery }) {
            //If initialization returned any errors, return them here
            val result: Error? = try {
                initializeSession(session)

            } catch (e: Exception) {
                loggingManager.sendErrorEvent(
                    JWTInvalid,
                    PaymentTransactionManagerEvent
                )
                Error.InitializationError
            }

            if (result != null) {
                return finishExecution(
                    session,
                    Response(error = result)
                )
            }

            //Deserialize request types again using JWT returned for JSINIT
            requests = try {
                parsePayload(jwt = session.intermediateJwt).requestTypes
            } catch (ex: Exception) {
                loggingManager.sendErrorEvent(
                    FailedExtractingRequestTypes,
                    PaymentTransactionManagerEvent
                )
                return finishExecution(
                    session,
                    Response(error = Error.ParsingError("Failed to extract request types from returned JWT"))
                )
            }

            if (requests.isEmpty()) {
                loggingManager.sendErrorEvent(
                    EmptyRequestTypes,
                    PaymentTransactionManagerEvent
                )
                return finishExecution(
                    session,
                    Response(error = Error.ParsingError("List of request types in returned JWT is empty"))
                )
            }
        }

        val firstResponse = executeSessionPart(session)

        if (firstResponse !is TrustPaymentsApiService.Result.Success) {
            return finishExecution(
                session,
                Response(error = Error.HttpFailure)
            )
        }

        //Find response with customerOutput set
        val customerOutputResponse =
            firstResponse.transactionResponses.find { it.customerOutput != null }
        val threeDQueryResponse = when (customerOutputResponse?.customerOutput) {
            CustomerOutput.ThreeDRedirect -> customerOutputResponse
            else -> return finishExecution(
                session,
                Response(listOf(firstResponse.responseJwt.token))
            )
        }

        //Parse payload of first response to later check if second request will be needed
        //or return error here if parsing fails
        val firstResponsePayload = try {
            val payload = parsePayload(jwt = firstResponse.newJwt.token)
            payload.validate()
            payload
        } catch (ex: Exception) {
            loggingManager.sendErrorEvent(
                FailedToParseServerResponse,
                PaymentTransactionManagerEvent
            )
            return finishExecution(
                session,
                Response(
                    listOf(firstResponse.responseJwt.token),
                    error = Error.ParsingError("Failed to parse server response")
                )
            )
        }

        //Challenge requested, execute 3DS session
        val cardinalSessionResponse = threeDSecureManager.executeSession(
            threeDQueryResponse.threeDVersion!!,
            threeDQueryResponse.acquirerTransactionReference!!,
            threeDQueryResponse.threeDPayload!!,
            activityProvider!!.invoke(),
        )

        val threeDResponse = when (cardinalSessionResponse) {
            is ThreeDSecureManager.ExecutionResult.SuccessV1 -> AdditionalTransactionResult(
                pares = cardinalSessionResponse.result
            )

            is ThreeDSecureManager.ExecutionResult.SuccessV2 -> AdditionalTransactionResult(
                threeDResponse = cardinalSessionResponse.result
            )

            is ThreeDSecureManager.ExecutionResult.Failure -> {

                return finishExecution(
                    session,
                    Response(
                        listOf(firstResponse.responseJwt.token),
                        error = Error.ThreeDSFailure(cardinalSessionResponse.error)
                    )
                )
            }
        }

        //If 3DS challenge completed successfully and there are no more requests to process, finish here
        if (firstResponsePayload.requestTypes.isEmpty()) {
            //No more requests to perform
            return finishExecution(
                session,
                Response(listOf(firstResponse.responseJwt.token), threeDResponse)
            )
        }

        //Execute remaining requests

        //Update JWT
        session.intermediateJwt = firstResponse.newJwt.token

        val secondResponse = when (cardinalSessionResponse) {
            is ThreeDSecureManager.ExecutionResult.SuccessV1 -> {
                executeSessionPart(
                    session,
                    pares = cardinalSessionResponse.result,
                )
            }

            is ThreeDSecureManager.ExecutionResult.SuccessV2 -> {
                executeSessionPart(
                    session,
                    threeDSecureJwt = cardinalSessionResponse.result,
                )
            }

            else -> {
                executeSessionPart(session)
            }
        }

        if (secondResponse !is TrustPaymentsApiService.Result.Success) {
            return finishExecution(
                session,
                Response(
                    listOf(firstResponse.responseJwt.token),
                    threeDResponse,
                    Error.HttpFailure
                )
            )
        }

        return finishExecution(
            session,
            Response(
                listOf(firstResponse.responseJwt.token, secondResponse.responseJwt.token),
                threeDResponse
            )
        )
    }

    private fun formatCardinalWarningsMessage(errors: List<DeviceSafetyWarning>): String? =
        if (errors.isNotEmpty()) {

            val list = mutableListOf<String>()
            for (warning: DeviceSafetyWarning in errors) {
                when (warning) {
                    is DeviceSafetyWarning.AppInstalledFromUntrustedSource -> list.add("App installed from untrusted source")
                    is DeviceSafetyWarning.DebuggerIsAttached -> list.add("Debugger attached")
                    is DeviceSafetyWarning.DeviceRooted -> list.add("Device rooted")
                    is DeviceSafetyWarning.EmulatorIsUsed -> list.add("Emulator used")
                    is DeviceSafetyWarning.SdkIntegrityTampered -> list.add("SDK integrity tampered")
                }
            }
            list.joinToString(", ")
        } else null

    private fun finishExecution(session: PaymentSession, response: Response): Response {
        session.intermediateJwt = ""
        return response
    }

    private suspend fun executeSessionPart(
        session: PaymentSession,
        threeDSecureJwt: String? = null,
        pares: String? = null,
    ): TrustPaymentsApiService.Result {

        return apiService.performTransaction(
            session.sessionId,
            merchantUsername,
            session.intermediateJwt,
            session.cacheJwt,
            session.cardPan,
            session.cardExpiryDate,
            session.cardSecurityCode,
            threeDSecureJwt,
            pares,
            session.walletToken,
            session.walletSource,
            apmCode = session.apmCode,
        )
    }

    private suspend fun initializeSession(session: PaymentSession): Error? {
        val result = apiService.initializeSession(
            session.sessionId, merchantUsername, session.intermediateJwt
        )

        if (result is TrustPaymentsApiService.Result.Success && result.transactionResponses.size == 1) {
            //if result seems correct

            val jsInitResponse = result.transactionResponses.first()

            if (jsInitResponse.errorCode == ResponseErrorCode.Ok && jsInitResponse.threeDInit != null) {
                //always replace JWT with new JWT after JSINIT request
                session.intermediateJwt = result.newJwt.token
                session.cacheJwt = jsInitResponse.cacheToken

                //TODO what to do if user does not execute the session?
                val sessionResult = threeDSecureManager.initSession(jsInitResponse.threeDInit)

                return if (sessionResult is ThreeDSecureManager.InitializationResult.Failure) {

                    loggingManager.sendErrorEvent(
                        InitError,
                        PaymentTransactionManagerEvent
                    )
                    Error.InitializationFailure(result.responseJwt.token, sessionResult.error)
                } else {
                    null
                }
            } else {
                if (jsInitResponse.errorCode == ResponseErrorCode.Ok) {
                    loggingManager.sendErrorEvent(
                        ThreeDInitNull,
                        PaymentTransactionManagerEvent
                    )
                } else {
                    loggingManager.sendErrorEvent(
                        InitSessionError,
                        PaymentTransactionManagerEvent,
                        errorCode = jsInitResponse.errorCode.code
                    )

                }
                return Error.InitializationFailure(result.responseJwt.token)
            }
        } else {
            return Error.InitializationError
        }
    }

    private fun checkForSafetyWarnings(): Error.SafetyError? {
        val localWarnings = threeDSecureManager.deviceSafetyWarnings

        if (isCardinalLive) {
            //for live environment fail if any of these critical warnings found
            val criticalWarnings = localWarnings.filter {
                it is DeviceSafetyWarning.DeviceRooted ||
                        it is DeviceSafetyWarning.EmulatorIsUsed ||
                        it is DeviceSafetyWarning.DebuggerIsAttached ||
                        it is DeviceSafetyWarning.AppInstalledFromUntrustedSource
            }

            return if (criticalWarnings.isNotEmpty()) {
                Error.SafetyError(criticalWarnings)
            } else {
                null
            }
        } else if (localWarnings.firstOrNull { it is DeviceSafetyWarning.DeviceRooted } != null &&
            localWarnings.firstOrNull { it is DeviceSafetyWarning.EmulatorIsUsed } == null) {
            //for non-live environment fail only if device is rooted and device is not an emulator
            //this is because some emulators are rooted by default

            return Error.SafetyError(listOf(DeviceSafetyWarning.DeviceRooted))
        } else {
            return null
        }
    }

    private fun parsePayload(jwt: String): JwtPayload {
        val token = JwtToken(jwt)
        val body = gson.fromJson(token.body, JwtBody::class.java)

        return body.payload
    }

    data class Response(
        val responseJwtList: List<String> = emptyList(),
        val additionalTransactionResult: AdditionalTransactionResult? = null,
        val error: Error? = null
    )

    private class JwtBody(payload: JwtPayload) {
        internal val payload = payload
            @JvmSynthetic get
    }

    private class JwtPayload private constructor(
        private val requesttypedescriptions: List<String>,
        returnurl: String,
        termurl: String
    ) {
        internal val termurl = termurl
            @JvmSynthetic get

        internal val returnurl = returnurl
            @JvmSynthetic get

        @JvmSynthetic
        internal fun validate() {
            //Accessing requestTypes causes conversion of raw request types to PaymentRequestType,
            //if any value fails to be assigned to the type, we consider request types to contain at least one illegal value
            val validation = requestTypes
        }

        internal val requestTypes: List<RequestType>
            @JvmSynthetic get() = requesttypedescriptions.map {
                when (it) {
                    RequestType.JsInit.serializedName -> RequestType.JsInit
                    RequestType.Error.serializedName -> RequestType.Error
                    RequestType.AccountCheck.serializedName -> RequestType.AccountCheck
                    RequestType.Auth.serializedName -> RequestType.Auth
                    RequestType.Subscription.serializedName -> RequestType.Subscription
                    RequestType.ThreeDQuery.serializedName -> RequestType.ThreeDQuery
                    RequestType.RiskDec.serializedName -> RequestType.RiskDec
                    RequestType.CacheTokenise.serializedName -> RequestType.CacheTokenise
                    else -> throw IllegalArgumentException("Unknown request type: $it")
                }
            }
    }
}

/**
 * Object passed in the transaction's completion callback.
 * Use it to get 3DS challenge results or APM's transaction status
 */
data class AdditionalTransactionResult(
    val pares: String? = null,
    val threeDResponse: String? = null,
    // Settle status of apm transaction
    val settleStatus: String? = null,
    // Reference of apm transaction
    val transactionReference: String? = null
)

sealed class Error {
    /**
     * Errors representing illegal states on payment session initialization and execution. They
     * indicate merchant's development errors, which should never occur on production environment.
     */
    sealed class DeveloperError(val message: String) : Error() {
        object ActivityProviderRequired :
            DeveloperError("Providing activityProvider parameter is mandatory if request list contains THREEDQUERY")

        object ActivityResultProviderRequired :
            DeveloperError("Providing activityResultProvider parameter is mandatory if request list contains THREEDQUERY")
    }

    /**
     * Generic error was returned for initialization or Cardinal initialization.
     */
    object InitializationError : Error()

    /**
     * An exception which wasn't caught before occured..
     */
    object UncaughtExceptionError : Error()

    /**
     * Error or ParsingError was returned as TransactionResponse for initialization.
     */
    data class InitializationFailure(
        val jwt: String,
        val initializationError: ThreeDSecureError? = null
    ) : Error()

    /**
     * Error returned when any safety warnings have been detected by Cardinal SDK
     */
    data class SafetyError(val errors: List<DeviceSafetyWarning>) : Error()

    /**
     * Error returned when parsing JWT or customerOutput fails
     */
    data class ParsingError(val errorMessage: String = "") : Error()

    /**
     * Error returned when HTTP communication fails
     */
    object HttpFailure : Error()

    /**
     * Error returned when 3DS challenge fails
     */
    data class ThreeDSFailure(val threeDSError: ThreeDSecureError) : Error()

    /**
     * Error returned when a required request type has not been found in the response
     */
    data class RequestTypeNotFound(val errorMessage: String = "") : Error()

    /**
     * Error returned when required parameters have not been found in the response
     */
    data class ParameterNotFound(val errorMessage: String = "") : Error()
}