#!/usr/bin/env bash
source .cicd_scripts/other/utils.sh

copy_artifacts_to_maven_local() {
    set -u -o pipefail -E

    local core_artifacts_path=$1
    local ui_artifacts_path=$2

    mkdir -p /root/.m2/repository/com/trustpayments/mobile/core/${VERSION_CODE}/
    cp -R ${core_artifacts_path}/* /root/.m2/repository/com/trustpayments/mobile/core/${VERSION_CODE}/
    mkdir -p /root/.m2/repository/com/trustpayments/mobile/ui/${VERSION_CODE}/
    cp -R ${ui_artifacts_path}/* /root/.m2/repository/com/trustpayments/mobile/ui/${VERSION_CODE}/
}
