package com.trustpayments.mobile.core.testcases.javasupport;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.rule.ActivityTestRule;

import com.trustpayments.mobile.core.PaymentSession;
import com.trustpayments.mobile.core.R;
import com.trustpayments.mobile.core.models.JwtResponse;
import com.trustpayments.mobile.core.models.api.response.CustomerOutput;
import com.trustpayments.mobile.core.models.api.response.RequestType;
import com.trustpayments.mobile.core.services.transaction.ActivityResultProvider;
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager;
import com.trustpayments.mobile.core.test.BuildConfig;
import com.trustpayments.mobile.core.testutils.BaseTestCase;
import com.trustpayments.mobile.core.testutils.ThreeDQueryResponse;
import com.trustpayments.mobile.core.utils.ThreeDQueryTestActivity;
import com.trustpayments.mobile.core.ui.WebActivityResult;
import com.trustpayments.mobile.core.util.JWTBuilder;
import com.trustpayments.mobile.core.util.ResponseParser;
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;

public class CardinalCommerce3DSV2JavaTestCases extends BaseTestCase {
    private PaymentTransactionManager paymentTransactionManager;

    private String jwtToken;

    private ThreeDQueryTestActivity activity;

    private ActivityResultProvider activityResultProvider;

    @Rule
    public ActivityTestRule initialActivityTestRule = new ActivityTestRule(ThreeDQueryTestActivity.class);

    private JWTBuilder jwtBuilder = new JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY);

    @Before
    @Override
    public void setUp() {
        super.setUp();

        Context app = ApplicationProvider.getApplicationContext();
        paymentTransactionManager = new PaymentTransactionManager(
                app, getGatewayType(), false, BuildConfig.MERCHANT_USERNAME,
                null, null, false
        );

        jwtToken = jwtBuilder.getFor3DSecure(new ArrayList<RequestType>() {{
            add(RequestType.ThreeDQuery);
            add(RequestType.Auth);
        }}, null);

        activity = (ThreeDQueryTestActivity) initialActivityTestRule.getActivity();

        activityResultProvider = new ActivityResultProvider();

        activity.resultListener = (Function1<WebActivityResult, Unit>) threeDSecureWebActivityResult -> {
            activityResultProvider.setResult(threeDSecureWebActivityResult);
            return null;
        };
    }

    @Test
    public void test_positive_stepUpAuthenticationForMastercard() throws Exception {
        // GIVEN
        PaymentSession session =
                paymentTransactionManager.createSession(jwtToken, "5200000000001096", getExpiryDate(), "123",
                        null, null);

        // WHEN
        ThreeDQueryResponse expectedTransactionResponse = new ThreeDQueryResponse("Y", "Y", "02");

        final CountDownLatch latch = new CountDownLatch(1);
        final PaymentTransactionManager.Response[] result = {null};

        new Thread(() -> {
            result[0] = paymentTransactionManager.executeSession(session,
                    activity, activityResultProvider);

            latch.countDown();
        }).start();

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.INSTANCE.getThreeDSecureCode());

        latch.await();

        List<JwtResponse> responses = ResponseParser.Companion.parse(result[0].getResponseJwtList());
        JwtResponse threeDQResponse = responses.get(0);
        JwtResponse authResponse = responses.get(1);

        Assert.assertEquals(RequestType.ThreeDQuery, threeDQResponse.getCustomerOutput().getRequestTypeDescription());
        Assert.assertEquals(RequestType.Auth, authResponse.getCustomerOutput().getRequestTypeDescription());

        Assert.assertEquals(CustomerOutput.ThreeDRedirect, threeDQResponse.getCustomerOutput().getCustomerOutput());

        Assert.assertEquals(expectedTransactionResponse.getEnrolled(), authResponse.getCustomerOutput().getEnrolled());
        Assert.assertEquals(expectedTransactionResponse.getParesStatus(), authResponse.getCustomerOutput().getStatus());
        Assert.assertNull(authResponse.getCustomerOutput().getXid());
        Assert.assertNotNull(authResponse.getCustomerOutput().getCavv());
        Assert.assertEquals(expectedTransactionResponse.getEciFlag(), authResponse.getCustomerOutput().getEci());
    }

    @Test
    public void test_negative_errorOnAuthenticationForAmex() throws Exception {
        // GIVEN
        PaymentSession session =
                paymentTransactionManager.createSession(jwtToken, "340000000001122", getExpiryDate(), "1234",
                        null, null);

        // WHEN
        ThreeDQueryResponse expectedTransactionResponse = new ThreeDQueryResponse("Y", "C", null);

        final CountDownLatch latch = new CountDownLatch(1);
        final PaymentTransactionManager.Response[] result = {null};

        new Thread(() -> {
            result[0] = paymentTransactionManager.executeSession(session,
                    activity, activityResultProvider);

            latch.countDown();
        }).start();

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.INSTANCE.getThreeDSecureCode());

        latch.await();

        List<JwtResponse> responses = ResponseParser.Companion.parse(result[0].getResponseJwtList());
        JwtResponse threeDQResponse = responses.get(0);

        Assert.assertEquals(RequestType.ThreeDQuery, threeDQResponse.getCustomerOutput().getRequestTypeDescription());
        Assert.assertEquals(CustomerOutput.ThreeDRedirect, threeDQResponse.getCustomerOutput().getCustomerOutput());

        Assert.assertEquals(expectedTransactionResponse.getEnrolled(), threeDQResponse.getCustomerOutput().getEnrolled());
        Assert.assertEquals(expectedTransactionResponse.getParesStatus(), threeDQResponse.getCustomerOutput().getStatus());
        Assert.assertNotNull(threeDQResponse.getCustomerOutput().getXid());
        Assert.assertNull(threeDQResponse.getCustomerOutput().getCavv());
        Assert.assertEquals(expectedTransactionResponse.getEciFlag(), threeDQResponse.getCustomerOutput().getEci());
    }

    private String getExpiryDate() {
        Calendar calendar = Calendar.getInstance();
        int expiryDateYear = calendar.get(Calendar.YEAR) + 3;
        return "01/" + expiryDateYear;
    }
}
