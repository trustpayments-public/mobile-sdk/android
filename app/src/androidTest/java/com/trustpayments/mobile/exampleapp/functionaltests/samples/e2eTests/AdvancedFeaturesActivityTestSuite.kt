package com.trustpayments.mobile.exampleapp.functionaltests.samples.e2eTests

import android.content.Context
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.trustpayments.mobile.exampleapp.MainActivity
import com.trustpayments.mobile.exampleapp.pageobjects.AdvancedFeaturePage
import com.trustpayments.mobile.exampleapp.pageobjects.AuthWith3DSecureFormPage
import com.trustpayments.mobile.exampleapp.pageobjects.HomePage
import com.trustpayments.mobile.exampleapp.pageobjects.PayByCardFormPage
import com.trustpayments.mobile.exampleapp.utils.TestUtils
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class AdvancedFeaturesActivityTestSuite {

    @get:Rule
    val activityScenarioRule = ActivityScenarioRule(MainActivity::class.java)

    private lateinit var context: Context
    private val homePage = HomePage()
    private val payByCardFormPage = PayByCardFormPage()
    private val authWith3DSecureFormPage = AuthWith3DSecureFormPage()
    private val advancedFeaturePage = AdvancedFeaturePage()

    @Before
    fun setUp() {
        context = InstrumentationRegistry.getInstrumentation().targetContext
        TestUtils.clearSharedPreferences(context)
    }
    @After
    fun after() {
        TestUtils.clearSharedPreferences(context)
    }


    @Test
    fun successfulBackgroundRequest() {
            homePage.tapAdvancedFeatures()
            advancedFeaturePage.tapBackgroundRequest()
            advancedFeaturePage.clickRunButton()
            Thread.sleep(10000)

            authWith3DSecureFormPage.fillSecureCode(CardsDataWith3DSecureV2.threeDSecureCode).tapOnSubmitButton()
            Thread.sleep(10000)
            Assert.assertTrue(
            "Alert with successful payment processing message was not displayed.",
            payByCardFormPage.isSuccessPaymentCompletedAlertDisplayed()
        )
    }
    @Test
    fun successfulAccountVerificationWithLaterAuth() {
            homePage.tapAdvancedFeatures()
            advancedFeaturePage.tapAccountVerificationWithLaterAuth()
            advancedFeaturePage.clickRunButton()
            Thread.sleep(10000)

            authWith3DSecureFormPage.fillSecureCode(CardsDataWith3DSecureV2.threeDSecureCode).tapOnSubmitButton()
            Thread.sleep(5000)
            Assert.assertTrue(
            "Alert with successful payment processing message was not displayed.",
            payByCardFormPage.isSuccessPaymentCompletedAlertDisplayed()
        )
    }
}