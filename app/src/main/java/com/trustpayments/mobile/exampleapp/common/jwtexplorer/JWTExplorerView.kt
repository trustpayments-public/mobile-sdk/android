package com.trustpayments.mobile.exampleapp.common.jwtexplorer

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.util.AttributeSet
import android.util.Base64
import android.view.View
import android.webkit.*
import android.widget.LinearLayout
import com.google.android.material.tabs.TabLayout
import com.trustpayments.mobile.exampleapp.R


@SuppressLint("SetJavaScriptEnabled")
class JWTExplorerView(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {
    private var stepsTabLayout: TabLayout? = null
    private var tokensTabLayout: TabLayout? = null
    private var webView: WebView? = null
    private var requestJWTToken: String = "[]"
    private var responseJWTToken: String = "[]"
    var mode: JWTEditorMode = JWTEditorMode.DEFAULT
        private set
    private var selectedStepIndex = 0
    private var selectedTokenIndex = 0
    private var steps: MutableList<JWTEditorStep> = mutableListOf()
    private var onTokenChangedListener: OnTokenChangedListener? = null

    init {
        inflate(context, R.layout.jwt_explorer_view, this)
        tokensTabLayout = findViewById(R.id.tokens_tabs_layout)
        stepsTabLayout = findViewById(R.id.steps_tabs_layout)
        webView = findViewById(R.id.webContainer)
        setupWebView()
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.JWTExplorerView)
        requestJWTToken = typedArray.getString(R.styleable.JWTExplorerView_requestJWTToken) ?: "[]"
        responseJWTToken =
            typedArray.getString(R.styleable.JWTExplorerView_responseJWTToken) ?: "[]"
        selectedTokenIndex =
            typedArray.getInteger(R.styleable.JWTExplorerView_selectedTokenIndex, 0)
        val stringMode =
            typedArray.getString(R.styleable.JWTExplorerView_mode) ?: JWTEditorMode.DEFAULT.name

        if (stringMode.uppercase() == JWTEditorMode.DEFAULT.name ||
            stringMode.uppercase() == JWTEditorMode.STEPS.name
        ) {
            mode = JWTEditorMode.valueOf(stringMode.uppercase())
        }

        typedArray.recycle()
    }

    private fun setupWebView(){
        webView?.settings?.javaScriptEnabled = true
        webView?.settings?.allowFileAccess = false
        @Suppress("DEPRECATION")
        webView?.settings?.allowFileAccessFromFileURLs = false
        @Suppress("DEPRECATION")
        webView?.settings?.allowUniversalAccessFromFileURLs = false
        webView?.settings?.allowContentAccess = false
        webView?.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = request?.url
                view?.context?.startActivity(intent)
                return true
            }
            override fun onLoadResource(view: WebView?, url: String?) {
                super.onLoadResource(view, url)
                if (view?.certificate != null){
                    throw Exception("External Resource load not allowed")
                }
            }
        }
        webView?.addJavascriptInterface(
            object {
                @JavascriptInterface
                fun tokenChanged(jsonText: String) {
                    val token = steps.getOrNull(selectedStepIndex)?.tokens?.getOrNull(
                        selectedTokenIndex
                    ) ?: return
                    requestJWTToken = jsonText
                    token.token = jsonText
                    onTokenChangedListener?.onTokenChanged(token)
                }
            },
            "Android"
        )
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        setupViews()
    }

    private fun setupViews() {
        setSelectedTabListeners()
        loadTabs()
        loadToken()
    }

    fun setOnTokenChangedListener(onTokenChangedListener: OnTokenChangedListener) {
        this.onTokenChangedListener = onTokenChangedListener
    }

    private fun setSelectedTabListeners() {
        tokensTabLayout?.clearOnTabSelectedListeners()
        tokensTabLayout?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                selectedTokenIndex = tab?.position ?: 0
                loadToken()
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}
            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })
        stepsTabLayout?.clearOnTabSelectedListeners()
        stepsTabLayout?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                selectedTokenIndex = 0
                tokensTabLayout?.selectTab(tokensTabLayout?.getTabAt(selectedTokenIndex))
                selectedStepIndex = tab?.position ?: 0
                loadToken()
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}
            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })
    }

    private fun loadTabs() {
        if (mode == JWTEditorMode.STEPS) {
            loadStepsModeTabs()
        } else {
            loadNormalModeTabs()
        }
    }

    private fun loadStepsModeTabs() {
        stepsTabLayout?.visibility = View.VISIBLE
        stepsTabLayout?.removeAllTabs()
        stepsTabLayout?.apply {
            steps.forEachIndexed { index, jwtEditorStep ->
                addTab(
                    newTab().setText(
                        jwtEditorStep.title
                    ), index == 0
                )
            }
        }
        tokensTabLayout?.removeAllTabs()
        tokensTabLayout?.apply {
            steps.firstOrNull()?.tokens?.forEachIndexed { index, jwtEditorToken ->
                addTab(
                    newTab().setText(
                        jwtEditorToken.title
                    ), index == 0
                )
            }
        }
        selectedTokenIndex = 0
        selectedStepIndex = 0
    }

    private fun loadNormalModeTabs() {
        steps.clear()
        steps.add(
            JWTEditorStep(
                "normal",
                "",
                listOf(
                    JWTEditorToken(
                        "request",
                        "normal",
                        context.getString(R.string.request_jwt),
                        requestJWTToken,
                        true
                    ),
                    JWTEditorToken(
                        "response",
                        "normal",
                        context.getString(R.string.response_jwt),
                        responseJWTToken,
                        false
                    )
                )
            )
        )
        loadStepsModeTabs()
        stepsTabLayout?.visibility = View.GONE
        selectedTokenIndex = 0
        selectedStepIndex = 0
    }

    private fun loadToken() {
        val step = steps.getOrNull(selectedStepIndex)
        val jwtText = step?.tokens?.getOrNull(selectedTokenIndex)?.token
            ?: "{ message: 'Feel free add your steps and tokens'}"
        val mode =
            if (step?.tokens?.getOrNull(selectedTokenIndex)?.editable == true) "form" else "view"
        val html = context.resources.openRawResource(R.raw.jwt_explorer)
            .bufferedReader().use { it.readText() }.replace("TOKEN", jwtText)
            .replace("VIEW_MODE", mode)

        val encodedHtml = Base64.encodeToString(html.toByteArray(), Base64.NO_PADDING)
        webView?.loadData(encodedHtml, "text/html", "base64")
    }

    fun setMode(mode: JWTEditorMode) {
        this.mode = mode
        setupViews()
    }

    fun setSteps(steps: List<JWTEditorStep>) {
        if (mode != JWTEditorMode.STEPS) {
            throw Exception("Set this value is only accepted on steps mode.")
        }
        this.steps.clear()
        this.steps.addAll(steps)
        loadStepsModeTabs()
        loadToken()
    }

    fun setRequestJwtToken(token: String) {
        if (mode != JWTEditorMode.DEFAULT) {
            throw Exception("Set this value is only accepted on default mode.")
        }
        requestJWTToken = token
        loadNormalModeTabs()
        loadToken()
    }

    fun getResponseJwtToken(): String {
        return responseJWTToken
    }

    fun setResponseJwtToken(token: String) {
        if (mode != JWTEditorMode.DEFAULT) {
            throw Exception("Set this value is only accepted on default mode.")
        }
        responseJWTToken = token
        loadNormalModeTabs()
        loadToken()
    }

    fun moveTokenTab(index: Int) {
        tokensTabLayout?.getTabAt(index)?.select()
    }

    fun moveStepTab(index: Int) {
        stepsTabLayout?.getTabAt(index)?.select()
    }
}

enum class JWTEditorMode {
    DEFAULT, STEPS
}

data class JWTEditorStep(
    val id: String,
    val title: String,
    val tokens: List<JWTEditorToken>
)

data class JWTEditorToken(
    val id: String,
    val stepId: String,
    val title: String,
    var token: String,
    val editable: Boolean
)

interface OnTokenChangedListener {
    fun onTokenChanged(token: JWTEditorToken)
}
