package com.trustpayments.mobile.ui.utils

import android.util.Log

internal fun log(message: String) {
    Log.d("MSDKUI", message)
}

internal fun log(tag: String, message: String) {
    Log.d(tag, message)
}

internal fun logWarning(tag: String, message: String) {
    Log.w(tag, message)
}