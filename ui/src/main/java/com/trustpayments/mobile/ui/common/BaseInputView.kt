package com.trustpayments.mobile.ui.common

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.os.Parcel
import android.os.Parcelable
import android.text.InputFilter
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.AttributeSet
import android.util.SparseArray
import android.util.TypedValue
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import com.trustpayments.mobile.ui.R
import com.trustpayments.mobile.ui.databinding.ViewPaymentInputBinding
import com.trustpayments.mobile.ui.model.PaymentInputType
import com.trustpayments.mobile.ui.utils.fromDpToPx
import com.trustpayments.mobile.ui.utils.fromPxToDp
import com.trustpayments.mobile.ui.utils.restoreChildViewStates
import com.trustpayments.mobile.ui.utils.saveChildViewStates


// Base class for all input type view components in SDK
@Suppress("LeakingThis") // explanation: https://stackoverflow.com/a/60571913
abstract class BaseInputView(
    context: Context, attrs: AttributeSet?
) : LinearLayout(context, attrs), PaymentView {

    constructor(context: Context) : this(context, null)

    internal var inputValidationListener: InputValidationListener? = null
        @JvmSynthetic get
        @JvmSynthetic set

    private lateinit var labelText: String

    private var errorTintColor: Int = 0

    internal var binding: ViewPaymentInputBinding

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = ViewPaymentInputBinding.inflate(inflater, this)

        orientation = VERTICAL

        attrs?.let {
            setupAttributes(it)
        }
    }

    override fun dispatchSaveInstanceState(container: SparseArray<Parcelable>?) {
        dispatchFreezeSelfOnly(container)
    }

    override fun dispatchRestoreInstanceState(container: SparseArray<Parcelable>?) {
        dispatchThawSelfOnly(container)
    }

    override fun onSaveInstanceState(): Parcelable? {
        return SavedState(super.onSaveInstanceState()).apply {
            childrenStates = saveChildViewStates()
            hasError = hasError()
        }
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        when (state) {
            is SavedState -> {
                super.onRestoreInstanceState(state.superState)
                state.childrenStates?.let { restoreChildViewStates(it) }
                if (state.hasError) {
                    setError(R.string.pvc_input_error_text)
                }
            }

            else -> super.onRestoreInstanceState(state)
        }
    }

    private fun setupAttributes(attrs: AttributeSet) {
        val typedArray = context.obtainStyledAttributes(
            attrs, R.styleable.BaseInputView,
            0, 0
        )

        labelText =
            typedArray.getString(R.styleable.BaseInputView_labelText) ?: getLabelText()

        binding.apply {
            label.text = getAsteriskSpannable(
                labelText, ContextCompat.getColor(context, R.color.colorAsterisk)
            )
            input.hint = typedArray.getString(R.styleable.BaseInputView_hintText) ?: getHintText()
            input.setCompoundDrawablesWithIntrinsicBounds(
                getStartIcon(), null, null, null
            )
        }


        typedArray.recycle()
    }

    // Mandatory attributes for all input views
    abstract fun getLabelText(): String

    abstract fun getHintText(): String

    abstract fun getStartIcon(): Drawable?
    // EO mandatory attributes

    // region Style attributes setters (triggered after the XML params usage for the drop-in view)
    fun setTextColor(color: Int) {
        binding.input.setTextColor(color)
    }

    fun setTextSize(textSize: Float) {
        binding.input.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)
    }

    fun setBackgroundShape(shape: Drawable) {
        binding.input.background = shape
    }

    fun setHintTextColor(color: Int) {
        binding.input.setHintTextColor(color)
    }

    fun setSpacing(spacing: Int) {
        val params = binding.inputContainer.layoutParams as LayoutParams
        params.setMargins(0, 0, 0, spacing)
        binding.inputContainer.layoutParams = params
    }

    fun setLabelTextColor(color: Int) {
        binding.label.setTextColor(color)
    }

    fun setLabelTextSize(textSize: Float) {
        binding.label.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)
    }

    fun setLabelTextSpacing(spacing: Int) {
        val params = binding.label.layoutParams as LayoutParams
        params.setMargins(0, 0, 0, spacing)
        binding.label.layoutParams = params
    }

    fun setErrorTextColor(color: Int) {
        binding.inputContainer.setErrorTextColor(ColorStateList.valueOf(color))
        errorTintColor = color
    }
    // endregion

    fun setError(errorResId: Int) {
        binding.apply {
            inputContainer.apply {
                error = resources.getString(errorResId)
                errorIconDrawable = null
            }
            input.backgroundTintList = ColorStateList.valueOf(errorTintColor)
        }
    }

    fun resetError() {
        binding.apply {
            inputContainer.error = null
            input.backgroundTintList = null
        }
    }

    private fun hasError() = !binding.inputContainer.error.isNullOrEmpty()

    fun setInputMaxLength(maxLength: Int) {
        val fArray = arrayOfNulls<InputFilter>(1)
        fArray[0] = InputFilter.LengthFilter(maxLength)
        binding.input.filters = fArray
    }

    fun getInputMaxLength(): Int {
        for (filter in binding.input.filters) {
            if (filter is InputFilter.LengthFilter) {
                return filter.max
            }
        }
        return 0
    }

    fun setInputBackgroundColor(@ColorInt inputBackgroundColor : Int){
        binding.inputContainer.boxBackgroundColor =inputBackgroundColor
    }

    fun setInputBorderColor(@ColorInt inputBorderColor : Int){
        val states = arrayOf(
            intArrayOf(android.R.attr.state_pressed),  // pressed state
            intArrayOf(-android.R.attr.state_pressed)  // default state
        )
        val colors = intArrayOf(
            inputBorderColor,  // color for pressed state
            inputBorderColor  // color for default state
        )
        binding.inputContainer.setBoxStrokeColorStateList(ColorStateList(states, colors))
    }

    fun setInputBorderWidth(inputBorderWidth : Float){
        binding.inputContainer.boxStrokeWidth = inputBorderWidth.fromDpToPx().toInt()
        val focusedBorderWidth = (inputBorderWidth.fromPxToDp() + 1).fromDpToPx()
        binding.inputContainer.boxStrokeWidthFocused = focusedBorderWidth.toInt()
    }

    fun setInputCornerRadius(inputCornerRadius : Float){
        binding.inputContainer.setBoxCornerRadii(
            inputCornerRadius,
            inputCornerRadius,
            inputCornerRadius,
            inputCornerRadius,
        )
    }

    private fun getAsteriskSpannable(text: String, color: Int): Spannable =
        SpannableString("$text *").apply {
            setSpan(
                ForegroundColorSpan(color),
                length - 1,
                length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }

    // state restoration in custom views
    internal class SavedState : BaseSavedState {

        var childrenStates: SparseArray<Parcelable>? = null

        var hasError = false

        constructor(superState: Parcelable?) : super(superState)

        constructor(source: Parcel) : super(source) {
            childrenStates = source.readSparseArray(javaClass.classLoader)
            hasError = source.readValue(javaClass.classLoader) as Boolean
        }

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeSparseArray(childrenStates)
            out.writeValue(hasError)
        }

        companion object {
            @Suppress("UNUSED")
            @JvmField
            val CREATOR = object : Parcelable.Creator<SavedState> {
                override fun createFromParcel(source: Parcel) = SavedState(source)
                override fun newArray(size: Int): Array<SavedState?> = arrayOfNulls(size)
            }
        }
    }

    /**
     * Interface used by drop-in view for receiving notifications about the input validation state change
     */
    internal interface InputValidationListener {
        fun onInputValid(paymentInputType: PaymentInputType, input: String)
        fun onInputInvalid(paymentInputType: PaymentInputType)
    }
}