.project-infrastructure-cicd-ref: &project-infrastructure-cicd-ref 78.0.1

.project-infrastructure-cicd: &project-infrastructure-cicd
  project: securetrading-gl/st-server-project/project-infrastructure-cicd
  ref: *project-infrastructure-cicd-ref

include:
  - file: /cicd_android.yml
    <<: *project-infrastructure-cicd

  - local: '/.gitlab/others/variables.yml'
  - local: '/.gitlab/others/browserstack.yml'
  - local: '/.gitlab/others/tags.yml'
  - local: '/.gitlab/build.yml'
  - local: '/.gitlab/post-build.yml'
  - local: '/.gitlab/fast-tests.yml'
  - local: '/.gitlab/coverage.yml'
  #- local: '/.gitlab/extended-tests.yml' #TEmporary disable e2e tests until assertions are fixed.
  - local: '/.gitlab/post-release.yml'

.image-versions:
  variables:
    IMAGE_MASTER_BASE_2204: registry.gitlab.com/trustpayments-public/public-docker-registry/base-2204:14.0.0
    IMAGE_BASE_PIPELINE_2204: registry.gitlab.com/trustpayments-public/public-docker-registry/base-pipeline-2204:14.0.0
    IMAGE_MASTER_BASE: ${IMAGE_MASTER_BASE_2204}
    IMAGE_BASE_PIPELINE: ${IMAGE_BASE_PIPELINE_2204}

.variables:
  variables:
    DOCKER_BUILD_ARGS_PROXY: ''
    IMAGE_MOBSF: opensecurity/mobile-security-framework-mobsf:v3.4.6 #Old version as has features required by team as per SI-1940
    IMAGE_DOCKER_STABLE: registry.gitlab.com/trustpayments-public/public-docker-registry/eks-docker-stable:14.0.0
    IMAGE_DOCKER_DIND: registry.gitlab.com/trustpayments-public/public-docker-registry/eks-docker-dind:14.0.0
    DOCKER_REGISTRY: "gitlab"
    IMAGE_BASE: "${CI_REGISTRY_IMAGE}/base:${CI_PIPELINE_ID}"
    IMAGE_PIPELINE: "${CI_REGISTRY_IMAGE}/pipeline:${CI_PIPELINE_ID}"

variables:
  PROJECT_INFRASTRUCTURE_CICD_REF: *project-infrastructure-cicd-ref

  # Job parameters
  JAVA_VERSION: 17
  ANDROID_CMDLINETOOLS_VERSION: 6514223
  KOTLIN_VERSION: 1.9.20

  # To be removed later
  GRADLE_BUILD_PARAM_DEXGUARD_USERNAME: ""
  GRADLE_BUILD_PARAM_DEXGUARD_PASSWORD: ""

  CARDINAL_GRADLE_PARAMETERS: -PCARDINAL_USERNAME=${PROD_CARDINAL_USERNAME} -PCARDINAL_API_KEY=${PROD_CARDINAL_API_KEY}  -PGITLAB_READ_TOKEN=${GITLAB_READ_ACCESS_TOKEN}
  GATEWAY_PARAMETERS: -PSTAGE_EU_GATEWAY_URL=${STAGE_EU_GATEWAY_URL} -PSTAGE_EU_GATEWAY_AUTH_TOKEN=${STAGE_EU_GATEWAY_AUTH_TOKEN}
  TEST_EU_GRADLE_PARAMETERS: -PMERCHANT_USERNAME_EU=${STAGE_EU_MERCHANT_USERNAME} -PMERCHANT_SITE_REFERENCE_EU=${STAGE_EU_SITE_REFERENCE} -PMERCHANT_JWT_KEY_EU=${STAGE_EU_MERCHANT_JWT_KEY}
  TEST_JSON_EU_GRADLE_PARAMETERS: -PMERCHANT_USERNAME_JSON_EU=${STAGE_JSON_EU_MERCHANT_USERNAME} -PMERCHANT_SITE_REFERENCE_JSON_EU=${STAGE_JSON_EU_SITE_REFERENCE} -PMERCHANT_PASSWORD_JSON_EU=${STAGE_JSON_EU_MERCHANT_PASSWORD}
  PROD_EU_GRADLE_PARAMETERS: -PMERCHANT_USERNAME=${PROD_EU_MERCHANT_USERNAME} -PMERCHANT_SITE_REFERENCE=${PROD_EU_SITE_REFERENCE} -PMERCHANT_JWT_KEY=${PROD_EU_MERCHANT_JWT_KEY}
  PROD_JSON_EU_GRADLE_PARAMETERS: -PMERCHANT_USERNAME_JSON=${PROD_JSON_EU_MERCHANT_USERNAME} -PMERCHANT_SITE_REFERENCE_JSON=${PROD_JSON_EU_SITE_REFERENCE} -PMERCHANT_PASSWORD_JSON=${PROD_JSON_EU_MERCHANT_PASSWORD}
  TEST_US_GRADLE_PARAMETERS: -PMERCHANT_USERNAME_US=${PROD_US_MERCHANT_USERNAME} -PMERCHANT_SITE_REFERENCE_US=${PROD_US_SITE_REFERENCE} -PMERCHANT_JWT_KEY_US=${PROD_US_MERCHANT_JWT_KEY}
  RELEASE_KEYSTORE_PARAMETERS: -PRELEASE_KEYSTORE_PASSWORD=${RELEASE_KEYSTORE_PASSWORD} -PRELEASE_KEY_PASSWORD=${RELEASE_KEY_PASSWORD} -PRELEASE_KEY_NAME=${RELEASE_KEY_NAME}
  GPG_PARAMETERS: -Psigning.gnupg.keyName=${GPG_KEY_ID} -Psigning.gnupg.passphrase=${GPG_KEY_PASSWORD}

  # Artifacts variables
  ARTIFACTS_BUILD_PATH: .artifacts_build
  ARTIFACT_RELEASE_DESCRIPTION_TYPE: "EXTENDED"

  ### Disabled Jobs:
  # Initialize
  # DISABLE_JOB_copy_cicd_scripts: 'true'
  # Pre-build
  DISABLE_JOB_print_variables: 'true'
  DISABLE_JOB_export_cleanup_artifacts: 'true'
  # DISABLE_JOB_docker_images_build: 'true'
  # DISABLE_JOB_docker_images_pull: 'true'
  # Build
  # utility-images-build - No need to disabling. Extends to enable.
  # Fast Tests
  DISABLE_JOB_unit_tests: 'true'
  DISABLE_JOB_ts_static_analysis: 'true'
  DISABLE_JOB_py_static_analysis: 'true' # TODO https://securetrading.atlassian.net/browse/STJS-950
  DISABLE_JOB_vuln_check: 'true'
  DISABLE_JOB_docker_static_analysis: 'true'
  # DISABLE_JOB_validate_merge_request_description: 'true'
  # Release
  # DISABLE_JOB_release: 'true'
  # Post-Release
  DISABLE_JOB_npm_publish: 'true'
  ### Disabled Jobs : End

  ### Disabled Activities
  # DISABLE_ACTIVITY_docker_image_build_cache: 'true'
  # DISABLE_ACTIVITY_generate_lerna_artifacts: 'true'
  ### Disabled Activities : End

  ##### Others - project specific #####
  # BUILD_IMAGES: "true"
  # CI_DEBUG_TRACE: "true" # For CI/CD extra logs (like actually using variables)
  # LOG_LEVEL: DEBUG # For bash and python scripts
  BUILD_IMAGES: 'true'
  #CI_DEBUG_TRACE: 'true'

  ### Schedules configuration - If you want to use defaults, don't create SCHEDULES_CONFIGURATION variable
  SCHEDULES_CONFIGURATION: |
    SCHEDULED_MASTER_BUILD:
      enabled: false
    SCHEDULED_SMOKE_TESTS:
      enabled: false
    SCHEDULED_NIGHTLY_TESTS_SUPERVISOR:
      enabled: false
    SCHEDULED_NIGHTLY_TESTS_WORKER:
      enabled: false
    SCHEDULED_PACT_VERIFICATION:
      enabled: false
  ### Schedules configuration : End

stages:
  - initialize
  - pre-build
  - prepare-release
  - build
  - post-build
  # Tests
  - fast-tests
  - coverage
  - extended-tests
  - validate-merge-request-description
  # Scheduled tests supervisor
  - scheduled-supervisor-prepare
  - scheduled-supervisor-check
  - scheduled-renovate-trigger
  # Release package
  - release
  - post-release
