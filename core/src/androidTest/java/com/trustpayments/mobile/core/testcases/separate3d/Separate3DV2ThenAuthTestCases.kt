package com.trustpayments.mobile.core.testcases.separate3d

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.rule.ActivityTestRule
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.R
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.services.api.TrustPaymentsGatewayType
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.testutils.createSessionFor
import com.trustpayments.mobile.core.testutils.getAsSuccess
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.util.PaymentSessionResponse
import com.trustpayments.mobile.core.util.parse
import com.trustpayments.mobile.core.utils.ThreeDQueryTestActivity
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2.threeDSecureCode
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class Separate3DV2ThenAuthTestCases : BaseTestCase() {
    private lateinit var paymentTransactionManager: PaymentTransactionManager

    private lateinit var activity: ThreeDQueryTestActivity

    @get:Rule
    val initialActivityTestRule = ActivityTestRule(ThreeDQueryTestActivity::class.java)

    private val jwtBuilder =
        JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    @Before
    override fun setUp() {
        super.setUp()

        val app = ApplicationProvider.getApplicationContext<Context>().applicationContext
        paymentTransactionManager =
            PaymentTransactionManager(
                app,
                TrustPaymentsGatewayType.EU,
                false,
                BuildConfig.MERCHANT_USERNAME,
                isLocationDataConsentGiven = false
            )

        activity = initialActivityTestRule.activity
    }

    //region General: 3DResponse V2 then separate Auth

    @Test
    fun test_positive_3DQueryV2_separateAuth() = runBlocking {
        // GIVEN
        val cardPan = "4000000000001091"
        val cvv = "123"

        val threeDResponse = runSuccessful3DQuery(cardPan, cvv)

        val secondToken = jwtBuilder.getStandardWith3DResponse(
            threeDResponse.transactionReference,
            threeDResponse.threeDResponse!!, listOf(RequestType.Auth)
        )
        val secondSession =
            paymentTransactionManager.createSessionFor(secondToken, cardPan, cvv)

        val secondSessionResult = paymentTransactionManager.executeSession(secondSession).parse()

        Assert.assertTrue(secondSessionResult is PaymentSessionResponse.Success)
    }

    @Test
    fun test_negative_3DQueryV2_separateAuth_wrongParentTransactionReference() = runBlocking {
        // GIVEN
        val cardPan = "4000000000001091"
        val cvv = "123"

        val threeDResponse = runSuccessful3DQuery(cardPan, cvv)

        val secondToken = jwtBuilder.getStandardWith3DResponse(
            "wrong parent transaction",
            threeDResponse.threeDResponse!!, listOf(RequestType.Auth)
        )
        val secondSession =
            paymentTransactionManager.createSessionFor(secondToken, cardPan, cvv)

        val secondSessionResult = paymentTransactionManager.executeSession(secondSession).parse()

        Assert.assertTrue(secondSessionResult is PaymentSessionResponse.Failure.TransactionFailure)
    }

    @Test
    fun test_negative_3DQueryV1_separateAuth_wrong3DResponse() = runBlocking {
        // GIVEN
        val cardPan = "4000000000001091"
        val cvv = "123"

        val threeDResponse = runSuccessful3DQuery(cardPan, cvv)

        val secondToken = jwtBuilder.getStandardWith3DResponse(
            threeDResponse.transactionReference,
            "wrong 3DResponse", listOf(RequestType.Auth)
        )
        val secondSession =
            paymentTransactionManager.createSessionFor(secondToken, cardPan, cvv)

        val secondSessionResult = paymentTransactionManager.executeSession(secondSession).parse()

        Assert.assertTrue(secondSessionResult is PaymentSessionResponse.Failure.TransactionFailure)
    }

    //endregion

    private fun PaymentSessionResponse.getTransactionReferenceFromSuccessfulResponse(): String =
        (this as PaymentSessionResponse.Success).allResponses.first().transactionReference

    private fun runSuccessful3DQuery(cardPan: String, cvv: String) = runBlocking {
        val firstToken = jwtBuilder.getStandard(listOf(RequestType.ThreeDQuery))
        val firstSession =
            paymentTransactionManager.createSessionFor(firstToken, cardPan, cvv)

        // WHEN
        lateinit var firstSessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            firstSessionResult =
                paymentTransactionManager.executeSession(firstSession) { activity }.parse()
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, threeDSecureCode)

        job.join()

        Assert.assertTrue(firstSessionResult is PaymentSessionResponse.Success)

        val firstResponse = firstSessionResult.getAsSuccess(0)
        Assert.assertNotNull(firstResponse.threeDResponse)

        firstResponse
    }
}