package com.trustpayments.mobile.ui.dropin

import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.RippleDrawable
import android.graphics.drawable.StateListDrawable
import android.os.Build
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.view.marginBottom
import androidx.lifecycle.Lifecycle
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.filters.SdkSuppress
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.trustpayments.mobile.ui.GooglePayButton
import com.trustpayments.mobile.ui.PanInputView
import com.trustpayments.mobile.ui.PayButton
import com.trustpayments.mobile.ui.R
import com.trustpayments.mobile.ui.model.GooglePayButtonColor
import com.trustpayments.mobile.ui.model.PaymentInputType
import com.trustpayments.mobile.ui.testutils.StyledDropInPaymentViewTestActivity
import com.trustpayments.mobile.ui.utils.fromDpToPx
import com.trustpayments.mobile.ui.utils.fromSpToPx
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class StyledDropInPaymentViewTest {

    @get:Rule
    val activityScenarioRule =
        ActivityScenarioRule(StyledDropInPaymentViewTestActivity::class.java)

    @Before
    fun setUp() {
        activityScenarioRule.scenario.onActivity {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
    }

    private fun findDropInView(activity: Activity): DropInPaymentView? =
        activity.findViewById(R.id.dropInPaymentView)

    private fun findPanInputView(activity: Activity): PanInputView? {
        val dropInPaymentView = findDropInView(activity)
        return dropInPaymentView?.findViewById(R.id.panInput)
    }

    private fun findPanInputEditText(activity: Activity): TextInputEditText? {
        val panInput = findPanInputView(activity)
        return panInput?.findViewById(R.id.input)
    }

    private fun findPanInputTextLayout(activity: Activity): TextInputLayout? {
        val panInput = findPanInputView(activity)
        return panInput?.findViewById(R.id.inputContainer)
    }

    private fun findPayButton(activity: Activity): PayButton? {
        val dropInPaymentView = findDropInView(activity)
        return dropInPaymentView?.findViewById(R.id.payButton)
    }

    private fun findGooglePayButton(activity: Activity): GooglePayButton? {
        val dropInPaymentView = findDropInView(activity)
        return dropInPaymentView?.findViewById(R.id.googlePayButton)
    }

    private fun findGooglePayButtonBackground(activity: Activity): View? {
        val googlePayButton = findGooglePayButton(activity)
        return googlePayButton?.findViewById(R.id.buttonBackground)
    }

    private fun findGooglePayButtonImage(activity: Activity): View? {
        val googlePayButton = findGooglePayButton(activity)
        return googlePayButton?.findViewById(R.id.googlePayImage)
    }

    @Test
    fun test_inputTextColor() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedInputTextColor = Color.parseColor("#B00020")

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val actualInputTextColor = findPanInputEditText(activity)?.textColors?.defaultColor
            assertEquals(expectedInputTextColor, actualInputTextColor)
        }
    }

    @Test
    fun test_inputTextSize() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedInputTextSize = 20.0f.fromSpToPx()

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val actualInputTextSize = findPanInputEditText(activity)?.textSize
            assertEquals(expectedInputTextSize, actualInputTextSize)
        }
    }

    @Test
    @SdkSuppress(minSdkVersion = Build.VERSION_CODES.N)
    fun test_inputBackgroundColor() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedInputColor = Color.parseColor("#929292")

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val actualInputColor = findPanInputTextLayout(activity)?.boxBackgroundColor
            assertEquals(expectedInputColor, actualInputColor)
        }
    }

   @Test
    @SdkSuppress(minSdkVersion = Build.VERSION_CODES.N)
    fun test_inputCornerRadius() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedCornerRadius = 10.0f.fromDpToPx()

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val inputTextLayout = findPanInputTextLayout(activity)
            assertNotNull(inputTextLayout)
            assertEquals(expectedCornerRadius, inputTextLayout?.boxCornerRadiusTopStart)
            assertEquals(expectedCornerRadius, inputTextLayout?.boxCornerRadiusTopEnd)
            assertEquals(expectedCornerRadius, inputTextLayout?.boxCornerRadiusBottomStart)
            assertEquals(expectedCornerRadius, inputTextLayout?.boxCornerRadiusBottomEnd)
        }
    }

    @Test
    fun test_inputHintTextColor() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedHintTextColor = Color.parseColor("#018786")

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val actualHintTextColor = findPanInputEditText(activity)?.hintTextColors?.defaultColor

            assertEquals(expectedHintTextColor, actualHintTextColor)
        }
    }

    @Test
    fun test_inputSpacing() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedSpacing = 20.fromDpToPx()

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)

            val actualSpacing = findPanInputTextLayout(activity)?.marginBottom

            assertEquals(expectedSpacing, actualSpacing)
        }
    }

    @Test
    fun test_payButtonTextColor() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedTextColor = Color.parseColor("#000000")

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val actualTextColor = findPayButton(activity)?.textColors?.defaultColor
            assertEquals(expectedTextColor, actualTextColor)
        }
    }

    @Test
    fun test_payButtonTextSize() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedTextSize = 20.0f.fromSpToPx()

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val actualTextSize = findPayButton(activity)?.textSize
            assertEquals(expectedTextSize, actualTextSize)
        }
    }

    @Test
    @SdkSuppress(minSdkVersion = Build.VERSION_CODES.N)
    fun test_disabledPayButtonBackgroundColor() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedIsPayButtonEnabled = false
        val expectedPayButtonColor = Color.parseColor("#595959")

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            val actualIsPayButtonEnabled = findPayButton(activity)?.isEnabled
            val actualPayButtonColor = findPayButton(activity)?.getButtonBackgroundColor()

            assertEquals(expectedIsPayButtonEnabled, actualIsPayButtonEnabled)
            assertEquals(expectedPayButtonColor, actualPayButtonColor)
        }
    }

    @Test
    @SdkSuppress(minSdkVersion = Build.VERSION_CODES.N)
    fun test_enabledPayButtonBackgroundColor() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val dropInPaymentViewListener =
            mockk<DropInPaymentView.DropInPaymentViewListener>()
        val expectedIsPayButtonEnabled = true
        val expectedPayButtonColor = Color.parseColor("#03DAC6")
        every { dropInPaymentViewListener.onInputValid(any(), any()) } returns Unit

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            dropInPaymentView.dropInPaymentViewListener = dropInPaymentViewListener

            dropInPaymentView.onInputValid(PaymentInputType.PAN, "")
            dropInPaymentView.onInputValid(PaymentInputType.ExpiryDate, "")
            dropInPaymentView.onInputValid(PaymentInputType.CVV, "")

            val actualIsPayButtonEnabled = findPayButton(activity)?.isEnabled
            val actualPayButtonColor = findPayButton(activity)?.getButtonBackgroundColor()

            assertEquals(expectedIsPayButtonEnabled, actualIsPayButtonEnabled)
            assertEquals(expectedPayButtonColor, actualPayButtonColor)
        }
    }

    @Test
    @SdkSuppress(minSdkVersion = Build.VERSION_CODES.N)
    fun test_payButtonCornerRadius() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedCornerRadius = 10.0f.fromDpToPx()

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val actualCornerRadius = findPayButton(activity)?.getButtonCornerRadius()
            assertEquals(expectedCornerRadius, actualCornerRadius)
        }
    }

    @Test
    fun test_payButtonPadding() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedPadding = List(4) {
            30.fromDpToPx()
        }

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)

            val payButton = findPayButton(activity)
            val actualPadding = listOf(
                payButton?.paddingLeft,
                payButton?.paddingTop,
                payButton?.paddingRight,
                payButton?.paddingBottom
            )

            assertEquals(expectedPadding, actualPadding)
        }
    }

    @Test
    @SdkSuppress(minSdkVersion = Build.VERSION_CODES.N)
    fun test_changeGooglePayConfig() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            dropInPaymentView.setupForGooglePayPayment(
                isEnabled = true,
                buttonColor = GooglePayButtonColor.WHITE,
                withText = false,
                withShadow = false
            )

            assertEquals(findGooglePayButton(activity)?.visibility, View.VISIBLE)
            assertEquals(
                findGooglePayButtonBackground(activity)?.tag,
                R.drawable.googlepay_button_no_shadow_background_white
            )
            assertEquals(
                findGooglePayButtonImage(activity)?.tag,
                R.drawable.googlepay_button_content_white
            )
        }
    }

    private fun View.getInputBackgroundColor() =
        ((this.background as StateListDrawable).current as GradientDrawable).color?.defaultColor

    private fun View.getInputCornerRadius() =
        ((this.background as StateListDrawable).current as GradientDrawable).cornerRadii?.get(0)

    private fun View.getButtonBackgroundColor() =
        ((this.background as RippleDrawable)
            .getDrawable(0).current as GradientDrawable).color?.defaultColor

    private fun View.getButtonCornerRadius() =
        ((this.background as RippleDrawable)
            .getDrawable(0).current as GradientDrawable).cornerRadii?.get(
            0
        )
}