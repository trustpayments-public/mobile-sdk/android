package com.trustpayments.mobile.core.services.api

import android.content.Context
import android.os.Build
import com.trustpayments.mobile.core.models.api.jwt.JwtToken
import com.trustpayments.mobile.core.models.api.request.TransactionRequest
import com.trustpayments.mobile.core.models.api.request.WebserviceRequest
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.models.api.response.TransactionResponse
import com.trustpayments.mobile.core.models.api.response.WebserviceResponse
import com.trustpayments.mobile.core.services.getGson
import com.trustpayments.mobile.core.services.transaction.LoggingManager
import com.trustpayments.mobile.core.services.transaction.ErrorMessages
import com.trustpayments.mobile.core.utils.logJson
import com.trustpayments.mobile.core.analytics.logger.protocol.LogLevel
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.services.api.TrustPaymentsApiService.Companion.EU_BACKUP_GATEWAY
import com.trustpayments.mobile.core.services.api.TrustPaymentsApiService.Companion.EU_PRODUCTION_GATEWAY
import com.trustpayments.mobile.core.services.api.TrustPaymentsApiService.Companion.EU_STAGING_GATEWAY
import com.trustpayments.mobile.core.services.api.TrustPaymentsApiService.Companion.US_PRODUCTION_GATEWAY
import okhttp3.CertificatePinner
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit

internal class TrustPaymentsApiService private constructor(
    context: Context,
    gatewayType: TrustPaymentsGatewayType,
    private val loggingManager: LoggingManager
) {
    companion object {

        internal const val EU_PRODUCTION_GATEWAY = "https://webservices.securetrading.net/"
        internal const val EU_BACKUP_GATEWAY = "https://webservices2.securetrading.net/"
        internal const val EU_STAGING_GATEWAY = BuildConfig.STAGE_GATEWAY_URL
        internal const val US_PRODUCTION_GATEWAY = "https://webservices.securetrading.us/"

        @JvmSynthetic
        internal fun getInstance(
            context: Context,
            gatewayType: TrustPaymentsGatewayType,
            loggingManager: LoggingManager
        ) = TrustPaymentsApiService(context, gatewayType, loggingManager)
    }

    private val retrofitApi: TrustPaymentsApi

    private val MAX_RETRY_NUMBER = 20
    private val MAX_RETRY_TIMEOUT_SEC = 40
    private val CONNECT_TIMEOUT_SEC = 5L
    private val READ_WRITE_TIMEOUT_SEC = 60L
    private val PARAM_TOKEN = "token"

    private val contentFormatVersion = "1.00"
    private val acceptFormatVersion = "2.00"
    private val sdkName = "MSDK"
    private val versionInfo =
        "$sdkName::Android::${BuildConfig.SDK_VERSION_NAME}::${Build.MODEL}::${Build.VERSION.RELEASE}"

    private val gson = getGson()

    init {
        val headerInterceptor = Interceptor { chain ->
            val newRequest = chain
                .request()
                .newBuilder()
                .addHeader("Accept", "application/json")
                .build()
            chain.proceed(newRequest)
        }

        val okHttpClient = OkHttpClient.Builder()
            .applyPinning(
                CertificatePinner.Builder().apply {
                    if (gatewayType.url == EU_PRODUCTION_GATEWAY) {
                        add(
                            "webservices.securetrading.net",
                            "sha256/kCv4KV+TUcfQ7XFk1Hk4oF2JlFRk9fObpVuZCKCZ/mk="
                        )
                        add(
                            "webservices.securetrading.net",
                            "sha256/yMZhDu5hIsQaSd5wdC0kIxImZ2BpJPz5YGXasZe0IGQ="
                        )
                    }
                }.build()
            )
            .apply {
                addInterceptor(
                    HttpLoggingInterceptor { message -> logJson(message) }.apply {
                        level = HttpLoggingInterceptor.Level.BODY
                    })
            }
            .readTimeout(READ_WRITE_TIMEOUT_SEC, TimeUnit.SECONDS)
            .writeTimeout(READ_WRITE_TIMEOUT_SEC, TimeUnit.SECONDS)
            .connectTimeout(CONNECT_TIMEOUT_SEC, TimeUnit.SECONDS)
            .addInterceptor(headerInterceptor)
            .addInterceptor { chain ->
                val originalRequest = chain.request()
                val modifiedRequest = BuildConfig.STAGE_GATEWAY_AUTH_TOKEN
                    .takeIf { it.isNotBlank() }?.let {
                        val newUrl = originalRequest.url.newBuilder()
                            .addQueryParameter(PARAM_TOKEN, it)
                            .build()
                        originalRequest.newBuilder()
                            .url(newUrl)
                            .build()
                    }
                chain.proceed(modifiedRequest ?: originalRequest)
            }
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(gatewayType.url)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        retrofitApi = retrofit.create(TrustPaymentsApi::class.java)
    }

    @JvmSynthetic
    internal suspend fun initializeSession(
        sessionId: String, username: String, jwt: String
    ): Result {
        val paymentRequest =
            TransactionRequest(
                sessionId,
                requestTypes = arrayOf(
                    RequestType.JsInit
                ),
            )
        val webRequest =
            WebserviceRequest(
                username,
                jwt,
                contentFormatVersion,
                versionInfo,
                acceptFormatVersion,
                arrayOf(paymentRequest)
            )

        return performRequest(webRequest)
    }

    @JvmSynthetic
    internal suspend fun performTransaction(
        sessionId: String,
        username: String,
        jwt: String,
        cacheToken: String? = null,
        cardNumber: String? = null,
        cardExpiryDate: String? = null,
        cardSecurityCode: String? = null,
        threeDResponse: String? = null,
        pares: String? = null,
        walletToken: String? = null,
        walletSource: String? = null,
        returnUrl: String? = null,
        apmCode: String? = null,
    ): Result {
        val paymentRequest =
            TransactionRequest(
                sessionId,
                threeDResponse = threeDResponse,
                pares = pares,
                cacheToken = cacheToken,
                cardNumber = cardNumber,
                cardExpiryDate = cardExpiryDate,
                cardSecurityCode = cardSecurityCode,
                walletToken = walletToken,
                walletSource = walletSource,
                returnUrl = returnUrl,
                apmCode = apmCode,
            )
        val webRequest =
            WebserviceRequest(
                username,
                jwt,
                contentFormatVersion,
                versionInfo,
                acceptFormatVersion,
                arrayOf(paymentRequest)
            )

        return performRequest(webRequest)
    }

    private suspend fun performRequest(request: WebserviceRequest): Result {
        return try {
            executeRequest(request)
        } catch (e: Exception) {
            loggingManager.sendExceptionEvent(e)
            Result.Failure
        }
    }

    private suspend fun executeRequest(request: WebserviceRequest): Result {
        val startTimeoutMillis = System.currentTimeMillis()
        var numberOfRetries = 0

        while (numberOfRetries < MAX_RETRY_NUMBER && (System.currentTimeMillis() + CONNECT_TIMEOUT_SEC * 1000 < startTimeoutMillis + MAX_RETRY_TIMEOUT_SEC * 1000)) {
            val response = try {
                retrofitApi.sendJwtRequest(request)
            } catch (ex: Exception) {
                when (ex) {
                    is SocketTimeoutException,
                    is ConnectException -> {
                        //this can happen if there is networking error (connect)
                        numberOfRetries++
                        continue
                    }
                    //this can happen if there is networking error (not connect) or parsing error (no jwt found, or response structure is incorrect)
                    else -> return Result.Failure
                }
            }

            //at this point either there is a HTTP status code error (in which case response was not yet deserialized)
            //or HTTP status code is ok, and response was successfully deserialized
            //we are treating single transaction deserialization errors as 'correct' responses, returning TransactionResponse.ParsingError
            return validateResponse(response)
        }

        //maximum number of retries has been reached
        return Result.Failure
    }

    private fun validateResponse(response: retrofit2.Response<WebserviceResponse>): Result {
        if (!response.isSuccessful) {
            loggingManager.sendErrorEvent(
                ErrorMessages.HttpFailure,
                ErrorMessages.TrustPaymentsApiServiceEvent,
                LogLevel.ERROR,
                response.code(),
                response.message()
            )
            return Result.Failure
        }

        val body = response.body()

        if (body == null) {
            loggingManager.sendErrorEvent(
                ErrorMessages.NoBody,
                ErrorMessages.TrustPaymentsApiServiceEvent,
                LogLevel.ERROR
            )
            return Result.Failure
        }

        val newJwt = JwtToken(body.body.payload.jwt)
        if (!newJwt.isValid) {
            loggingManager.sendErrorEvent(
                ErrorMessages.JWTInvalid,
                ErrorMessages.TrustPaymentsApiServiceEvent,
                LogLevel.ERROR
            )
            return Result.Failure
        }

        return Result.Success(body.jwt, newJwt, body.body.payload.transactionResponses.toList())
    }

    internal sealed class Result {
        data class Success(
            val responseJwt: JwtToken,
            val newJwt: JwtToken,
            val transactionResponses: List<TransactionResponse>
        ) : Result()

        object Failure : Result()
    }
}

internal interface TrustPaymentsApi {
    @POST("jwt/")
    suspend fun sendJwtRequest(@Body request: WebserviceRequest): Response<WebserviceResponse>
}

enum class TrustPaymentsGatewayType(val url: String) {
    EU(EU_STAGING_GATEWAY.takeIf { it.isNotBlank() } ?: EU_PRODUCTION_GATEWAY),
    EU_BACKUP(EU_BACKUP_GATEWAY),
    US(US_PRODUCTION_GATEWAY)
}