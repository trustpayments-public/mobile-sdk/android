package com.trustpayments.mobile.core.testcases.locale

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.PaymentSession
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.testutils.DateHelper
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.util.ResponseParser
import com.trustpayments.mobile.utils.core.testcardsdata.GeneralCardsData.failedCardNumber
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class PayloadLocaleTestCases : BaseTestCase() {
    private lateinit var paymentTransactionManager: PaymentTransactionManager

    private val jwtBuilder =
        JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    private val failedCardNum = failedCardNumber
    private val cvv = "123"
    private val expiryDate = DateHelper.getValidCardExpiryDate()

    @Before
    override fun setUp() {
        super.setUp()

        val app = ApplicationProvider.getApplicationContext<Context>().applicationContext
        paymentTransactionManager =
            PaymentTransactionManager(app, getGatewayType(), false, BuildConfig.MERCHANT_USERNAME, isLocationDataConsentGiven = false)
    }

    @Test
    fun test_positive_en_GB_locale() = runBlocking {
        // GIVEN

        val session = createLocaleSession(
            listOf(
                RequestType.Auth
            )
        )
        val result = paymentTransactionManager.executeSession(session)
        val response = ResponseParser.parse(result.responseJwtList[0])

        Assert.assertTrue(response?.responses?.first()?.errorMessage == "Bank System Error")

    }

    @Test
    fun test_positive_en_US_locale() = runBlocking {
        // GIVEN

        val session = createLocaleSession(
            listOf(
                RequestType.Auth
            ), "en_US"
        )
        val result = paymentTransactionManager.executeSession(session)
        val response = ResponseParser.parse(result.responseJwtList[0])

        Assert.assertTrue(response?.responses?.first()?.errorMessage == "Bank System Error")

    }

    @Test
    fun test_positive_fr_FR_locale() = runBlocking {
        // GIVEN

        val session = createLocaleSession(
            listOf(
                RequestType.Auth
            ), "fr_FR"
        )
        val result = paymentTransactionManager.executeSession(session)
        val response = ResponseParser.parse(result.responseJwtList[0])

        Assert.assertTrue(response?.responses?.first()?.errorMessage == "Erreur de système bancaire")

    }

    @Test
    fun test_positive_deDe_locale() = runBlocking {
        // GIVEN

        val session = createLocaleSession(
            listOf(
                RequestType.Auth
            ), "de_DE"
        )
        val result = paymentTransactionManager.executeSession(session)
        val response = ResponseParser.parse(result.responseJwtList[0])

        Assert.assertTrue(response?.responses?.first()?.errorMessage == "Banksystemfehler")

    }

    @Test
    fun test_positive_cyGb_locale() = runBlocking {
        // GIVEN

        val session = createLocaleSession(
            listOf(
                RequestType.Auth
            ), "cy_GB"
        )
        val result = paymentTransactionManager.executeSession(session)
        val response = ResponseParser.parse(result.responseJwtList[0])

        Assert.assertTrue(response?.responses?.first()?.errorMessage == "Gwall yn System y Banc")

    }

    @Test
    fun test_positive_daDk_locale() = runBlocking {
        // GIVEN

        val session = createLocaleSession(
            listOf(
                RequestType.Auth
            ), "da_DK"
        )
        val result = paymentTransactionManager.executeSession(session)
        val response = ResponseParser.parse(result.responseJwtList[0])

        Assert.assertTrue(response?.responses?.first()?.errorMessage == "Banksystemfejl")

    }

    @Test
    fun test_positive_esEs_locale() = runBlocking {
        // GIVEN

        val session = createLocaleSession(
            listOf(
                RequestType.Auth
            ), "es_ES"
        )
        val result = paymentTransactionManager.executeSession(session)
        val response = ResponseParser.parse(result.responseJwtList[0])

        Assert.assertTrue(response?.responses?.first()?.errorMessage == "Error del sistema del banco")

    }

    @Test
    fun test_positive_nlNl_locale() = runBlocking {
        // GIVEN

        val session = createLocaleSession(
            listOf(
                RequestType.Auth
            ), "nl_NL"
        )
        val result = paymentTransactionManager.executeSession(session)
        val response = ResponseParser.parse(result.responseJwtList[0])

        Assert.assertTrue(response?.responses?.first()?.errorMessage == "Fout in banksysteem")

    }

    @Test
    fun test_positive_noNo_locale() = runBlocking {
        // GIVEN

        val session = createLocaleSession(
            listOf(
                RequestType.Auth
            ), "no_NO"
        )
        val result = paymentTransactionManager.executeSession(session)
        val response = ResponseParser.parse(result.responseJwtList[0])

        Assert.assertTrue(response?.responses?.first()?.errorMessage == "Banksystemfeil")

    }

    @Test
    fun test_positive_svSe_locale() = runBlocking {
        // GIVEN

        val session = createLocaleSession(
            listOf(
                RequestType.Auth
            ), "sv_SE"
        )
        val result = paymentTransactionManager.executeSession(session)
        val response = ResponseParser.parse(result.responseJwtList[0])

        Assert.assertTrue(response?.responses?.first()?.errorMessage == "Banksystemfel")

    }

    @Test
    fun test_positive_itIt_locale() = runBlocking {
        // GIVEN

        val session = createLocaleSession(
            listOf(
                RequestType.Auth
            ), "it_IT"
        )
        val result = paymentTransactionManager.executeSession(session)
        val response = ResponseParser.parse(result.responseJwtList[0])

        Assert.assertTrue(response?.responses?.first()?.errorMessage == "Errore di sistema bancario")

    }

    private fun createLocaleSession(
        requests: List<RequestType>,
        locale: String? = null
    ): PaymentSession {
        val jwt = jwtBuilder.getForLocale(requests, locale)
        return PaymentSession({ jwt }, failedCardNum, expiryDate, cvv)
    }


}