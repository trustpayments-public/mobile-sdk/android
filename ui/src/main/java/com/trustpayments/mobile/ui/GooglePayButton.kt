package com.trustpayments.mobile.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import com.trustpayments.mobile.ui.common.PaymentView
import com.trustpayments.mobile.ui.databinding.GooglepayButtonBinding
import com.trustpayments.mobile.ui.model.GooglePayButtonColor

class GooglePayButton(
    context: Context, attrs: AttributeSet?
) : RelativeLayout(context, attrs), PaymentView {

    private var binding: GooglepayButtonBinding

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = GooglepayButtonBinding.inflate(inflater, this, true)
        attrs?.let {
            setupAttributes(it)
        }
    }

    private fun setupAttributes(attrs: AttributeSet) {
        val typedArray = context.obtainStyledAttributes(
            attrs,
            R.styleable.GooglePayButton,
            0,
            0
        )

        val buttonColor = GooglePayButtonColor.values().find {
            it.color == typedArray.getInt(
                R.styleable.GooglePayButton_buttonColor,
                GooglePayButtonColor.BLACK.color
            )
        }!!
        val withText = typedArray.getBoolean(R.styleable.GooglePayButton_withText, true)
        val withShadow = typedArray.getBoolean(R.styleable.GooglePayButton_withShadow, true)

        setupButtonView(buttonColor, withText, withShadow)

        typedArray.recycle()
    }

    fun setupButtonView(
        color: GooglePayButtonColor = GooglePayButtonColor.BLACK,
        withText: Boolean = true,
        withShadow: Boolean = true
    ) {
        val imageResource: Int
        val backgroundResource: Int
        if (color == GooglePayButtonColor.BLACK) {
            imageResource =
                if (withText)
                    R.drawable.buy_with_googlepay_button_content
                else
                    R.drawable.googlepay_button_content
            backgroundResource =
                if (withShadow)
                    R.drawable.googlepay_button_background
                else
                    R.drawable.googlepay_button_no_shadow_background
        } else {
            imageResource =
                if (withText)
                    R.drawable.buy_with_googlepay_button_content_white
                else
                    R.drawable.googlepay_button_content_white
            backgroundResource =
                if (withShadow)
                    R.drawable.googlepay_button_background_white
                else
                    R.drawable.googlepay_button_no_shadow_background_white
        }

        binding.googlePayImage.apply {
            setImageResource(imageResource)
            tag = imageResource
        }

        binding.buttonBackground.apply {
            setBackgroundResource(backgroundResource)
            tag = backgroundResource
        }
    }
}