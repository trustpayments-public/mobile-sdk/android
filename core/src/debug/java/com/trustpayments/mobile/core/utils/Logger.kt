package com.trustpayments.mobile.core.utils

import android.util.Log
import org.json.JSONObject

internal fun log(message: String) {
    log("TPSDK", message)
}

internal fun log(tag: String, message: String) {
    message.lines().forEach {
        it.chunked(2000).forEach {line ->
            Log.d(tag, line)
        }
    }
}

internal fun logJson(json: String) {
    logJson("TPSDK", json)
}

internal fun logJson(tag: String, json: String) {
    try {
        val message = JSONObject(json).toString(4)

        log(tag, message)
    } catch (ex: Exception) {
        log(tag, json)
    }
}