package com.trustpayments.mobile.ui.model

enum class PaymentInputType {
    PAN,
    ExpiryDate,
    CVV
}