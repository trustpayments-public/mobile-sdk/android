package com.trustpayments.mobile.core

import android.app.Activity
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.api.Status
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.TaskCompletionSource
import com.google.android.gms.wallet.AutoResolveHelper
import com.google.android.gms.wallet.PaymentData
import com.google.android.gms.wallet.PaymentsClient
import com.google.android.gms.wallet.WalletConstants
import com.trustpayments.mobile.core.googlepay.TPGooglePayManager
import com.trustpayments.mobile.core.googlepay.TPGooglePayManager.Companion.centsToString
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.unmockkStatic
import io.mockk.verify
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.android.controller.ActivityController
import org.robolectric.annotation.Config
import kotlin.reflect.full.declaredMemberFunctions
import kotlin.reflect.jvm.isAccessible


@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.M])
class GooglePayTest {

    private var activity: Activity? = null

    @Before
    fun setup() {
        val activityController: ActivityController<Activity> = Robolectric.buildActivity(
            Activity::class.java
        )
        activity = activityController.get()
    }

    @Test
    fun test_tpGooglePayParamValues() {
        //GIVEN
        val expectedCountryCode = "GB"
        val expectedCurrencyCode = "GBP"
        val expectedGatewayMerchantId = BuildConfig.SITE_REFERENCE
        val expectedActivity = activity!!

        //WHEN
        val gPayBuilder = TPGooglePayManager.Builder(
            expectedActivity,
            WalletConstants.ENVIRONMENT_TEST,
            expectedCountryCode,
            expectedCurrencyCode,
            expectedGatewayMerchantId
        )
        val tpGPayManager = gPayBuilder.build()
        val gPayInstance2 = TPGooglePayManager(gPayBuilder)

        //THEN
        assertEquals(gPayBuilder.countryCode, expectedCountryCode)
        assertEquals(gPayBuilder.currencyCode, expectedCurrencyCode)
        assertEquals(gPayBuilder.gatewayMerchantId, expectedGatewayMerchantId)

        assertEquals(tpGPayManager.countryCode, expectedCountryCode)
        assertEquals(tpGPayManager.currencyCode, expectedCurrencyCode)
        assertEquals(tpGPayManager.gatewayMerchantId, expectedGatewayMerchantId)

        assertEquals(gPayInstance2.countryCode, expectedCountryCode)
        assertEquals(gPayInstance2.currencyCode, expectedCurrencyCode)
        assertEquals(gPayInstance2.gatewayMerchantId, expectedGatewayMerchantId)

        assertEquals(gPayBuilder.activity, expectedActivity)
        assertEquals(tpGPayManager.activity, expectedActivity)
        assertEquals(gPayInstance2.activity, expectedActivity)
    }

    @Test
    fun test_tpGooglePayParamValues_init_direct() {
        //GIVEN
        val expectedCountryCode = "GB"
        val expectedCurrencyCode = "GBP"
        val expectedGatewayMerchantId = BuildConfig.SITE_REFERENCE

        //WHEN
        val gPayBuilder = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "US",
            "USD",
            "TEST"
        )
        gPayBuilder.apply {
            countryCode = expectedCountryCode
            currencyCode = expectedCurrencyCode
            gatewayMerchantId = expectedGatewayMerchantId
        }
        val tpGPayManager = gPayBuilder.build()

        //THEN
        assertEquals(gPayBuilder.countryCode, expectedCountryCode)
        assertEquals(gPayBuilder.currencyCode, expectedCurrencyCode)
        assertEquals(gPayBuilder.gatewayMerchantId, expectedGatewayMerchantId)

        assertEquals(tpGPayManager.countryCode, expectedCountryCode)
        assertEquals(tpGPayManager.currencyCode, expectedCurrencyCode)
        assertEquals(tpGPayManager.gatewayMerchantId, expectedGatewayMerchantId)
    }

    @Test
    fun test_tpGooglePayDefaultValues() {
        //GIVEN
        val expectedApiVersion = 2
        val expectedApiVersionMinor = 0

        val expectedBillingAddressRequired = false
        val expectedFormat = "FULL"
        val expectedBillingPhoneNumberRequired = false

        val expectedShippingAddressRequired = false
        val expectedAllowedCountryCodes = JSONArray(emptyList<String>())
        val expectedShippingPhoneNumberRequired = false

        val expectedRequestCode = TPGooglePayManager.REQUEST_CODE

        val expectedGateway = "trustpayments"

        val expectedSupportedMethods = JSONArray(
            listOf(
                "PAN_ONLY",
                "CRYPTOGRAM_3DS"
            )
        )

        val expectedSupportedNetworks = JSONArray(
            listOf(
                "AMEX",
                "DISCOVER",
                "JCB",
                "MASTERCARD",
                "VISA"
            )
        )

        val hasMerchantName = false

        //WHEN
        val gPayBuilder = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        )
        val tpGPayManager = gPayBuilder.build()
        val gPayInstance2 = TPGooglePayManager(gPayBuilder)

        //THEN
        assertEquals(expectedApiVersion, gPayBuilder.apiVersion)
        assertEquals(expectedApiVersionMinor, gPayBuilder.apiVersionMinor)
        assertEquals(expectedGateway, gPayBuilder.gateway)
        assertEquals(expectedBillingAddressRequired, gPayBuilder.billingAddressRequired)
        assertEquals(expectedShippingAddressRequired, gPayBuilder.shippingAddressRequired)

        assertEquals(expectedGateway, gPayInstance2.gateway)
        assertEquals(expectedBillingAddressRequired, gPayInstance2.billingAddressRequired)
        assertEquals(expectedShippingAddressRequired, gPayInstance2.shippingAddressRequired)

        assertEquals(tpGPayManager.baseRequest.getInt("apiVersion"), expectedApiVersion)
        assertEquals(
            tpGPayManager.baseRequest.getInt("apiVersionMinor"),
            expectedApiVersionMinor
        )
        assertEquals(expectedGateway, tpGPayManager.gateway)
        assertEquals(tpGPayManager.billingAddressRequired, expectedBillingAddressRequired)
        assertEquals(
            tpGPayManager.billingAddressParameters.getString("format"),
            expectedFormat
        )
        assertEquals(
            tpGPayManager.billingAddressParameters.getBoolean("phoneNumberRequired"),
            expectedBillingPhoneNumberRequired
        )
        assertEquals(tpGPayManager.shippingAddressRequired, expectedShippingAddressRequired)
        assertEquals(
            tpGPayManager.shippingAddressParameters.getBoolean("phoneNumberRequired"),
            expectedShippingPhoneNumberRequired
        )
        assertEquals(
            tpGPayManager.shippingAddressParameters.getJSONArray("allowedCountryCodes"),
            expectedAllowedCountryCodes
        )
        assertEquals(tpGPayManager.requestCode, expectedRequestCode)
        assertEquals(tpGPayManager.allowedCardAuthMethods, expectedSupportedMethods)
        assertEquals(tpGPayManager.allowedCardNetworks, expectedSupportedNetworks)
        assertEquals(tpGPayManager.merchantInfo.has("merchantName"), hasMerchantName)
    }

    @Test
    fun test_tpGooglePaySetApiVersion() {
        //GIVEN
        val expectedApiVersion = 1

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).setApiVersion(expectedApiVersion)
            .build()

        //THEN
        assertEquals(tpGooglePay.baseRequest.getInt("apiVersion"), expectedApiVersion)
    }

    @Test
    fun test_tpGooglePaySetApiVersion_direct() {
        //GIVEN
        val expectedApiVersion = 1

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).apply {
            apiVersion = expectedApiVersion
        }
            .build()

        //THEN
        assertEquals(tpGooglePay.baseRequest.getInt("apiVersion"), expectedApiVersion)
    }

    @Test
    fun test_tpGooglePaySetApiVersionMinor() {
        //GIVEN
        val expectedApiVersionMinor = 1

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).setApiVersionMinor(expectedApiVersionMinor)
            .build()

        //THEN
        assertEquals(tpGooglePay.baseRequest.getInt("apiVersionMinor"), expectedApiVersionMinor)
    }

    @Test
    fun test_tpGooglePaySetApiVersionMinor_direct() {
        //GIVEN
        val expectedApiVersionMinor = 1

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).apply {
            apiVersionMinor = expectedApiVersionMinor
        }
            .build()

        //THEN
        assertEquals(tpGooglePay.baseRequest.getInt("apiVersionMinor"), expectedApiVersionMinor)
    }

    @Test
    fun test_tpGooglePaySetGateway() {
        //GIVEN
        val expectedGateway = "gateway"

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).setGateway(expectedGateway)
            .build()

        //THEN
        assertEquals(expectedGateway, tpGooglePay.gateway)
    }

    @Test
    fun test_tpGooglePaySetGateway_direct() {
        //GIVEN
        val expectedGateway = "gateway"

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        )
            .apply {
                gateway = expectedGateway
            }
            .build()

        //THEN
        assertEquals(expectedGateway, tpGooglePay.gateway)
    }

    @Test
    fun test_tpGooglePaySetBillingInfo() {
        //GIVEN
        val expectedBillingAddressRequired = true
        val expectedFormat = "MIN"
        val expectedBillingPhoneNumberRequired = true

        //WHEN
        val gPayBuilder = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        )
        gPayBuilder.setBillingAddressRequired(
            expectedBillingAddressRequired,
            format = expectedFormat,
            phoneNumberRequired = expectedBillingPhoneNumberRequired
        )
        val tpGPayManager = gPayBuilder.build()

        //THEN
        assertEquals(expectedBillingAddressRequired, tpGPayManager.billingAddressRequired)
        assertEquals(
            expectedFormat,
            tpGPayManager.billingAddressParameters.getString("format"),

            )
        assertEquals(
            expectedBillingPhoneNumberRequired,
            tpGPayManager.billingAddressParameters.getBoolean("phoneNumberRequired"),
        )
    }

    @Test
    fun test_tpGooglePaySetBillingInfo_direct() {
        //GIVEN
        val expectedBillingAddressRequired = true
        val expectedFormat = "MIN"
        val expectedBillingPhoneNumberRequired = true

        //WHEN
        val gPayBuilder = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        )
        gPayBuilder.apply {
            billingAddressRequired = expectedBillingAddressRequired
            format = expectedFormat
            billingPhoneNumberRequired = expectedBillingPhoneNumberRequired
        }
        val tpGPayManager = gPayBuilder.build()

        //THEN
        assertEquals(expectedBillingAddressRequired, tpGPayManager.billingAddressRequired)
        assertEquals(
            expectedFormat,
            tpGPayManager.billingAddressParameters.getString("format"),

            )
        assertEquals(
            expectedBillingPhoneNumberRequired,
            tpGPayManager.billingAddressParameters.getBoolean("phoneNumberRequired"),
        )
    }

    @Test
    fun test_tpGooglePaySetShippingInfo() {
        //GIVEN
        val expectedShippingAddressRequired = true
        val expectedAllowedCountryCodes = JSONArray(listOf("GB"))
        val expectedShippingPhoneNumberRequired = true

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).setShippingAddressRequired(
            expectedShippingAddressRequired,
            allowedCountryCodes = listOf("GB"),
            phoneNumberRequired = expectedShippingPhoneNumberRequired
        )
            .build()

        //THEN
        assertEquals(tpGooglePay.shippingAddressRequired, expectedShippingAddressRequired)
        assertEquals(
            tpGooglePay.shippingAddressParameters.getBoolean("phoneNumberRequired"),
            expectedShippingPhoneNumberRequired
        )
        assertEquals(
            tpGooglePay.shippingAddressParameters.getJSONArray("allowedCountryCodes"),
            expectedAllowedCountryCodes
        )
    }

    @Test
    fun test_tpGooglePaySetShippingInfo_direct() {
        //GIVEN
        val expectedShippingAddressRequired = true
        val expectedAllowedCountryCodes = JSONArray(listOf("GB"))
        val expectedShippingPhoneNumberRequired = true

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).apply {
            shippingAddressRequired = expectedShippingAddressRequired
            allowedCountryCodes = listOf("GB")
            shippingPhoneNumberRequired = expectedShippingPhoneNumberRequired
        }
            .build()

        //THEN
        assertEquals(tpGooglePay.shippingAddressRequired, expectedShippingAddressRequired)
        assertEquals(
            tpGooglePay.shippingAddressParameters.getBoolean("phoneNumberRequired"),
            expectedShippingPhoneNumberRequired
        )
        assertEquals(
            tpGooglePay.shippingAddressParameters.getJSONArray("allowedCountryCodes"),
            expectedAllowedCountryCodes
        )
    }

    @Test
    fun test_tpGooglePaySetRequestCode() {
        //GIVEN
        val expectedRequestCode = 8989

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).setRequestCode(expectedRequestCode)
            .build()

        //THEN
        assertEquals(tpGooglePay.requestCode, expectedRequestCode)
    }

    @Test
    fun test_tpGooglePaySetRequestCode_direct() {
        //GIVEN
        val expectedRequestCode = 8989

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).apply {
            requestCode = expectedRequestCode
        }
            .build()

        //THEN
        assertEquals(tpGooglePay.requestCode, expectedRequestCode)
    }

    @Test
    fun test_tpGooglePaySetAllowedCardAuthMethods() {
        //GIVEN
        val expectedAllowedCardAuthMethods = listOf("PAN_ONLY")

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).setAllowedCardAuthMethods(expectedAllowedCardAuthMethods)
            .build()

        //THEN
        assertEquals(JSONArray(expectedAllowedCardAuthMethods), tpGooglePay.allowedCardAuthMethods)
    }

    @Test
    fun test_tpGooglePaySetAllowedCardAuthMethods_direct() {
        //GIVEN
        val expectedAllowedCardAuthMethods = listOf("PAN_ONLY")

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        )
            .apply {
                allowedCardAuthMethods = expectedAllowedCardAuthMethods
            }
            .build()

        //THEN
        assertEquals(JSONArray(expectedAllowedCardAuthMethods), tpGooglePay.allowedCardAuthMethods)
    }

    @Test
    fun test_tpGooglePaySetAllowedCardNetworks() {
        //GIVEN
        val expectedSupportedNetworks = JSONArray(
            listOf(
                "AMEX",
                "DISCOVER",
                "JCB",
                "MASTERCARD",
                "VISA"
            )
        )

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).setAllowedCardNetworks(
            listOf(
                "AMEX",
                "DISCOVER",
                "JCB",
                "MASTERCARD",
                "VISA"
            )
        )
            .build()

        //THEN
        assertEquals(tpGooglePay.allowedCardNetworks, expectedSupportedNetworks)
    }

    @Test
    fun test_tpGooglePaySetAllowedCardNetworks_direct() {
        //GIVEN
        val expectedSupportedNetworks = JSONArray(
            listOf(
                "AMEX",
                "VISA"
            )
        )

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).apply {
            allowedCardNetworks = listOf(
                "AMEX",
                "VISA"
            )
        }
            .build()

        //THEN
        assertEquals(tpGooglePay.allowedCardNetworks, expectedSupportedNetworks)
    }

    @Test
    fun test_tpGooglePaySetMerchantName() {
        //GIVEN
        val hasMerchantName = true
        val expectedMerchantName = BuildConfig.MERCHANT_USERNAME

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).setMerchantName(expectedMerchantName)
            .build()

        //THEN
        assertEquals(tpGooglePay.merchantInfo.has("merchantName"), hasMerchantName)
        assertEquals(tpGooglePay.merchantInfo.getString("merchantName"), expectedMerchantName)
    }

    @Test
    fun test_tpGooglePaySetMerchantName_direct() {
        //GIVEN
        val hasMerchantName = true
        val expectedMerchantName = BuildConfig.MERCHANT_USERNAME

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).apply {
            merchantName = expectedMerchantName
        }
            .build()

        //THEN
        assertEquals(tpGooglePay.merchantInfo.has("merchantName"), hasMerchantName)
        assertEquals(tpGooglePay.merchantInfo.getString("merchantName"), expectedMerchantName)
    }

    @Test
    fun test_tpGooglePayOnActivityResultInvalidData() {
        //GIVEN
        val expectedResult = null

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).build()

        val result = tpGooglePay.onActivityResult(
            tpGooglePay.requestCode,
            AppCompatActivity.RESULT_OK,
            Intent()
        )

        //THEN
        assertEquals(result, expectedResult)
    }

    @Test
    fun test_tpGooglePayOnActivityResultEmptyData() {
        //GIVEN
        val expectedResult = null

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).build()

        val result =
            tpGooglePay.onActivityResult(tpGooglePay.requestCode, AppCompatActivity.RESULT_OK, null)

        //THEN
        assertEquals(result, expectedResult)
    }

    @Test
    fun test_tpGooglePayOnActivityResultCanceled() {
        //GIVEN
        val expectedResult = null

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).build()

        val result = tpGooglePay.onActivityResult(
            tpGooglePay.requestCode,
            AppCompatActivity.RESULT_CANCELED,
            null
        )

        //THEN
        assertEquals(result, expectedResult)
    }

    @Test
    fun test_tpGooglePayOnActivityResultError() {
        //GIVEN
        val expectedResult = null

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).build()

        val result = tpGooglePay.onActivityResult(
            tpGooglePay.requestCode,
            AutoResolveHelper.RESULT_ERROR,
            null
        )

        //THEN
        assertEquals(result, expectedResult)
    }

    @Test
    fun test_tpGooglePayOnActivityResultErrorWithStatus() {
        //GIVEN
        val expectedResult = "INTERNAL_ERROR"

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).build()

        val result = tpGooglePay.onActivityResult(
            tpGooglePay.requestCode,
            AutoResolveHelper.RESULT_ERROR,
            Intent().apply {
                putExtra(
                    "com.google.android.gms.common.api.AutoResolveHelper.status",
                    Status(Status.RESULT_INTERNAL_ERROR.statusCode, "INTERNAL_ERROR")
                )
            }
        )

        //THEN
        assertEquals(result, expectedResult)
    }

    @Test
    fun test_tpGooglePayHandlePaymentSuccessNull() {
        //GIVEN
        val expectedResult = null

        //WHEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).build()

        val result = tpGooglePay.onActivityResult(
            tpGooglePay.requestCode,
            AppCompatActivity.RESULT_OK,
            Intent()
        )

        //THEN
        assertEquals(result, expectedResult)
    }

    @Test
    fun test_gatewayTokenizationSpecification() {
        //GIVEN
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).build()
        val method =
            TPGooglePayManager::class.declaredMemberFunctions.single {
                it.name == "gatewayTokenizationSpecification"
            }
        method.isAccessible = true

        //WHEN
        val result = method.call(tpGooglePay) as JSONObject

        //THEN
        assertTrue(result.has("type"))
        assertEquals("PAYMENT_GATEWAY", result.getString("type"))
        assertTrue(result.has("parameters"))
    }

    @Test
    fun test_baseCardPaymentMethod() {
        //GIVEN
        val tpGooglePay: TPGooglePayManager = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).build()
        val method =
            TPGooglePayManager::class.declaredMemberFunctions.single {
                it.name == "baseCardPaymentMethod"
            }
        method.isAccessible = true

        //WHEN
        val result = method.call(tpGooglePay) as JSONObject

        //THEN
        assertTrue(result.has("type"))
        assertEquals("CARD", result.getString("type"))
        assertTrue(result.has("parameters"))
    }

    @Test
    fun test_cardPaymentMethod() {
        //GIVEN
        val tpGooglePay: TPGooglePayManager = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).build()
        val method =
            TPGooglePayManager::class.declaredMemberFunctions.single {
                it.name == "cardPaymentMethod"
            }
        method.isAccessible = true

        //WHEN
        val result = method.call(tpGooglePay) as JSONObject

        //THEN
        assertTrue(result.has("tokenizationSpecification"))
    }

    @Test
    fun test_getPaymentDataRequest() {
        //GIVEN
        val input = 1050L
        val expectedResult = "10.50"
        val tpGooglePay: TPGooglePayManager = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).build()
        val method =
            TPGooglePayManager::class.declaredMemberFunctions.single {
                it.name == "getPaymentDataRequest"
            }
        method.isAccessible = true

        //WHEN
        val result = method.call(tpGooglePay, input) as JSONObject
        val finalBaseRequest = tpGooglePay.baseRequest
        //THEN
        assertTrue(finalBaseRequest.has("allowedPaymentMethods"))
        assertTrue(finalBaseRequest.has("transactionInfo"))
        assertTrue(result.has("allowedPaymentMethods"))
        assertTrue(result.has("transactionInfo"))
        assertEquals(
            expectedResult, result
                .getJSONObject("transactionInfo")
                .getString("totalPrice")
        )
    }

    @Test
    fun test_getTransactionInfo() {
        //GIVEN
        val expectedResult = "10.50"
        val tpGooglePay: TPGooglePayManager = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).build()
        val method =
            TPGooglePayManager::class.declaredMemberFunctions.single {
                it.name == "getTransactionInfo"
            }
        method.isAccessible = true

        //WHEN
        val result = method.call(tpGooglePay, expectedResult) as JSONObject

        //THEN
        assertTrue(result.has("totalPriceStatus"))
        assertEquals("FINAL", result.getString("totalPriceStatus"))
        assertTrue(result.has("totalPrice"))
        assertEquals(expectedResult, result.getString("totalPrice"))
    }

    @Test
    fun test_isReadyToPayRequest() {
        //GIVEN
        val tpGooglePay: TPGooglePayManager = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).build()
        val method =
            TPGooglePayManager::class.declaredMemberFunctions.single {
                it.name == "isReadyToPayRequest"
            }
        method.isAccessible = true

        //WHEN
        val result = method.call(tpGooglePay) as JSONObject
        val finalBaseRequest = tpGooglePay.baseRequest
        //THEN
        assertTrue(finalBaseRequest.has("allowedPaymentMethods"))
        assertTrue(result.has("allowedPaymentMethods"))
    }

    @Test
    fun test_handlePaymentSuccess() {
        //GIVEN
        val expectedResult = "TEST"
        val paymentData: PaymentData = mockk()
        every { paymentData.toJson() } returns expectedResult
        val tpGooglePay: TPGooglePayManager = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).build()
        val method =
            TPGooglePayManager::class.declaredMemberFunctions.single {
                it.name == "handlePaymentSuccess"
            }
        method.isAccessible = true

        //WHEN
        val result = method.call(tpGooglePay, paymentData) as String
        //THEN
        assertEquals(expectedResult, result)
    }

    @Test
    fun test_handlePaymentSuccess_via_result() {
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).build()
        val mockData = mockk<Intent>()
        val paymentData = mockk<PaymentData>()
        val expectedResult= "test"

        mockkStatic(PaymentData::class)
        every {
            PaymentData.getFromIntent(any())
        } returns  paymentData
        every {
            paymentData.toJson()
        } returns  expectedResult

        val result = tpGooglePay.onActivityResult(
            requestCode = 8686,
            resultCode = AppCompatActivity.RESULT_OK,
            data = mockData
        )
        verify(exactly = 1) {
            paymentData.toJson()
        }
        assertEquals(expectedResult, result)
        unmockkStatic(PaymentData::class)
    }

    @Test
    fun test_handlePaymentSuccess_exception() {
        val tpGooglePay = TPGooglePayManager.Builder(
            activity!!,
            WalletConstants.ENVIRONMENT_TEST,
            "GB",
            "GBP",
            BuildConfig.SITE_REFERENCE
        ).build()
        val mockData = mockk<Intent>()
        val paymentData = mockk<PaymentData>()

        mockkStatic(PaymentData::class)
        every {
            PaymentData.getFromIntent(any())
        } returns  paymentData
        every {
            paymentData.toJson()
        } throws JSONException("")

        val result = tpGooglePay.onActivityResult(
            requestCode = 8686,
            resultCode = AppCompatActivity.RESULT_OK,
            data = mockData
        )
        verify(exactly = 1) {
            paymentData.toJson()
        }
        assertNull(result)
        unmockkStatic(PaymentData::class)
    }

    @Test
    fun test_centsToString() {
        assertEquals("12.34", 1234L.centsToString())
        assertEquals("10.00", 1000L.centsToString())
        assertEquals("0.01", 1L.centsToString())
        assertEquals("0.00", 0L.centsToString())
        assertEquals("-0.50", (-50L).centsToString())
    }

    private fun mockTPGooglePayManagerBuilder(
        activity: Activity,
        paymentsClient: PaymentsClient
    ): TPGooglePayManager.Builder {
        val builder = mockk<TPGooglePayManager.Builder>()

        every { builder.apiVersion } returns 1
        every { builder.apiVersionMinor } returns 0
        every { builder.allowedCardAuthMethods } returns listOf("PAN_ONLY")
        every { builder.allowedCardNetworks } returns listOf("VISA")
        every { builder.billingAddressRequired } returns false
        every { builder.format } returns "FULL"
        every { builder.billingPhoneNumberRequired } returns false
        every { builder.shippingAddressRequired } returns false
        every { builder.shippingPhoneNumberRequired } returns false
        every { builder.allowedCountryCodes } returns listOf("+1")
        every { builder.paymentsClient } returns paymentsClient
        every { builder.countryCode } returns "+1"
        every { builder.currencyCode } returns "GBP"
        every { builder.gatewayMerchantId } returns "1"
        every { builder.gateway } returns "trustpayments"
        every { builder.requestCode } returns 1
        every { builder.activity } returns activity
        every { builder.merchantName } returns "Test_App"
        return builder
    }

    @Test
    fun test_possiblyShowGooglePayButton_enabled_flow() {
        // Arrange
        val paymentsClient = mockk<PaymentsClient>()
        val activity = mockk<Activity>()
        val builder = mockTPGooglePayManagerBuilder(activity, paymentsClient)

        val mockTask = mockk<Task<Boolean>>()
        every { paymentsClient.isReadyToPay(any()) } returns mockTask

        val taskSource = TaskCompletionSource<Boolean>()
        val task: Task<Boolean> = taskSource.task
        var listener: OnCompleteListener<Boolean>? = null
        every { mockTask.addOnCompleteListener(any()) } answers {
            listener = it.invocation.args[0] as? OnCompleteListener<Boolean>
            task
        }

        val tpGPayManager = TPGooglePayManager(builder)
        val showGooglePayButtonCallback =
            mockk<TPGooglePayManager.ShowGooglePayButtonCallback>(relaxed = true)
        taskSource.setResult(true)
        // Act
        tpGPayManager.possiblyShowGooglePayButton(showGooglePayButtonCallback)
        listener?.onComplete(task)
        // Assert
        verify { showGooglePayButtonCallback.canShowButton(true) }
    }

    @Test
    fun test_possiblyShowGooglePayButton_disabled_flow() {
        // Arrange
        val paymentsClient = mockk<PaymentsClient>()
        val activity = mockk<Activity>()
        val builder = mockTPGooglePayManagerBuilder(activity, paymentsClient)

        val mockTask = mockk<Task<Boolean>>()
        every { paymentsClient.isReadyToPay(any()) } returns mockTask

        val taskSource = TaskCompletionSource<Boolean>()
        val task: Task<Boolean> = taskSource.task
        var listener: OnCompleteListener<Boolean>? = null
        every { mockTask.addOnCompleteListener(any()) } answers {
            listener = it.invocation.args[0] as? OnCompleteListener<Boolean>
            task
        }

        val tpGPayManager = TPGooglePayManager(builder)
        val showGooglePayButtonCallback =
            mockk<TPGooglePayManager.ShowGooglePayButtonCallback>(relaxed = true)
        taskSource.setResult(false)
        // Act
        tpGPayManager.possiblyShowGooglePayButton(showGooglePayButtonCallback)
        listener?.onComplete(task)
        // Assert
        verify { showGooglePayButtonCallback.canShowButton(false) }
    }

    @Test
    fun test_requestPayment() {
        // Arrange
        val activity = mockk<Activity>()
        val paymentsClient = mockk<PaymentsClient>()
        val paymentData = mockk<Task<PaymentData>>()
        val builder = mockTPGooglePayManagerBuilder(
            activity, paymentsClient
        )
        val expectedRequestCode = builder.requestCode!!

        val taskSource = TaskCompletionSource<PaymentData>()
        val task: Task<PaymentData> = taskSource.task
        every { paymentsClient.loadPaymentData(any()) } returns paymentData
        every { paymentData.addOnCompleteListener(any()) } answers {
            task
        }
        mockkStatic(AutoResolveHelper::class)
        every {
            AutoResolveHelper.resolveTask(paymentData, activity, any())
        } just Runs
        val tpGPayManager = TPGooglePayManager(builder)

        // Act
        tpGPayManager.requestPayment(1234L)

        // Assert
        verify {
            AutoResolveHelper.resolveTask(paymentData, activity, expectedRequestCode)
        }
        unmockkStatic(AutoResolveHelper::class)
    }
}