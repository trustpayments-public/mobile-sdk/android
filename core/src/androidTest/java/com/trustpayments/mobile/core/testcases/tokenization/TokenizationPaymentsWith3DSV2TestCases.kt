package com.trustpayments.mobile.core.testcases.tokenization

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.rule.ActivityTestRule
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.R
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.services.api.TrustPaymentsGatewayType
import com.trustpayments.mobile.core.services.transaction.Error
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.testutils.createSessionFor
import com.trustpayments.mobile.core.testutils.getAsSuccess
import com.trustpayments.mobile.core.util.CredentialsOnFile
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.util.PaymentSessionResponse
import com.trustpayments.mobile.core.util.parse
import com.trustpayments.mobile.core.utils.ThreeDQueryTestActivity
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2.threeDSecureCode
import junit.framework.Assert.assertNull
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class TokenizationPaymentsWith3DSV2TestCases: BaseTestCase() {
    private lateinit var paymentTransactionManager: PaymentTransactionManager

    private lateinit var activity: ThreeDQueryTestActivity

    @get:Rule
    val initialActivityTestRule = ActivityTestRule(ThreeDQueryTestActivity::class.java)

    private val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    @Before
    override fun setUp() {
        super.setUp()

        val app = ApplicationProvider.getApplicationContext<Context>().applicationContext
        paymentTransactionManager =
            PaymentTransactionManager(app, TrustPaymentsGatewayType.EU, false, BuildConfig.MERCHANT_USERNAME, isLocationDataConsentGiven = false)

        activity = initialActivityTestRule.activity
    }

    @Test
    fun test_positive_visa() = runBlocking {
        // GIVEN
        val cardPan = "4111111111111111"
        val cvv = "123"

        val accountCheckResponse = runSuccessfulAccountCheck(cardPan, cvv)

        Thread.sleep(5000)

        val jwt = jwtBuilder.getStandard(listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        ), CredentialsOnFile.UsePreviouslySaved, accountCheckResponse.transactionReference)
        val session = paymentTransactionManager.createSessionFor(jwt, cvv = cvv)

        lateinit var result: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            result = paymentTransactionManager.executeSession(session) {
                activity
            }.parse()
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, threeDSecureCode)

        job.join()

        Assert.assertTrue(result is PaymentSessionResponse.Success)
    }

    @Test
    fun test_positive_amex() = runBlocking {
        // GIVEN
        val cardPan = "340000000001098"
        val cvv = "1234"

        val accountCheckResponse = runSuccessfulAccountCheck(cardPan, cvv)

        Thread.sleep(5000)

        val jwt = jwtBuilder.getStandard(listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        ), CredentialsOnFile.UsePreviouslySaved, accountCheckResponse.transactionReference)
        val session = paymentTransactionManager.createSessionFor(jwt, cvv = cvv)

        lateinit var result: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            result = paymentTransactionManager.executeSession(session) {
                activity
            }.parse()
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, threeDSecureCode)

        job.join()

        Assert.assertTrue(result is PaymentSessionResponse.Success)
    }

    @Test
    fun test_negative_invalidTransactionReference() = runBlocking {
        // GIVEN
        val cvv = "123"

        val jwt = jwtBuilder.getStandard(listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        ), CredentialsOnFile.UsePreviouslySaved, "invalid transaction reference")
        val session = paymentTransactionManager.createSessionFor(jwt, cvv = cvv)

        lateinit var result: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            result = paymentTransactionManager.executeSession(session) {
                activity
            }
        }

        job.join()

        val error = result.error as Error.InitializationFailure
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertNull(error.initializationError)
    }

    //This is a setup step required to get valid transaction reference
    private fun runSuccessfulAccountCheck(cardPan: String, cvv: String) = runBlocking {
        val firstToken = jwtBuilder.getStandard(listOf(
            RequestType.AccountCheck
        ), CredentialsOnFile.SaveForFutureUse)
        val firstSession =
            paymentTransactionManager.createSessionFor(firstToken, cardPan, cvv)

        // WHEN
        val result = paymentTransactionManager.executeSession(firstSession).parse()

        Assert.assertTrue(result is PaymentSessionResponse.Success)

        result.getAsSuccess(0)
    }
}