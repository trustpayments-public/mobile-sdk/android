package com.trustpayments.mobile.ui.testutils

import androidx.appcompat.app.AppCompatActivity
import com.trustpayments.mobile.ui.R

class StyledDropInPaymentViewTestActivity : AppCompatActivity(R.layout.activity_styled_drop_in_payment_view_test)