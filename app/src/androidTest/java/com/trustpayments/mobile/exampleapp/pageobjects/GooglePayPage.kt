package com.trustpayments.mobile.exampleapp.pageobjects

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import com.trustpayments.mobile.exampleapp.R
import org.hamcrest.CoreMatchers
class GooglePayPage : BaseAppPage() {

    private val buyWithGPayButton = CoreMatchers.allOf(
        ViewMatchers.isDescendantOfA(ViewMatchers.withId(R.id.dropInPaymentView)),
        ViewMatchers.withId(R.id.googlePayButton),

    )

    private val continueButton = CoreMatchers.allOf(
        ViewMatchers.isDescendantOfA(ViewMatchers.withId(R.id.dropInPaymentView)),
        ViewMatchers.withText("Continue")

        )
    fun tapOnGPayButton(): GooglePayPage {
        Espresso.onView(buyWithGPayButton).perform(ViewActions.scrollTo(), ViewActions.click())
        return this
    }

    fun tapOnContinueButton(): GooglePayPage {
        Espresso.onView(continueButton).perform(ViewActions.scrollTo(), ViewActions.click())
        return this
    }
}