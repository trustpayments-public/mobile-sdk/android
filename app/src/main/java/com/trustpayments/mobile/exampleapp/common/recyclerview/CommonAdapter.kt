package com.trustpayments.mobile.exampleapp.common.recyclerview

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.trustpayments.mobile.exampleapp.databinding.ListHeaderRowBinding
import com.trustpayments.mobile.exampleapp.databinding.ListNavItemRowBinding

class CommonAdapter(private val itemsList: List<ListItem>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemViewType(position: Int): Int =
        if (itemsList[position] is ListItem.Header) 0 else 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        return if (viewType == 0)
            HeaderViewHolder(ListHeaderRowBinding.inflate(layoutInflater, parent, false))
        else
            SampleRowViewHolder(ListNavItemRowBinding.inflate(layoutInflater, parent, false))
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is HeaderViewHolder) {
            holder.bindHeader(itemsList[holder.adapterPosition] as ListItem.Header)
        } else if (holder is SampleRowViewHolder) {
            holder.bindSampleItem(itemsList[holder.adapterPosition] as ListItem.SampleItem)
        }
    }

    override fun getItemCount(): Int = itemsList.size

    inner class SampleRowViewHolder(private val binding: ListNavItemRowBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindSampleItem(listItem: ListItem.SampleItem) {
            binding.title.text = binding.root.context.getString(listItem.titleRes)
            binding.root.setOnClickListener {
                val context = binding.root.context
                context.startActivity(Intent(context, listItem.launchActivityClass))
            }
        }
    }

    inner class HeaderViewHolder(private val binding: ListHeaderRowBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindHeader(header: ListItem.Header) {
            binding.root.text = binding.root.context.getString(header.titleRes)
        }
    }
}