package com.trustpayments.mobile.core.analytics.logger.protocol

/**
 * Contains the configuration data related to analytic client.
 * */
data class AnalyticClientSettings(
    var dsn: String? = null,
    var merchantUsername: String? = null,
)
