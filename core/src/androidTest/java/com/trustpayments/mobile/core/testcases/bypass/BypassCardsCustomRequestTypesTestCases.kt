package com.trustpayments.mobile.core.testcases.bypass

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.rule.ActivityTestRule
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.models.api.response.ResponseErrorCode
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.testutils.CardType
import com.trustpayments.mobile.core.testutils.areAllResponsesSuccessful
import com.trustpayments.mobile.core.testutils.createSessionFor
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.util.ResponseParser
import com.trustpayments.mobile.core.utils.ThreeDQueryTestActivity
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class BypassCardsCustomRequestTypesTestCases: BaseTestCase() {

    private lateinit var paymentTransactionManager: PaymentTransactionManager
    
    private val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    private val cardsToBypass = listOf(
        CardType.Visa,
        CardType.Amex,
        CardType.Mastercard,
        CardType.Discover,
        CardType.JCB,
        CardType.Diners
    ).map { it.serializedName }

    lateinit var cardPan: String
    lateinit var cvv: String

    private lateinit var activity: ThreeDQueryTestActivity

    @get:Rule
    val initialActivityTestRule = ActivityTestRule(ThreeDQueryTestActivity::class.java)

    @Before
    override fun setUp() {
        super.setUp()

        val app = ApplicationProvider.getApplicationContext<Context>().applicationContext
        paymentTransactionManager =
            PaymentTransactionManager(app, getGatewayType(), false, BuildConfig.MERCHANT_USERNAME, isLocationDataConsentGiven = false)

        cardPan = "4000000000001026"
        cvv = "123"

        activity = initialActivityTestRule.activity
    }

    @Test
    fun test_failed_3DQ_bypass() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getStandard(
            listOf(
                RequestType.ThreeDQuery
            ), bypassCards = cardsToBypass
        )

        val session =
            paymentTransactionManager.createSessionFor(jwtToken, cardPan, cvv)

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }

        // THEN
        assertFalse(sessionResult.areAllResponsesSuccessful())

        val parsedJwtResponses = ResponseParser.parse(sessionResult.responseJwtList)!!

        assertEquals(1, parsedJwtResponses.size)
        assertEquals(1, parsedJwtResponses[0].responses.size)
        assertEquals(RequestType.Error, parsedJwtResponses[0].responses[0].requestTypeDescription)
        assertEquals(ResponseErrorCode.Bypass, parsedJwtResponses[0].responses[0].errorCode)
    }

    @Test
    fun test_positive_3DQ_Auth() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getStandard(
            listOf(
                RequestType.ThreeDQuery,
                RequestType.Auth
            ), bypassCards = cardsToBypass
        )

        val session =
            paymentTransactionManager.createSessionFor(jwtToken, cardPan, cvv)

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        val parsedJwtResponses = ResponseParser.parse(sessionResult.responseJwtList)!!

        assertEquals(1, parsedJwtResponses.size)
        assertEquals(1, parsedJwtResponses[0].responses.size)
        assertEquals(RequestType.Auth, parsedJwtResponses[0].responses[0].requestTypeDescription)
    }

    @Test
    fun test_positive_AccountCheck_3DQ() = runBlocking {
        val jwtToken = jwtBuilder.getStandard(
            listOf(
                RequestType.AccountCheck,
                RequestType.ThreeDQuery
            ), bypassCards = cardsToBypass
        )

        val session =
            paymentTransactionManager.createSessionFor(jwtToken, cardPan, cvv)

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        val parsedJwtResponses = ResponseParser.parse(sessionResult.responseJwtList)!!

        assertEquals(1, parsedJwtResponses.size)
        assertEquals(1, parsedJwtResponses[0].responses.size)
        assertEquals(
            RequestType.AccountCheck,
            parsedJwtResponses[0].responses[0].requestTypeDescription
        )
    }

    @Test
    fun test_positive_AccountCheck_3DQ_Auth() = runBlocking {
        val jwtToken = jwtBuilder.getStandard(
            listOf(
                RequestType.AccountCheck,
                RequestType.ThreeDQuery,
                RequestType.Auth
            ), bypassCards = cardsToBypass
        )

        val session =
            paymentTransactionManager.createSessionFor(jwtToken, cardPan, cvv)

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        val parsedJwtResponses = ResponseParser.parse(sessionResult.responseJwtList)!!

        assertEquals(1, parsedJwtResponses.size)
        assertEquals(2, parsedJwtResponses[0].responses.size)
        assertEquals(
            RequestType.AccountCheck,
            parsedJwtResponses[0].responses[0].requestTypeDescription
        )
        assertEquals(RequestType.Auth, parsedJwtResponses[0].responses[1].requestTypeDescription)
    }

    @Test
    fun test_positive_AccountCheck_3DQ_Auth_Subscription() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForSubscription(
            listOf(
                RequestType.AccountCheck,
                RequestType.ThreeDQuery,
                RequestType.Auth,
                RequestType.Subscription
            ), cardsToBypass
        )

        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "4000000000001026", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        val parsedJwtResponses = ResponseParser.parse(sessionResult.responseJwtList)!!

        assertEquals(1, parsedJwtResponses.size)
        assertEquals(3, parsedJwtResponses[0].responses.size)
        assertEquals(
            RequestType.AccountCheck,
            parsedJwtResponses[0].responses[0].requestTypeDescription
        )
        assertEquals(RequestType.Auth, parsedJwtResponses[0].responses[1].requestTypeDescription)
        assertEquals(
            RequestType.Subscription,
            parsedJwtResponses[0].responses[2].requestTypeDescription
        )
    }

    @Test
    fun test_positive_RiskDec_AccountCheck_3DQ_Auth() = runBlocking {
        val jwtToken = jwtBuilder.getStandard(
            listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.ThreeDQuery,
                RequestType.Auth
            ), bypassCards = cardsToBypass
        )

        val session =
            paymentTransactionManager.createSessionFor(jwtToken, cardPan, cvv)

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        val parsedJwtResponses = ResponseParser.parse(sessionResult.responseJwtList)!!

        assertEquals(1, parsedJwtResponses.size)
        assertEquals(3, parsedJwtResponses[0].responses.size)
        assertEquals(RequestType.RiskDec, parsedJwtResponses[0].responses[0].requestTypeDescription)
        assertEquals(
            RequestType.AccountCheck,
            parsedJwtResponses[0].responses[1].requestTypeDescription
        )
        assertEquals(RequestType.Auth, parsedJwtResponses[0].responses[2].requestTypeDescription)
    }

    @Test
    fun test_positive_3DQ_Auth_RiskDec() = runBlocking {
        val jwtToken = jwtBuilder.getStandard(
            listOf(
                RequestType.ThreeDQuery,
                RequestType.Auth,
                RequestType.RiskDec
            ), bypassCards = cardsToBypass
        )

        val session =
            paymentTransactionManager.createSessionFor(jwtToken, cardPan, cvv)

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        val parsedJwtResponses = ResponseParser.parse(sessionResult.responseJwtList)!!

        assertEquals(1, parsedJwtResponses.size)
        assertEquals(2, parsedJwtResponses[0].responses.size)
        assertEquals(RequestType.Auth, parsedJwtResponses[0].responses[0].requestTypeDescription)
        assertEquals(RequestType.RiskDec, parsedJwtResponses[0].responses[1].requestTypeDescription)
    }
}