package com.trustpayments.mobile.exampleapp.functionaltests.samples

import android.content.Context
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.trustpayments.mobile.exampleapp.BuildConfig
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.common.jwt.StandardPayload
import com.trustpayments.mobile.exampleapp.samples.backgroundrequest.BackgroundRequestSampleActivity
import com.trustpayments.mobile.exampleapp.utils.TestUtils
import com.trustpayments.mobile.ui.model.RequestType
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import org.hamcrest.Matchers.not
import org.junit.*

class BackgroundRequestActivityTestSuite {
    @get:Rule
    val activityScenarioRule = ActivityScenarioRule(BackgroundRequestSampleActivity::class.java)
    private lateinit var json: Json

    @Before
    fun setUp() {
        json = Json { encodeDefaults = false }
    }

    @After
    fun tearDown() {
        activityScenarioRule.scenario?.close()
    }

    @Test
    fun testBackgroundRequestSuccess(): Unit = runBlocking {

        // Move the activity to the RESUMED state
        activityScenarioRule.scenario.moveToState(Lifecycle.State.RESUMED)

        // Perform actions inside the activity
        activityScenarioRule.scenario?.onActivity { activity ->
            // Modify the Payload to simulate an success scenario
            activity.payload = json.encodeToString(
                StandardPayload.serializer(), StandardPayload(
                    BuildConfig.SITE_REFERENCE,
                    "GBP",
                    1050,
                    listOf(RequestType.Auth.serializedName),
                )
            )
        }

        // Click the "Run" button
        onView(withId(R.id.btn_run)).perform(click())

        // Check that the "Run" button is disabled
        onView(withId(R.id.btn_run)).check(matches(not(isEnabled())))

        // Wait for the success dialog to become visible within 30 seconds
        TestUtils.waitForVisibleView(
            R.id.loading_indicator,
            30000,
            false,
            withText(R.string.hs_transaction_success)
        )

        onView(withText(R.string.hs_transaction_success))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
            .perform(click())
    }

    @Test
    fun testBackgroundRequestError(): Unit = runBlocking {
        // Move the activity to the RESUMED state
        activityScenarioRule.scenario.moveToState(Lifecycle.State.RESUMED)

        // Perform actions inside the activity
        activityScenarioRule.scenario?.onActivity { activity ->
            // Modify the payload to simulate an error scenario
            activity.payload = json.encodeToString(
                StandardPayload.serializer(), StandardPayload(
                    BuildConfig.SITE_REFERENCE,
                    "NONE",
                    1050,
                    listOf(RequestType.ThreeDQuery.serializedName, RequestType.Auth.serializedName),
                )
            )

            // Find views within the activity
            val statusTextView = activity.findViewById<TextView>(R.id.status)
            val runButton = activity.findViewById<Button>(R.id.btn_run)

            // Click the "Run" button
            runButton.performClick()

            // Launch a coroutine to perform asynchronous actions
            launch {
                // Wait for the loading indicator to become invisible within 5 seconds
                TestUtils.waitForInvisibleView(R.id.loading_indicator, 5000, false)

                // Get the success text from resources
                val successText = activity.getString(R.string.hs_transaction_success)

                // Build the expected status success text
                val statusSuccessText =
                    activity.getString(R.string.background_request_status, successText)

                // Assert that the status TextView does not display the expected success text
                Assert.assertNotEquals(statusSuccessText, statusTextView.text.toString())
            }
        }
    }

    // Test to check if the toolbar is visible
    @Test
    fun testToolbarVisibility() {
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()))
    }

    // Test to check the initial texts in the activity
    @Test
    fun testInitialTexts() {
        // Perform actions inside the activity
        var expectedRequestTypeText = ""
        var expectedStatusText = ""
        activityScenarioRule.scenario?.onActivity { activity ->
            expectedRequestTypeText =
                activity.getString(R.string.background_request_current_request_type_s,
                    activity.getPayload()?.requestTypeDescriptions?.reduce { acc, s -> "$acc + $s" })
            val readyText = activity.getString(R.string.background_request_ready)
            expectedStatusText = activity.getString(R.string.background_request_status, readyText)
        }

        onView(withId(R.id.current_request_type)).check(matches(withText(expectedRequestTypeText)))
        onView(withId(R.id.status)).check(matches(withText(expectedStatusText)))
    }

    // Test to check if the loading indicator is visible after button click
    @Test
    fun testLoadingIndicatorVisibility() {
        onView(withId(R.id.btn_run)).perform(click())
        onView(withId(R.id.loading_indicator)).check(matches(isDisplayed()))
    }

    // Test to check if the JWTExplorerView is visible after button click
    @Test
    fun testJWTExplorerViewVisibility() {
        onView(withId(R.id.btn_run)).perform(click())
        onView(withId(R.id.jwt_explorer_view)).check(matches(isDisplayed()))
    }

    // Test to check if the progress bar is visible after button click
    @Test
    fun testProgressBarVisibility() {
        onView(withId(R.id.btn_run)).perform(click())
        onView(withId(R.id.loading_indicator)).check(matches(isDisplayed()))
    }

    // Test to check if the available request types info dialog is showed when the toolbar menu item is clicked
    @Test
    fun testToolbarMenuItemDisplaysAvailableRequestTypesDialog() {
        // Click on the toolbar menu item
        onView(withId(R.id.info))
            .perform(click())

        // Get the application context
        val context: Context = InstrumentationRegistry.getInstrumentation().targetContext

        // Retrieve expected dialog title and text from string resources
        val expectedDialogTitle = context.getString(R.string.background_request_available_request_types_title)
        val expectedDialogText = context.resources.getStringArray(R.array.background_request_available_request_types)
            .joinToString("\n\n")

        // Check if the dialog is displayed with the expected title and text
        onView(withText(expectedDialogTitle))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))

        onView(withText(expectedDialogText))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
    }
}
