package com.trustpayments.mobile.exampleapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class MockActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.jwt_explorer_container)
    }
}