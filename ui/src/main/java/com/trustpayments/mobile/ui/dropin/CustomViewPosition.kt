package com.trustpayments.mobile.ui.dropin

enum class CustomViewPosition {
    UnderPan,
    UnderExpiryDate,
    UnderCvv,
    UnderPayButton
}