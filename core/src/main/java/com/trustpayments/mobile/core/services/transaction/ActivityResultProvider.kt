package com.trustpayments.mobile.core.services.transaction

import android.content.Intent
import androidx.activity.result.ActivityResultLauncher
import com.trustpayments.mobile.core.ui.WebActivityResult

@Deprecated(message = "This class is deprecated and will be removed in a future release.")
data class ActivityResultProvider(
    var result: WebActivityResult? = null,
    var resultLauncher: ActivityResultLauncher<Intent>? = null
)