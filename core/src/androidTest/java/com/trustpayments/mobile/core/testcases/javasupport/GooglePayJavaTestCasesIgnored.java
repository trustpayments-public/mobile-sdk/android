package com.trustpayments.mobile.core.testcases.javasupport;

import android.view.View;
import android.widget.Button;

import androidx.test.rule.ActivityTestRule;
import androidx.test.uiautomator.By;
import androidx.test.uiautomator.Until;

import com.google.android.gms.wallet.WalletConstants;
import com.trustpayments.mobile.core.BuildConfig;
import com.trustpayments.mobile.core.googlepay.TPGooglePayManager;
import com.trustpayments.mobile.core.test.R;
import com.trustpayments.mobile.core.testutils.BaseTestCase;
import com.trustpayments.mobile.core.utils.GooglePayTestActivity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import java.util.Objects;

@Ignore
public class GooglePayJavaTestCasesIgnored extends BaseTestCase {

    private GooglePayTestActivity activity;

    @Rule
    public ActivityTestRule initialActivityTestRule = new ActivityTestRule(GooglePayTestActivity.class);

    @Before
    public void setUp(){
        activity = (GooglePayTestActivity) initialActivityTestRule.getActivity();
    }

    @Test
    public void test_googlePayPossiblyShowGooglePayButton(){
        //GIVEN
        activity.runOnUiThread(() -> {
            activity.setButton(activity.findViewById(R.id.googlePayButton));
            Objects.requireNonNull(activity.getButton()).setVisibility(View.GONE);
        });

        //WHEN
        TPGooglePayManager tpGooglePayManager = new TPGooglePayManager.Builder(
                activity,
                WalletConstants.ENVIRONMENT_TEST,
                "GB",
                "GBP",
                BuildConfig.SITE_REFERENCE
        ).build();
        tpGooglePayManager.possiblyShowGooglePayButton(b -> {
            if(b)
                Objects.requireNonNull(activity.getButton()).setVisibility(View.VISIBLE);
        });
        getUiDevice().wait(Until.findObject(By.clazz(Button.class)), 3000L);

        //THEN
        Assert.assertTrue(getUiDevice().hasObject(By.clazz(Button.class)));
    }

}
