package com.trustpayments.mobile.ui

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import com.trustpayments.mobile.ui.card.CardValidator
import com.trustpayments.mobile.ui.card.model.CardType
import com.trustpayments.mobile.ui.common.BaseInputView
import com.trustpayments.mobile.ui.model.PaymentInputType
import com.trustpayments.mobile.ui.utils.toInt
import com.trustpayments.mobile.ui.utils.unmask

/**
 * UI component for handling card number
 */
class PanInputView(context: Context, attrs: AttributeSet?) : BaseInputView(context, attrs),
    PanInputViewModel.PanInputCallbacks {

    private val viewModel = PanInputViewModel(CardValidator(), this)

    internal var cardTypeListener: CardTypeListener? = null
        @JvmSynthetic get
        @JvmSynthetic set

    init {
        initView()
    }

    private var beforeText = ""

    private fun initView() {
        binding.apply {
            input.textAlignment = View.TEXT_ALIGNMENT_VIEW_START
            input.inputType = InputType.TYPE_CLASS_NUMBER or
                    InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD or
                    InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS

            input.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {}

                override fun beforeTextChanged(
                    text: CharSequence?, start: Int, count: Int, after: Int
                ) {
                    beforeText = text.toString()
                }

                override fun onTextChanged(
                    text: CharSequence?, start: Int, before: Int, count: Int
                ) {
                    val cardNumber = viewModel.formatCardNumber(text.toString().unmask())

                    // get cursor position
                    val initialCursorPosition = start + before
                    // check if length before change and after masking is the same - this will be
                    // true only when a separator sign has been deleted
                    val separatorDeleted =
                        beforeText.isNotEmpty() && beforeText.length == cardNumber.length
                    // get digit count after cursor position
                    val numOfDigitsToRightOfCursor = getNumberOfDigits(
                        beforeText.substring(
                            initialCursorPosition, beforeText.length
                        )
                    )
                    input.removeTextChangedListener(this)
                    input.setText(cardNumber)
                    // set new cursor position
                    input.setSelection(
                        getNewCursorPosition(
                            numOfDigitsToRightOfCursor, cardNumber, separatorDeleted
                        )
                    )
                    input.addTextChangedListener(this)
                }

            })

            input.setOnFocusChangeListener { _, hasFocus ->
                if (!hasFocus) {
                    viewModel.validateCardNumber(input.text.toString().unmask())
                }
            }
        }
    }

    override fun getLabelText(): String = resources.getString(R.string.pvc_pan_label)

    override fun getHintText(): String = resources.getString(R.string.pvc_pan_hint)

    override fun getStartIcon(): Drawable? =
        ContextCompat.getDrawable(context, R.drawable.ic_credit_card)

    override fun onCardTypeDetected(cardType: CardType) {
        // pass info about the change to the drop-in view
        cardTypeListener?.onCardTypeDetected(cardType)
    }

    override fun onPanValid(input: String) {
        resetError()
        // pass valid input data to the drop-in view
        inputValidationListener?.onInputValid(PaymentInputType.PAN, input)
    }

    override fun onPanInvalid(showError: Boolean) {
        if (showError) {
            setError(R.string.pvc_input_error_text)
        } else {
            resetError()
        }
        // notify a drop-in view about validation state change
        inputValidationListener?.onInputInvalid(PaymentInputType.PAN)
    }

    override fun onIconChanged(iconRes: Int) {
        val drawable = ContextCompat.getDrawable(context, iconRes)
        binding.input.setCompoundDrawablesWithIntrinsicBounds(
            drawable, null, null, null
        )
    }

    override fun onPanLengthChanged(maxLength: Int) {
        if (maxLength != getInputMaxLength()) {
            setInputMaxLength(maxLength)
        }
    }

    private fun getNewCursorPosition(
        digitCountToRightOfCursor: Int, numberString: String, separatorDeleted: Boolean
    ): Int {
        var position = 0
        var c = digitCountToRightOfCursor
        for (i in numberString.reversed()) {
            if (c == 0) break

            if (i.isDigit()) c--
            position++
        }
        return numberString.length - position - separatorDeleted.toInt()
    }

    private fun getNumberOfDigits(text: String): Int {
        var count = 0
        for (i in text) if (i.isDigit()) count++
        return count
    }

    /**
     * Callback used for passing the info about a detected card type to the drop-in view
     */
    internal interface CardTypeListener {
        fun onCardTypeDetected(cardType: CardType)
    }
}