package com.trustpayments.mobile.core

import org.junit.Assert.*
import org.junit.Test

class PaymentSessionTest {
    @Test
    fun test_initializationIsCorrectForMutableSession() {
        val testJWT = "testJWT"
        val testCardPan = "1111111111111111"
        val testCardExpiryDate = null

        val session = PaymentSession({testJWT}, testCardPan, testCardExpiryDate)

        assertEquals(testJWT, session.jwtProvider.invoke())
        assertEquals(testCardPan, session.cardPan)
        assertNull(session.cardSecurityCode)
        assertEquals(testCardExpiryDate, session.cardExpiryDate)
    }

    @Test
    fun test_initializationIsCorrectForImmutableSession() {
        val testJWT = "testJWT"
        val testCardPan = "1111111111111111"
        val testCardExpiryDate = null

        val session = PaymentSession({ testJWT }, testCardPan, testCardExpiryDate)

        assertEquals(testJWT, session.jwtProvider.invoke())
        assertEquals(testCardPan, session.cardPan)
        assertNull(session.cardSecurityCode)
        assertEquals(testCardExpiryDate, session.cardExpiryDate)
    }

    @Test
    fun test_sessionIdIsCorrectAndRandom() {
        val testJWT = "testJWT"
        val testCardPan = "1111111111111111"
        val testCardExpiryDate = null

        val session = PaymentSession({ testJWT }, testCardPan, testCardExpiryDate)

        assertEquals(10, session.sessionId.length)

        val session2 = PaymentSession({ testJWT }, testCardPan, testCardExpiryDate)

        assertEquals(10, session2.sessionId.length)

        assertNotEquals(session.sessionId, session2.sessionId)
    }
}
