package com.trustpayments.mobile.ui.card.model

enum class CardType(val serializedName: String) {
    Visa("VISA"),
    Mastercard("MASTERCARD"),
    Amex("AMEX"),
    Maestro("MAESTRO"),
    Discover("DISCOVER"),
    Diners("DINERS"),
    JCB("JCB"),
    Unknown("")
}