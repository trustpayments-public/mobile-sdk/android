package com.trustpayments.mobile.core.utils

import android.util.Log
import org.json.JSONObject

internal fun log(message: String) {
    Log.d("MSDK", message)
}

internal fun log(tag: String, message: String) {
    Log.d(tag, message)
}

internal fun logJson(json: String) {
    try {
        Log.d("MSDK", JSONObject(json).toString(4))
    } catch (ex: Exception) {
        Log.d("MSDK", json)
    }
}

internal fun logJson(tag: String, json: String) {
    try {
        Log.d(tag, JSONObject(json).toString(4))
    } catch (ex: Exception) {
        Log.d(tag, json)
    }
}