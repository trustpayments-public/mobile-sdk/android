package com.trustpayments.mobile.core.testcases.eu

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.rule.ActivityTestRule
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.services.transaction.ActivityResultProvider
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.testutils.*
import com.trustpayments.mobile.core.ui.WebActivityResult
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.utils.ThreeDQueryTestActivity
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class RiskDecTestCases : BaseTestCase() {

    private lateinit var paymentTransactionManager: PaymentTransactionManager

    private lateinit var activity: ThreeDQueryTestActivity

    @get:Rule
    val initialActivityTestRule = ActivityTestRule(ThreeDQueryTestActivity::class.java)

    private val jwtBuilder =
        JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    @Before
    override fun setUp() {
        super.setUp()

        val app = ApplicationProvider.getApplicationContext<Context>().applicationContext
        paymentTransactionManager =
            PaymentTransactionManager(app, getGatewayType(), false, BuildConfig.MERCHANT_USERNAME, isLocationDataConsentGiven = false)

        activity = initialActivityTestRule.activity
    }

    //region .riskDec
    @Test
    fun test_positive_riskDec_accept() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            2011, listOf(
                RequestType.RiskDec
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "ACCEPT",
            fraudControlResponseCodes = listOf("0100", "0150")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            2033, listOf(
                RequestType.RiskDec
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "CHALLENGE",
            fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_deny() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            3044, listOf(
                RequestType.RiskDec
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "DENY",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }
    //endregion

    //region .threeDQuery, .auth, .riskDec
    @Test
    fun test_positive_threedquery_auth_riskDec_accept() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1011, listOf(
                RequestType.ThreeDQuery,
                RequestType.Auth,
                RequestType.RiskDec
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "ACCEPT",
            fraudControlResponseCodes = listOf("0100", "0150")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_threedquery_auth_riskDec_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1033, listOf(
                RequestType.ThreeDQuery,
                RequestType.Auth,
                RequestType.RiskDec
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "CHALLENGE",
            fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_threedquery_auth_riskDec_deny() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1044, listOf(
                RequestType.ThreeDQuery,
                RequestType.Auth,
                RequestType.RiskDec
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "DENY",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_threedquery_auth_riskDec_noscore() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            70000, listOf(
                RequestType.ThreeDQuery,
                RequestType.Auth,
                RequestType.RiskDec
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "4242424242424242", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "NOSCORE",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertFalse(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }
    //endregion

    //region .riskDec, .threeDQuery, .auth
    @Test
    fun test_positive_riskDec_threedquery_auth_accept() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            3011, listOf(
                RequestType.RiskDec,
                RequestType.ThreeDQuery,
                RequestType.Auth
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "ACCEPT",
            fraudControlResponseCodes = listOf("0100", "0150")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_threedquery_auth_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            3033, listOf(
                RequestType.RiskDec,
                RequestType.ThreeDQuery,
                RequestType.Auth
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "CHALLENGE",
            fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_threedquery_auth_deny() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            3044, listOf(
                RequestType.RiskDec,
                RequestType.ThreeDQuery,
                RequestType.Auth
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "DENY",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }
    //endregion

    //region .riskDec, .accountCheck, .threeDQuery, .auth
    @Test
    fun test_positive_riskDec_accountcheck_threedquery_auth_accept() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1011, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.ThreeDQuery,
                RequestType.Auth
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "ACCEPT",
            fraudControlResponseCodes = listOf("0100", "0150")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_accountcheck_threedquery_auth_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1033, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.ThreeDQuery,
                RequestType.Auth
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "CHALLENGE",
            fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_accountcheck_threedquery_auth_deny() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1044, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.ThreeDQuery,
                RequestType.Auth
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "DENY",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }
    //endregion

    //region .riskDec, .threeDQuery, .accountCheck
    @Test
    fun test_positive_riskDec_threedquery_accountcheck_accept() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1011, listOf(
                RequestType.RiskDec,
                RequestType.ThreeDQuery,
                RequestType.AccountCheck
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "ACCEPT",
            fraudControlResponseCodes = listOf("0100", "0150")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_threedquery_accountcheck_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1033, listOf(
                RequestType.RiskDec,
                RequestType.ThreeDQuery,
                RequestType.AccountCheck
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "CHALLENGE",
            fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_threedquery_accountcheck_deny() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1044, listOf(
                RequestType.RiskDec,
                RequestType.ThreeDQuery,
                RequestType.AccountCheck
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "DENY",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }
    //endregion

    //region .riskDec, .threeDQuery
    @Test
    fun test_positive_riskDec_threedquery_accept() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            2011, listOf(
                RequestType.RiskDec,
                RequestType.ThreeDQuery
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "ACCEPT",
            fraudControlResponseCodes = listOf("0100", "0150")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_threedquery_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            2033, listOf(
                RequestType.RiskDec,
                RequestType.ThreeDQuery
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "CHALLENGE",
            fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_threedquery_deny() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            3044, listOf(
                RequestType.RiskDec,
                RequestType.ThreeDQuery
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "DENY",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }
    //endregion

    //region .riskDec, .accountCheck, .auth
    @Test
    fun test_positive_riskDec_accountcheck_auth_accept() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1011, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.Auth
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "ACCEPT",
            fraudControlResponseCodes = listOf("0100", "0150")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_accountcheck_auth_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1033, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.Auth
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "CHALLENGE",
            fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_accountcheck_auth_deny() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1044, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.Auth
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "DENY",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }
    //endregion

    //region .riskDec, .accountCheck
    @Test
    fun test_positive_riskDec_accountcheck_accept() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1011, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "ACCEPT",
            fraudControlResponseCodes = listOf("0100", "0150")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_accountcheck_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1033, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "CHALLENGE",
            fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_accountcheck_deny() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1044, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "DENY",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }
    //endregion

    //region .auth, .riskDec
    @Test
    fun test_positive_auth_riskDec_accept() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            2011, listOf(
                RequestType.Auth,
                RequestType.RiskDec
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "ACCEPT",
            fraudControlResponseCodes = listOf("0100", "0150")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_auth_riskDec_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            2033, listOf(
                RequestType.Auth,
                RequestType.RiskDec
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "CHALLENGE",
            fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_auth_riskDec_deny() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            3044, listOf(
                RequestType.Auth,
                RequestType.RiskDec
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "DENY",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }
    //endregion

    //region .riskDec, .auth
    @Test
    fun test_positive_riskDec_auth_accept() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            2011, listOf(
                RequestType.RiskDec,
                RequestType.Auth
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "ACCEPT",
            fraudControlResponseCodes = listOf("0100", "0150")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_auth_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            2033, listOf(
                RequestType.RiskDec,
                RequestType.Auth
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "CHALLENGE",
            fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_auth_deny() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            3044, listOf(
                RequestType.RiskDec,
                RequestType.Auth
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "DENY",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }
    //endregion

    //region .riskDec, .accountCheck, .threeDQuery
    @Test
    fun test_positive_riskDec_accountcheck_threedquery_accept() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1011, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.ThreeDQuery
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "ACCEPT",
            fraudControlResponseCodes = listOf("0100", "0150")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_accountcheck_threedquery_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1033, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.ThreeDQuery
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "CHALLENGE",
            fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_accountcheck_threedquery_deny() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDec(
            1044, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.ThreeDQuery
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "DENY",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }
    //endregion

    //region .riskDec, .accountCheck, .subscription
    @Test
    fun test_positive_riskDec_accountcheck_subscription_accept() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            1011, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "ACCEPT",
            fraudControlResponseCodes = listOf("0100", "0150")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_accountcheck_subscription_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            1033, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "CHALLENGE",
            fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_accountcheck_subscription_deny() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            1044, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "DENY",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }
    //endregion

    //region .riskDec, .accountCheck, .threeDQuery, .auth, .subscription
    @Test
    fun test_positive_riskDec_accountcheck_threedquery_auth_subscription_accept() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            1011, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.ThreeDQuery,
                RequestType.Auth,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "ACCEPT",
            fraudControlResponseCodes = listOf("0100", "0150")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_accountcheck_threedquery_auth_subscription_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            1033, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.ThreeDQuery,
                RequestType.Auth,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "CHALLENGE",
            fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_accountcheck_threedquery_auth_subscription_deny() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            1044, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.ThreeDQuery,
                RequestType.Auth,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "DENY",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }
    //endregion

    //region .riskDec, .threeDQuery, .accountCheck, .subscription
    @Test
    fun test_positive_riskDec_threedquery_accountcheck_subscription_accept() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            1011, listOf(
                RequestType.RiskDec,
                RequestType.ThreeDQuery,
                RequestType.AccountCheck,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "ACCEPT",
            fraudControlResponseCodes = listOf("0100", "0150")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_threedquery_accountcheck_subscription_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            1033, listOf(
                RequestType.RiskDec,
                RequestType.ThreeDQuery,
                RequestType.AccountCheck,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "CHALLENGE",
            fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_threedquery_accountcheck_subscription_deny() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            1044, listOf(
                RequestType.RiskDec,
                RequestType.ThreeDQuery,
                RequestType.AccountCheck,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "DENY",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }
    //endregion

    //region .riskDec, .threeDQuery, .auth, .subscription
    @Test
    fun test_positive_riskDec_threedquery_auth_subscription_accept() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            2011, listOf(
                RequestType.RiskDec,
                RequestType.ThreeDQuery,
                RequestType.Auth,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "ACCEPT",
            fraudControlResponseCodes = listOf("0100", "0150")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_threedquery_auth_subscription_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            2033, listOf(
                RequestType.RiskDec,
                RequestType.ThreeDQuery,
                RequestType.Auth,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "CHALLENGE",
            fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_threedquery_auth_subscription_deny() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            3044, listOf(
                RequestType.RiskDec,
                RequestType.ThreeDQuery,
                RequestType.Auth,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "DENY",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }
    //endregion

    //region .riskDec, .accountCheck, .auth, .subscription
    @Test
    fun test_positive_riskDec_accountcheck_auth_subscription_accept() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            1011, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.Auth,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "ACCEPT",
            fraudControlResponseCodes = listOf("0100", "0150")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_accountcheck_auth_subscription_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            1033, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.Auth,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "CHALLENGE",
            fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_accountcheck_auth_subscription_deny() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            1044, listOf(
                RequestType.RiskDec,
                RequestType.AccountCheck,
                RequestType.Auth,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "DENY",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }
    //endregion

    //region .riskDec, .auth, .subscription
    @Test
    fun test_positive_riskDec_auth_subscription_accept() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            1011, listOf(
                RequestType.RiskDec,
                RequestType.Auth,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "ACCEPT",
            fraudControlResponseCodes = listOf("0100", "0150")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_auth_subscription_challenge() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            1033, listOf(
                RequestType.RiskDec,
                RequestType.Auth,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "CHALLENGE",
            fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }

    @Test
    fun test_positive_riskDec_auth_subscription_deny() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getForRiskDecWithSubscription(
            1044, listOf(
                RequestType.RiskDec,
                RequestType.Auth,
                RequestType.Subscription
            )
        )
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedFraudControlValues = RiskDecResponse(
            fraudControlShieldStatusCode = "DENY",
            fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponse()

        // THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
            expectedFraudControlValues.fraudControlShieldStatusCode,
            actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
            expectedFraudControlValues.fraudControlResponseCodes.contains(
                actualRiskDecTransactionResponse.fraudControlResponseCode
            )
        )
    }
    //endregion
}