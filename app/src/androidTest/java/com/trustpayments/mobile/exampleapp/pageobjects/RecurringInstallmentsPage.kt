package com.trustpayments.mobile.exampleapp.pageobjects

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import com.trustpayments.mobile.exampleapp.R

class RecurringInstallmentsPage : BaseAppPage() {
    fun selectSubscriptionType(subscriptionType: String) : RecurringInstallmentsPage{
        val viewId = when (subscriptionType) {
            "Recurring" -> R.id.recurring_option
            "Installments" -> R.id.installment_option
            else -> throw IllegalArgumentException("Invalid option name")
        }
//        Espresso.onView(ViewMatchers.withId(viewId))
//            .perform(ViewActions.scrollTo(), ViewActions.click())
        Espresso.onView(ViewMatchers.withId(viewId)).perform(ViewActions.click())


        return this
    }

    fun selectNumberOfPayments(numberOfPayments: String) : RecurringInstallmentsPage {
        val viewId = when (numberOfPayments) {
            "3" -> R.id.three_payments
            "6" -> R.id.six_payments
            "12" -> R.id.twelve_payments
            else -> throw IllegalArgumentException("Invalid option name")
        }
        Espresso.onView(ViewMatchers.withId(viewId)).perform(ViewActions.click())

        return this

    }

    fun selectFrequency(frequency: String) : RecurringInstallmentsPage {
        val viewId = when (frequency) {
            "7 days" -> R.id.seven_days
            "1 month" -> R.id.one_month
            "2 months" -> R.id.two_months
            else -> throw IllegalArgumentException("Invalid option name")
        }
        Espresso.onView(ViewMatchers.withId(viewId)).perform(ViewActions.click())

        return this
    }

    fun clickPerformSubscriptionButton(){
        Espresso.onView(ViewMatchers.withId(R.id.btn_run)).perform(ViewActions.click())
    }
}