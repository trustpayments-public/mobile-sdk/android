package com.trustpayments.mobile.ui.testutils

import androidx.appcompat.app.AppCompatActivity
import com.trustpayments.mobile.ui.R

class TokenizationDropInPaymentViewTestActivity : AppCompatActivity(R.layout.activity_drop_in_payment_view_test)