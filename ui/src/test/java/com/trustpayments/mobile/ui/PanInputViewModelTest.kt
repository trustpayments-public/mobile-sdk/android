package com.trustpayments.mobile.ui

import com.trustpayments.mobile.ui.card.CardValidator
import com.trustpayments.mobile.ui.card.model.CardType
import com.trustpayments.mobile.ui.testutils.ValidCardNumbers
import com.trustpayments.mobile.ui.testutils.ValidMaskedCardNumbers
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.slot
import io.mockk.verify
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class PanInputViewModelTest {

    @MockK
    private lateinit var cardValidator: CardValidator

    @MockK
    private lateinit var panInputCallbacks: PanInputViewModel.PanInputCallbacks

    private lateinit var viewModel: PanInputViewModel

    private val actualCardIcons = mutableListOf<Int>()

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        val cardIconSlot = slot<Int>()
        every { panInputCallbacks.onIconChanged(capture(cardIconSlot)) } answers {
            actualCardIcons.add(cardIconSlot.captured)
        }
        every { panInputCallbacks.onPanLengthChanged(any()) } returns Unit
        every { cardValidator.checkWithLuhn(any()) } returns true
        every { panInputCallbacks.onPanValid(any()) } returns Unit
        every { cardValidator.isNumberLengthValid(any(), any()) } returns true
        every { panInputCallbacks.onCardTypeDetected(any()) } returns Unit

        viewModel = PanInputViewModel(cardValidator, panInputCallbacks)
    }

    @Test
    fun test_CorrectCardNumberFormattingForVisa() {
        // GIVEN
        every { cardValidator.getCardType(any()) } returns CardType.Visa

        val cardNumbers = ValidCardNumbers.getForVisa()
        val expectedCardNumbers = ValidMaskedCardNumbers.getForVisa()
        val expectedCardIcons = List(cardNumbers.size) {
            R.drawable.ic_visa
        }

        // WHEN
        val actualCardNumbers = List(cardNumbers.size) { index ->
            viewModel.formatCardNumber(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedCardNumbers, actualCardNumbers)
        assertEquals(expectedCardIcons, actualCardIcons)
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onIconChanged(any()) }
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanLengthChanged(any()) }
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanValid(any()) }
    }

    @Test
    fun test_CorrectCardNumberValidationForVisa() {
        // GIVEN
        val cardNumberSlot = slot<String>()
        val actualCardNumbers = mutableListOf<String>()
        every { cardValidator.getCardType(any()) } returns CardType.Visa
        every { panInputCallbacks.onPanValid(capture(cardNumberSlot)) } answers {
            actualCardNumbers.add(cardNumberSlot.captured)
        }

        val cardNumbers = ValidCardNumbers.getForVisa()
        val expectedCardNumbers = ValidCardNumbers.getForVisa()

        // WHEN
        for (cardNumber in cardNumbers) {
            viewModel.validateCardNumber(cardNumber)
        }

        // THEN
        assertEquals(expectedCardNumbers, actualCardNumbers)
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanValid(any()) }
    }

    @Test
    fun test_CorrectCardNumberFormattingForMastercard() {
        // GIVEN
        every { cardValidator.getCardType(any()) } returns CardType.Mastercard
        val cardNumbers = ValidCardNumbers.getForMastercard()
        val expectedCardNumbers = ValidMaskedCardNumbers.getForMastercard()
        val expectedCardIcons = List(cardNumbers.size) {
            R.drawable.ic_mastercard
        }

        // WHEN
        val actualCardNumbers = List(cardNumbers.size) { index ->
            viewModel.formatCardNumber(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedCardNumbers, actualCardNumbers)
        assertEquals(expectedCardIcons, actualCardIcons)
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onIconChanged(any()) }
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanLengthChanged(any()) }
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanValid(any()) }
    }

    @Test
    fun test_CorrectCardNumberValidationForMastercard() {
        // GIVEN
        val cardNumberSlot = slot<String>()
        val actualCardNumbers = mutableListOf<String>()
        every { cardValidator.getCardType(any()) } returns CardType.Mastercard
        every { panInputCallbacks.onPanValid(capture(cardNumberSlot)) } answers {
            actualCardNumbers.add(cardNumberSlot.captured)
        }

        val cardNumbers = ValidCardNumbers.getForMastercard()
        val expectedCardNumbers = ValidCardNumbers.getForMastercard()

        // WHEN
        for (cardNumber in cardNumbers) {
            viewModel.validateCardNumber(cardNumber)
        }

        // THEN
        assertEquals(expectedCardNumbers, actualCardNumbers)
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanValid(any()) }
    }

    @Test
    fun test_CorrectCardNumberFormattingForAmex() {
        // GIVEN
        every { cardValidator.getCardType(any()) } returns CardType.Amex

        val cardNumbers = ValidCardNumbers.getForAmex()
        val expectedCardNumbers = ValidMaskedCardNumbers.getForAmex()
        val expectedCardIcons = List(cardNumbers.size) {
            R.drawable.ic_amex
        }

        // WHEN
        val actualCardNumbers = List(cardNumbers.size) { index ->
            viewModel.formatCardNumber(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedCardNumbers, actualCardNumbers)
        assertEquals(expectedCardIcons, actualCardIcons)
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onIconChanged(any()) }
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanLengthChanged(any()) }
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanValid(any()) }
    }

    @Test
    fun test_CorrectCardNumberValidationForAmex() {
        // GIVEN
        val cardNumberSlot = slot<String>()
        val actualCardNumbers = mutableListOf<String>()
        every { cardValidator.getCardType(any()) } returns CardType.Amex
        every { panInputCallbacks.onPanValid(capture(cardNumberSlot)) } answers {
            actualCardNumbers.add(cardNumberSlot.captured)
        }

        val cardNumbers = ValidCardNumbers.getForAmex()
        val expectedCardNumbers = ValidCardNumbers.getForAmex()

        // WHEN
        for (cardNumber in cardNumbers) {
            viewModel.validateCardNumber(cardNumber)
        }

        // THEN
        assertEquals(expectedCardNumbers, actualCardNumbers)
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanValid(any()) }
    }

    @Test
    fun test_CorrectCardNumberFormattingForMaestro() {
        // GIVEN
        every { cardValidator.getCardType(any()) } returns CardType.Maestro

        val cardNumbers = ValidCardNumbers.getForMaestro()
        val expectedCardNumbers = ValidMaskedCardNumbers.getForMaestro()
        val expectedCardIcons = List(cardNumbers.size) {
            R.drawable.ic_maestro
        }

        // WHEN
        val actualCardNumbers = List(cardNumbers.size) { index ->
            viewModel.formatCardNumber(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedCardNumbers, actualCardNumbers)
        assertEquals(expectedCardIcons, actualCardIcons)
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onIconChanged(any()) }
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanLengthChanged(any()) }
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanValid(any()) }
    }

    @Test
    fun test_CorrectCardNumberValidationForMaestro() {
        // GIVEN
        val cardNumberSlot = slot<String>()
        val actualCardNumbers = mutableListOf<String>()
        every { cardValidator.getCardType(any()) } returns CardType.Maestro
        every { panInputCallbacks.onPanValid(capture(cardNumberSlot)) } answers {
            actualCardNumbers.add(cardNumberSlot.captured)
        }

        val cardNumbers = ValidCardNumbers.getForMaestro()
        val expectedCardNumbers = ValidCardNumbers.getForMaestro()

        // WHEN
        for (cardNumber in cardNumbers) {
            viewModel.validateCardNumber(cardNumber)
        }

        // THEN
        assertEquals(expectedCardNumbers, actualCardNumbers)
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanValid(any()) }
    }

    @Test
    fun test_CorrectCardNumberFormattingForDiscover() {
        // GIVEN
        every { cardValidator.getCardType(any()) } returns CardType.Discover

        val cardNumbers = ValidCardNumbers.getForDiscover()
        val expectedCardNumbers = ValidMaskedCardNumbers.getForDiscover()
        val expectedCardIcons = List(cardNumbers.size) {
            R.drawable.ic_discover
        }

        // WHEN
        val actualCardNumbers = List(cardNumbers.size) { index ->
            viewModel.formatCardNumber(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedCardNumbers, actualCardNumbers)
        assertEquals(expectedCardIcons, actualCardIcons)
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onIconChanged(any()) }
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanLengthChanged(any()) }
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanValid(any()) }
    }

    @Test
    fun test_CorrectCardNumberValidationForDiscover() {
        // GIVEN
        val cardNumberSlot = slot<String>()
        val actualCardNumbers = mutableListOf<String>()
        every { cardValidator.getCardType(any()) } returns CardType.Discover
        every { panInputCallbacks.onPanValid(capture(cardNumberSlot)) } answers {
            actualCardNumbers.add(cardNumberSlot.captured)
        }

        val cardNumbers = ValidCardNumbers.getForDiscover()
        val expectedCardNumbers = ValidCardNumbers.getForDiscover()

        // WHEN
        for (cardNumber in cardNumbers) {
            viewModel.validateCardNumber(cardNumber)
        }

        // THEN
        assertEquals(expectedCardNumbers, actualCardNumbers)
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanValid(any()) }
    }

    @Test
    fun test_CorrectCardNumberFormattingForDiners() {
        // GIVEN
        every { cardValidator.getCardType(any()) } returns CardType.Diners

        val cardNumbers = ValidCardNumbers.getForDiners()
        val expectedCardNumbers = ValidMaskedCardNumbers.getForDiners()
        val expectedCardIcons = List(cardNumbers.size) {
            R.drawable.ic_diners
        }

        // WHEN
        val actualCardNumbers = List(cardNumbers.size) { index ->
            viewModel.formatCardNumber(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedCardNumbers, actualCardNumbers)
        assertEquals(expectedCardIcons, actualCardIcons)
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onIconChanged(any()) }
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanLengthChanged(any()) }
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanValid(any()) }
    }

    @Test
    fun test_CorrectCardNumberValidationForDiners() {
        // GIVEN
        val cardNumberSlot = slot<String>()
        val actualCardNumbers = mutableListOf<String>()
        every { cardValidator.getCardType(any()) } returns CardType.Diners
        every { panInputCallbacks.onPanValid(capture(cardNumberSlot)) } answers {
            actualCardNumbers.add(cardNumberSlot.captured)
        }

        val cardNumbers = ValidCardNumbers.getForDiners()
        val expectedCardNumbers = ValidCardNumbers.getForDiners()

        // WHEN
        for (cardNumber in cardNumbers) {
            viewModel.validateCardNumber(cardNumber)
        }

        // THEN
        assertEquals(expectedCardNumbers, actualCardNumbers)
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanValid(any()) }
    }

    @Test
    fun test_CorrectCardNumberFormattingForJCB() {
        // GIVEN
        every { cardValidator.getCardType(any()) } returns CardType.JCB

        val cardNumbers = ValidCardNumbers.getForJCB()
        val expectedCardNumbers = ValidMaskedCardNumbers.getForJCB()
        val expectedCardIcons = List(cardNumbers.size) {
            R.drawable.ic_jcb
        }

        // WHEN
        val actualCardNumbers = List(cardNumbers.size) { index ->
            viewModel.formatCardNumber(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedCardNumbers, actualCardNumbers)
        assertEquals(expectedCardIcons, actualCardIcons)
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onIconChanged(any()) }
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanLengthChanged(any()) }
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanValid(any()) }
    }

    @Test
    fun test_CorrectCardNumberValidationForJCB() {
        // GIVEN
        val cardNumberSlot = slot<String>()
        val actualCardNumbers = mutableListOf<String>()
        every { cardValidator.getCardType(any()) } returns CardType.JCB
        every { panInputCallbacks.onPanValid(capture(cardNumberSlot)) } answers {
            actualCardNumbers.add(cardNumberSlot.captured)
        }

        val cardNumbers = ValidCardNumbers.getForJCB()
        val expectedCardNumbers = ValidCardNumbers.getForJCB()

        // WHEN
        for (cardNumber in cardNumbers) {
            viewModel.validateCardNumber(cardNumber)
        }

        // THEN
        assertEquals(expectedCardNumbers, actualCardNumbers)
        verify(exactly = expectedCardNumbers.size) { panInputCallbacks.onPanValid(any()) }
    }
}