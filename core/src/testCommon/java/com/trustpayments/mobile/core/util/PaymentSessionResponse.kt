package com.trustpayments.mobile.core.util

import com.trustpayments.mobile.core.models.DeviceSafetyWarning
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.models.api.response.ResponseErrorCode
import com.trustpayments.mobile.core.services.transaction.Error
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager

sealed class PaymentSessionResponse {
    sealed class Failure(): PaymentSessionResponse() {
        /**
         * Errors representing illegal states on payment session initialization and execution. They
         * indicate merchant's development errors, which should never occur on production environment.
         */
        sealed class DeveloperError(val message: String): Failure() {
            object SessionAlreadyExecuted: DeveloperError("This session has already been executed")
            object InvalidRequestTypes: DeveloperError("Invalid request types provided in JWT payload")
            object ActivityProviderRequired: DeveloperError("Providing activityProvider parameter is mandatory if request list contains THREEDQUERY")
            object ActivityResultProviderRequired: DeveloperError("Providing activityResultProvider parameter is mandatory if request list contains THREEDQUERY")
        }

        /**
         * Generic error was returned for initialization or Cardinal initialization.
         */
        object InitializationError: Failure()

        /**
         * Error returned when any safety warnings have been detected by Cardinal SDK
         */
        data class SafetyError(val errors: List<DeviceSafetyWarning>): Failure()

        /**
         * At least one transaction returned Error or ParsingError.
         */
        data class TransactionFailure(val responseParts: List<ResponsePart<OldTransactionResponse>>): Failure() {
            val allResponses: List<OldTransactionResponse>
                get() = responseParts.flatMapTo(arrayListOf()) { it.responses  }
        }
    }

    data class Success(val responseParts: List<ResponsePart<OldTransactionResponse.PaymentSuccess>>): PaymentSessionResponse() {
        val allResponses: List<OldTransactionResponse.PaymentSuccess>
            get() = responseParts.flatMapTo(arrayListOf()) { it.responses  }
    }
}

class ResponsePart <T : OldTransactionResponse> internal constructor(val jwt: String?, val responses: List<T>, val newJwt: String = "") {
    fun isSuccessful() = responses.none { it is OldTransactionResponse.Error }
}

@Deprecated("This function converts new SDK response format to the old one and should be treated as a temporary solution used during migration. It should no longer be used when writing new tests or fixing old ones.")
fun PaymentTransactionManager.Response.parse(): PaymentSessionResponse {
    if (this.responseJwtList.isEmpty()) {
        val error = this.error
        if (error is Error.SafetyError) {
            return PaymentSessionResponse.Failure.SafetyError(error.errors)
        }
        if (error is Error.InitializationError) {
            return PaymentSessionResponse.Failure.InitializationError
        }
        if (error is Error.ParsingError) {
            return PaymentSessionResponse.Failure.DeveloperError.InvalidRequestTypes
        }
        if (error is Error.HttpFailure) {
            return PaymentSessionResponse.Failure.TransactionFailure(listOf(ResponsePart(null, listOf(OldTransactionResponse.Error.ParsingError()) as List<OldTransactionResponse>)))
        }
    } else {
        val responseParts = this.responseJwtList.map {
            ResponsePart(it, ResponseParser.parse(it)!!.responses.map {
                if (it.errorCode == ResponseErrorCode.Ok) {
                    OldTransactionResponse.PaymentSuccess(it.transactionReference!!, it.requestTypeDescription!!, it.settleStatus, it.liveStatus!!,
                        it.customerOutput, it.baseAmount, it.currencyISO3a, it.maskedPan!!, it.paymentTypeDescription!!, it.cavv,
                        it.eci, it.enrolled, it.status, it.xid, it.acquirerTransactionReference, it.acsUrl, it.threeDPayload, it.threeDVersion)
                } else {
                    OldTransactionResponse.Error.TransactionError(it.errorCode, it.errorMessage, it.errorData, "ERROR", it.customerOutput)
                }
            })
        }

        val allResponses = arrayListOf<OldTransactionResponse>()
        responseParts.forEach { allResponses += it.responses }

        var threeDQ = allResponses.find { (it as? OldTransactionResponse.PaymentSuccess)?.requestTypeDescription == RequestType.ThreeDQuery }

        if (this.additionalTransactionResult != null) {
            threeDQ as OldTransactionResponse.PaymentSuccess
            threeDQ.pares = this.additionalTransactionResult?.pares
            threeDQ.threeDResponse = this.additionalTransactionResult?.threeDResponse
        }

        val error = this.error
        if (error is Error.ThreeDSFailure) {
            threeDQ = OldTransactionResponse.Error.ThreeDSecureError(threeDQ as OldTransactionResponse.PaymentSuccess, error.threeDSError)

            return PaymentSessionResponse.Failure.TransactionFailure(listOf(
                ResponsePart(this.responseJwtList[0], responseParts[0].responses.map {
                    if (it is OldTransactionResponse.PaymentSuccess && it.requestTypeDescription == RequestType.ThreeDQuery) {
                        threeDQ
                    } else it
                })))
        } else  if (error is Error.HttpFailure) {
            return PaymentSessionResponse.Failure.TransactionFailure(listOf(
                ResponsePart(this.responseJwtList[0], allResponses),
                ResponsePart(null, listOf(OldTransactionResponse.Error.ParsingError()) as List<OldTransactionResponse>)))
        } else {
            return if (allResponses.all { it is OldTransactionResponse.PaymentSuccess }) {
                PaymentSessionResponse.Success(responseParts.map {
                    ResponsePart(it.jwt, it.responses.map { it as OldTransactionResponse.PaymentSuccess })
                })
            } else {
                PaymentSessionResponse.Failure.TransactionFailure(responseParts)
            }
        }
    }

    throw IllegalStateException("Conversion case not covered properly")
}