package com.trustpayments.mobile.exampleapp.samples.digitalWalletPayment

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.google.android.gms.wallet.WalletConstants
import com.trustpayments.mobile.core.googlepay.TPGooglePayManager
import com.trustpayments.mobile.core.services.api.TrustPaymentsGatewayType
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.exampleapp.BuildConfig
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.common.jwtexplorer.JWTEditorToken
import com.trustpayments.mobile.exampleapp.common.jwtexplorer.OnTokenChangedListener
import com.trustpayments.mobile.exampleapp.databinding.ActivityDigitalWalletPaymentBinding
import com.trustpayments.mobile.exampleapp.utils.Utils
import com.trustpayments.mobile.exampleapp.utils.Utils.hideKeyboard
import com.trustpayments.mobile.ui.dropin.DropInPaymentView
import com.trustpayments.mobile.ui.model.PaymentInputType

class DigitalWalletPaymentActivity :
    AppCompatActivity(),
    DropInPaymentView.DropInPaymentViewListener,
    DropInPaymentView.DropInGooglePayPaymentViewListener {

    private val viewBinding by lazy {
        ActivityDigitalWalletPaymentBinding.inflate(layoutInflater)
    }

    /*
     * PaymentTransactionManager is responsible for executing the transaction via
     * Card Payment gateway.
     * */
    private val paymentTransactionManager by lazy {
        PaymentTransactionManager(
            context = this@DigitalWalletPaymentActivity,
            gatewayType = TrustPaymentsGatewayType.EU,
            isCardinalLive = false,
            merchantUsername = BuildConfig.MERCHANT_USERNAME,
            isLocationDataConsentGiven = true,
        )
    }

    /*
     * GooglePayTransactionManager(TPGooglePayManager) is responsible for executing the transaction
     * through GooglePay Wallet.
     * */
    private val googlePayTransactionManager by lazy {
        TPGooglePayManager.Builder(
            activity = this@DigitalWalletPaymentActivity,
            environment = WalletConstants.ENVIRONMENT_TEST,
            countryCode = "GB",
            currencyCode = "GBP",
            gatewayMerchantId = BuildConfig.SITE_REFERENCE,
        ).run {
            build()
        }
    }
    private val viewModel: DigitalPaymentViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(viewBinding.root)
        setSupportActionBar(viewBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewBinding.toolbar.setNavigationOnClickListener { finish() }
        setUiElements()
        setObservers()
    }

    private fun setUiElements() {
        viewBinding.apply {
            jwtExplorer.apply {
                setOnTokenChangedListener(
                    object :
                        OnTokenChangedListener {
                        override fun onTokenChanged(token: JWTEditorToken) {
                            viewModel.onPayloadChanged(token)
                        }
                    },
                )
            }
            googlePayTransactionManager.possiblyShowGooglePayButton(
                object : TPGooglePayManager.ShowGooglePayButtonCallback {
                    override fun canShowButton(boolean: Boolean) {
                        dropInPaymentView.setupForGooglePayPayment(boolean)
                    }
                },
            )
            dropInPaymentView.apply {
                dropInPaymentViewListener = this@DigitalWalletPaymentActivity
                dropInGooglePayPaymentViewListener = this@DigitalWalletPaymentActivity
                dropInPaymentView.setupForGooglePayPayment(true)
            }
        }
    }

    private fun setObservers() {
        viewBinding.apply {
            viewModel.requestPayload.observe(this@DigitalWalletPaymentActivity) {
                jwtExplorer.setRequestJwtToken(it)
            }
            viewModel.responsePayload.observe(this@DigitalWalletPaymentActivity) {
                jwtExplorer.apply {
                    setResponseJwtToken(it)
                    it.takeIf { it.isNotBlank() }?.also {
                        moveTokenTab(1)
                    }
                }
            }
            viewModel.isShowingGooglePayload.observe(this@DigitalWalletPaymentActivity) {
                tvResponseStatus.text = if (it) {
                    getString(R.string.google_payment_payload)
                } else {
                    getString(R.string.card_payment_payload)
                }
            }
            viewModel.uiState.observe(this@DigitalWalletPaymentActivity) {
                viewBinding.tvResponseStatus.requestFocus()
                hideKeyboard(viewBinding.root)
                when (it) {
                    is DigitalPaymentViewModel.UIState.Loading -> {
                        progressBar.isVisible = true
                    }

                    is DigitalPaymentViewModel.UIState.Idle -> {
                        progressBar.isVisible = false
                    }

                    is DigitalPaymentViewModel.UIState.CardPaymentSuccess -> {
                        progressBar.isVisible = false
                        Utils.showDialog(
                            context = this@DigitalWalletPaymentActivity,
                            title = getString(R.string.hs_transaction_success),
                            message = getString(R.string.card_payment_executed_success),
                        )
                    }

                    is DigitalPaymentViewModel.UIState.GooglePaymentSuccess -> {
                        progressBar.isVisible = false
                        Utils.showDialog(
                            context = this@DigitalWalletPaymentActivity,
                            title = getString(R.string.hs_transaction_success),
                            message = getString(R.string.google_payment_executed_success),
                        )
                    }

                    is DigitalPaymentViewModel.UIState.Error -> {
                        progressBar.isVisible = false
                        Utils.showDialog(
                            context = this@DigitalWalletPaymentActivity,
                            title = getString(R.string.error),
                            message = it.message,
                        )
                    }
                }
            }
        }
    }

    override fun onInputValid(
        paymentInputType: PaymentInputType,
        input: String,
    ) {
        viewModel.onUpdateCardData(paymentInputType, input)
    }

    override fun onPayButtonClicked() {
        viewModel.onPayButtonClicked(
            context = this@DigitalWalletPaymentActivity,
            paymentTransactionManager = paymentTransactionManager,
            activityProvider = {
                this@DigitalWalletPaymentActivity
            },
        )
    }

    override fun onGooglePayClicked() {
        googlePayTransactionManager.requestPayment(viewModel.googlePaymentAmount)
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?,
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        val googlePayResult =
            googlePayTransactionManager.onActivityResult(requestCode, resultCode, data)
        if (googlePayResult != null) {
            // Updates Trust payment SDK with GooglePay transaction result.
            viewModel.onGoogleWalletPayment(
                context = this@DigitalWalletPaymentActivity,
                walletToken = googlePayResult,
                paymentTransactionManager = paymentTransactionManager,
                activityProvider = {
                    this@DigitalWalletPaymentActivity
                },
            )
        }
    }
}
