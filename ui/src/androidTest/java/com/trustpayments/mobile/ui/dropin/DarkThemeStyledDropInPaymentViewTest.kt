package com.trustpayments.mobile.ui.dropin

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.RippleDrawable
import android.graphics.drawable.StateListDrawable
import android.os.Build
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.AppCompatButton
import androidx.lifecycle.Lifecycle
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.filters.SdkSuppress
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.trustpayments.mobile.ui.PanInputView
import com.trustpayments.mobile.ui.R
import com.trustpayments.mobile.ui.model.PaymentInputType
import com.trustpayments.mobile.ui.testutils.StyledDropInPaymentViewTestActivity
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DarkThemeStyledDropInPaymentViewTest {

    @get:Rule
    val activityScenarioRule =
        ActivityScenarioRule(StyledDropInPaymentViewTestActivity::class.java)

    @Before
    fun setUp() {
        activityScenarioRule.scenario.onActivity {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }
    }

    @Test
    fun test_inputTextColor() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedInputTextColor = Color.parseColor("#FFFFFF")

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)

            val panInput = dropInPaymentView.findViewById<PanInputView>(R.id.panInput)
            val inputView = panInput.findViewById<TextInputEditText>(R.id.input)
            val actualInputTextColor = inputView.textColors.defaultColor

            Assert.assertEquals(expectedInputTextColor, actualInputTextColor)
        }
    }

    @Test
    @SdkSuppress(minSdkVersion = Build.VERSION_CODES.N)
    fun test_inputBackgroundColor() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedInputColor = Color.parseColor("#006A94")

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)

            val panInput = dropInPaymentView.findViewById<PanInputView>(R.id.panInput)
            val inputView = panInput.findViewById<TextInputLayout>(R.id.inputContainer)
            val actualInputColor = inputView.boxBackgroundColor

            Assert.assertEquals(expectedInputColor, actualInputColor)
        }
    }

    @Test
    fun test_inputHintTextColor() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedHintTextColor = Color.parseColor("#7B0094")

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)

            val panInput = dropInPaymentView.findViewById<PanInputView>(R.id.panInput)
            val inputView = panInput.findViewById<TextInputEditText>(R.id.input)
            val actualHintTextColor = inputView.hintTextColors.defaultColor

            Assert.assertEquals(expectedHintTextColor, actualHintTextColor)
        }
    }

    @Test
    fun test_payButtonTextColor() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedTextColor = Color.parseColor("#FFFFFF")

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)

            val payButton = dropInPaymentView.findViewById<AppCompatButton>(R.id.payButton)
            val actualTextColor = payButton.textColors.defaultColor

            Assert.assertEquals(expectedTextColor, actualTextColor)
        }
    }

    @Test
    @SdkSuppress(minSdkVersion = Build.VERSION_CODES.N)
    fun test_disabledPayButtonBackgroundColor() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedIsPayButtonEnabled = false
        val expectedPayButtonColor = Color.parseColor("#6DA399")

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)

            val payButton = dropInPaymentView.findViewById<View>(R.id.payButton)
            val actualIsPayButtonEnabled = payButton.isEnabled
            val actualPayButtonColor = payButton.getButtonBackgroundColor()

            Assert.assertEquals(expectedIsPayButtonEnabled, actualIsPayButtonEnabled)
            Assert.assertEquals(expectedPayButtonColor, actualPayButtonColor)
        }
    }

    @Test
    @SdkSuppress(minSdkVersion = Build.VERSION_CODES.N)
    fun test_enabledPayButtonBackgroundColor() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val dropInPaymentViewListener =
            mockk<DropInPaymentView.DropInPaymentViewListener>()
        val expectedIsPayButtonEnabled = true
        val expectedPayButtonColor = Color.parseColor("#009476")
        every { dropInPaymentViewListener.onInputValid(any(), any()) } returns Unit

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            dropInPaymentView.dropInPaymentViewListener = dropInPaymentViewListener

            dropInPaymentView.onInputValid(PaymentInputType.PAN, "")
            dropInPaymentView.onInputValid(PaymentInputType.ExpiryDate, "")
            dropInPaymentView.onInputValid(PaymentInputType.CVV, "")

            val payButton = dropInPaymentView.findViewById<View>(R.id.payButton)
            val actualIsPayButtonEnabled = payButton.isEnabled
            val actualPayButtonColor = payButton.getButtonBackgroundColor()

            Assert.assertEquals(expectedIsPayButtonEnabled, actualIsPayButtonEnabled)
            Assert.assertEquals(expectedPayButtonColor, actualPayButtonColor)
        }
    }

    private fun View.getInputBackgroundColor() =
        ((this.background as StateListDrawable).current as GradientDrawable).color?.defaultColor

    private fun View.getButtonBackgroundColor() =
        ((this.background as RippleDrawable)
            .getDrawable(0).current as GradientDrawable).color?.defaultColor
}