package com.trustpayments.mobile.core.services.api

import okhttp3.CertificatePinner
import okhttp3.OkHttpClient

fun OkHttpClient.Builder.applyPinning(pinner: CertificatePinner): OkHttpClient.Builder {
    this.certificatePinner(pinner)
    return this
}