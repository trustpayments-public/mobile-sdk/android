package com.trustpayments.mobile.core.analytics.logger

import com.trustpayments.mobile.core.analytics.logger.protocol.AnalyticClientSettings
import com.trustpayments.mobile.core.analytics.logger.protocol.AnalyticEvent
import com.trustpayments.mobile.core.utils.log
import java.util.UUID

/**
 * Generic class for providing analytic services.
 * Acts as a repository for recoding analytic events.
 * 3rd party analytic client integration can be included here.
 * */
class AnalyticHub(private val clientSettings: AnalyticClientSettings) {

    /**
     * Used for initializing analytic client and setting up its configurations.
     * */
    fun initialize() {
        // TODO: Initialize 3rd party analytic client configurations here.
        // Since project is not using any 3rd party analytic client, block is empty at the moment
    }

    fun captureErrorEvent(event: AnalyticEvent): UUID {
        return event.eventId
    }
}