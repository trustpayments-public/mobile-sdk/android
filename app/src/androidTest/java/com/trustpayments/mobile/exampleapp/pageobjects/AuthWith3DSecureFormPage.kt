package com.trustpayments.mobile.exampleapp.pageobjects

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.cardinalcommerce.cardinalmobilesdk.R

class AuthWith3DSecureFormPage : BaseAppPage() {


    //region Elements
    private val threeDSecureCodeInput = ViewMatchers.withId(R.id.codeEditTextField)
    private val submitButton = ViewMatchers.withId(R.id.submitAuthenticationButton)
    private val cancelButton = ViewMatchers.withId(R.id.toolbarButton)
    //endregion

    //region Actions
    fun fillSecureCode(threeDSecureCode: String): AuthWith3DSecureFormPage {
        waitUntilWebIsLoaded()
        Espresso.onView(threeDSecureCodeInput).perform(ViewActions.typeText(threeDSecureCode))
        return this;
    }

    fun tapOnSubmitButton(): AuthWith3DSecureFormPage {
        Espresso.onView(submitButton).perform(ViewActions.scrollTo(), ViewActions.click())
        return this
    }

    fun isSubmitButtonEnabled(){
        Espresso.onView(submitButton).check(ViewAssertions.matches(ViewMatchers.isEnabled()))
    }

    fun waitUntilWebIsLoaded(): AuthWith3DSecureFormPage {
        waitUntilElementIsDisplayed(threeDSecureCodeInput, 10)
        return this
    }

    fun tapCancelButton(): AuthWith3DSecureFormPage {
        Espresso.onView(cancelButton).perform(ViewActions.click())
        return this
    }
}