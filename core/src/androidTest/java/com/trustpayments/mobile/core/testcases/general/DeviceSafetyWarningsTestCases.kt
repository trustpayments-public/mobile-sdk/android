package com.trustpayments.mobile.core.testcases.general

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.rule.ActivityTestRule
import com.trustpayments.mobile.core.models.DeviceSafetyWarning
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.services.api.TrustPaymentsGatewayType
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.test.BuildConfig
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.utils.ThreeDQueryTestActivity
import com.trustpayments.mobile.core.testutils.createSessionFor
import com.trustpayments.mobile.core.testutils.getResultAsSafetyError
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.util.PaymentSessionResponse
import com.trustpayments.mobile.core.util.parse
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DeviceSafetyWarningsTestCases: BaseTestCase() {

    private lateinit var paymentTransactionManager: PaymentTransactionManager

    private lateinit var jwtToken: String

    private lateinit var app: Context

    @get:Rule
    val initialActivityTestRule = ActivityTestRule(ThreeDQueryTestActivity::class.java)

    private val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    @Before
    override fun setUp() {
        super.setUp()

        app = ApplicationProvider.getApplicationContext<Context>().applicationContext

        jwtToken = jwtBuilder.getFor3DSecure(listOf(RequestType.Auth))
    }

    @Test
    fun test_negative_executionFailsWithLiveCardinal() = runBlocking {
        // GIVEN
        val isCardinalLive = true
        paymentTransactionManager =
            PaymentTransactionManager(app, TrustPaymentsGatewayType.EU, isCardinalLive, BuildConfig.MERCHANT_USERNAME, isLocationDataConsentGiven = false)
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        // WHEN
        val sessionResult = paymentTransactionManager.executeSession(session).parse()

        // THEN
        assertTrue(sessionResult is PaymentSessionResponse.Failure.SafetyError)
        assertTrue(sessionResult.getResultAsSafetyError().errors.contains(DeviceSafetyWarning.AppInstalledFromUntrustedSource))
    }
}