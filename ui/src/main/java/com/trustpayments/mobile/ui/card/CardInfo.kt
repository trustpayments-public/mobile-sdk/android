package com.trustpayments.mobile.ui.card

import com.trustpayments.mobile.ui.R
import com.trustpayments.mobile.ui.card.model.CardType


internal object CardInfo {

    const val DEFAULT_INPUT_MASK = "#### #### #### #### ###"

    @JvmSynthetic
    fun getIinRanges(cardType: CardType): List<IntRange> =
        // https://www.barclaycard.co.uk/business/files/BIN-Rules-UK.pdf
        when (cardType) {
            CardType.Visa -> listOf(400000..499999)
            CardType.Mastercard -> listOf(510000..559999, 222100..272099)
            CardType.Amex -> listOf(340000..349999, 370000..379999)
            CardType.Maestro -> listOf(
                500000..509999, 560000..601099, 601200..622125, 622926..623999, 627000..628199,
                628900..639999, 660000..699999
            )
            CardType.Discover -> listOf(
                601100..601199, 622126..622925, 624000..626999, 628200..628899, 640000..659999
            )
            CardType.Diners -> listOf(
                300000..305999, 309500..309599, 380000..399999, 360000..369999
            )
            CardType.JCB -> listOf(352800..358999)
            CardType.Unknown -> listOf(0..1)
        }

    @JvmSynthetic
    fun getValidNumberLengths(cardType: CardType): List<Int> =
        when (cardType) {
            CardType.Visa -> listOf(13, 16, 19)
            CardType.Mastercard -> listOf(16)
            CardType.Amex -> listOf(15)
            CardType.Maestro -> listOf(12, 13, 14, 15, 16, 17, 18, 19)
            CardType.Discover -> listOf(14, 15, 16, 17, 18, 19)
            CardType.Diners -> listOf(14, 15, 16, 17, 18, 19)
            CardType.JCB -> listOf(15, 16, 19)
            CardType.Unknown -> listOf(16)
        }

    @JvmSynthetic
    fun getInputMask(cardType: CardType): String =
        when (cardType) {
            CardType.Mastercard -> "#### #### #### ####"
            // 4-6-5
            CardType.Amex -> "#### ###### #####"
            // 4-6-4 (for card numbers shorter than 15 digits, 4-4-4-4 for the rest)
            CardType.Diners -> "#### ###### ####"
            CardType.Unknown -> "#### #### #### ####"
            else -> DEFAULT_INPUT_MASK
        }

    @JvmSynthetic
    fun getIcon(cardType: CardType): Int =
        when (cardType) {
            CardType.Visa -> R.drawable.ic_visa
            CardType.Mastercard -> R.drawable.ic_mastercard
            CardType.Amex -> R.drawable.ic_amex
            CardType.Maestro -> R.drawable.ic_maestro
            CardType.Discover -> R.drawable.ic_discover
            CardType.Diners -> R.drawable.ic_diners
            CardType.JCB -> R.drawable.ic_jcb
            CardType.Unknown -> R.drawable.ic_credit_card
        }

    @JvmSynthetic
    fun getMinNumberLength(cardType: CardType) =
        getValidNumberLengths(cardType).first()

    @JvmSynthetic
    fun getMaxNumberLength(cardType: CardType) =
        getValidNumberLengths(cardType).last()

    @JvmSynthetic
    fun getMaxMaskLength(cardType: CardType) =
        getInputMask(cardType).length

    @JvmSynthetic
    fun getCvvLength(cardType: CardType): Int =
        when (cardType) {
            CardType.Amex -> 4
            else -> 3
        }
 }