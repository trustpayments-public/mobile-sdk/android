package com.trustpayments.mobile.core

import android.app.Activity
import android.content.Context
import android.os.Build
import com.trustpayments.mobile.core.models.DeviceSafetyWarning
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.services.api.TrustPaymentsApiService
import com.trustpayments.mobile.core.services.api.TrustPaymentsGatewayType
import com.trustpayments.mobile.core.services.transaction.*
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.util.PaymentSessionResponse
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.M])
class PaymentTransactionManagerCardinalWarningsTest {
    private lateinit var paymentTransactionManager: PaymentTransactionManager

    @MockK
    private lateinit var mockContext: Context

    @MockK
    private lateinit var mockLoggingManager: LoggingManager

    @MockK
    private lateinit var mockApiService: TrustPaymentsApiService

    @MockK
    private lateinit var mockThreeDSecureManager: ThreeDSecureManager

    @MockK
    private lateinit var mockActivity: Activity

    @MockK
    private lateinit var mockActivityResultProvider: ActivityResultProvider

    private val mockActivityProvider = { mockActivity }

    private val MERCHANT_USERNAME = "testUsername"

    private val REQUEST_CARD_PAN = "1111111111111111"
    private val REQUEST_CARD_EXPIRY_DATE = "12/23"
    private val REQUEST_CARD_CVV = "123"

    private val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    @Before
    fun setup() {
        MockKAnnotations.init(this)

        mockkObject(LoggingManager.Companion)
        every { LoggingManager.Companion.getInstance(any())} returns mockLoggingManager
        every { mockLoggingManager.sendErrorEvent(any(), any(),any(), any(), any()) } returns Unit
        every { mockLoggingManager.sendExceptionEvent(any()) } returns Unit
        every { mockLoggingManager.init() } returns Unit
        every { mockLoggingManager.release() } returns Unit

        mockkObject(ThreeDSecureManager.Companion)
        every { ThreeDSecureManager.Companion.getInstance(any(), any(), any(), any(), any(), any()) } returns mockThreeDSecureManager

        mockkObject(TrustPaymentsApiService.Companion)
        every { TrustPaymentsApiService.Companion.getInstance(any(), any(), any()) } returns mockApiService

        every { mockThreeDSecureManager.createSession() } just Runs
    }

    @Test
    fun test_negative_InitFailsWithCardinalWarningForLive_DeviceRooted() = runBlocking {
        //GIVEN
        val session = createNormalSession(listOf(RequestType.Auth))
        paymentTransactionManager = PaymentTransactionManager(
            mockContext, TrustPaymentsGatewayType.EU, true, MERCHANT_USERNAME, isLocationDataConsentGiven = false)
        every { mockThreeDSecureManager.deviceSafetyWarnings } returns listOf(DeviceSafetyWarning.DeviceRooted)

        //WHEN
        val result = paymentTransactionManager.executeSession(session)

        //THEN
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationNeverCalled()
        verifyThreeDSecureManagerExecutionNeverCalled()
        verifyApiServiceInitializationNeverCalled()
        verifyApiServiceTransactionNeverCalled()

        val safetyWarning = result.error as Error.SafetyError
        assertEquals(1, safetyWarning.errors.size)
        assertEquals(DeviceSafetyWarning.DeviceRooted, safetyWarning.errors[0])
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
    }

    @Test
    fun test_negative_InitFailsWithCardinalWarningForNotLive_DeviceRootedWithoutEmulator() = runBlocking {
        //GIVEN
        val session = createNormalSession(listOf(RequestType.Auth))
        paymentTransactionManager = PaymentTransactionManager(
            mockContext, TrustPaymentsGatewayType.EU, false, MERCHANT_USERNAME, isLocationDataConsentGiven = false)
        every { mockThreeDSecureManager.deviceSafetyWarnings } returns listOf(DeviceSafetyWarning.DeviceRooted)

        //WHEN
        val result = paymentTransactionManager.executeSession(session)

        //THEN
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationNeverCalled()
        verifyThreeDSecureManagerExecutionNeverCalled()
        verifyApiServiceInitializationNeverCalled()
        verifyApiServiceTransactionNeverCalled()

        val safetyWarning = result.error as Error.SafetyError
        assertEquals(1, safetyWarning.errors.size)
        assertEquals(DeviceSafetyWarning.DeviceRooted, safetyWarning.errors[0])
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
    }

    @Test
    fun test_positive_InitSucceedsWithCardinalWarningForNotLive_DeviceRootedWithEmulator() = runBlocking {
        //GIVEN
        val session = createNormalSession(listOf(RequestType.ThreeDQuery))
        paymentTransactionManager = PaymentTransactionManager(
            mockContext, TrustPaymentsGatewayType.EU, false, MERCHANT_USERNAME, isLocationDataConsentGiven = false)
        every { mockThreeDSecureManager.deviceSafetyWarnings } returns listOf(
            DeviceSafetyWarning.DeviceRooted, DeviceSafetyWarning.EmulatorIsUsed)

        //WHEN
        apiServiceReturnsErrorInitResult()

        val result = paymentTransactionManager.executeSession(session, mockActivityProvider)

        //THEN
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(Error.InitializationError, result.error)
    }

    @Test
    fun test_positive_InitSucceedsWithCardinalWarningForNotLive_DeviceRootedWithEmulatorWithActivityResultProvider() = runBlocking {
        //GIVEN
        val session = createNormalSession(listOf(RequestType.ThreeDQuery))
        paymentTransactionManager = PaymentTransactionManager(
            mockContext, TrustPaymentsGatewayType.EU, false, MERCHANT_USERNAME, isLocationDataConsentGiven = false)
        every { mockThreeDSecureManager.deviceSafetyWarnings } returns listOf(
            DeviceSafetyWarning.DeviceRooted, DeviceSafetyWarning.EmulatorIsUsed)

        //WHEN
        apiServiceReturnsErrorInitResult()

        val result = paymentTransactionManager.executeSession(session, mockActivityProvider, mockActivityResultProvider)

        //THEN
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(Error.InitializationError, result.error)
    }

    @Test
    fun test_negative_InitFailsWithCardinalWarningForLive_EmulatorUsed() = runBlocking {
        //GIVEN
        val session = createNormalSession(listOf(RequestType.Auth))
        paymentTransactionManager = PaymentTransactionManager(
            mockContext, TrustPaymentsGatewayType.EU, true, MERCHANT_USERNAME, isLocationDataConsentGiven = false)
        every { mockThreeDSecureManager.deviceSafetyWarnings } returns listOf(DeviceSafetyWarning.EmulatorIsUsed)

        //WHEN
        val result = paymentTransactionManager.executeSession(session)

        //THEN
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationNeverCalled()
        verifyThreeDSecureManagerExecutionNeverCalled()
        verifyApiServiceInitializationNeverCalled()
        verifyApiServiceTransactionNeverCalled()

        val safetyWarning = result.error as Error.SafetyError
        assertEquals(1, safetyWarning.errors.size)
        assertEquals(DeviceSafetyWarning.EmulatorIsUsed, safetyWarning.errors[0])
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
    }

    @Test
    fun test_positive_InitFailsWithCardinalWarningForNotLive_EmulatorUsed() = runBlocking {
        //GIVEN
        val session = createNormalSession(listOf(RequestType.ThreeDQuery))
        paymentTransactionManager = PaymentTransactionManager(
            mockContext, TrustPaymentsGatewayType.EU, false, MERCHANT_USERNAME, isLocationDataConsentGiven = false)
        every { mockThreeDSecureManager.deviceSafetyWarnings } returns listOf(DeviceSafetyWarning.EmulatorIsUsed)

        //WHEN
        apiServiceReturnsErrorInitResult()

        val result = paymentTransactionManager.executeSession(session, mockActivityProvider)

        //THEN
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(Error.InitializationError, result.error)
    }

    @Test
    fun test_positive_InitFailsWithCardinalWarningForNotLive_EmulatorUsedWithActivityResultProvider() = runBlocking {
        //GIVEN
        val session = createNormalSession(listOf(RequestType.ThreeDQuery))
        paymentTransactionManager = PaymentTransactionManager(
            mockContext, TrustPaymentsGatewayType.EU, false, MERCHANT_USERNAME, isLocationDataConsentGiven = false)
        every { mockThreeDSecureManager.deviceSafetyWarnings } returns listOf(DeviceSafetyWarning.EmulatorIsUsed)

        //WHEN
        apiServiceReturnsErrorInitResult()

        val result = paymentTransactionManager.executeSession(session, mockActivityProvider, mockActivityResultProvider)

        //THEN
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(Error.InitializationError, result.error)
    }

    @Test
    fun test_negative_InitFailsWithCardinalWarningForLive_DebuggerUsed() = runBlocking {
        //GIVEN
        val session = createNormalSession(listOf(RequestType.Auth))
        paymentTransactionManager = PaymentTransactionManager(
            mockContext, TrustPaymentsGatewayType.EU, true, MERCHANT_USERNAME, isLocationDataConsentGiven = false)
        every { mockThreeDSecureManager.deviceSafetyWarnings } returns listOf(DeviceSafetyWarning.DebuggerIsAttached)

        //WHEN
        val result = paymentTransactionManager.executeSession(session)

        //THEN
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationNeverCalled()
        verifyThreeDSecureManagerExecutionNeverCalled()
        verifyApiServiceInitializationNeverCalled()
        verifyApiServiceTransactionNeverCalled()

        val safetyWarning = result.error as Error.SafetyError
        assertEquals(1, safetyWarning.errors.size)
        assertEquals(DeviceSafetyWarning.DebuggerIsAttached, safetyWarning.errors[0])
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
    }

    @Test
    fun test_positive_InitFailsWithCardinalWarningForNotLive_DebuggerUsed() = runBlocking {
        //GIVEN
        val session = createNormalSession(listOf(RequestType.ThreeDQuery))
        paymentTransactionManager = PaymentTransactionManager(
            mockContext, TrustPaymentsGatewayType.EU, false, MERCHANT_USERNAME, isLocationDataConsentGiven = false)
        every { mockThreeDSecureManager.deviceSafetyWarnings } returns listOf(DeviceSafetyWarning.DebuggerIsAttached)

        //WHEN
        apiServiceReturnsErrorInitResult()

        val result = paymentTransactionManager.executeSession(session, mockActivityProvider)

        //THEN
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(Error.InitializationError, result.error)
    }

    @Test
    fun test_positive_InitFailsWithCardinalWarningForNotLive_DebuggerUsedWithActivityResultProvider() = runBlocking {
        //GIVEN
        val session = createNormalSession(listOf(RequestType.ThreeDQuery))
        paymentTransactionManager = PaymentTransactionManager(
            mockContext, TrustPaymentsGatewayType.EU, false, MERCHANT_USERNAME, isLocationDataConsentGiven = false)
        every { mockThreeDSecureManager.deviceSafetyWarnings } returns listOf(DeviceSafetyWarning.DebuggerIsAttached)

        //WHEN
        apiServiceReturnsErrorInitResult()

        val result = paymentTransactionManager.executeSession(session, mockActivityProvider, mockActivityResultProvider)

        //THEN
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(Error.InitializationError, result.error)
    }

    @Test
    fun test_negative_InitFailsWithCardinalWarningForLive_AppFromUntrustedSource() = runBlocking {
        //GIVEN
        val session = createNormalSession(listOf(RequestType.Auth))
        paymentTransactionManager = PaymentTransactionManager(
            mockContext, TrustPaymentsGatewayType.EU, true, MERCHANT_USERNAME, isLocationDataConsentGiven = false)
        every { mockThreeDSecureManager.deviceSafetyWarnings } returns listOf(DeviceSafetyWarning.AppInstalledFromUntrustedSource)

        //WHEN
        val result = paymentTransactionManager.executeSession(session)

        //THEN
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationNeverCalled()
        verifyThreeDSecureManagerExecutionNeverCalled()
        verifyApiServiceInitializationNeverCalled()
        verifyApiServiceTransactionNeverCalled()

        val safetyWarning = result.error as Error.SafetyError
        assertEquals(1, safetyWarning.errors.size)
        assertEquals(DeviceSafetyWarning.AppInstalledFromUntrustedSource, safetyWarning.errors[0])
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
    }

    @Test
    fun test_positive_InitFailsWithCardinalWarningForNotLive_AppFromUntrustedSource() = runBlocking {
        //GIVEN
        val session = createNormalSession(listOf(RequestType.ThreeDQuery))
        paymentTransactionManager = PaymentTransactionManager(
            mockContext, TrustPaymentsGatewayType.EU, false, MERCHANT_USERNAME, isLocationDataConsentGiven = false)
        every { mockThreeDSecureManager.deviceSafetyWarnings } returns listOf(DeviceSafetyWarning.AppInstalledFromUntrustedSource)

        //WHEN
        apiServiceReturnsErrorInitResult()

        val result = paymentTransactionManager.executeSession(session, mockActivityProvider)

        //THEN
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(Error.InitializationError, result.error)
    }

    @Test
    fun test_positive_InitFailsWithCardinalWarningForNotLive_AppFromUntrustedSourceWithActivityResultProvider() = runBlocking {
        //GIVEN
        val session = createNormalSession(listOf(RequestType.ThreeDQuery))
        paymentTransactionManager = PaymentTransactionManager(
            mockContext, TrustPaymentsGatewayType.EU, false, MERCHANT_USERNAME, isLocationDataConsentGiven = false)
        every { mockThreeDSecureManager.deviceSafetyWarnings } returns listOf(DeviceSafetyWarning.AppInstalledFromUntrustedSource)

        //WHEN
        apiServiceReturnsErrorInitResult()

        val result = paymentTransactionManager.executeSession(session, mockActivityProvider, mockActivityResultProvider)

        //THEN
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(Error.InitializationError, result.error)
    }

    //GIVEN helpers

    private fun createNormalSession(requests: List<RequestType>): PaymentSession {
        val jwt = jwtBuilder.getStandard(requests)
        return PaymentSession({ jwt }, REQUEST_CARD_PAN, REQUEST_CARD_EXPIRY_DATE, REQUEST_CARD_CVV)
    }

    //WHEN helpers

    private fun apiServiceReturnsErrorInitResult() {
        coEvery { mockApiService.initializeSession(any(), any(), any()) } returns TrustPaymentsApiService.Result.Failure
    }

    //THEN helpers

    private fun verifyApiServiceInitializationNeverCalled() {
        coVerify(exactly = 0) { mockApiService.initializeSession(any(), any(), any()) }
    }

    private fun verifyApiServiceTransactionNeverCalled() {
        coVerify(exactly = 0) { mockApiService.performTransaction(any(), any(), any(), any(), any(), any(), any(), any(), any()) }
    }

    private fun verifyThreeDSecureManagerExecutionNeverCalled() {
        coVerify(exactly = 0) { mockThreeDSecureManager.executeSession(any(), any(), any(), any()) }
    }

    private fun verifyThreeDSecureManagerInitializationNeverCalled() {
        coVerify(exactly = 0) { mockThreeDSecureManager.initSession(any()) }
    }

    private fun verifyThreeDSecureManagerCreationCalledOnce() {
        coVerify(exactly = 1) { mockThreeDSecureManager.createSession() }
    }

    private fun PaymentSessionResponse.getFirstDeviceSafetyWarning(): DeviceSafetyWarning
            = (this as PaymentSessionResponse.Failure.SafetyError).errors.first()
}