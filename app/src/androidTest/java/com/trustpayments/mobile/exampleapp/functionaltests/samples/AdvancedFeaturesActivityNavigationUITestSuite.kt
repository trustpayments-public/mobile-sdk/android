package com.trustpayments.mobile.exampleapp.functionaltests.samples

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.samples.accountverificationwithlaterauth.AccountVerificationWithLaterAuthSampleActivity
import com.trustpayments.mobile.exampleapp.samples.advancedfeatures.AdvancedFeaturesActivity
import com.trustpayments.mobile.exampleapp.samples.backgroundrequest.BackgroundRequestSampleActivity
import org.junit.Rule
import org.junit.Test

class AdvancedFeaturesActivityNavigationUITestSuite {

    @get:Rule
    val activityScenarioRule = ActivityScenarioRule(AdvancedFeaturesActivity::class.java)

    @Test
    fun testBackgroundRequestItemClickAction() {
        Intents.init()
        Espresso.onView(ViewMatchers.withId(R.id.samples_list))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0,
                    ViewActions.click()
                )
            )

        Intents.intended(IntentMatchers.hasComponent(BackgroundRequestSampleActivity::class.java.name))
        Intents.release()
    }

    @Test
    fun testAccountVerificationItemClickAction() {
        Intents.init()
        Espresso.onView(ViewMatchers.withId(R.id.samples_list))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    1,
                    ViewActions.click()
                )
            )

        Intents.intended(IntentMatchers.hasComponent(AccountVerificationWithLaterAuthSampleActivity::class.java.name))
        Intents.release()
    }

}
