package com.trustpayments.mobile.core.testutils

import android.app.Activity
import android.util.DisplayMetrics
import android.widget.Button
import android.widget.EditText
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.uiautomator.*
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.test.R
import org.hamcrest.CoreMatchers

class GooglePayTestPage(private val device : UiDevice, private val activity : Activity) {

    companion object {
        const val DEFAULT_TIMEOUT = 3000L
    }

    private val payButton = CoreMatchers.allOf(
            ViewMatchers.withId(R.id.googlePayButton)
    )

    /* problem: uiAutomator seems to not be able to recognize the pay button sometimes
    *  leaving us with the task to "guess" where the button is.
    * */
    fun tapContinue(displayMetrics : DisplayMetrics){
        device.wait(Until.findObject(By.clazz(Button::class.java)), DEFAULT_TIMEOUT)
        if(device.hasObject(By.clazz(Button::class.java))){
            device.findObject(UiSelector().className(Button::class.java)).click()
        } else {
            // guessing
            device.click(displayMetrics.widthPixels / 2,
                (displayMetrics.heightPixels - displayMetrics.heightPixels * 0.06).toInt()
            )
            device.click( (displayMetrics.widthPixels - displayMetrics.widthPixels * 0.1).toInt(),
                (displayMetrics.heightPixels - displayMetrics.heightPixels * 0.06).toInt()
            )
            device.click( (displayMetrics.widthPixels - displayMetrics.widthPixels * 0.1).toInt(),
                (displayMetrics.heightPixels - displayMetrics.heightPixels * 0.01).toInt()
            )
        }
        device.wait(Until.findObject(By.clazz(EditText::class.java.name)), DEFAULT_TIMEOUT)
        if(device.hasObject(By.clazz(EditText::class.java.name))){
            device.findObject(By.clazz(EditText::class.java.name)).text = BuildConfig.DEVICE_PASSWORD
            device.pressEnter()
            device.waitForIdle()
        }
    }

    fun tapPayWithGooglePay(){
        device.findObject(
                UiSelector().text(activity.getString(R.string.pay_with_google_pay))
        ).clickAndWaitForNewWindow()
    }

    fun tapPayWithGooglePayEspresso(){
        Espresso.onView(payButton).perform(ViewActions.click())
    }

    fun tapCardOptions(){
        device.findObject(
                UiSelector().textContains("Visa")
        ).click()
    }

    fun waitResult(){
        device.wait(Until.findObject(By.desc("result")), DEFAULT_TIMEOUT)
    }

    fun goBack() {
        device.pressBack()
    }

    fun waitButton(){
        device.wait(Until.findObject(By.text(activity.getString(R.string.pay_with_google_pay))), DEFAULT_TIMEOUT)
    }

}