package com.trustpayments.mobile.ui.card

import com.trustpayments.mobile.ui.R
import com.trustpayments.mobile.ui.card.model.CardType
import org.junit.Assert.assertEquals
import org.junit.Test

class CardIconTest {

    @Test
    fun test_CorrectIconForVisa() {
        // GIVEN
        val expectedCardIcon = R.drawable.ic_visa

        // WHEN
        val actualCardIcon = CardInfo.getIcon(CardType.Visa)

        // THEN
        assertEquals(expectedCardIcon, actualCardIcon)
    }

    @Test
    fun test_CorrectIconForMastercard() {
        // GIVEN
        val expectedCardIcon = R.drawable.ic_mastercard

        // WHEN
        val actualCardIcon = CardInfo.getIcon(CardType.Mastercard)

        // THEN
        assertEquals(expectedCardIcon, actualCardIcon)
    }

    @Test
    fun test_CorrectIconForAmex() {
        // GIVEN
        val expectedCardIcon = R.drawable.ic_amex

        // WHEN
        val actualCardIcon = CardInfo.getIcon(CardType.Amex)

        // THEN
        assertEquals(expectedCardIcon, actualCardIcon)
    }

    @Test
    fun test_CorrectIconForMaestro() {
        // GIVEN
        val expectedCardIcon = R.drawable.ic_maestro

        // WHEN
        val actualCardIcon = CardInfo.getIcon(CardType.Maestro)

        // THEN
        assertEquals(expectedCardIcon, actualCardIcon)
    }

    @Test
    fun test_CorrectIconForDiscover() {
        // GIVEN
        val expectedCardIcon = R.drawable.ic_discover

        // WHEN
        val actualCardIcon = CardInfo.getIcon(CardType.Discover)

        // THEN
        assertEquals(expectedCardIcon, actualCardIcon)
    }

    @Test
    fun test_CorrectIconForDiners() {
        // GIVEN
        val expectedCardIcon = R.drawable.ic_diners

        // WHEN
        val actualCardIcon = CardInfo.getIcon(CardType.Diners)

        // THEN
        assertEquals(expectedCardIcon, actualCardIcon)
    }

    @Test
    fun test_CorrectIconForJCB() {
        // GIVEN
        val expectedCardIcon = R.drawable.ic_jcb

        // WHEN
        val actualCardIcon = CardInfo.getIcon(CardType.JCB)

        // THEN
        assertEquals(expectedCardIcon, actualCardIcon)
    }

    @Test
    fun test_CorrectIconForUnknown() {
        // GIVEN
        val expectedCardIcon = R.drawable.ic_credit_card

        // WHEN
        val actualCardIcon = CardInfo.getIcon(CardType.Unknown)

        // THEN
        assertEquals(expectedCardIcon, actualCardIcon)
    }
}