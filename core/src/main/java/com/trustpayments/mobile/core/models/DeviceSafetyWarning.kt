package com.trustpayments.mobile.core.models

sealed class DeviceSafetyWarning {
    object DeviceRooted: DeviceSafetyWarning()
    object SdkIntegrityTampered: DeviceSafetyWarning()
    object EmulatorIsUsed: DeviceSafetyWarning()
    object DebuggerIsAttached: DeviceSafetyWarning()
    object AppInstalledFromUntrustedSource: DeviceSafetyWarning()
}