package com.trustpayments.mobile.ui.dropin

import android.app.Activity
import android.os.Build
import android.text.InputType
import android.text.SpannedString
import android.text.style.ForegroundColorSpan
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.trustpayments.mobile.ui.CvvInputView
import com.trustpayments.mobile.ui.ExpiryDateInputView
import com.trustpayments.mobile.ui.GooglePayButton
import com.trustpayments.mobile.ui.PanInputView
import com.trustpayments.mobile.ui.PayButton
import com.trustpayments.mobile.ui.R
import com.trustpayments.mobile.ui.card.model.CardType
import com.trustpayments.mobile.ui.model.GooglePayButtonColor
import com.trustpayments.mobile.ui.model.PaymentInputType
import com.trustpayments.mobile.ui.testutils.DropInPaymentViewTestActivity
import com.trustpayments.mobile.ui.utils.setOnSingleClickListener
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThrows
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.P])
class DropInPaymentViewTest {

    @get:Rule
    val activityScenarioRule =
        ActivityScenarioRule(DropInPaymentViewTestActivity::class.java)

    private fun findDropInView(activity: Activity): DropInPaymentView? =
        activity.findViewById(R.id.dropInPaymentView)

    private fun findPanInputView(activity: Activity): PanInputView? {
        val dropInPaymentView = findDropInView(activity)
        return dropInPaymentView?.findViewById(R.id.panInput)
    }

    private fun findPanInputEditText(activity: Activity): TextInputEditText? {
        val panInput = findPanInputView(activity)
        return panInput?.findViewById(R.id.input)
    }

    private fun findPanInputLabel(activity: Activity): TextView? {
        val panInput = findPanInputView(activity)
        return panInput?.findViewById(R.id.label)
    }

    private fun findPanInputTextLayout(activity: Activity): TextInputLayout? {
        val panInput = findPanInputView(activity)
        return panInput?.findViewById(R.id.inputContainer)
    }

    private fun findPayButton(activity: Activity): PayButton? {
        val dropInPaymentView = findDropInView(activity)
        return dropInPaymentView?.findViewById(R.id.payButton)
    }

    private fun findGooglePayButton(activity: Activity): GooglePayButton? {
        val dropInPaymentView = findDropInView(activity)
        return dropInPaymentView?.findViewById(R.id.googlePayButton)
    }

    private fun findGooglePayButtonBackground(activity: Activity): View? {
        val googlePayButton = findGooglePayButton(activity)
        return googlePayButton?.findViewById(R.id.buttonBackground)
    }

    private fun findGooglePayButtonImage(activity: Activity): View? {
        val googlePayButton = findGooglePayButton(activity)
        return googlePayButton?.findViewById(R.id.googlePayImage)
    }


    private fun findCvvInputView(activity: Activity): CvvInputView? {
        val dropInPaymentView = findDropInView(activity)
        return dropInPaymentView?.findViewById(R.id.cvvInput)
    }

    private fun findCvvInputEditText(activity: Activity): TextInputEditText? {
        val cvvView = findCvvInputView(activity)
        return cvvView?.findViewById(R.id.input)
    }

    private fun findCvvInputLabel(activity: Activity): TextView? {
        val cvvView = findCvvInputView(activity)
        return cvvView?.findViewById(R.id.label)
    }

    private fun findExpireInputView(activity: Activity): ExpiryDateInputView? {
        val dropInPaymentView = findDropInView(activity)
        return dropInPaymentView?.findViewById(R.id.expireInput)
    }

    private fun findExpireInputEditText(activity: Activity): TextInputEditText? {
        val expireInputView = findExpireInputView(activity)
        return expireInputView?.findViewById(R.id.input)
    }

    private fun findExpireInputLabel(activity: Activity): TextView? {
        val expireInputView = findExpireInputView(activity)
        return expireInputView?.findViewById(R.id.label)
    }

    @Test
    fun test_CorrectCardNumberHint() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedCardNumberHint = "xxxx xxxx xxxx xxxx"

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->

            val actualCardNumberHint = findPanInputEditText(activity)?.hint.toString()
            assertEquals(expectedCardNumberHint, actualCardNumberHint)
        }
    }

    @Test
    fun test_CorrectExpiryDateHint() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedExpiryDateHint = "MM/YY"

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val actualExpiryDateHint = findExpireInputEditText(activity)?.hint.toString()
            assertEquals(expectedExpiryDateHint, actualExpiryDateHint)
        }
    }

    @Test
    fun test_CorrectCvvHint() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedCvvHint = "***"

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val actualCvvHint = findCvvInputEditText(activity)?.hint.toString()
            assertEquals(expectedCvvHint, actualCvvHint)
        }
    }

    @Test
    fun test_CorrectCardNumberLabel() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedCardNumberLabel = "Card number"

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val actualCardNumberLabel = findPanInputLabel(activity)?.text.toString()
                .takeLabelWithoutAsterisk()
            assertEquals(expectedCardNumberLabel, actualCardNumberLabel)
        }
    }

    @Test
    fun test_CorrectExpiryDateLabel() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedExpiryDateLabel = "Expiry date"

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val actualExpiryDateLabel = findExpireInputLabel(activity)?.text.toString()
                .takeLabelWithoutAsterisk()
            assertEquals(expectedExpiryDateLabel, actualExpiryDateLabel)
        }
    }

    @Test
    fun test_CorrectCvvLabel() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedCvvLabel = "CVV"

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val actualCvvLabel = findCvvInputLabel(activity)?.text.toString()
                .takeLabelWithoutAsterisk()
            assertEquals(expectedCvvLabel, actualCvvLabel)
        }
    }

    @Test
    fun test_CorrectRedAsteriskForCardNumber() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedAsteriskSign = "*"
        var expectedAsteriskColor: Int

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val cardNumberLabelText = findPanInputLabel(activity)?.text
            val actualAsteriskSign = cardNumberLabelText.toString().takeLast(1)
            expectedAsteriskColor = ContextCompat.getColor(activity, R.color.colorAsterisk)
            val actualAsteriskColor = cardNumberLabelText?.getAsteriskColor()

            assertEquals(expectedAsteriskSign, actualAsteriskSign)
            assertEquals(expectedAsteriskColor, actualAsteriskColor)
        }
    }

    @Test
    fun test_CorrectRedAsteriskForExpiryDate() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedAsteriskSign = "*"
        var expectedAsteriskColor: Int

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val expiryDateLabelText = findPanInputLabel(activity)?.text
            val actualAsteriskSign = expiryDateLabelText.toString().takeLast(1)
            expectedAsteriskColor = ContextCompat.getColor(activity, R.color.colorAsterisk)
            val actualAsteriskColor = expiryDateLabelText?.getAsteriskColor()

            assertEquals(expectedAsteriskSign, actualAsteriskSign)
            assertEquals(expectedAsteriskColor, actualAsteriskColor)
        }
    }

    @Test
    fun test_CorrectRedAsteriskForCvv() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedAsteriskSign = "*"
        var expectedAsteriskColor: Int

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val cvvLabelText = findPanInputLabel(activity)?.text
            val actualAsteriskSign = cvvLabelText.toString().takeLast(1)
            expectedAsteriskColor = ContextCompat.getColor(activity, R.color.colorAsterisk)
            val actualAsteriskColor = cvvLabelText?.getAsteriskColor()

            assertEquals(expectedAsteriskSign, actualAsteriskSign)
            assertEquals(expectedAsteriskColor, actualAsteriskColor)
        }
    }

    @Test
    fun test_CorrectPayButtonText() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedPayButtonText = "Pay"

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val actualPayButtonText =
                activity.findViewById<Button>(R.id.payButton).text.toString()
            assertEquals(expectedPayButtonText, actualPayButtonText)
        }
    }

    @Test
    fun test_DropInPaymentViewForInvalidForm() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val dropInPaymentViewListener =
            mockk<DropInPaymentView.DropInPaymentViewListener>()
        val expectedIsPayButtonEnabled = false
        every { dropInPaymentViewListener.onInputValid(any(), any()) } returns Unit

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            dropInPaymentView.dropInPaymentViewListener = dropInPaymentViewListener

            dropInPaymentView.onInputValid(PaymentInputType.PAN, "")

            val actualIsPayButtonEnabled = findPayButton(activity)?.isEnabled

            assertEquals(expectedIsPayButtonEnabled, actualIsPayButtonEnabled)
            verify(exactly = 1) { dropInPaymentViewListener.onInputValid(any(), any()) }
        }
    }

    @Test
    fun test_DropInPaymentViewForValidForm() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val dropInPaymentViewListener =
            mockk<DropInPaymentView.DropInPaymentViewListener>()
        val expectedIsPayButtonEnabled = true
        every { dropInPaymentViewListener.onInputValid(any(), any()) } returns Unit
        every { dropInPaymentViewListener.onPayButtonClicked() } returns Unit

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            dropInPaymentView.dropInPaymentViewListener = dropInPaymentViewListener
            findPayButton(activity)?.setOnSingleClickListener(defaultInterval = 0) {
                dropInPaymentViewListener.onPayButtonClicked()
            }

            dropInPaymentView.onInputValid(PaymentInputType.PAN, "")
            dropInPaymentView.onInputValid(PaymentInputType.ExpiryDate, "")
            dropInPaymentView.onInputValid(PaymentInputType.CVV, "")

            val actualIsPayButtonEnabled = findPayButton(activity)?.isEnabled

            findPayButton(activity)?.performClick()

            assertEquals(expectedIsPayButtonEnabled, actualIsPayButtonEnabled)
            verify(exactly = 3) { dropInPaymentViewListener.onInputValid(any(), any()) }
            verify(exactly = 1) { dropInPaymentViewListener.onPayButtonClicked() }
        }
    }

    @Test
    fun test_CorrectCardNumberFlags() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedFlags = InputType.TYPE_CLASS_NUMBER or
                InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)

            val actualFlags = findPanInputEditText(activity)?.inputType

            assertEquals(expectedFlags, actualFlags)
        }
    }

    @Test
    fun test_CorrectExpiryDateFlags() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedFlags = InputType.TYPE_CLASS_NUMBER or
                InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)

            val actualFlags = findExpireInputEditText(activity)?.inputType

            assertEquals(expectedFlags, actualFlags)
        }
    }

    @Test
    fun test_CorrectCvvFlags() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedFlags = InputType.TYPE_CLASS_NUMBER or
                InputType.TYPE_NUMBER_VARIATION_PASSWORD or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val actualFlags = findCvvInputEditText(activity)?.inputType
            assertEquals(expectedFlags, actualFlags)
        }
    }

    @Test
    fun test_CorrectWindowFlags() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedFlags = WindowManager.LayoutParams.FLAG_SECURE

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val actualFlags = activity.window.attributes.flags

            assertTrue(actualFlags and expectedFlags != 0)
        }
    }

    @Test
    fun test_DropInPaymentViewCustomViewUnderPan() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedIsUnderPanVisibility = true
        val expectedIsUnderCvvVisibility = false
        val expectedIsUnderExpiryDateVisibility = false

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            val viewToAdd = View(dropInPaymentView.context)
            dropInPaymentView.setCustomView(viewToAdd, CustomViewPosition.UnderPan)

            val actualIsUnderPanVisibility =
                activity.findViewById<FrameLayout>(R.id.underPanView).visibility == View.VISIBLE
            val actualIsUnderExpiryDateVisibility =
                activity.findViewById<FrameLayout>(R.id.underDateView).visibility == View.VISIBLE
            val actualIsUnderCvvVisibility =
                activity.findViewById<FrameLayout>(R.id.underCvvView).visibility == View.VISIBLE

            assertEquals(expectedIsUnderPanVisibility, actualIsUnderPanVisibility)
            assertEquals(expectedIsUnderExpiryDateVisibility, actualIsUnderExpiryDateVisibility)
            assertEquals(expectedIsUnderCvvVisibility, actualIsUnderCvvVisibility)
        }
    }

    @Test
    fun test_DropInPaymentViewCustomViewUnderExpiryDate() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedIsUnderPanVisibility = false
        val expectedIsUnderCvvVisibility = false
        val expectedIsUnderExpiryDateVisibility = true

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            val viewToAdd = View(dropInPaymentView.context)
            dropInPaymentView.setCustomView(viewToAdd, CustomViewPosition.UnderExpiryDate)

            val actualIsUnderPanVisibility =
                activity.findViewById<FrameLayout>(R.id.underPanView).visibility == View.VISIBLE
            val actualIsUnderExpiryDateVisibility =
                activity.findViewById<FrameLayout>(R.id.underDateView).visibility == View.VISIBLE
            val actualIsUnderCvvVisibility =
                activity.findViewById<FrameLayout>(R.id.underCvvView).visibility == View.VISIBLE

            assertEquals(expectedIsUnderPanVisibility, actualIsUnderPanVisibility)
            assertEquals(expectedIsUnderExpiryDateVisibility, actualIsUnderExpiryDateVisibility)
            assertEquals(expectedIsUnderCvvVisibility, actualIsUnderCvvVisibility)
        }
    }

    @Test
    fun test_DropInPaymentViewCustomViewUnderCvv() {
        // GIVEN
        val scenario = activityScenarioRule.scenario
        val expectedIsUnderPanVisibility = false
        val expectedIsUnderCvvVisibility = true
        val expectedIsUnderExpiryDateVisibility = false

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            val viewToAdd = View(dropInPaymentView.context)
            dropInPaymentView.setCustomView(viewToAdd, CustomViewPosition.UnderCvv)

            val actualIsUnderPanVisibility =
                activity.findViewById<FrameLayout>(R.id.underPanView).visibility == View.VISIBLE
            val actualIsUnderExpiryDateVisibility =
                activity.findViewById<FrameLayout>(R.id.underDateView).visibility == View.VISIBLE
            val actualIsUnderCvvVisibility =
                activity.findViewById<FrameLayout>(R.id.underCvvView).visibility == View.VISIBLE

            assertEquals(expectedIsUnderPanVisibility, actualIsUnderPanVisibility)
            assertEquals(expectedIsUnderExpiryDateVisibility, actualIsUnderExpiryDateVisibility)
            assertEquals(expectedIsUnderCvvVisibility, actualIsUnderCvvVisibility)
        }
    }

    @Test
    fun test_setupForTokenizedPayment_PAN() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            val viewToAdd = View(dropInPaymentView.context)
            dropInPaymentView.setCustomView(viewToAdd, CustomViewPosition.UnderCvv)
            dropInPaymentView.setupForTokenizedPayment(setOf(PaymentInputType.PAN), null)

            val actualPanInputVisibility = activity.findViewById<View>(R.id.panInput).visibility
            val actualExpireInputVisibility =
                activity.findViewById<View>(R.id.expireInput).visibility
            val actualCvvInputVisibility = activity.findViewById<View>(R.id.cvvInput).visibility

            assertEquals(View.VISIBLE, actualPanInputVisibility)
            assertEquals(View.GONE, actualExpireInputVisibility)
            assertEquals(View.GONE, actualCvvInputVisibility)
        }
    }

    @Test
    fun test_setupForTokenizedPayment_ExpiryDate() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            val viewToAdd = View(dropInPaymentView.context)
            dropInPaymentView.setCustomView(viewToAdd, CustomViewPosition.UnderCvv)
            dropInPaymentView.setupForTokenizedPayment(setOf(PaymentInputType.ExpiryDate), null)

            val actualPanInputVisibility = activity.findViewById<View>(R.id.panInput).visibility
            val actualExpireInputVisibility =
                activity.findViewById<View>(R.id.expireInput).visibility
            val actualCvvInputVisibility = activity.findViewById<View>(R.id.cvvInput).visibility

            assertEquals(View.GONE, actualPanInputVisibility)
            assertEquals(View.VISIBLE, actualExpireInputVisibility)
            assertEquals(View.GONE, actualCvvInputVisibility)
        }
    }

    @Test
    fun test_setupForTokenizedPayment_Cvv_Only() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            val viewToAdd = View(dropInPaymentView.context)
            dropInPaymentView.setCustomView(viewToAdd, CustomViewPosition.UnderCvv)

            assertThrows(IllegalArgumentException::class.java) {
                dropInPaymentView.setupForTokenizedPayment(setOf(PaymentInputType.CVV), null)
            }
        }
    }

    @Test
    fun test_setupForTokenizedPayment_Cvv_With_Card() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            val viewToAdd = View(dropInPaymentView.context)
            dropInPaymentView.setCustomView(viewToAdd, CustomViewPosition.UnderCvv)

            dropInPaymentView.setupForTokenizedPayment(setOf(PaymentInputType.CVV), CardType.Visa)

            val actualPanInputVisibility = activity.findViewById<View>(R.id.panInput).visibility
            val actualExpireInputVisibility =
                activity.findViewById<View>(R.id.expireInput).visibility
            val actualCvvInputVisibility = activity.findViewById<View>(R.id.cvvInput).visibility

            assertEquals(View.GONE, actualPanInputVisibility)
            assertEquals(View.GONE, actualExpireInputVisibility)
            assertEquals(View.VISIBLE, actualCvvInputVisibility)
        }
    }

    @Test
    fun test_setupForGooglePayPayment_enabled_mode() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            val viewToAdd = View(dropInPaymentView.context)
            dropInPaymentView.setCustomView(viewToAdd, CustomViewPosition.UnderCvv)

            dropInPaymentView.setupForGooglePayPayment(
                isEnabled = true,
                buttonColor = GooglePayButtonColor.WHITE,
                withText = true,
                withShadow = false
            )

            val actualBtnVisibility = activity.findViewById<View>(R.id.googlePayButton).visibility

            assertEquals(View.VISIBLE, actualBtnVisibility)
        }
    }

    @Test
    fun test_setupForGooglePayPayment_disabled_mode() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            val viewToAdd = View(dropInPaymentView.context)
            dropInPaymentView.setCustomView(viewToAdd, CustomViewPosition.UnderCvv)

            dropInPaymentView.setupForGooglePayPayment(isEnabled = false)

            val actualBtnVisibility = activity.findViewById<View>(R.id.googlePayButton).visibility

            assertEquals(View.GONE, actualBtnVisibility)
        }
    }

    @Test
    fun `test setCustomView`() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            dropInPaymentView.setCustomView(
                R.layout.activity_google_pay_view_test,
                CustomViewPosition.UnderCvv
            )
            val underCvvView = activity.findViewById<FrameLayout>(R.id.underCvvView)
            assertEquals(1, underCvvView.childCount)
            val childView = underCvvView.getChildAt(0)
            assertEquals(R.id.activity_google_pay_view_test, childView.id)
        }
    }

    @Test
    fun `test DropInPaymentViewListener`() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            val dropInPaymentViewListener = mockk<DropInPaymentView.DropInPaymentViewListener>() {
                every { onInputValid(any(), any()) } returns Unit
            }
            dropInPaymentView.dropInPaymentViewListener = dropInPaymentViewListener
            dropInPaymentView.onInputValid(PaymentInputType.PAN, "")
            verify(exactly = 1) { dropInPaymentViewListener.onInputValid(any(), any()) }
            assertEquals(dropInPaymentViewListener, dropInPaymentView.dropInPaymentViewListener)
        }
    }

    @Test
    fun `test DropInGooglePayPaymentViewListener`() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            val dropInGooglePayPaymentViewListener =
                mockk<DropInPaymentView.DropInGooglePayPaymentViewListener>()
            dropInPaymentView.dropInGooglePayPaymentViewListener =
                dropInGooglePayPaymentViewListener
            assertEquals(
                dropInGooglePayPaymentViewListener,
                dropInPaymentView.dropInGooglePayPaymentViewListener
            )
        }
    }

    @Test
    fun `test onCardTypeDetected`() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            val cvvInputView = dropInPaymentView.findViewById<CvvInputView>(R.id.cvvInput)
            dropInPaymentView.onCardTypeDetected(CardType.Amex)
            assertEquals(
                3,
                cvvInputView.getHintText().length
            )
        }
    }

    @Test
    fun `test onInputInvalid`() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            val payButton = dropInPaymentView.findViewById<PayButton>(R.id.payButton)
            dropInPaymentView.onInputInvalid(PaymentInputType.PAN)
            assertTrue(payButton.isEnabled.not())
        }
    }

    @Test
    fun `test setPayButtonVisibility`() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val dropInPaymentView =
                activity.findViewById<DropInPaymentView>(R.id.dropInPaymentView)
            val payButton = dropInPaymentView.findViewById<PayButton>(R.id.payButton)
            dropInPaymentView.setPayButtonVisibility(View.GONE)
            assertTrue(payButton.visibility == View.GONE)
        }
    }

    private fun String.takeLabelWithoutAsterisk(): String =
        this.take(this.length - 2)

    private fun CharSequence.getAsteriskColor(): Int =
        (this as SpannedString).getSpans(
            0, this.length, ForegroundColorSpan::class.java
        )[0].foregroundColor
}