package com.trustpayments.mobile.core.models

import com.trustpayments.mobile.core.models.api.response.TransactionResponse

data class JwtResponse(
    val customerOutput: TransactionResponse?,
    val responses: List<TransactionResponse>
)