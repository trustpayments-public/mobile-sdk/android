package com.trustpayments.mobile.ui.card

import com.trustpayments.mobile.ui.card.model.CardType
import com.trustpayments.mobile.ui.testutils.ValidCardNumbers
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class CardTypeTest {

    private lateinit var cardValidator: CardValidator

    @Before
    fun setUp() {
        cardValidator = CardValidator()
    }

    @Test
    fun test_VisaTypeForNumberStartingFrom4() {
        // GIVEN
        val cardNumber = "4"
        val expectedCardType = CardType.Visa

        // WHEN
        val actualCardType = cardValidator.getCardType(cardNumber)

        // THEN
        assertEquals(expectedCardType, actualCardType)
    }

    @Test
    fun test_UnknownTypeForSingleDigitDifferentThan4() {
        // GIVEN
        val cardNumber = "5"
        val expectedCardType = CardType.Unknown

        // WHEN
        val actualCardType = cardValidator.getCardType(cardNumber)

        // THEN
        assertEquals(expectedCardType, actualCardType)
    }

    @Test
    fun test_UnknownTypeForNonDigit() {
        // GIVEN
        val cardNumber = ""
        val expectedCardType = CardType.Unknown

        // WHEN
        val actualCardType = cardValidator.getCardType(cardNumber)

        // THEN
        assertEquals(expectedCardType, actualCardType)
    }

    @Test
    fun test_CorrectCardTypeForVisa() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForVisa()
        val expectedCardTypes = List(cardNumbers.size) {
            CardType.Visa
        }

        // WHEN
        val actualCardTypes = List(cardNumbers.size) { index ->
            cardValidator.getCardType(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedCardTypes, actualCardTypes)
    }

    @Test
    fun test_CorrectCardTypeForMastercard() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForMastercard()
        val expectedCardTypes = List(cardNumbers.size) {
            CardType.Mastercard
        }

        // WHEN
        val actualCardTypes = List(cardNumbers.size) { index ->
            cardValidator.getCardType(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedCardTypes, actualCardTypes)
    }

    @Test
    fun test_CorrectCardTypeForAmex() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForAmex()
        val expectedCardTypes = List(cardNumbers.size) {
            CardType.Amex
        }

        // WHEN
        val actualCardTypes = List(cardNumbers.size) { index ->
            cardValidator.getCardType(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedCardTypes, actualCardTypes)
    }

    @Test
    fun test_CorrectCardTypeForMaestro() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForMaestro()
        val expectedCardTypes = List(cardNumbers.size) {
            CardType.Maestro
        }

        // WHEN
        val actualCardTypes = List(cardNumbers.size) { index ->
            cardValidator.getCardType(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedCardTypes, actualCardTypes)
    }

    @Test
    fun test_CorrectCardTypeForDiscover() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForDiscover()
        val expectedCardTypes = List(cardNumbers.size) {
            CardType.Discover
        }

        // WHEN
        val actualCardTypes = List(cardNumbers.size) { index ->
            cardValidator.getCardType(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedCardTypes, actualCardTypes)
    }

    @Test
    fun test_CorrectCardTypeForDiners() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForDiners()
        val expectedCardTypes = List(cardNumbers.size) {
            CardType.Diners
        }

        // WHEN
        val actualCardTypes = List(cardNumbers.size) { index ->
            cardValidator.getCardType(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedCardTypes, actualCardTypes)
    }

    @Test
    fun test_CorrectCardTypeForJCB() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForJCB()
        val expectedCardTypes = List(cardNumbers.size) {
            CardType.JCB
        }

        // WHEN
        val actualCardTypes = List(cardNumbers.size) { index ->
            cardValidator.getCardType(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedCardTypes, actualCardTypes)
    }

    @Test
    fun test_CorrectCardTypeForUnknown() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForUnknown()
        val expectedCardTypes = List(cardNumbers.size) {
            CardType.Unknown
        }

        // WHEN
        val actualCardTypes = List(cardNumbers.size) { index ->
            cardValidator.getCardType(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedCardTypes, actualCardTypes)
    }
}