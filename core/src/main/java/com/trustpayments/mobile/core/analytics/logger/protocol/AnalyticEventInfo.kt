package com.trustpayments.mobile.core.analytics.logger.protocol

/**
 * Contains meta data related to the analytic incident.
 * */
class AnalyticEventInfo {

    var logLevel: LogLevel = LogLevel.DEBUG
    var message: String? = null
    var data: MutableMap<String, Any> = mutableMapOf()

    fun setData(key: String, value: Any) {
        data[key] = value
    }

    fun getData(key: String): Any? {
        return data[key]
    }
}