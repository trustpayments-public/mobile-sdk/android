package com.trustpayments.mobile.ui.card

import com.trustpayments.mobile.ui.card.model.CardType
import com.trustpayments.mobile.ui.testutils.ValidCardNumbers
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class CardNumberTest {

    private lateinit var cardValidator: CardValidator

    @Before
    fun setUp() {
        cardValidator = CardValidator()
    }

    @Test
    fun test_ValidCardNumberForVisa() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForVisa()
        val expectedResults = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = List(cardNumbers.size) { index ->
            cardValidator.checkWithLuhn(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedResults, actualResult)
    }

    @Test
    fun test_ValidCardNumberForMastercard() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForMastercard()
        val expectedResults = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = List(cardNumbers.size) { index ->
            cardValidator.checkWithLuhn(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedResults, actualResult)
    }

    @Test
    fun test_ValidCardNumberForAmex() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForAmex()
        val expectedResults = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = List(cardNumbers.size) { index ->
            cardValidator.checkWithLuhn(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedResults, actualResult)
    }

    @Test
    fun test_ValidCardNumberForMaestro() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForMaestro()
        val expectedResults = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = List(cardNumbers.size) { index ->
            cardValidator.checkWithLuhn(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedResults, actualResult)
    }

    @Test
    fun test_ValidCardNumberForDiscover() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForDiscover()
        val expectedResults = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = List(cardNumbers.size) { index ->
            cardValidator.checkWithLuhn(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedResults, actualResult)
    }

    @Test
    fun test_ValidCardNumberForDiners() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForDiners()
        val expectedResults = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = List(cardNumbers.size) { index ->
            cardValidator.checkWithLuhn(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedResults, actualResult)
    }

    @Test
    fun test_ValidCardNumberForJCB() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForJCB()
        val expectedResults = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = List(cardNumbers.size) { index ->
            cardValidator.checkWithLuhn(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedResults, actualResult)
    }

    @Test
    fun test_InvalidCardNumberForUnknown() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForUnknown()
        val expectedResults = List(cardNumbers.size) {
            false
        }

        // WHEN
        val actualResult = List(cardNumbers.size) { index ->
            cardValidator.checkWithLuhn(cardNumbers[index])
        }

        // THEN
        assertEquals(expectedResults, actualResult)
    }

    @Test
    fun test_ValidCardNumberLengthForVisa() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForVisa()
        val expectedResults = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = List(cardNumbers.size) { index ->
            cardValidator.isNumberLengthValid(cardNumbers[index], CardType.Visa)
        }

        // THEN
        assertEquals(expectedResults, actualResult)
    }

    @Test
    fun test_ValidCardNumberLengthForMastercard() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForMastercard()
        val expectedResults = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = List(cardNumbers.size) { index ->
            cardValidator.isNumberLengthValid(cardNumbers[index], CardType.Mastercard)
        }

        // THEN
        assertEquals(expectedResults, actualResult)
    }

    @Test
    fun test_ValidCardNumberLengthForAmex() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForAmex()
        val expectedResults = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = List(cardNumbers.size) { index ->
            cardValidator.isNumberLengthValid(cardNumbers[index], CardType.Amex)
        }

        // THEN
        assertEquals(expectedResults, actualResult)
    }

    @Test
    fun test_ValidCardNumberLengthForMaestro() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForMaestro()
        val expectedResults = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = List(cardNumbers.size) { index ->
            cardValidator.isNumberLengthValid(cardNumbers[index], CardType.Maestro)
        }

        // THEN
        assertEquals(expectedResults, actualResult)
    }

    @Test
    fun test_ValidCardNumberLengthForDiscover() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForDiscover()
        val expectedResults = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = List(cardNumbers.size) { index ->
            cardValidator.isNumberLengthValid(cardNumbers[index], CardType.Discover)
        }

        // THEN
        assertEquals(expectedResults, actualResult)
    }

    @Test
    fun test_ValidCardNumberLengthForDiners() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForDiners()
        val expectedResults = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = List(cardNumbers.size) { index ->
            cardValidator.isNumberLengthValid(cardNumbers[index], CardType.Diners)
        }

        // THEN
        assertEquals(expectedResults, actualResult)
    }

    @Test
    fun test_ValidCardNumberLengthForJCB() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForJCB()
        val expectedResults = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = List(cardNumbers.size) { index ->
            cardValidator.isNumberLengthValid(cardNumbers[index], CardType.JCB)
        }

        // THEN
        assertEquals(expectedResults, actualResult)
    }

    @Test
    fun test_InvalidCardNumberLengthForUnknown() {
        // GIVEN
        val cardNumbers = listOf(
            "1234", "1234567890"
        )
        val expectedResults = List(cardNumbers.size) {
            false
        }

        // WHEN
        val actualResult = List(cardNumbers.size) { index ->
            cardValidator.isNumberLengthValid(cardNumbers[index], CardType.Unknown)
        }

        // THEN
        assertEquals(expectedResults, actualResult)
    }

    @Test
    fun test_CorrectMinCardNumberLengthForVisa() {
        // GIVEN
        val expectedResult = 13

        // WHEN
        val actualResult = CardInfo.getMinNumberLength(CardType.Visa)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CorrectMaxCardNumberLengthForVisa() {
        // GIVEN
        val expectedResult = 19

        // WHEN
        val actualResult = CardInfo.getMaxNumberLength(CardType.Visa)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CorrectMinCardNumberLengthForMastercard() {
        // GIVEN
        val expectedResult = 16

        // WHEN
        val actualResult = CardInfo.getMinNumberLength(CardType.Mastercard)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CorrectMaxCardNumberLengthForMastercard() {
        // GIVEN
        val expectedResult = 16

        // WHEN
        val actualResult = CardInfo.getMaxNumberLength(CardType.Mastercard)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CorrectMinCardNumberLengthForAmex() {
        // GIVEN
        val expectedResult = 15

        // WHEN
        val actualResult = CardInfo.getMinNumberLength(CardType.Amex)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CorrectMaxCardNumberLengthForAmex() {
        // GIVEN
        val expectedResult = 15

        // WHEN
        val actualResult = CardInfo.getMaxNumberLength(CardType.Amex)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CorrectMinCardNumberLengthForMaestro() {
        // GIVEN
        val expectedResult = 12

        // WHEN
        val actualResult = CardInfo.getMinNumberLength(CardType.Maestro)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CorrectMaxCardNumberLengthForMaestro() {
        // GIVEN
        val expectedResult = 19

        // WHEN
        val actualResult = CardInfo.getMaxNumberLength(CardType.Maestro)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CorrectMinCardNumberLengthForDiscover() {
        // GIVEN
        val expectedResult = 14

        // WHEN
        val actualResult = CardInfo.getMinNumberLength(CardType.Discover)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CorrectMaxCardNumberLengthForDiscover() {
        // GIVEN
        val expectedResult = 19

        // WHEN
        val actualResult = CardInfo.getMaxNumberLength(CardType.Discover)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CorrectMinCardNumberLengthForDiners() {
        // GIVEN
        val expectedResult = 14

        // WHEN
        val actualResult = CardInfo.getMinNumberLength(CardType.Diners)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CorrectMaxCardNumberLengthForDiners() {
        // GIVEN
        val expectedResult = 19

        // WHEN
        val actualResult = CardInfo.getMaxNumberLength(CardType.Diners)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CorrectMinCardNumberLengthForJCB() {
        // GIVEN
        val expectedResult = 15

        // WHEN
        val actualResult = CardInfo.getMinNumberLength(CardType.JCB)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CorrectMaxCardNumberLengthForJCB() {
        // GIVEN
        val expectedResult = 19

        // WHEN
        val actualResult = CardInfo.getMaxNumberLength(CardType.JCB)

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CardNumberContainedInIinRangesForVisa() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForVisa()
        val expectedResult = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = mutableListOf<Boolean>()
        for (cardNumber in cardNumbers) {
            for(iinRange in CardInfo.getIinRanges(CardType.Visa)) {
                val iin = cardNumber.take(6).toInt()
                actualResult.add(iin in iinRange)
            }
        }

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CardNumberContainedInIinRangesForMastercard() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForMastercard()
        val expectedResult = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = mutableListOf<Boolean>()
        for (cardNumber in cardNumbers) {
            for(iinRange in CardInfo.getIinRanges(CardType.Mastercard)) {
                if (cardNumber.take(6).toInt() in iinRange) {
                    actualResult.add(true)
                }
            }
        }

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CardNumberContainedInIinRangesForAmex() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForAmex()
        val expectedResult = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = mutableListOf<Boolean>()
        for (cardNumber in cardNumbers) {
            for(iinRange in CardInfo.getIinRanges(CardType.Amex)) {
                if (cardNumber.take(6).toInt() in iinRange) {
                    actualResult.add(true)
                }
            }
        }

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CardNumberContainedInIinRangesForMaestro() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForMaestro()
        val expectedResult = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = mutableListOf<Boolean>()
        for (cardNumber in cardNumbers) {
            for(iinRange in CardInfo.getIinRanges(CardType.Maestro)) {
                if (cardNumber.take(6).toInt() in iinRange) {
                    actualResult.add(true)
                }
            }
        }

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CardNumberContainedInIinRangesForDiscover() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForDiscover()
        val expectedResult = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = mutableListOf<Boolean>()
        for (cardNumber in cardNumbers) {
            for(iinRange in CardInfo.getIinRanges(CardType.Discover)) {
                if (cardNumber.take(6).toInt() in iinRange) {
                    actualResult.add(true)
                }
            }
        }

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CardNumberContainedInIinRangesForDiners() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForDiners()
        val expectedResult = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = mutableListOf<Boolean>()
        for (cardNumber in cardNumbers) {
            for(iinRange in CardInfo.getIinRanges(CardType.Diners)) {
                if (cardNumber.take(6).toInt() in iinRange) {
                    actualResult.add(true)
                }
            }
        }

        // THEN
        assertEquals(expectedResult, actualResult)
    }

    @Test
    fun test_CardNumberContainedInIinRangesForJCB() {
        // GIVEN
        val cardNumbers = ValidCardNumbers.getForJCB()
        val expectedResult = List(cardNumbers.size) {
            true
        }

        // WHEN
        val actualResult = mutableListOf<Boolean>()
        for (cardNumber in cardNumbers) {
            for(iinRange in CardInfo.getIinRanges(CardType.JCB)) {
                if (cardNumber.take(6).toInt() in iinRange) {
                    actualResult.add(true)
                }
            }
        }

        // THEN
        assertEquals(expectedResult, actualResult)
    }
}