package com.trustpayments.mobile.core.testcases.googlepay

import android.content.Context
import android.os.Build
import android.util.DisplayMetrics
import android.view.View
import androidx.test.core.app.ApplicationProvider
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiSelector
import com.google.android.gms.wallet.WalletConstants
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.googlepay.TPGooglePayManager
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.models.api.response.ResponseErrorCode
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.test.R
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.testutils.GooglePayInstances
import com.trustpayments.mobile.core.testutils.GooglePayTestPage
import com.trustpayments.mobile.core.testutils.RiskDecResponse
import com.trustpayments.mobile.core.testutils.TrustPaymentsJsonApiService
import com.trustpayments.mobile.core.testutils.areAllResponsesSuccessful
import com.trustpayments.mobile.core.testutils.getResponseCount
import com.trustpayments.mobile.core.testutils.getRiskDecTransactionResponse
import com.trustpayments.mobile.core.testutils.getRiskDecTransactionResponseWithThreeDQuery
import com.trustpayments.mobile.core.testutils.getTransactionResponseWithTypeOrNull
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.util.ResponseParser
import com.trustpayments.mobile.core.utils.GooglePayTestActivity
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2
import io.mockk.every
import io.mockk.mockkConstructor
import io.mockk.unmockkAll
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertNotNull
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.junit.After
import org.junit.Assert
import org.junit.Assume.assumeTrue
import org.junit.Before
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test

@Ignore
class GooglePayTestCasesIgnored : BaseTestCase() {

    private lateinit var activity: GooglePayTestActivity

    private lateinit var googlePayTestPage : GooglePayTestPage

    private lateinit var displayMetrics : DisplayMetrics

    private lateinit var tpGooglePayManager : TPGooglePayManager

    private var baseAmount = 1050

    private val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    private lateinit var paymentTransactionManager : PaymentTransactionManager

    @get:Rule
    val initialActivityTestRule = ActivityTestRule(GooglePayTestActivity::class.java)

    @After
    fun unmock(){
        unmockkAll()
    }

    @Before
    override fun setUp() {
        super.setUp()
        activity = initialActivityTestRule.activity
        googlePayTestPage = GooglePayTestPage(uiDevice, activity)
        displayMetrics = DisplayMetrics()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val display = activity.display
            display?.getRealMetrics(displayMetrics)
        } else {
            @Suppress("DEPRECATION")
            val display = activity.windowManager.defaultDisplay
            @Suppress("DEPRECATION")
            display.getMetrics(displayMetrics)
        }

        activity.run {
            button = findViewById(R.id.googlePayButton)
            textView = findViewById(R.id.text)
            button?.setOnClickListener {
                tpGooglePayManager.requestPayment(baseAmount.toLong())
            }
            resultCallback = { requestCode, resultCode, data ->
                val string = tpGooglePayManager.onActivityResult(requestCode, resultCode, data)
                textView?.text = string
                textView?.visibility = View.VISIBLE
            }
        }

        val app = ApplicationProvider.getApplicationContext<Context>().applicationContext
        paymentTransactionManager = PaymentTransactionManager(
                app,
                getGatewayType(),
                false,
                BuildConfig.MERCHANT_USERNAME,
                isLocationDataConsentGiven = false

        )
    }

    @Test
    fun test_googlePayNegativePossiblyShowGooglePayButton() {
        //GIVEN
        val expectedText = "NOT OK"
        activity.runOnUiThread {
            activity.button?.visibility = View.GONE
        }
        tpGooglePayManager = TPGooglePayManager.Builder(
                activity,
                WalletConstants.ENVIRONMENT_TEST,
                "GB",
                "GBP",
                BuildConfig.SITE_REFERENCE
        )
                .setApiVersion(-1)
                .setApiVersionMinor(-1)
                .build()

        //WHEN
        tpGooglePayManager.possiblyShowGooglePayButton(object : TPGooglePayManager.ShowGooglePayButtonCallback{
            override fun canShowButton(boolean: Boolean) {
                activity.runOnUiThread {
                    if(boolean)
                        activity.button?.visibility = View.VISIBLE
                    else {
                        activity.textView?.text = expectedText
                        activity.textView?.visibility = View.VISIBLE
                    }
                }
            }
        })

        googlePayTestPage.waitResult()

        //THEN
        assertEquals(activity.textView?.visibility, View.VISIBLE)
        assertEquals(activity.textView?.text, expectedText)
    }

    @Test
    fun test_googlePayAltNegativePossiblyShowGooglePayButton() {
        //GIVEN
        assumeTrue(
                "Can only run on API Level 28 or newer because of the mockk static",
                Build.VERSION.SDK_INT >= 28
        )
        tpGooglePayManager = TPGooglePayManager.Builder(
                activity,
                WalletConstants.ENVIRONMENT_TEST,
                "GB",
                "GBP",
                BuildConfig.SITE_REFERENCE
        ).build()
        mockkConstructor(JSONArray::class)
        every { anyConstructed<JSONArray>().put(any<Any>()) } throws JSONException("")
        val expectedText = "NOT OK"
        activity.runOnUiThread {
            activity.button?.visibility = View.GONE
        }

        //WHEN
        tpGooglePayManager.possiblyShowGooglePayButton(object : TPGooglePayManager.ShowGooglePayButtonCallback{
            override fun canShowButton(boolean: Boolean) {
                activity.runOnUiThread {
                    if(boolean)
                        activity.button?.visibility = View.VISIBLE
                    else {
                        activity.textView?.text = expectedText
                        activity.textView?.visibility = View.VISIBLE
                    }
                }
            }
        })

        googlePayTestPage.waitResult()

        //THEN
        assertEquals(activity.textView?.visibility, View.VISIBLE)
        assertEquals(activity.textView?.text, expectedText)
    }

    @Test
    fun test_googlePayPositivePossiblyShowGooglePayButton() {
        //GIVEN
        activity.runOnUiThread {
            activity.button?.visibility = View.GONE
        }
        tpGooglePayManager = TPGooglePayManager.Builder(
                activity,
                WalletConstants.ENVIRONMENT_TEST,
                "GB",
                "GBP",
                BuildConfig.SITE_REFERENCE
        ).build()

        //WHEN
        tpGooglePayManager.possiblyShowGooglePayButton(object : TPGooglePayManager.ShowGooglePayButtonCallback{
            override fun canShowButton(boolean: Boolean) {
                activity.runOnUiThread {
                    if(boolean) {
                        activity.button?.visibility = View.VISIBLE
                    }
                }
            }
        })

        googlePayTestPage.waitButton()

        //THEN
        assertEquals(activity.button?.visibility, View.VISIBLE)
    }

    @Test
    fun test_googlePayPositiveRequestPayment() {
        //GIVEN
        tpGooglePayManager = TPGooglePayManager.Builder(
                activity,
                WalletConstants.ENVIRONMENT_TEST,
                "GB",
                "GBP",
                BuildConfig.SITE_REFERENCE
        ).build()

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        uiDevice.waitForIdle()

        //THEN
        assertTrue(uiDevice.hasObject(By.textContains(centsToString(baseAmount.toLong()))))
        googlePayTestPage.goBack()
    }

    @Test
    fun test_googlePayNegativeRequestPayment() {
        //GIVEN
        tpGooglePayManager = TPGooglePayManager.Builder(
                activity,
                WalletConstants.ENVIRONMENT_TEST,
                "GB",
                "GBP",
                BuildConfig.SITE_REFERENCE
        ).setApiVersion(-1)
                .build()

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        uiDevice.waitForIdle()

        //THEN
        assertEquals(uiDevice.hasObject(By.textContains(centsToString(baseAmount.toLong()))), false)
    }

    @Test
    fun test_googlePayAltNegativeRequestPayment() {
        //GIVEN
        assumeTrue(
                "Can only run on API Level 28 or newer because of the mockk static",
                Build.VERSION.SDK_INT >= 28
        )
        tpGooglePayManager = TPGooglePayManager.Builder(
                activity,
                WalletConstants.ENVIRONMENT_TEST,
                "GB",
                "GBP",
                BuildConfig.SITE_REFERENCE
        ).build()
        mockkConstructor(JSONArray::class)
        every { anyConstructed<JSONArray>().put(any<Any>()) } throws JSONException("")

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        uiDevice.waitForIdle()

        //THEN
        assertEquals(uiDevice.hasObject(By.textContains(centsToString(baseAmount.toLong()))), false)
    }

    @Test
    fun test_googlePayBillingMin() {
        //GIVEN
        tpGooglePayManager = GooglePayInstances.billingAddressMin(activity)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()

        //THEN
        val text = activity.textView?.text?.toString()
        assertNotNull(text)
        val billingAddress = JSONObject(text!!).getJSONObject("paymentMethodData")
                .getJSONObject("info")
                .getJSONObject("billingAddress")
        assertTrue(billingAddress.has("countryCode"))
        assertTrue(billingAddress.has("postalCode"))
        assertTrue(billingAddress.has("name"))
    }

    @Test
    fun test_googlePayBillingFull() {
        //GIVEN
        tpGooglePayManager = GooglePayInstances.billingAddressFull(activity)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()

        //THEN
        val text = activity.textView?.text?.toString()
        assertNotNull(text)
        val billingAddress = JSONObject(text!!).getJSONObject("paymentMethodData")
                .getJSONObject("info")
                .getJSONObject("billingAddress")
        assertTrue(billingAddress.has("countryCode"))
        assertTrue(billingAddress.has("postalCode"))
        assertTrue(billingAddress.has("name"))
        assertTrue(billingAddress.has("locality"))
        assertTrue(billingAddress.has("address1"))
    }

    @Test
    fun test_googlePayBillingPhone() {
        //GIVEN
        tpGooglePayManager = GooglePayInstances.billingAddressPhone(activity)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()

        //THEN
        val text = activity.textView?.text?.toString()
        assertNotNull(text)
        val billingAddress = JSONObject(text!!).getJSONObject("paymentMethodData")
                .getJSONObject("info")
                .getJSONObject("billingAddress")
        assertTrue(billingAddress.has("phoneNumber"))
    }

    @Test
    fun test_googlePayCustomRequestCode() {
        //GIVEN
        val customRequestCode = 111

        activity.resultCallback = { requestCode, _, _ ->
            activity.textView?.text = requestCode.toString()
            activity.textView?.visibility = View.VISIBLE
        }

        tpGooglePayManager = GooglePayInstances.customRequestCode(activity, customRequestCode)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()

        //THEN
        val text = activity.textView?.text?.toString()
        assertTrue(text == customRequestCode.toString())
    }

    @Test
    fun test_googlePayShippingAddressGB() {
        //GIVEN
        tpGooglePayManager = GooglePayInstances.shippingAddressGB(activity)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()

        //THEN
        val text = activity.textView?.text?.toString()
        assertNotNull(text)
        val shippingAddress = JSONObject(text!!).getJSONObject("shippingAddress")
        assertTrue(shippingAddress.has("countryCode"))
        assertTrue(shippingAddress.getString("countryCode") == "GB")
    }

    @Test
    fun test_googlePayShippingAddressPhone() {
        //GIVEN
        tpGooglePayManager = GooglePayInstances.shippingAddressPhone(activity)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()

        //THEN
        val text = activity.textView?.text?.toString()
        assertNotNull(text)
        val jsonObject = JSONObject(text!!)
        assertTrue(jsonObject.has("shippingAddress"))
        val shippingAddress = jsonObject.getJSONObject("shippingAddress")
        assertTrue(shippingAddress.has("phoneNumber"))
    }

    @Test
    fun test_googlePaySupportedNetworkVisaOnly() {
        //GIVEN
        tpGooglePayManager = GooglePayInstances.supportedNetworkVisaOnly(activity)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapCardOptions()

        //THEN
        assertFalse(uiDevice.findObject(UiSelector().textContains("Mastercard")).isEnabled)
        assertFalse(uiDevice.findObject(UiSelector().textContains("Discover")).isEnabled)
        assertFalse(uiDevice.findObject(UiSelector().textContains("Amex")).isEnabled)
        googlePayTestPage.goBack()
    }

    @Test
    fun test_googlePayPositiveAuth() = runBlocking {
        //GIVEN
        baseAmount = 999
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()

        assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(walletToken = text!!, requestTypes = listOf(RequestType.Auth))

        val session = paymentTransactionManager.createSession({token})
        val result = paymentTransactionManager.executeSession(session)
        val parsedResponse = ResponseParser.parse(result.responseJwtList.first())

        //THEN
        assertNotNull(parsedResponse)
        assertEquals(1, parsedResponse!!.responses.size)
        assertEquals("Ok", parsedResponse.responses[0].errorMessage)
        assertEquals(
                ResponseErrorCode.Ok,
                parsedResponse.responses[0].errorCode
        )
        assertNotNull(parsedResponse.responses[0].requestTypeDescription)
        assertEquals(
                "AUTH",
                parsedResponse.responses[0].requestTypeDescription!!.serializedName
        )
    }

    @Test
    fun test_googlePayFullRefund() = runBlocking {
        //GIVEN
        val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(walletToken = text!!, requestTypes = listOf(RequestType.Auth))

        val session = paymentTransactionManager.createSession({token})
        val result = paymentTransactionManager.executeSession(session)
        val parsedResponse = ResponseParser.parse(result.responseJwtList.first())

        assertNotNull(parsedResponse)
        assertEquals(1, parsedResponse!!.responses.size)

        val transactionReference = parsedResponse.responses.first().transactionReference
        val apiService = TrustPaymentsJsonApiService()
        val jsonWebResult = apiService.performRefund(null, arrayOf("REFUND"), "GBP", "1050", transactionReference)

        assertNotNull(jsonWebResult)

        val successJsonWebResult = jsonWebResult as TrustPaymentsJsonApiService.Result.Success

        //THEN
        assertEquals("Ok", successJsonWebResult.transactionResponses[0].errorMessage)
        assertEquals(
                ResponseErrorCode.Ok,
                successJsonWebResult.transactionResponses[0].errorCode
        )
        assertEquals(
                "REFUND",
                successJsonWebResult.transactionResponses[0].requestTypeDescription
        )
    }

    @Test
    fun test_googlePayPartialRefund() = runBlocking {
        //GIVEN
        val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()

        assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(walletToken = text!!, requestTypes = listOf(RequestType.Auth))

        val session = paymentTransactionManager.createSession({token})
        val result = paymentTransactionManager.executeSession(session)
        val parsedResponse = ResponseParser.parse(result.responseJwtList.first())

        assertNotNull(parsedResponse)
        assertEquals(1, parsedResponse!!.responses.size)

        val transactionReference = parsedResponse.responses.first().transactionReference
        val apiService = TrustPaymentsJsonApiService()
        val jsonWebResult = apiService.performRefund(null, arrayOf("REFUND"), "GBP", "550", transactionReference)

        assertNotNull(jsonWebResult)

        val successJsonWebResult = jsonWebResult as TrustPaymentsJsonApiService.Result.Success

        //THEN
        assertEquals("Ok", successJsonWebResult.transactionResponses[0].errorMessage)
        assertEquals(
                ResponseErrorCode.Ok,
                successJsonWebResult.transactionResponses[0].errorCode
        )
        assertEquals(
                "REFUND",
                successJsonWebResult.transactionResponses[0].requestTypeDescription
        )
    }

    @Test
    fun test_googlePayPositiveRiskDecAccept() = runBlocking {
        //GIVEN
        baseAmount = 2011

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "ACCEPT",
                fraudControlResponseCodes = listOf("0100", "0150")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        Assert.assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(RequestType.RiskDec)
        )

        val session = paymentTransactionManager.createSession({token})
        val result = paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = result.getRiskDecTransactionResponse()

        //THEN
        assertTrue(result.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(1, result.getResponseCount())
        assertNotNull(result.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
    }

    @Test
    fun test_googlePayPositiveRiskDecChallenge() = runBlocking {
        //GIVEN
        baseAmount = 2033

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "CHALLENGE",
                fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        Assert.assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(RequestType.RiskDec),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})
        val result = paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = result.getRiskDecTransactionResponse()

        //THEN
        assertTrue(result.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(1, result.getResponseCount())
        assertNotNull(result.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
    }

    @Test
    fun test_googlePayPositiveRiskDecDeny() = runBlocking {
        //GIVEN
        baseAmount = 3044

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "DENY",
                fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(RequestType.RiskDec),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})
        val result = paymentTransactionManager.executeSession(session) { activity }
        val actualRiskDecTransactionResponse = result.getRiskDecTransactionResponse()

        //THEN
        assertTrue(result.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(1, result.getResponseCount())
        assertNotNull(result.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
    }

    @Test
    fun test_googlePayPositiveThreeDQueryAuthRiskDecAccept() = runBlocking {
        //GIVEN
        baseAmount = 2011

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "ACCEPT",
                fraudControlResponseCodes = listOf("0100", "0150")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        Assert.assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.ThreeDQuery,
                        RequestType.Auth,
                        RequestType.RiskDec
                )
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponseWithThreeDQuery()

        //THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(3, sessionResult.getResponseCount())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 1, RequestType.RiskDec))
    }

    @Test
    fun test_googlePayPositiveThreeDQueryAuthRiskDecChallenge() = runBlocking {
        //GIVEN
        baseAmount = 2033

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "CHALLENGE",
                fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        Assert.assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.ThreeDQuery,
                        RequestType.Auth,
                        RequestType.RiskDec
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponseWithThreeDQuery()

        //THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(3, sessionResult.getResponseCount())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 1, RequestType.RiskDec))
    }

    @Test
    fun test_googlePayPositiveThreeDQueryAuthRiskDecDeny() = runBlocking {
        //GIVEN
        baseAmount = 3044

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "DENY",
                fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.ThreeDQuery,
                        RequestType.Auth,
                        RequestType.RiskDec
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponseWithThreeDQuery()

        //THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(3, sessionResult.getResponseCount())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 1, RequestType.RiskDec))
    }

    @Test
    fun test_googlePayPositiveThreeDQueryAuthRiskDecNoScore() = runBlocking {
        //GIVEN
        baseAmount = 70000

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "NOSCORE",
                fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.ThreeDQuery,
                        RequestType.Auth,
                        RequestType.RiskDec
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponseWithThreeDQuery()

        //THEN
        assertFalse(sessionResult.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(3, sessionResult.getResponseCount())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 1, RequestType.RiskDec))
    }

    @Test
    fun test_googlePayPositiveRiskDecThreeDQueryAuthAccept() = runBlocking {
        //GIVEN
        baseAmount = 2011

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "ACCEPT",
                fraudControlResponseCodes = listOf("0100", "0150")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        Assert.assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.RiskDec,
                        RequestType.ThreeDQuery,
                        RequestType.Auth
                )
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponseWithThreeDQuery()

        //THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(3, sessionResult.getResponseCount())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 1, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
    }

    @Test
    fun test_googlePayPositiveRiskDecThreeDQueryAuthChallenge() = runBlocking {
        //GIVEN
        baseAmount = 2033

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "CHALLENGE",
                fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        Assert.assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.RiskDec,
                        RequestType.ThreeDQuery,
                        RequestType.Auth
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponseWithThreeDQuery()

        //THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(3, sessionResult.getResponseCount())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 1, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
    }

    @Test
    fun test_googlePayPositiveRiskDecThreeDQueryAuthDeny() = runBlocking {
        //GIVEN
        baseAmount = 3044

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "DENY",
                fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.RiskDec,
                        RequestType.ThreeDQuery,
                        RequestType.Auth
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponseWithThreeDQuery()

        //THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(3, sessionResult.getResponseCount())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 1, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
    }

    @Test
    fun test_positiveRiskDecThreeDQueryAuthSubscriptionAccept() = runBlocking {
        //GIVEN
        baseAmount = 2011

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "ACCEPT",
                fraudControlResponseCodes = listOf("0100", "0150")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        Assert.assertNotNull(text)

        val token = jwtBuilder.getForGooglePaySubscription(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.RiskDec,
                        RequestType.ThreeDQuery,
                        RequestType.Auth,
                        RequestType.Subscription
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponseWithThreeDQuery()

        //THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(4, sessionResult.getResponseCount())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 1, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 1, RequestType.Subscription))
    }

    @Test
    fun test_positiveRiskDecThreeDQueryAuthSubscriptionChallenge() = runBlocking {
        //GIVEN
        baseAmount = 2033

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "CHALLENGE",
                fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        Assert.assertNotNull(text)

        val token = jwtBuilder.getForGooglePaySubscription(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.RiskDec,
                        RequestType.ThreeDQuery,
                        RequestType.Auth,
                        RequestType.Subscription
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponseWithThreeDQuery()

        //THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(4, sessionResult.getResponseCount())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 1, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 1, RequestType.Subscription))
    }

    @Test
    fun test_positiveRiskDecThreeDQueryAuthSubscriptionDeny() = runBlocking {
        //GIVEN
        baseAmount = 3044

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "DENY",
                fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePaySubscription(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.RiskDec,
                        RequestType.ThreeDQuery,
                        RequestType.Auth,
                        RequestType.Subscription
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponseWithThreeDQuery()

        //THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(4, sessionResult.getResponseCount())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 1, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 1, RequestType.Subscription))
    }

    @Test
    fun test_positiveRiskDecAccountCheckThreeDQueryAuthAccept() = runBlocking {
        //GIVEN
        baseAmount = 2011

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "ACCEPT",
                fraudControlResponseCodes = listOf("0100", "0150")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        Assert.assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.RiskDec,
                        RequestType.AccountCheck,
                        RequestType.ThreeDQuery,
                        RequestType.Auth
                )
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponseWithThreeDQuery()

        //THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(4, sessionResult.getResponseCount())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 1, RequestType.AccountCheck))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 2, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
    }

    @Test
    fun test_positiveRiskDecAccountCheckThreeDQueryAuthChallenge() = runBlocking {
        //GIVEN
        baseAmount = 2033

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "CHALLENGE",
                fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        Assert.assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.RiskDec,
                        RequestType.AccountCheck,
                        RequestType.ThreeDQuery,
                        RequestType.Auth
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponseWithThreeDQuery()

        //THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(4, sessionResult.getResponseCount())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 1, RequestType.AccountCheck))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 2, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
    }

    @Test
    fun test_positiveRiskDecAccountCheckThreeDQueryAuthDeny() = runBlocking {
        //GIVEN
        baseAmount = 3044

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "DENY",
                fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.RiskDec,
                        RequestType.AccountCheck,
                        RequestType.ThreeDQuery,
                        RequestType.Auth
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponseWithThreeDQuery()

        //THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(4, sessionResult.getResponseCount())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 1, RequestType.AccountCheck))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 2, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
    }

    @Test
    fun test_positiveRiskDecAccountCheckThreeDQueryAuthSubscriptionAccept() = runBlocking {
        //GIVEN
        baseAmount = 2011

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "ACCEPT",
                fraudControlResponseCodes = listOf("0100", "0150")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        Assert.assertNotNull(text)

        val token = jwtBuilder.getForGooglePaySubscription(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.RiskDec,
                        RequestType.AccountCheck,
                        RequestType.ThreeDQuery,
                        RequestType.Auth,
                        RequestType.Subscription
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponseWithThreeDQuery()

        //THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(5, sessionResult.getResponseCount())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 1, RequestType.AccountCheck))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 2, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 1, RequestType.Subscription))
    }

    @Test
    fun test_positiveRiskDecAccountCheckThreeDQueryAuthSubscriptionChallenge() = runBlocking {
        //GIVEN
        baseAmount = 2033

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "CHALLENGE",
                fraudControlResponseCodes = listOf("0300", "0330", "0500")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        Assert.assertNotNull(text)

        val token = jwtBuilder.getForGooglePaySubscription(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.RiskDec,
                        RequestType.AccountCheck,
                        RequestType.ThreeDQuery,
                        RequestType.Auth,
                        RequestType.Subscription
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponseWithThreeDQuery()

        //THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(5, sessionResult.getResponseCount())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 1, RequestType.AccountCheck))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 2, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 1, RequestType.Subscription))
    }

    @Test
    fun test_positiveRiskDecAccountCheckThreeDQueryAuthSubscriptionDeny() = runBlocking {
        //GIVEN
        baseAmount = 3044

        val expectedFraudControlValues = RiskDecResponse(
                fraudControlShieldStatusCode = "DENY",
                fraudControlResponseCodes = listOf("0250", "0400", "0600", "0700", "0800", "1300")
        )

        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePaySubscription(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.RiskDec,
                        RequestType.AccountCheck,
                        RequestType.ThreeDQuery,
                        RequestType.Auth,
                        RequestType.Subscription
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        val actualRiskDecTransactionResponse = sessionResult.getRiskDecTransactionResponseWithThreeDQuery()

        //THEN
        assertTrue(sessionResult.areAllResponsesSuccessful())

        assertEquals(
                expectedFraudControlValues.fraudControlShieldStatusCode,
                actualRiskDecTransactionResponse.fraudControlShieldStatusCode
        )
        assertTrue(
                expectedFraudControlValues.fraudControlResponseCodes.contains(
                        actualRiskDecTransactionResponse.fraudControlResponseCode
                )
        )
        assertEquals(5, sessionResult.getResponseCount())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 1, RequestType.AccountCheck))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 2, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 1, RequestType.Subscription))
    }

    @Test
    fun test_googlePayPositiveSubscriptionAccountCheckAndSubscription() = runBlocking {
        //GIVEN
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePaySubscription(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.AccountCheck,
                        RequestType.Subscription
                )
        )

        val session = paymentTransactionManager.createSession({token})
        val result = paymentTransactionManager.executeSession(session)
        val parsedResponse = ResponseParser.parse(result.responseJwtList.first())

        //THEN
        assertNotNull(parsedResponse)
        assertEquals(2, parsedResponse!!.responses.size)

        assertEquals("Ok", parsedResponse.responses[0].errorMessage)
        assertEquals(
                ResponseErrorCode.Ok,
                parsedResponse.responses[0].errorCode
        )
        assertNotNull(parsedResponse.responses[0].requestTypeDescription)
        assertEquals(
                "ACCOUNTCHECK",
                parsedResponse.responses[0].requestTypeDescription!!.serializedName
        )

        assertEquals("Ok", parsedResponse.responses[1].errorMessage)
        assertEquals(
                ResponseErrorCode.Ok,
                parsedResponse.responses[1].errorCode
        )
        assertNotNull(parsedResponse.responses[1].requestTypeDescription)
        assertEquals(
                "SUBSCRIPTION",
                parsedResponse.responses[1].requestTypeDescription!!.serializedName
        )
    }

    @Test
    fun test_googlePayNegativeSubscriptionAccountCheckAndSubscription() = runBlocking {
        //GIVEN
        baseAmount = 149712
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePaySubscription(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.AccountCheck,
                        RequestType.Subscription
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})
        val result = paymentTransactionManager.executeSession(session)
        val parsedResponse = ResponseParser.parse(result.responseJwtList.first())

        //THEN
        assertNotNull(parsedResponse)
        assertEquals(2, parsedResponse!!.responses.size)

        assertEquals("Ok", parsedResponse.responses[0].errorMessage)
        assertEquals(
                ResponseErrorCode.Ok,
                parsedResponse.responses[0].errorCode
        )
        assertNotNull(parsedResponse.responses[0].requestTypeDescription)
        assertEquals(
                "ACCOUNTCHECK",
                parsedResponse.responses[0].requestTypeDescription!!.serializedName
        )

        assertEquals("Invalid process", parsedResponse.responses[1].errorMessage)
        assertEquals(
                ResponseErrorCode.InvalidProcess,
                parsedResponse.responses[1].errorCode
        )
        assertNotNull(parsedResponse.responses[1].requestTypeDescription)
        assertEquals(
                "SUBSCRIPTION",
                parsedResponse.responses[1].requestTypeDescription!!.serializedName
        )
    }

    @Test
    fun test_googlePayPositiveSubscriptionAuthAndSubscription() = runBlocking {
        //GIVEN
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePaySubscription(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.Auth,
                        RequestType.Subscription
                )
        )

        val session = paymentTransactionManager.createSession({token})
        val result = paymentTransactionManager.executeSession(session)
        val parsedResponse = ResponseParser.parse(result.responseJwtList.first())

        //THEN
        assertNotNull(parsedResponse)
        assertEquals(2, parsedResponse!!.responses.size)

        assertEquals("Ok", parsedResponse.responses[0].errorMessage)
        assertEquals(
                ResponseErrorCode.Ok,
                parsedResponse.responses[0].errorCode
        )
        assertNotNull(parsedResponse.responses[0].requestTypeDescription)
        assertEquals(
                "AUTH",
                parsedResponse.responses[0].requestTypeDescription!!.serializedName
        )

        assertEquals("Ok", parsedResponse.responses[1].errorMessage)
        assertEquals(
                ResponseErrorCode.Ok,
                parsedResponse.responses[1].errorCode
        )
        assertNotNull(parsedResponse.responses[1].requestTypeDescription)
        assertEquals(
                "SUBSCRIPTION",
                parsedResponse.responses[1].requestTypeDescription!!.serializedName
        )
    }

    @Test
    fun test_googlePayNegativeSubscriptionAuthAndSubscription() = runBlocking {
        //GIVEN
        baseAmount = 149712
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePaySubscription(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.Auth,
                        RequestType.Subscription
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})
        val result = paymentTransactionManager.executeSession(session)
        val parsedResponse = ResponseParser.parse(result.responseJwtList.first())

        //THEN
        assertNotNull(parsedResponse)
        assertEquals(2, parsedResponse!!.responses.size)

        assertEquals("Ok", parsedResponse.responses[0].errorMessage)
        assertEquals(
                ResponseErrorCode.Ok,
                parsedResponse.responses[0].errorCode
        )
        assertNotNull(parsedResponse.responses[0].requestTypeDescription)
        assertEquals(
                "AUTH",
                parsedResponse.responses[0].requestTypeDescription!!.serializedName
        )

        assertEquals("Invalid process", parsedResponse.responses[1].errorMessage)
        assertEquals(
                ResponseErrorCode.InvalidProcess,
                parsedResponse.responses[1].errorCode
        )
        assertNotNull(parsedResponse.responses[1].requestTypeDescription)
        assertEquals(
                "SUBSCRIPTION",
                parsedResponse.responses[1].requestTypeDescription!!.serializedName
        )
    }

    // .accountCheck, .threeDQuery, .auth]
    @Test
    fun test_googlePayPositiveAccountCheckThreeDQueryAuth() = runBlocking {
        //GIVEN
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.AccountCheck,
                        RequestType.ThreeDQuery,
                        RequestType.Auth
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        //THEN
        assertEquals(3, sessionResult.getResponseCount())
        assertTrue(sessionResult.areAllResponsesSuccessful())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.AccountCheck))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 1, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
    }

    // .riskDec, .accountCheck, .threeDQuery, .auth]
    @Test
    fun test_googlePayPositiveRiskDecAccountCheckThreeDQueryAuth() = runBlocking {
        //GIVEN
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.RiskDec,
                        RequestType.AccountCheck,
                        RequestType.ThreeDQuery,
                        RequestType.Auth
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        //THEN
        assertEquals(4, sessionResult.getResponseCount())
        assertTrue(sessionResult.areAllResponsesSuccessful())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 1, RequestType.AccountCheck))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 2, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
    }

    // .riskDec, .accountCheck, .threeDQuery, .auth, .subscription]
    @Test
    fun test_googlePayPositiveRiskDecAccountCheckThreeDQueryAuthSubscription() = runBlocking {
        //GIVEN
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePaySubscription(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.RiskDec,
                        RequestType.AccountCheck,
                        RequestType.ThreeDQuery,
                        RequestType.Auth,
                        RequestType.Subscription
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        //THEN
        assertEquals(5, sessionResult.getResponseCount())
        assertTrue(sessionResult.areAllResponsesSuccessful())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 1, RequestType.AccountCheck))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 2, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 1, RequestType.Subscription))
    }

    // .riskDec, .threeDQuery, .auth]
    @Test
    fun test_googlePayPositiveRiskDecThreeDQueryAuth() = runBlocking {
        //GIVEN
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.RiskDec,
                        RequestType.ThreeDQuery,
                        RequestType.Auth
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        //THEN
        assertEquals(3, sessionResult.getResponseCount())
        assertTrue(sessionResult.areAllResponsesSuccessful())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 1, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
    }

    // .riskDec, .threeDQuery, .auth, .subscription]
    @Test
    fun test_googlePayPositiveRiskDecThreeDQueryAuthSubscription() = runBlocking {
        //GIVEN
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePaySubscription(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.RiskDec,
                        RequestType.ThreeDQuery,
                        RequestType.Auth,
                        RequestType.Subscription
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        //THEN
        assertEquals(4, sessionResult.getResponseCount())
        assertTrue(sessionResult.areAllResponsesSuccessful())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.RiskDec))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 1, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 1, RequestType.Subscription))
    }

    // .threeDQuery, .accountCheck, .subscription]
    @Test
    fun test_googlePayPositiveThreeDQueryAccountCheckSubscription() = runBlocking {
        //GIVEN
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePaySubscription(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.ThreeDQuery,
                        RequestType.AccountCheck,
                        RequestType.Subscription
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        //THEN
        assertEquals(3, sessionResult.getResponseCount())
        assertTrue(sessionResult.areAllResponsesSuccessful())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.AccountCheck))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 1, RequestType.Subscription))
    }

    // .threeDQuery, .auth]
    @Test
    fun test_googlePayPositiveThreeDQueryAuth() = runBlocking {
        //GIVEN
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.ThreeDQuery,
                        RequestType.Auth
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        //THEN
        assertEquals(2, sessionResult.getResponseCount())
        assertTrue(sessionResult.areAllResponsesSuccessful())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
    }

    // .threeDQuery, .auth, .riskDec]
    @Test
    fun test_googlePayPositiveThreeDQueryAuthRiskDec() = runBlocking {
        //GIVEN
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.ThreeDQuery,
                        RequestType.Auth,
                        RequestType.RiskDec
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        //THEN
        assertEquals(3, sessionResult.getResponseCount())
        assertTrue(sessionResult.areAllResponsesSuccessful())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 1, RequestType.RiskDec))
    }

    // .threeDQuery, .auth, .subscription]
    @Test
    fun test_googlePayPositiveThreeDQueryAuthSubscription() = runBlocking {
        //GIVEN
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePaySubscription(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.ThreeDQuery,
                        RequestType.Auth,
                        RequestType.Subscription
                ),
                baseAmount = baseAmount
        )

        val session = paymentTransactionManager.createSession({token})

        lateinit var sessionResult: PaymentTransactionManager.Response
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                    paymentTransactionManager.executeSession(session) { activity }
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, CardsDataWith3DSecureV2.threeDSecureCode)
        job.join()

        //THEN
        assertEquals(3, sessionResult.getResponseCount())
        assertTrue(sessionResult.areAllResponsesSuccessful())
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(0, 0, RequestType.ThreeDQuery))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 0, RequestType.Auth))
        assertNotNull(sessionResult.getTransactionResponseWithTypeOrNull(1, 1, RequestType.Subscription))
    }

    @Test
    fun test_googlePayOverrideBilling() = runBlocking {
        //GIVEN
        val billingFirstName = "testbilling"
        val postCode = "LL54 6RL"
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)
        val apiService = TrustPaymentsJsonApiService()

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.Auth
                ),
                billingContactDetailsOverride = 1,
                billingFirstName = billingFirstName,
                billingPostCode = postCode
        )

        val session = paymentTransactionManager.createSession({token})
        val result = paymentTransactionManager.executeSession(session)
        val parsedResponse = ResponseParser.parse(result.responseJwtList.first())

        assertNotNull(parsedResponse)
        assertEquals(1, parsedResponse!!.responses.size)
        assertNotNull(parsedResponse.responses[0].transactionReference)

        val transactionReference = parsedResponse.responses[0].transactionReference!!

        val jsonWebResult = apiService.performTransactionQuery(
                arrayOf("TRANSACTIONQUERY"),
                "GBP",
                baseAmount.toString(),
                arrayOf(transactionReference)
        )

        assertNotNull(jsonWebResult)

        val successJsonWebResult = jsonWebResult as TrustPaymentsJsonApiService.Result.Success

        //THEN
        assertEquals("Ok", successJsonWebResult.transactionResponses[0].errorMessage)
        assertEquals(
                ResponseErrorCode.Ok,
                successJsonWebResult.transactionResponses[0].errorCode
        )
        assertEquals(
                "TRANSACTIONQUERY",
                successJsonWebResult.transactionResponses[0].requestTypeDescription
        )
        assertEquals(1, successJsonWebResult.transactionResponses[0].records.size)
        assertEquals(
                billingFirstName,
                successJsonWebResult.transactionResponses[0].records[0].billingfirstname
        )
        assertEquals(
            postCode,
            successJsonWebResult.transactionResponses[0].records[0].billingpostcode
        )
    }

    @Test
    fun test_googlePayOverrideShipping() = runBlocking {
        //GIVEN
        val customerFirstName = "testcustomer"
        val postCode = "LL54 6RL"
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)
        val apiService = TrustPaymentsJsonApiService()

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.Auth
                ),
                customerContactDetailsOverride = 1,
                customerFirstName = customerFirstName,
                customerPostCode = postCode
        )

        val session = paymentTransactionManager.createSession({token})
        val result = paymentTransactionManager.executeSession(session)
        val parsedResponse = ResponseParser.parse(result.responseJwtList.first())

        assertNotNull(parsedResponse)
        assertEquals(1, parsedResponse!!.responses.size)
        assertNotNull(parsedResponse.responses[0].transactionReference)

        val transactionReference = parsedResponse.responses[0].transactionReference!!

        val jsonWebResult = apiService.performTransactionQuery(
                arrayOf("TRANSACTIONQUERY"),
                "GBP",
                baseAmount.toString(),
                arrayOf(transactionReference)
        )

        assertNotNull(jsonWebResult)

        val successJsonWebResult = jsonWebResult as TrustPaymentsJsonApiService.Result.Success

        //THEN
        assertEquals("Ok", successJsonWebResult.transactionResponses[0].errorMessage)
        assertEquals(
                ResponseErrorCode.Ok,
                successJsonWebResult.transactionResponses[0].errorCode
        )
        assertEquals(
                "TRANSACTIONQUERY",
                successJsonWebResult.transactionResponses[0].requestTypeDescription
        )
        assertEquals(1, successJsonWebResult.transactionResponses[0].records.size)
        assertEquals(
                customerFirstName,
                successJsonWebResult.transactionResponses[0].records[0].customerfirstname
        )
        assertEquals(
            postCode,
            successJsonWebResult.transactionResponses[0].records[0].customerpostcode
        )
    }

    @Test
    fun test_googlePayOverrideBillingAndShipping() = runBlocking {
        //GIVEN
        val billingFirstName = "testbilling"
        val customerFirstName = "testcustomer"
        val postCode = "LL54 6RL"
        tpGooglePayManager = GooglePayInstances.customMerchantName(activity, BuildConfig.MERCHANT_USERNAME)
        val apiService = TrustPaymentsJsonApiService()

        //WHEN
        googlePayTestPage.tapPayWithGooglePay()
        googlePayTestPage.tapContinue(displayMetrics)
        googlePayTestPage.waitResult()
        val text = activity.textView?.text?.toString()
        assertNotNull(text)

        val token = jwtBuilder.getForGooglePay(
                walletToken = text!!,
                requestTypes = listOf(
                        RequestType.Auth
                ),
                billingContactDetailsOverride = 1,
                customerContactDetailsOverride = 1,
                billingFirstName = billingFirstName,
                customerFirstName = customerFirstName,
                billingPostCode = postCode,
                customerPostCode = postCode
        )

        val session = paymentTransactionManager.createSession({token})
        val result = paymentTransactionManager.executeSession(session)
        val parsedResponse = ResponseParser.parse(result.responseJwtList.first())

        assertNotNull(parsedResponse)
        assertEquals(1, parsedResponse!!.responses.size)
        assertNotNull(parsedResponse.responses[0].transactionReference)

        val transactionReference = parsedResponse.responses[0].transactionReference!!

        val jsonWebResult = apiService.performTransactionQuery(
                arrayOf("TRANSACTIONQUERY"),
                "GBP",
                baseAmount.toString(),
                arrayOf(transactionReference)
        )

        assertNotNull(jsonWebResult)

        val successJsonWebResult = jsonWebResult as TrustPaymentsJsonApiService.Result.Success

        //THEN
        assertEquals("Ok", successJsonWebResult.transactionResponses[0].errorMessage)
        assertEquals(
                ResponseErrorCode.Ok,
                successJsonWebResult.transactionResponses[0].errorCode
        )
        assertEquals(
                "TRANSACTIONQUERY",
                successJsonWebResult.transactionResponses[0].requestTypeDescription
        )
        assertEquals(1, successJsonWebResult.transactionResponses[0].records.size)
        assertEquals(
                billingFirstName,
                successJsonWebResult.transactionResponses[0].records[0].billingfirstname
        )
        assertEquals(
                customerFirstName,
                successJsonWebResult.transactionResponses[0].records[0].customerfirstname
        )
        assertEquals(
            postCode,
            successJsonWebResult.transactionResponses[0].records[0].billingpostcode
        )
        assertEquals(
            postCode,
            successJsonWebResult.transactionResponses[0].records[0].customerpostcode
        )
    }

    private fun centsToString(cents: Long) = String.format("%.2f", cents.toFloat()/ 100)
}
