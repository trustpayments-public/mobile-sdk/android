package com.trustpayments.mobile.core.util

import com.trustpayments.mobile.core.models.api.response.*

sealed class OldTransactionResponse {
    data class PaymentSuccess(
        val transactionReference: String,

        val requestTypeDescription: RequestType,

        val settleStatus: ResponseSettlementStatus?,
        val liveStatus: ResponseLiveStatus,
        val customerOutput: CustomerOutput?,

        val baseAmount: Long?, //may be empty if ACCOUNTCHECK
        val currencyISO3a: String?, //may be empty if ACCOUNTCHECK

        val maskedPan: String,
        val paymentTypeDescription: String,

        val cavv: String?,
        val eci: String?,
        val enrolled: String?,
        val status: String?,
        val xid: String?,

        val acquirerTransactionReference: String?, //TODO rework into separate response subtypes
        val acsUrl: String?, //TODO rework into separate response subtypes
        val threeDPayload: String?, //TODO rework into separate response subtypes
        val threeDVersion: String? //TODO rework into separate response subtypes
    ): OldTransactionResponse() {
        var pares: String? = null   //TODO rework into separate response subtypes
            internal set
        var threeDResponse: String? = null //TODO rework into separate response subtypes
            internal set
    }

    data class InitializationError(
        val initResponse: ConfigurationSuccess,
        val error: com.trustpayments.mobile.core.services.transaction.ThreeDSecureError): OldTransactionResponse()

    data class ConfigurationSuccess(
        val cacheToken: String,
        val requestTypeDescription: RequestType,
        val threeDInit: String

    ): OldTransactionResponse()

    sealed class Error: OldTransactionResponse() {
        data class TransactionError(
            val errorCode: ResponseErrorCode,
            val errorMessage: String, //always returned, "Ok" if successful
            val errorData: Array<String>?, //may be empty if no error

            val requestTypeDescription: String, //always returned - "ERROR" if error
            val customerOutput: CustomerOutput?
        ): Error()

        data class ThreeDSecureError(
            val threeDResponse: PaymentSuccess,
            val error: com.trustpayments.mobile.core.services.transaction.ThreeDSecureError): Error()

        data class ParsingError(
            val errorMessage: String = ""
        ) : Error()
    }
}