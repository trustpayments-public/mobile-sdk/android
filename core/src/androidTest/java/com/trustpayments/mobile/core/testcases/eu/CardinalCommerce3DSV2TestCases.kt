package com.trustpayments.mobile.core.testcases.eu

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.rule.ActivityTestRule
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.R
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.testutils.createSessionFor
import com.trustpayments.mobile.core.testutils.createThreeDQueryResponse
import com.trustpayments.mobile.core.testutils.getAs3DError
import com.trustpayments.mobile.core.testutils.getAsFailure
import com.trustpayments.mobile.core.testutils.getAsSuccess
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.util.PaymentSessionResponse
import com.trustpayments.mobile.core.util.parse
import com.trustpayments.mobile.core.utils.ThreeDQueryTestActivity
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV2.threeDSecureCode
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test


/**
 * Test cases for the version 2 of 3-D Secure. Taken from the Cardinal Commerce wiki:
 * https://cardinaldocs.atlassian.net/wiki/spaces/CCen/pages/903577725/EMV+3DS+2.0+Test+Cases
 */
class CardinalCommerce3DSV2TestCases: BaseTestCase() {

    private lateinit var paymentTransactionManager: PaymentTransactionManager

    private lateinit var jwtToken: String

    private lateinit var activity: ThreeDQueryTestActivity

    @get:Rule
    val initialActivityTestRule = ActivityTestRule(ThreeDQueryTestActivity::class.java)

    private val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    @Before
    override fun setUp() {
        super.setUp()

        val app = ApplicationProvider.getApplicationContext<Context>().applicationContext
        paymentTransactionManager =
            PaymentTransactionManager(app, getGatewayType(), false, BuildConfig.MERCHANT_USERNAME, isLocationDataConsentGiven = false)

        jwtToken = jwtBuilder.getFor3DSecure(listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        ))

        activity = initialActivityTestRule.activity
    }


    // region Test Case 1: Successful Frictionless Authentication
    @Test
    fun test_positive_frictionlessAuthenticationForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = "Y",
            eciFlag = "02"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNull(actualAuthTransactionResponse.xid)
        assertNotNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }

    @Test
    fun test_positive_frictionlessAuthenticationForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000001007", "1234")
        
        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = "Y",
            eciFlag = "05"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNotNull(actualAuthTransactionResponse.xid)
        assertNotNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }
    // endregion

    // region Test Case 2: Failed Frictionless Authentication
    @Test
    fun test_negative_frictionlessAuthenticationForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001013", "123")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = "N",
            eciFlag = null
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsFailure(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualTDQTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualTDQTransactionResponse.status)
        assertNull(actualTDQTransactionResponse.xid)
        assertNull(actualTDQTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualTDQTransactionResponse.eci)
    }

    @Test
    fun test_negative_frictionlessAuthenticationForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000001015", "1234")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = "N",
            eciFlag = null
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsFailure(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualTDQTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualTDQTransactionResponse.status)
        assertNotNull(actualTDQTransactionResponse.xid)
        assertNull(actualTDQTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualTDQTransactionResponse.eci)
    }
    // endregion

    // region Test Case 3: Attempts Stand-In Frictionless Authentication
    @Test
    fun test_positive_standInFrictionlessAuthenticationForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001021", "123")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = "A",
            eciFlag = "01"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNull(actualAuthTransactionResponse.xid)
        assertNotNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }

    @Test
    fun test_positive_standInFrictionlessAuthenticationForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000001023", "1234")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = "A",
            eciFlag = "06"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNotNull(actualAuthTransactionResponse.xid)
        assertNotNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }
    // endregion

    // region Test Case 4: Unavailable Frictionless Authentication from the Issuer
    @Test
    fun test_negative_unavailableFrictionlessAuthenticationForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001039", "123")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = "U",
            eciFlag = "00"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNull(actualAuthTransactionResponse.xid)
        assertNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }

    @Test
    fun test_negative_unavailableFrictionlessAuthenticationForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000001031", "1234")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = "U",
            eciFlag = "07"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNotNull(actualAuthTransactionResponse.xid)
        assertNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }
    // endregion

    // region Test Case 5: Rejected Frictionless Authentication by the Issuer
    @Test
    fun test_negative_rejectedFrictionlessAuthenticationForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001047", "123")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = "R",
            eciFlag = null
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsFailure(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualTDQTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualTDQTransactionResponse.status)
        assertNull(actualTDQTransactionResponse.xid)
        assertNull(actualTDQTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualTDQTransactionResponse.eci)
    }

    @Test
    fun test_negative_rejectedFrictionlessAuthenticationForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000001049", "1234")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = "R",
            eciFlag = null
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsFailure(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualTDQTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualTDQTransactionResponse.status)
        assertNotNull(actualTDQTransactionResponse.xid)
        assertNull(actualTDQTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualTDQTransactionResponse.eci)
    }
    // endregion

    // region Test Case 6: Authentication Not Available on Lookup
    @Test
    fun test_negative_authenticationNotAvailableOnLookupForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001054", "123")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "U",
            paresStatus = null,
            eciFlag = "00"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNull(actualAuthTransactionResponse.xid)
        assertNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }

    @Test
    fun test_negative_authenticationNotAvailableOnLookupForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000001056", "1234")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "U",
            paresStatus = null,
            eciFlag = "07"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNull(actualAuthTransactionResponse.xid)
        assertNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }
    // endregion

    // region Test Case 7: Error on Lookup
    @Test
    fun test_negative_errorOnLookupForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001062", "123")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "U",
            paresStatus = null,
            eciFlag = "00"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsFailure(0)

        // THEN
        //TDQ fails, expecting no params
    }

    @Test
    fun test_negative_errorOnLookupForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000001064", "1234")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "U",
            paresStatus = null,
            eciFlag = "07"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsFailure(0)

        // THEN
        //TDQ fails, expecting no params
    }
    // endregion

    // region Test Case 8: Timeout on cmpi_lookup Transaction

    @Test
    fun test_negative_timeoutOnCMPILookupTransactionForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001070", "123")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "U",
            paresStatus = null,
            eciFlag = null
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsFailure(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNull(actualAuthTransactionResponse.xid)
        assertNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }

    @Test
    fun test_negative_timeoutOnCMPILookupTransactionForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000001072", "1234")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "U",
            paresStatus = null,
            eciFlag = null
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsFailure(0)
        val actualAuthTransactionResponse = sessionResult.getAsFailure(1)

        // THEN
//        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
//        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
//        assertNull(actualAuthTransactionResponse.xid)
//        assertNull(actualAuthTransactionResponse.cavv)
//        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }
    // endregion

    // region Test Case 9: Bypassed Authentication
    @Test
    fun test_positive_bypassedAuthenticationForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001088", "123")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "B",
            paresStatus = null,
            eciFlag = "00"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
            as PaymentSessionResponse.Success

        // THEN
        assertEquals(1, sessionResult.responseParts.size)
        assertEquals(2, sessionResult.allResponses.size)

        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNull(actualAuthTransactionResponse.xid)
        assertNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }

    @Test
    fun test_positive_bypassedAuthenticationForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000001080", "1234")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "B",
            paresStatus = null,
            eciFlag = "07"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()

        // THEN
        sessionResult as PaymentSessionResponse.Success
        assertEquals(1, sessionResult.responseParts.size)
        assertEquals(2, sessionResult.allResponses.size)

        val actualResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNotNull(actualAuthTransactionResponse.xid)
        assertNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }
    // endregion


    // region Test Case 10: Successful Step Up Authentication
    @Test
    fun test_positive_stepUpAuthenticationForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001096", "123")

        val expectedTransactionResponse =
            createThreeDQueryResponse(
                enrolled = "Y",
                paresStatus = "Y",
                eciFlag = "02"
            )

        // WHEN
        lateinit var sessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }.parse()
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, threeDSecureCode)

        job.join()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNull(actualAuthTransactionResponse.xid)
        assertNotNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }

    @Test
    fun test_positive_stepUpAuthenticationForForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000001098", "1234")

        val expectedTransactionResponse =
            createThreeDQueryResponse(
                enrolled = "Y",
                paresStatus = "Y",
                eciFlag = "05"
            )

        // WHEN
        lateinit var sessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }.parse()
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, threeDSecureCode)

        job.join()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNotNull(actualAuthTransactionResponse.xid)
        assertNotNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }
    // endregion

    // region Test Case 11: Failed Step Up Authentication
    @Test
    fun test_negative_stepUpAuthenticationForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001104", "123")

        val expectedTransactionResponse =
            createThreeDQueryResponse(
                enrolled = "Y",
                paresStatus = "C",
                eciFlag = null
            )

        // WHEN
        lateinit var sessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }.parse()
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, threeDSecureCode)

        job.join()
        val actualTDQTransactionResponse = sessionResult.getAs3DError(0).threeDResponse

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualTDQTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualTDQTransactionResponse.status)
        assertNull(actualTDQTransactionResponse.xid)
        assertNull(actualTDQTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualTDQTransactionResponse.eci)
    }

    @Test
    fun test_negative_stepUpAuthenticationForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000001106", "1234")

        val expectedTransactionResponse =
            createThreeDQueryResponse(
                enrolled = "Y",
                paresStatus = "C",
                eciFlag = null
            )

        // WHEN
        lateinit var sessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }.parse()
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, threeDSecureCode)

        job.join()
        val actualTDQTransactionResponse = sessionResult.getAs3DError(0).threeDResponse

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualTDQTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualTDQTransactionResponse.status)
        assertNotNull(actualTDQTransactionResponse.xid)
        assertNull(actualTDQTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualTDQTransactionResponse.eci)
    }
    // endregion

    // region Test Case 11: Step Up Authentication is Unavailable
    //Logic of this has been changed. It is no longer desired for authentication to fail for Master cards
    //ending with number 2.

    //Temporarily commented out due to gateway bug https://securetrading.atlassian.net/browse/MSDKANDRD-743
//    @Test
//    fun test_negative_stepUpAuthenticationIsUnavailableForMastercard() = runBlocking {
//        // GIVEN
//        val session =
//            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001112", "123")
//
//        val expectedTransactionResponse =
//            createThreeDQueryResponse(
//                enrolled = "Y",
//                paresStatus = "U",
//                eciFlag = "00"
//            )
//
//        // WHEN
//        lateinit var sessionResult: PaymentSessionResponse
//        val job = launch(Dispatchers.Unconfined) {
//            sessionResult =
//                paymentTransactionManager.executeSession(session) { activity }.parse()
//        }
//
//        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, threeDSecureCode)
//
//        job.join()
//        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
//        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)
//
//        // THEN
//        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
//        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
//        assertNull(actualAuthTransactionResponse.xid)
//        assertNull(actualAuthTransactionResponse.cavv)
//        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
//    }

    @Test
    fun test_negative_stepUpAuthenticationIsUnavailableForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000001114", "1234")

        val expectedTransactionResponse =
            createThreeDQueryResponse(
                enrolled = "Y",
                paresStatus = "U",
                eciFlag = "07"
            )

        // WHEN
        lateinit var sessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }.parse()
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, threeDSecureCode)

        job.join()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNotNull(actualAuthTransactionResponse.xid)
        assertNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }
    // endregion

    // region Test Case 13: Error on Authentication
    @Test
    fun test_negative_errorOnAuthenticationForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001120", "123")

        val expectedTransactionResponse =
            createThreeDQueryResponse(
                enrolled = "Y",
                paresStatus = "C",
                eciFlag = null
            )

        // WHEN
        lateinit var sessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }.parse()
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, threeDSecureCode)

        job.join()
        val actualTDQTransactionResponse = sessionResult.getAs3DError(0).threeDResponse

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualTDQTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualTDQTransactionResponse.status)
        assertNull(actualTDQTransactionResponse.xid)
        assertNull(actualTDQTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualTDQTransactionResponse.eci)
    }

    @Test
    fun test_negative_errorOnAuthenticationForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000001122", "1234")
        
        val expectedTransactionResponse =
            createThreeDQueryResponse(
                enrolled = "Y",
                paresStatus = "C",
                eciFlag = null
            )

        // WHEN
        lateinit var sessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }.parse()
        }

        handle3DSV2Popup(R.id.codeEditTextField, R.id.submitAuthenticationButton, threeDSecureCode)

        job.join()
        val actualTDQTransactionResponse = sessionResult.getAs3DError(0).threeDResponse

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualTDQTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualTDQTransactionResponse.status)
        assertNotNull(actualTDQTransactionResponse.xid)
        assertNull(actualTDQTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualTDQTransactionResponse.eci)
    }
    // endregion
}