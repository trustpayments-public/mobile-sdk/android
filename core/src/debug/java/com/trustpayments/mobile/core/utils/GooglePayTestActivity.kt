package com.trustpayments.mobile.core.utils

import android.content.Intent
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.trustpayments.mobile.core.R

class GooglePayTestActivity : AppCompatActivity(R.layout.activity_google_pay_test) {
    var resultCallback : ((Int, Int, Intent?) -> Unit)? = null
    var button : Button? = null
    var textView : TextView? = null

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        resultCallback?.invoke(requestCode, resultCode, data)
    }
}