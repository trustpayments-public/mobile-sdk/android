package com.trustpayments.mobile.ui

import com.trustpayments.mobile.ui.card.CardValidator
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class ExpiryDateInputViewModelTest {

    @MockK
    private lateinit var cardValidator: CardValidator

    @MockK
    private lateinit var expiryDateInputCallbacks: ExpiryDateInputViewModel.ExpiryDateInputCallbacks

    private lateinit var viewModel: ExpiryDateInputViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        viewModel = ExpiryDateInputViewModel(cardValidator, expiryDateInputCallbacks)
    }

    @Test
    fun test_CorrectDateFormatForFirstMonthDigitGreaterThanSeptember() {
        // GIVEN
        every { expiryDateInputCallbacks.onExpiryDateInvalid(any()) } returns Unit

        val dateInput = "1"
        val expectedFormattedDate = "1"

        // WHEN
        val actualFormattedDate = viewModel.formatExpiryDate(dateInput)

        // THEN
        assertEquals(expectedFormattedDate, actualFormattedDate)
        verify(exactly = 1) { expiryDateInputCallbacks.onExpiryDateInvalid(any()) }
    }

    @Test
    fun test_CorrectDateFormatForBothMonthDigitsGreaterThanSeptember() {
        // GIVEN
        every { expiryDateInputCallbacks.onExpiryDateInvalid(any()) } returns Unit

        val dateInput = "11"
        val expectedFormattedDate = "11/"

        // WHEN
        val actualFormattedDate = viewModel.formatExpiryDate(dateInput)

        // THEN
        assertEquals(expectedFormattedDate, actualFormattedDate)
        verify(exactly = 1) { expiryDateInputCallbacks.onExpiryDateInvalid(any()) }
    }

    @Test
    fun test_CorrectDateFormatForFirstMonthDigitLessThanSeptember() {
        // GIVEN
        every { expiryDateInputCallbacks.onExpiryDateInvalid(any()) } returns Unit

        val dateInput = "5"
        val expectedFormattedDate = "05/"

        // WHEN
        val actualFormattedDate = viewModel.formatExpiryDate(dateInput)

        // THEN
        assertEquals(expectedFormattedDate, actualFormattedDate)
        verify(exactly = 1) { expiryDateInputCallbacks.onExpiryDateInvalid(any()) }
    }

    @Test
    fun test_CorrectDateFormatForMonthDigitsGreaterThanDecember() {
        // GIVEN
        every { expiryDateInputCallbacks.onExpiryDateInvalid(any()) } returns Unit

        val dateInput = "14"
        val expectedFormattedDate = "14/"

        // WHEN
        val actualFormattedDate = viewModel.formatExpiryDate(dateInput)

        // THEN
        assertEquals(expectedFormattedDate, actualFormattedDate)
        verify(exactly = 1) { expiryDateInputCallbacks.onExpiryDateInvalid(any()) }
    }

    @Test
    fun test_CorrectDateFormatForThreeMonthDigits() {
        // GIVEN
        every { expiryDateInputCallbacks.onExpiryDateInvalid(any()) } returns Unit

        val dateInput = "123/"
        val expectedFormattedDate = "12/3"

        // WHEN
        val actualFormattedDate = viewModel.formatExpiryDate(dateInput)

        // THEN
        assertEquals(expectedFormattedDate, actualFormattedDate)
        verify(exactly = 1) { expiryDateInputCallbacks.onExpiryDateInvalid(any()) }
    }

    @Test
    fun test_CorrectDateFormatForThreeYearDigits() {
        // GIVEN
        every { expiryDateInputCallbacks.onExpiryDateInvalid(any()) } returns Unit

        val dateInput = "/123"
        val expectedFormattedDate = "1/23"

        // WHEN
        val actualFormattedDate = viewModel.formatExpiryDate(dateInput)

        // THEN
        assertEquals(expectedFormattedDate, actualFormattedDate)
        verify(exactly = 1) { expiryDateInputCallbacks.onExpiryDateInvalid(any()) }
    }

    @Test
    fun test_CorrectDateFormatForFullDate() {
        // GIVEN
        every { cardValidator.isExpiryDateValid(any(), any()) } returns true
        every { expiryDateInputCallbacks.onExpiryDateValid(any()) } returns Unit

        val dateInput = "12/22"
        val expectedFormattedDate = "12/22"

        // WHEN
        val actualFormattedDate = viewModel.formatExpiryDate(dateInput)

        // THEN
        assertEquals(expectedFormattedDate, actualFormattedDate)
        verify(exactly = 1) { expiryDateInputCallbacks.onExpiryDateValid(any()) }
    }

    @Test
    fun test_InvalidDateFormatForFullDate() {
        // GIVEN
        every { cardValidator.isExpiryDateValid(any(), any()) } returns false
        every { expiryDateInputCallbacks.onExpiryDateInvalid(any()) } returns Unit

        val dateInput = "12/19"

        // WHEN
        viewModel.validateExpiryDate(dateInput)

        // THEN
        verify(exactly = 1) { expiryDateInputCallbacks.onExpiryDateInvalid(any()) }
    }
}