package com.trustpayments.mobile.exampleapp.common.recyclerview

import android.app.Activity
import androidx.annotation.StringRes

sealed class ListItem {
    data class Header(@StringRes val titleRes: Int) : ListItem()
    data class SampleItem(@StringRes val titleRes: Int, val launchActivityClass: Class<out Activity>) :
        ListItem()
}