package com.trustpayments.mobile.core.utils

internal fun log(message: String) {
    //Logging disabled
}

internal fun log(tag: String, message: String) {
    //Logging disabled
}

internal fun logJson(json: String) {
    //Logging disabled
}

internal fun logJson(tag: String, json: String) {
    //Logging disabled
}