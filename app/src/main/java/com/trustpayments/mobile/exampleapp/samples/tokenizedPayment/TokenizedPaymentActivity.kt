package com.trustpayments.mobile.exampleapp.samples.tokenizedPayment

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.trustpayments.mobile.core.services.api.TrustPaymentsGatewayType
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.exampleapp.BuildConfig
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.common.jwtexplorer.JWTEditorToken
import com.trustpayments.mobile.exampleapp.common.jwtexplorer.OnTokenChangedListener
import com.trustpayments.mobile.exampleapp.databinding.ActivityTokenizedPaymentBinding
import com.trustpayments.mobile.exampleapp.utils.Utils
import com.trustpayments.mobile.exampleapp.utils.Utils.hideKeyboard
import com.trustpayments.mobile.ui.dropin.DropInPaymentView
import com.trustpayments.mobile.ui.model.PaymentInputType

class TokenizedPaymentActivity : AppCompatActivity(), DropInPaymentView.DropInPaymentViewListener,
    DropInPaymentView.DropInInvalidInputListener {

    private val viewBinding by lazy {
        ActivityTokenizedPaymentBinding.inflate(layoutInflater)
    }

    // PaymentTransactionManager responsible for executing the transaction.
    private val paymentTransactionManager by lazy {
        PaymentTransactionManager(
            context = this@TokenizedPaymentActivity,
            gatewayType = TrustPaymentsGatewayType.EU,
            isCardinalLive = false,
            merchantUsername = BuildConfig.MERCHANT_USERNAME,
            isLocationDataConsentGiven = true,
        )
    }
    private val viewModel: TokenizedPaymentViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(viewBinding.root)
        setSupportActionBar(viewBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewBinding.toolbar.setNavigationOnClickListener { finish() }
        setUiElements()
        setObservers()
    }

    private fun setUiElements() {
        viewBinding.apply {
            btnAddCard.setOnClickListener {
                hideKeyboard(it)
                viewModel.onAddCardButtonClicked(
                    context = this@TokenizedPaymentActivity,
                    paymentTransactionManager = paymentTransactionManager,
                    activityProvider = {
                        this@TokenizedPaymentActivity
                    },
                )
            }
            btnAuthorize.setOnClickListener {
                (viewModel.uiState.value as? TokenizedPaymentViewModel.UIState.Authorized)?.also {
                    TokenizedPaymentTriggerActivity.launchActivity(
                        context = this@TokenizedPaymentActivity,
                        token = it.token,
                        cvv = it.cvv
                    )
                }
            }
            jwtExplorer.apply {
                setOnTokenChangedListener(
                    object : OnTokenChangedListener {
                        override fun onTokenChanged(token: JWTEditorToken) {
                            viewModel.onPayloadChanged(token)
                        }
                    },
                )
            }
            dropInPaymentView.apply {
                dropInPaymentViewListener = this@TokenizedPaymentActivity
                dropInInvalidInputListener = this@TokenizedPaymentActivity
                setPayButtonVisibility(View.GONE)
            }
        }
    }

    private fun setObservers() {
        viewBinding.apply {
            viewModel.requestPayload.observe(this@TokenizedPaymentActivity) {
                jwtExplorer.setRequestJwtToken(it)
            }
            viewModel.responsePayload.observe(this@TokenizedPaymentActivity) {
                jwtExplorer.apply {
                    setResponseJwtToken(it)
                    it.takeIf { it.isNotBlank() }?.also {
                        moveTokenTab(1)
                    }
                }
            }
            viewModel.uiState.observe(this@TokenizedPaymentActivity) {
                when (it) {
                    is TokenizedPaymentViewModel.UIState.Loading -> {
                        progressBar.isVisible = true
                        btnAddCard.isEnabled = false
                    }

                    is TokenizedPaymentViewModel.UIState.Idle -> {
                        tvStatus.text = getString(R.string.status_token_na)
                        btnAddCard.isEnabled = true
                        btnAuthorize.isEnabled = false
                        progressBar.isVisible = false
                    }

                    is TokenizedPaymentViewModel.UIState.Authorized -> {
                        tvStatus.text =
                            String.format(getString(R.string.status_token_created), it.token)
                        progressBar.isVisible = false
                        btnAddCard.isEnabled = true
                        btnAuthorize.isEnabled = true
                        Utils.showDialog(
                            context = this@TokenizedPaymentActivity,
                            title = getString(R.string.hs_transaction_success),
                            message = getString(R.string.token_creation_success)
                        )
                    }

                    is TokenizedPaymentViewModel.UIState.Error -> {
                        tvStatus.text = getString(R.string.status_token_failure)
                        btnAddCard.isEnabled = true
                        btnAuthorize.isEnabled = false
                        progressBar.isVisible = false
                        Utils.showDialog(
                            context = this@TokenizedPaymentActivity,
                            title = getString(R.string.error),
                            message = it.message
                        )
                    }
                }
            }
        }
    }

    override fun onInputValid(
        paymentInputType: PaymentInputType,
        input: String,
    ) {
        viewModel.onUpdateCardData(paymentInputType, input)
    }

    override fun onInputInvalid(paymentInputType: PaymentInputType) {
        viewModel.onUpdateCardData(paymentInputType, "")
    }

    override fun onPayButtonClicked() {
        // Not utilizing DropInPaymentView buy button since custom button is available in UI.
    }
}
