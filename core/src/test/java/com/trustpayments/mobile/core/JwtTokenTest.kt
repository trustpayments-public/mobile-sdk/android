package com.trustpayments.mobile.core

import android.os.Build
import android.util.Base64
import com.trustpayments.mobile.core.models.api.jwt.JwtToken
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.M])
class JwtTokenTest {
    @Test
    fun test_urlDecoding() {
        //GIVEN
        val token = "aaaa-.aaaa_.bbbb"

        //WHEN
        val jwt = JwtToken(token)

        //THEN
        assertEquals("aaaa+.aaaa/.bbbb", jwt.urlDecodedToken)
    }

    @Test
    fun test_tokenInvalid_notThreeParts() {
        //GIVEN
        val token = "aaaa.aaaa"

        //WHEN
        val jwt = JwtToken(token)

        //THEN
        assertFalse(jwt.isValid)
    }

    @Test
    fun test_tokenInvalid_emptyPart() {
        //GIVEN
        val token = "aaaa.aaaa."

        //WHEN
        val jwt = JwtToken(token)

        //THEN
        assertFalse(jwt.isValid)
    }

    @Test
    fun test_tokenCorrectlyDividedIntoParts() {
        //GIVEN
        val token = "aaaa.bbbb.cccc"

        //WHEN
        val jwt = JwtToken(token)

        //THEN
        assertEquals("aaaa", jwt.encodedHeader)
        assertEquals("bbbb", jwt.encodedBody)
        assertEquals("cccc", jwt.encodedSignature)
    }

    @Test
    fun test_tokenCorrectlyDecoded() {
        //GIVEN
        val headerDecoded = "aaaa"
        val bodyDecoded = "bbbb"
        val signatureDecoded = "cccc"
        val token =
            "${String(Base64.encode(headerDecoded.toByteArray(), 0))}." +
                    "${String(Base64.encode(bodyDecoded.toByteArray(), 0))}." +
                    "${String(Base64.encode(signatureDecoded.toByteArray(), 0))}"

        //WHEN
        val jwt = JwtToken(token)

        //THEN
        assertEquals(headerDecoded, jwt.header)
        assertEquals(bodyDecoded, jwt.body)
        assertEquals(signatureDecoded, jwt.signature)
    }
}