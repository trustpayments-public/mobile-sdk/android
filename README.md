# TrustPayments Android SDK

[![Maven Central](https://img.shields.io/maven-central/v/com.trustpayments.mobile/core)](https://search.maven.org/search?q=com.trustpayments.mobile)

The Trust Payments Android SDK allows you to seamlessly integrate a prebuilt or custom UI in your app to accept card payments & comply with the Strong Customer Authentication (SCA) mandate.

For more information, visit the [What is Trust Payments Android SDK?](https://help.trustpayments.com/hc/en-us/articles/4404020512785-What-is-our-Android-SDK) page.

## Features in Trust Payments Android SDK

- Card payments
- Tokenization payments
- Recurring and installments payments
- Digital wallet payments (Google Pay)
- Customization of UI components to fit with your business branding
- Locale and custom translations

If you have already built your own payment checkout views in your Android app but need a method to process payments to the Trust Payments gateway, you can use our [Payment Transaction Manager API](https://help.trustpayments.com/hc/en-us/articles/4402964728721-Getting-started-with-Android-SDK#h_01FB41H2P5HB63ZCY01ZXJGQVF) for integration.

## Requirements

For the requirements you must meet to install our Android SDK, please visit the [Android SDK requirements](https://help.trustpayments.com/hc/en-us/articles/4402965426065-Android-SDK-requirements) page.

## Documentation

For integration guides and official documentation, please visit the [Getting started with Android SDK](https://help.trustpayments.com/hc/en-us/articles/4402964728721-Getting-started-with-Android-SDK) page.

## Support

If you need assistance with your integration or are experiencing issues with our payments SDK, please contact our Technical Support Team at [support@trustpayments.com](mailto:support@trustpayments.com).


## Sample App

The sample app included in the project demonstrates how to integrate our payment SDK into your application so that you can accept card payments in your business.

Please follow the steps below to compile and run it.

### List of features included in the sample app

| Feature                              | Description                                                                                              |
| --------------------------------- | --------------------------------------------------------------------------------------------------- |
| Drop-in form                    | Prebuilt UI for enabling card payments in an app easily.                                                |
| Card Payments                  | Demonstrates how a card payment can be made using our SDK.                                           |
| Tokenized Payments          | Demonstrates how a merchant can store a transaction reference and execute transactions in the future. |
| Recurring and Installments | Demonstrates how a merchant can offer recurring and installment payment options in an app.        |
| Google Payments              | Demonstrates how a merchant can integrate Google Pay as a payment option in their app using our SDK. |

## Getting started with sample App

#### Adding Credentials

After downloading the project, create a file named `credentials.properties` in the root directory. This file should include merchant credentials obtained from Trust Payments.

Please contact our support team to obtain your credentials: [Request credentials from our support team.](https://help.trustpayments.com/hc/en-us/requests/new)

```xml
MERCHANT_USERNAME=<merchant_username>
MERCHANT_SITE_REFERENCE=<merchant_site_reference_id>
MERCHANT_JWT_KEY=<jwt_key_assigned_to_merchant>
GITLAB_READ_TOKEN=<token_used_to_download_cardinal_sdk_from_gitLab>
```

#### Setting up Java Settings

1. Open the project using Android Studio.
2. Go to `File` -> `Settings` -> `Build, Execution, Deployment` -> `Build Tools` -> `Gradle` -> `Gradle Projects` -> `Gradle JDK`.
3. Select **jbr-17** as the Gradle JDK.

#### Setting up Build Variants

1. In Android Studio, navigate to the **Build Variants** tab from the left sidebar menu.
2. Select **prodDebug** as the **app module** variant if you want to debug the sample app with production environment.
3. Select **prodRelease** as the app variant if you want to experience production mode.
4. Sync and build the project.

Note:
There are several build flavors defined in the core module's `build.gradle` file.
- `prod`: Uses production gateway mode. The ```credentials.properties``` file should contain values for `MERCHANT_USERNAME`, `MERCHANT_SITE_REFERENCE`, and `MERCHANT_JWT_KEY`.
- `stageEU`: Uses production gateway mode unless `STAGE_GATEWAY_URL` is not defined in the ```credentials.properties``` file. You need to have values for `MERCHANT_USERNAME_EU`, `MERCHANT_SITE_REFERENCE_EU`, and `MERCHANT_JWT_KEY_EU` in the ```credentials.properties``` file.
- Please note that the product flavors `stageEU`, `stageUS`, and `dev` are internal to the Trust Payments team.

Now you should be able to run the sample project successfully.

## SDK Usage

### Integrating SDK Artefacts as a Dependency from Maven

In your app-level `build.gradle` file, add the following dependencies:

```groovy
// Trust Payments SDK core dependency
implementation 'com.trustpayments.mobile:core:<latest_version>'

// Optional UI module providing ready-to-use "drop-in" view
implementation 'com.trustpayments.mobile:ui:<latest_version>'
```

You will need to refer to the following resources and include the latest package version numbers in the placeholders above:

- Android SDK Core artifact: https://search.maven.org/artifact/com.trustpayments.mobile/core
- Android SDK UI artifact: https://search.maven.org/artifact/com.trustpayments.mobile/ui

In your root-level `build.gradle` file, add the following dependency to get the Cardinal SDK.

```groovy
allprojects {
    repositories {
        google()
        ...
        maven {
            url  "https://gitlab.com/api/v4/projects/56100229/packages/maven"
            name "GitLab"
            credentials(HttpHeaderCredentials) {
                name = "Private-Token"
                value = <gitlab_token>
            }
            authentication {
                header(HttpHeaderAuthentication)
            }
        }
    }
}
```

Please replace the <gitlab_token> with token shared from support team.

### Integrating SDK to your project using the source code

If you wish to integrate the source code of the SDK into your project, please include both the Core and UI modules.

```groovy
// Trust Payments SDK core dependency
implementation project(":core")

// Optional UI module providing ready-to-use "drop-in" view
implementation project(":ui")
```
Please refer previous section for fetching the latest cardinal version.

In your root-level `build.gradle` file, add the following dependency to get the Cardinal SDK.

```groovy
allprojects {
    repositories {
        google()
        ...
        maven {
            url  "https://gitlab.com/api/v4/projects/56100229/packages/maven"
            name "GitLab"
            credentials(HttpHeaderCredentials) {
                name = "Private-Token"
                value = <gitlab_token>
            }
            authentication {
                header(HttpHeaderAuthentication)
            }
        }
    }
}
```
To learn more about **JSON Web Tokens (JWT)**, please refer to this guide: [Understanding JSON Web Tokens](https://help.trustpayments.com/hc/en-us/articles/4402982320273-JSON-Web-Token).

### Performing a Basic Card Payment

To execute a basic card payment operation, adhere to the following steps:

```kotlin
// Construct PaymentTransactionManager object
val paymentTransactionManager = PaymentTransactionManager(
    context = this@CardPaymentActivity,  // Context of the current activity
    gatewayType = TrustPaymentsGatewayType.EU,  // Specify the TrustPaymentsGatewayType (EU or US)
    isCardinalLive = false,  // Indicates whether to use Cardinal live instance for 3DS authentication
    merchantUsername = BuildConfig.MERCHANT_USERNAME,  // Merchant username for authentication
    isLocationDataConsentGiven = true  // Indicates whether location data consent is given
)

// Builds payload request containing payment information
val transactionPayload = TokenizationPayload(
    siteReference = BuildConfig.SITE_REFERENCE,
    currencyISO3a = "GBP",
    baseAmount = 1050,
    requestTypeDescriptions = listOf(
        RequestType.ThreeDQuery.serializedName,
        RequestType.Auth.serializedName,
    ),
    credentialsOnFile = CredentialsOnFile.SaveForFutureUse,
    parentTransactionReference = null,
)

// Builds JWT Token using transaction payload
val jwtToken = Utils.buildJWT(
    merchantUsername = BuildConfig.MERCHANT_USERNAME,
    payload = transactionPayload.buildRequestPayload(),
)

// Create payment session
val session = paymentTransactionManager.createSession(
    jwtProvider = { jwtToken },
    cardPan = pan,
    cardExpiryDate = expiryDate,
    cardSecurityCode = cvv,
)

// Executes payment request using PaymentTransactionManager
val result = paymentTransactionManager.executeSession(
    session = session,
    activityProvider = activityProvider
)              
```
To learn more about **JSON Web Tokens (JWT)**, please refer to this guide: [Understanding JSON Web Tokens](https://help.trustpayments.com/hc/en-us/articles/4402982320273-JSON-Web-Token).

For more advanced usage scenarios, we encourage you to explore the sample app or refer to our [comprehensive documentation](https://help.trustpayments.com/hc/en-us/articles/4402964728721-Getting-started-with-Android-SDK). These resources provide detailed guidance on handling advanced use cases with the Trust Payments Android SDK.

## Test Card Information

To facilitate testing and integration of the Trust Payments Android SDK, we provide test card information. Test cards mimic different card types, expiration dates, and transaction outcomes, allowing you to thoroughly test your integration.

Below are some example cards:

| Type                          | Card Number      |
|-------------------------------|------------------|
| Frictionless Visa Card        | 4000000000001026 |
| Frictionless MasterCard       | 5200000000001005 |
| Frictionless Amex Card        | 340000000000611  |
| Non-Frictionless Visa Card    | 4111111111111111 |
| Non-Frictionless Visa Card    | 4000000000001091 |
| Non-Frictionless MasterCard   | 5200000000001096 |
| Non-Frictionless Amex Card    | 340000000001098  |
| Unauthenticated Error Visa Card| 4000000000001018 |

Feel free to use these test card numbers along with their respective details to simulate different payment scenarios and ensure the smooth functioning of your integration with the Trust Payments Android SDK.

To test Google Payment checkout flow, refer to this guide: [Getting started with Google Pay for Android SDK](https://help.trustpayments.com/hc/en-us/articles/4404827002385-Getting-started-with-Google-Pay-for-Android-SDK).

## License

The Trust Payments Android SDK is open source and available under the MIT license. See the [LICENSE](LICENSE) file for more info.
