package com.trustpayments.mobile.exampleapp.utils


import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import com.trustpayments.mobile.core.models.JwtResponse
import com.trustpayments.mobile.core.models.api.response.ResponseErrorCode
import com.trustpayments.mobile.core.models.api.response.TransactionResponse
import com.trustpayments.mobile.core.services.transaction.Error
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.util.ResponseParser
import com.trustpayments.mobile.exampleapp.BuildConfig
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.common.jwt.BasePayload
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.security.Keys
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import java.util.Date
import javax.crypto.SecretKey

object Utils {
    private val json = Json { encodeDefaults = true }

    @JvmStatic
    fun buildJWT(merchantUsername: String, payload: Any?): String {
        val key: SecretKey = Keys.hmacShaKeyFor(BuildConfig.JWT_KEY.toByteArray())

        return Jwts.builder()
            .setHeader(Jwts.header().setType("JWT"))
            .setIssuer(merchantUsername)
            .setIssuedAt(Date())
            .claim(
                "payload",
                payload
            )
            .signWith(key, SignatureAlgorithm.HS256)
            .compact()
    }

    @JvmStatic
    fun createRequestPayload(payload: BasePayload) =
        json.encodeToJsonElement(payload).extractedContent

    fun BasePayload.buildRequestPayload() = run {
        json.encodeToJsonElement(this).extractedContent
    }

    @JvmStatic
    fun decodeJwt(jwtToken: String): String? {
        return try {
            val chunks: List<String> = jwtToken.split(".")
            return String(android.util.Base64.decode(chunks[1], 0))
        } catch (e: Exception) {
            "{}"
        }
    }

    @JvmStatic
    fun jwtStringListToDecodedJsonArrayString(jwtStringList: List<String>): String =
        (jwtStringList.fold("[") { acc, s -> "$acc${decodeJwt(s) ?: "{}"}," })
            .dropLast(1) + "]"


    @JvmStatic
    fun findExecuteSessionErrorMessage(
        response: PaymentTransactionManager.Response,
        context: Context
    ): String? {
        if (response.error != null) {
            return when (val error = response.error) {
                is Error.InitializationFailure -> {
                    val parsedResponse = ResponseParser.parse(error.jwt)
                    parsedResponse?.responses?.first()?.errorMessage + ": " +
                            parsedResponse?.responses?.first()?.errorData?.first().orEmpty()
                }

                is Error.SafetyError -> error.errorMessage(context)
                is Error.ThreeDSFailure ->
                    context.getString(R.string.hs_3dsec_transaction_failure_message)

                else -> context.getString(R.string.hs_transaction_failed)
            }
        }

        val parsedResponse = ResponseParser.parse(response.responseJwtList)
            ?: return context.getString(R.string.hs_transaction_failed_parsing_dialog_message)

        val allResponses: List<TransactionResponse> =
            parsedResponse.flatMap { r: JwtResponse -> r.responses }
        val errorResponse = allResponses.find { it.errorCode != ResponseErrorCode.Ok }

        return if (errorResponse != null) {
            errorResponse.errorMessage + ":\n" + (errorResponse.errorData
                ?: emptyArray<String>()).joinToString("\n")
        } else {
            null
        }
    }

    @JvmStatic
    fun findExecuteSessionErrorData(
        response: PaymentTransactionManager.Response,
        context: Context
    ): String? {
        if (response.error != null) {
            return when (val error = response.error) {
                is Error.InitializationFailure -> {
                    jwtStringListToDecodedJsonArrayString(listOf(error.jwt))
                }

                is Error.SafetyError -> error.errorMessage(context)
                is Error.ThreeDSFailure ->
                    "{ error: '${context.getString(R.string.hs_3dsec_transaction_failure_message)}' }"

                else -> "{ error: '${context.getString(R.string.hs_transaction_failed)}' }"
            }
        }

        val parsedResponse = ResponseParser.parse(response.responseJwtList)
            ?: return "{ error: '${context.getString(R.string.hs_transaction_failed_parsing_dialog_message)}' }"

        val allResponses: List<TransactionResponse> =
            parsedResponse.flatMap { r: JwtResponse -> r.responses }
        val errorResponse = allResponses.find { it.errorCode != ResponseErrorCode.Ok }

        return if (errorResponse != null) {
            jwtStringListToDecodedJsonArrayString(response.responseJwtList)
        } else {
            null
        }
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showDialog(context: Context, title: String, message: String) {
        val alertDialogBuilder = AlertDialog.Builder(context)
        alertDialogBuilder.setTitle(title)
        alertDialogBuilder.setMessage(message)
        alertDialogBuilder.setPositiveButton(context.getString(R.string.ok_text)) { dialog, _ ->
            dialog.dismiss()
        }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }
}