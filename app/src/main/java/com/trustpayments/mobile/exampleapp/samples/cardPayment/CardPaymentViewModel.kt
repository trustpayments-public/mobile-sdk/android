package com.trustpayments.mobile.exampleapp.samples.cardPayment

import android.app.Activity
import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.util.ResponseParser
import com.trustpayments.mobile.exampleapp.BuildConfig
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.common.jwt.CredentialsOnFile
import com.trustpayments.mobile.exampleapp.common.jwt.TokenizationPayload
import com.trustpayments.mobile.exampleapp.common.jwtexplorer.JWTEditorToken
import com.trustpayments.mobile.exampleapp.domain.RequestType
import com.trustpayments.mobile.exampleapp.utils.Utils
import com.trustpayments.mobile.exampleapp.utils.Utils.buildRequestPayload
import com.trustpayments.mobile.ui.model.PaymentInputType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json

/**
 * View-model associated with Tokenize Payment AUTH Screen.
 * */
class CardPaymentViewModel : ViewModel() {

    // Notify activity regarding different UI use cases.
    internal var uiState: MutableLiveData<UIState> = MutableLiveData()

    // Used to notify JWT Explorer view regarding the request payload in use.
    internal var requestPayload: MutableLiveData<String> = MutableLiveData()

    // Used to notify JWT Explorer view regarding the response payload received by SDK.
    internal var responsePayload: MutableLiveData<String> = MutableLiveData()

    // Request payload that is allowed to edit for the current use case.
    private var editablePayload: TokenizationPayload? = null

    // Credential details of the card.
    private var pan = ""
    private var expiryDate = ""
    private var cvv = ""

    init {
        uiState.postValue(UIState.Idle)
        // Builds payload request. Contains site-reference and payment information.
        editablePayload =
            TokenizationPayload(
                siteReference = BuildConfig.SITE_REFERENCE,
                currencyISO3a = "GBP",
                baseAmount = 1050,
                requestTypeDescriptions = listOf(
                    RequestType.ThreeDQuery.serializedName,
                    RequestType.Auth.serializedName,
                ),
                credentialsOnFile = CredentialsOnFile.SaveForFutureUse,
                parentTransactionReference = null,
            ).apply {
                updateRequestPayloadInfoUI(
                    Json.encodeToString(
                        TokenizationPayload.serializer(),
                        this
                    )
                )
            }
    }

    /**
     * Triggered by user when clicked on Pay Button in UI.
     * */
    fun onPayButtonClicked(
        context: Context,
        paymentTransactionManager: PaymentTransactionManager,
        activityProvider: (() -> Activity)? = null,
    ) {
        uiState.postValue(UIState.Loading)
        if (pan.isBlank() || expiryDate.isBlank() || cvv.isBlank()) {
            uiState.postValue(UIState.Error(context.getString(R.string.provide_card_data)))
            return
        }
        if (editablePayload == null) {
            uiState.postValue(UIState.Error(context.getString(R.string.payload_error)))
            return
        }
        val transactionPayload = editablePayload!!
        // Builds JWT Token using transaction Payload.
        val jwtToken = Utils.buildJWT(
            merchantUsername = BuildConfig.MERCHANT_USERNAME,
            payload = transactionPayload.buildRequestPayload(),
        )
        updateRequestPayloadInfoUI(
            Json.encodeToString(
                TokenizationPayload.serializer(),
                transactionPayload
            )
        )
        updateResponsePayloadInfoUI(emptyList())
        viewModelScope.launch(Dispatchers.IO) {
            // Create payment session.
            val session = paymentTransactionManager.createSession(
                jwtProvider = { jwtToken },
                cardPan = pan,
                cardExpiryDate = expiryDate,
                cardSecurityCode = cvv,
            )
            // Executes payment request using PaymentTransactionManager.
            val result = paymentTransactionManager.executeSession(
                session = session,
                activityProvider = activityProvider
            )
            updateResponsePayloadInfoUI(result.responseJwtList)
            // Process response received by SDK for executed transaction.
            val parsedResult = ResponseParser.parse(result.responseJwtList)

            val errorMessage = Utils.findExecuteSessionErrorMessage(result, context)
            val transactionReference =
                parsedResult?.firstOrNull()
                    ?.customerOutput?.transactionReference
            if (transactionReference.isNullOrBlank() || errorMessage.isNullOrBlank().not()) {
                uiState.postValue(
                    UIState.Error(
                        errorMessage ?: context.getString(R.string.hs_transaction_failed),
                    ),
                )
            } else {
                uiState.postValue(UIState.Success)
            }
        }
    }

    /**
     * Updated view-model in case user modifies Request payload using JWT Explorer View.
     * @param token contains modified request payload.
     * */
    fun onPayloadChanged(token: JWTEditorToken) {
        val json = Json { encodeDefaults = false }
        val payload: TokenizationPayload? =
            try {
                json.decodeFromString(TokenizationPayload.serializer(), token.token)
            } catch (e: Exception) {
                Log.d(TAG, "PayloadChanged: Exception: ${e.localizedMessage}")
                null
            }
        editablePayload = payload
    }

    /**
     * Updates view-model when user enter card details using Drop-In-Payment View.
     * */
    fun onUpdateCardData(paymentInputType: PaymentInputType, input: String) {
        when (paymentInputType) {
            PaymentInputType.PAN -> pan = input
            PaymentInputType.ExpiryDate -> expiryDate = input
            PaymentInputType.CVV -> cvv = input
        }
    }

    /**
     * Notify JWT Explorer view in UI regarding the request payload in use.
     * */
    private fun updateRequestPayloadInfoUI(payload: String) {
        requestPayload.postValue(payload)
    }

    /**
     * Notify JWT Explorer view regarding the response payload received by SDK.
     * */
    private fun updateResponsePayloadInfoUI(jwtResponseList: List<String>) {
        val payload = jwtResponseList.takeIf { it.isNotEmpty() }?.let {
            Utils.jwtStringListToDecodedJsonArrayString(jwtResponseList)
        } ?: ""
        responsePayload.postValue(payload)
    }

    companion object {
        private const val TAG = "CardPaymentVM"
    }

    /**
     * Represents different UI states associated with Tokenize Payment AUTH Screen.
     * */
    sealed class UIState {
        data object Idle : UIState()
        data object Loading : UIState()
        data object Success : UIState()
        class Error(val message: String) : UIState()
    }
}
