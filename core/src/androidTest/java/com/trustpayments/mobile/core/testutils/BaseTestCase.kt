package com.trustpayments.mobile.core.testutils

import android.view.KeyEvent
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import androidx.annotation.IdRes
import androidx.test.espresso.Espresso
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import androidx.test.runner.lifecycle.Stage
import androidx.test.uiautomator.Configurator
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiObject
import androidx.test.uiautomator.UiSelector
import com.trustpayments.mobile.core.services.api.TrustPaymentsGatewayType
import com.trustpayments.mobile.core.test.BuildConfig

open class BaseTestCase {
    protected val codeInputRetries = 3

    val uiDevice: UiDevice by lazy {
        UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    }

    open fun setUp() {
        Configurator.getInstance().waitForIdleTimeout = AutoTestTimeouts.UI_AUTOMATOR_TIMEOUT
        Configurator.getInstance().waitForSelectorTimeout = AutoTestTimeouts.UI_AUTOMATOR_TIMEOUT
        Configurator.getInstance().actionAcknowledgmentTimeout = AutoTestTimeouts.UI_AUTOMATOR_TIMEOUT
        Configurator.getInstance().scrollAcknowledgmentTimeout = AutoTestTimeouts.UI_AUTOMATOR_TIMEOUT

        ActivityLifecycleMonitorRegistry.getInstance().addLifecycleCallback { activity, stage ->
            if(stage == Stage.RESUMED) {
                activity.window.clearFlags(WindowManager.LayoutParams.FLAG_SECURE)
            }
        }
    }

    protected fun handle3DSV1Popup(verificationCode: String) {
        val verificationCodeInput = uiDevice.findObject(UiSelector().className(EditText::class.java))
        verificationCodeInput.setText(verificationCode)
        val submitButton: UiObject = uiDevice.findObject(UiSelector().className(Button::class.java))
        submitButton.click()
    }

    protected fun handle3DSV2Popup(@IdRes codeInputEditTextId: Int, @IdRes submitButtonId: Int, verificationCode: String) {
        val codeInputMatcher = ViewMatchers.withId(codeInputEditTextId)

        waitUntilElementIsDisplayed(codeInputMatcher)

        var retryCounter = 0
        do {
            clearEditText(Espresso.onView(codeInputMatcher))
            Espresso.onView(codeInputMatcher).perform(ViewActions.typeText(verificationCode))
            retryCounter++
        } while (retryCounter <= codeInputRetries)

        Espresso.onView(ViewMatchers.withId(submitButtonId))
            .perform(ViewActions.closeSoftKeyboard(), ViewActions.click())
    }

    private fun clearEditText(view: ViewInteraction) {
        view.perform(ViewActions.click())
            .perform(ViewActions.pressKey(KeyEvent.KEYCODE_DEL))
            .perform(ViewActions.pressKey(KeyEvent.KEYCODE_DEL))
            .perform(ViewActions.pressKey(KeyEvent.KEYCODE_DEL))
            .perform(ViewActions.pressKey(KeyEvent.KEYCODE_DEL))
    }

    protected fun getGatewayType() = if (BuildConfig.GATEWAY === "US") {
        TrustPaymentsGatewayType.US
    } else {
        TrustPaymentsGatewayType.EU
    }
}