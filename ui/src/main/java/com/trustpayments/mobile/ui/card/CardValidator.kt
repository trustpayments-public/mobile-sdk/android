package com.trustpayments.mobile.ui.card

import com.trustpayments.mobile.ui.card.model.CardType
import java.util.*

internal class CardValidator {

    @JvmSynthetic
    fun getCardType(input: String): CardType {
        var cardNumber = input

        // check if number is longer then 2 or if it's not a Visa card
        if (cardNumber.length < 2 && cardNumber != "4" ) {
            return CardType.Unknown
        }

        // if input is shorter than 6 characters fill missing characters with '0'
        cardNumber = cardNumber.padEnd(6, '0')

        // limit to 6 digits if longer value is provided
        cardNumber = cardNumber.take(6)

        // go through ranges of specified card types and find a correct one (or return unknown)
        val iin = cardNumber.toInt()
        for(cardType in CardType.values()) {
            for(iinRange in CardInfo.getIinRanges(cardType)) {
                if (iin in iinRange) {
                    return cardType
                }
            }
        }

        return CardType.Unknown
    }

    /**
     * Validates a card number (PAN) by using the Luhn algorithm.
     *
     * @param cardNumber the card PAN number.
     *
     * @return True if the card number was compliant with the Luhn algorithm, False otherwise.
     */
    @JvmSynthetic
    fun checkWithLuhn(cardNumber: String): Boolean {
        var checksum: Int = 0

        for (i in cardNumber.length - 1 downTo 0 step 2) {
            checksum += cardNumber[i] - '0'
        }
        for (i in cardNumber.length - 2 downTo 0 step 2) {
            val n: Int = (cardNumber[i] - '0') * 2
            checksum += if (n > 9) n - 9 else n
        }

        return checksum%10 == 0
    }

    @JvmSynthetic
    fun isNumberLengthValid(cardNumber: String, cardType: CardType): Boolean =
        cardNumber.length in CardInfo.getValidNumberLengths(cardType)

    @JvmSynthetic
    fun isExpiryDateValid(month: String, year: String): Boolean {
        if (month.length < 2 || year.length < 2){
            return false
        }

        val calendar = Calendar.getInstance()
        val currentMonth = calendar.get(Calendar.MONTH) + 1
        val currentYear = calendar.get(Calendar.YEAR)
        val expiryMonth = month.toInt()
        val expiryYear = year.toInt() + 2000
        if (expiryMonth > 12) {
            return false
        }
        if (expiryYear < currentYear ||
            (expiryYear == currentYear && expiryMonth < currentMonth)) {
            return false
        }

        return true
    }

    @JvmSynthetic
    fun isCvvValid(cvv: String, cardType: CardType) =
        cvv.length == CardInfo.getCvvLength(cardType)
}