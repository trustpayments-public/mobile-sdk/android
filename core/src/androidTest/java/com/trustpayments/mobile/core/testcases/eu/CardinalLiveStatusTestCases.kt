package com.trustpayments.mobile.core.testcases.eu

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.rule.ActivityTestRule
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.services.transaction.Error
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.services.transaction.ThreeDSecureError
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.testutils.createSessionFor
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.util.PaymentSessionResponse
import com.trustpayments.mobile.core.util.parse
import com.trustpayments.mobile.core.utils.ThreeDQueryTestActivity
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNull
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CardinalLiveStatusTestCases: BaseTestCase() {

    private lateinit var jwtToken: String

    private lateinit var activity: ThreeDQueryTestActivity

    private val app = ApplicationProvider.getApplicationContext<Context>().applicationContext

    @get:Rule
    val initialActivityTestRule = ActivityTestRule(ThreeDQueryTestActivity::class.java)

    private val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    @Before
    override fun setUp() {
        super.setUp()

        jwtToken = jwtBuilder.getFor3DSecure(listOf(RequestType.ThreeDQuery))

        activity = initialActivityTestRule.activity
    }

    @Test
    fun test_positive_threeDSecureSucceedsWithStagingCardinal() = runBlocking {
        // GIVEN
        val isCardinalLive = false
        val paymentTransactionManager =
            PaymentTransactionManager(app, getGatewayType(), isCardinalLive, BuildConfig.MERCHANT_USERNAME, isLocationDataConsentGiven = false)
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()

        // THEN
        Assert.assertTrue(sessionResult is PaymentSessionResponse.Success)
    }

    @Test
    fun test_negative_threeDSecureFailsWithLiveCardinal() = runBlocking {
        // GIVEN
        val isCardinalLive = true
        val paymentTransactionManager =
            PaymentTransactionManager(app, getGatewayType(), isCardinalLive, BuildConfig.MERCHANT_USERNAME, isLocationDataConsentGiven = false)
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000001005", "123")

        paymentTransactionManager.isCardinalLive = false //needed to disable cardinal warnings

        // WHEN
        val result =
            paymentTransactionManager.executeSession(session) { activity }

        // THEN
        val error = result.error as Error.InitializationFailure
        val errorCode = error.initializationError as ThreeDSecureError.GeneralError
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(1010, errorCode.errorCode)
    }
}