package com.trustpayments.mobile.exampleapp.pageobjects

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import com.trustpayments.mobile.exampleapp.R
class AdvancedFeaturePage : BaseAppPage(){

    private val recyclerView = ViewMatchers.withId(R.id.samples_list)
    private lateinit var context: Context



    fun tapBackgroundRequest() {
        Espresso.onView(ViewMatchers.withId(R.id.samples_list))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0,
                    ViewActions.click()
                )
            )
    }

    fun tapAccountVerificationWithLaterAuth() {
        Espresso.onView(ViewMatchers.withId(R.id.samples_list))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    1,
                    ViewActions.click()
                )
            )
    }

    fun clickRunButton(){
        Espresso.onView(ViewMatchers.withId(R.id.btn_run)).perform(ViewActions.click())
    }
}