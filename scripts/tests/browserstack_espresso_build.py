import argparse
import json
import os
import sys
from time import time, sleep

import requests
from requests.adapters import HTTPAdapter
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.util.retry import Retry
from typing import List

# Add common cicd_libraries to path.
sys.path.append(os.path.abspath(__file__).split('/scripts')[0] + '/.cicd_scripts')
from cicd_libraries.initialize_logging import logging

ESPRESSO_BUILD_TIMEOUT_SECONDS = 60 * 60
ESPRESSO_FAILED_STATUS = ('failed', 'error', 'timedout')
ESPRESSO_SUCCESS_STATUS = 'passed'
BROWSERSTACK_API_URL = 'https://api-cloud.browserstack.com/app-automate'


def _browserstack_espresso_upload_file(url, user, access_key, file_path):
    with open(file_path, 'rb') as file:
        return _browserstack_post(url, user, access_key, files={'file': file})


def _browserstack_get(url, user, access_key, params=None, data=None, raise_for_status=True):
    auth = HTTPBasicAuth(user, access_key)
    response = _gen_session().get(url, auth=auth, params=params, data=data)
    return _browserstack_api_response(response, raise_for_status)


def _browserstack_post(url, user, access_key, files=None, params=None, json_data=None, raise_for_status=True):
    auth = HTTPBasicAuth(user, access_key)
    response = _gen_session().post(url, auth=auth, files=files, params=params, json=json_data)
    return _browserstack_api_response(response, raise_for_status)


def _browserstack_api_response(response, should_raise_for_status=True):
    logging.debug(f'response.text: {response.text}')
    if should_raise_for_status:
        response.raise_for_status()
    return response.status_code, json.loads(response.text)


def _gen_session():
    session = requests.Session()
    retry = Retry(connect=3, backoff_factor=0.5)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


def _get_command_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--browserstack-device', type=str, dest='browserstack_device', required=True)
    parser.add_argument('--browserstack-user', type=str, dest='browserstack_user', required=True)
    parser.add_argument('--browserstack-access-key', type=str, dest='browserstack_access_key', required=True)
    parser.add_argument('--app-file', type=str, dest='app_file', required=True)
    parser.add_argument('--tests-file', type=str, dest='tests_file', required=True)
    parser.add_argument('--google-username', type=str, dest='google_username', required=True)
    parser.add_argument('--google-password', type=str, dest='google_password', required=True)
    parser.add_argument('--number-of-shards', type=int, dest='number_of_shards', default=1)
    parser.add_argument('--tests-class', type=str, dest='tests_class', required=False)

    return parser.parse_args()


def _validate_parameters(args):
    if not args.app_file:
        ValueError('APP file path is empty')

    if not args.tests_file:
        ValueError('Tests file path is empty')

    if not args.browserstack_device:
        ValueError('Browserstack device is empty')


def _wait_for_espresso_build(user, access_key, build_id: str):
    start = time()
    while True:
        _, buildResponse = _browserstack_get(
            url=f'{BROWSERSTACK_API_URL}/espresso/v2/builds/{build_id}',
            user=user,
            access_key=access_key,
        )

        espresso_devices = buildResponse['devices']

        for device in espresso_devices:
            espresso_sessions = device['sessions']

            for session in espresso_sessions:
                session_id = session['id']

                _, sessionResponse = _browserstack_get(
                    url=f'{BROWSERSTACK_API_URL}/espresso/v2/builds/{build_id}/sessions/{session_id}',
                    user=user,
                    access_key=access_key,
                )

                failedTestCasesNumber = sessionResponse['testcases']['status']['failed']
                timedoutTestCasesNumber = sessionResponse['testcases']['status']['timedout']
                errorTestCasesNumber = sessionResponse['testcases']['status']['error']

                logging.info(f'Espresso build: {build_id} failed: {failedTestCasesNumber} timedout: {timedoutTestCasesNumber} error: {errorTestCasesNumber}')

                if failedTestCasesNumber > 0:
                    raise RuntimeError(f'Espresso build: {build_id} failed')
                elif timedoutTestCasesNumber > 0:
                    raise RuntimeError(f'Espresso build: {build_id} failed')
                elif errorTestCasesNumber > 0:
                    raise RuntimeError(f'Espresso build: {build_id} failed')

        espresso_build_status = buildResponse['status']
        logging.info(f'Espresso build: {build_id} status: {espresso_build_status}')

        if espresso_build_status in ESPRESSO_FAILED_STATUS:
            raise RuntimeError(f'Espresso build: {build_id} failed with status: {espresso_build_status}')
        elif espresso_build_status == ESPRESSO_SUCCESS_STATUS:
            break

        if time() - start > ESPRESSO_BUILD_TIMEOUT_SECONDS:
            raise TimeoutError(f'Espresso build: {build_id} timeout: {ESPRESSO_BUILD_TIMEOUT_SECONDS}s')

        sleep(15)

    return buildResponse

def main():
    args = _get_command_args()
    logging.info('Browserstack espresso build')

    logging.info(f'browserstack-device: {args.browserstack_device}')
    logging.info(f'app-file: {args.app_file}')
    logging.info(f'tests-file: {args.tests_file}')
    logging.info(f'tests-class: {args.tests_class}')

    logging.info('Validate parameters')
    _validate_parameters(args)

    logging.info(f'Upload file: {args.app_file} to browserstack')
    _, response = _browserstack_espresso_upload_file(
        url=f'{BROWSERSTACK_API_URL}/espresso/v2/app',
        user=args.browserstack_user,
        access_key=args.browserstack_access_key,
        file_path=args.app_file
    )
    app_url = response['app_url']

    logging.info(f'Upload file: {args.tests_file} to browserstack')
    _, response = _browserstack_espresso_upload_file(
        url=f'{BROWSERSTACK_API_URL}/espresso/v2/test-suite',
        user=args.browserstack_user,
        access_key=args.browserstack_access_key,
        file_path=args.tests_file
    )
    test_url = response['test_suite_url']

    logging.info(f'Trigger new build with apk: {app_url} and tests: {test_url}')

    json_data_with_tests_class = {
        'devices': [args.browserstack_device],
        'app': app_url,
        'deviceLogs': True,
        'testSuite': test_url,
        'coverage': True,
        'class': [args.tests_class]
    }
    json_data_without_tests_class = {
        'devices': [args.browserstack_device],
        'app': app_url,
        'deviceLogs': True,
        'testSuite': test_url,
        'coverage': True
    }

    _, response = _browserstack_post(
        url=f'{BROWSERSTACK_API_URL}/espresso/v2/build',
        user=args.browserstack_user,
        access_key=args.browserstack_access_key,
        json_data=(json_data_without_tests_class if args.tests_class is None else json_data_with_tests_class)
    )

    espresso_build_id = response['build_id']
    espresso_build_url = f'https://app-automate.browserstack.com/dashboard/v2/builds/{espresso_build_id}'

    logging.info(f'Waiting for espresso build: {espresso_build_url} to finish ...')
    _wait_for_espresso_build(
        user=args.browserstack_user,
        access_key=args.browserstack_access_key,
        build_id=espresso_build_id
    )

    logging.info(f'Browserstack espresso build: {espresso_build_url} finished successfully')


if __name__ == '__main__':
    main()
