package com.trustpayments.mobile.core.testcases.eu.cachetokenise

import android.content.Context
import android.text.TextUtils
import androidx.test.core.app.ApplicationProvider
import androidx.test.rule.ActivityTestRule
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.models.api.response.ResponseErrorCode
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.utils.ThreeDQueryTestActivity
import com.trustpayments.mobile.core.testutils.TrustPaymentsJsonApiService
import com.trustpayments.mobile.core.testutils.createSessionFor
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.util.ResponseParser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CacheTokeniseTestCases: BaseTestCase() {

    private lateinit var paymentTransactionManager: PaymentTransactionManager

    @get:Rule
    val initialActivityTestRule = ActivityTestRule(ThreeDQueryTestActivity::class.java)

    private val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    @Before
    override fun setUp() {
        super.setUp()

        val app = ApplicationProvider.getApplicationContext<Context>().applicationContext
        paymentTransactionManager =
            PaymentTransactionManager(app, getGatewayType(), false, BuildConfig.MERCHANT_USERNAME, isLocationDataConsentGiven = false)
    }

    @Test
    fun positive_cacheTokenise() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getStandard(listOf(
            RequestType.CacheTokenise
        ))

        val session = paymentTransactionManager.createSessionFor(jwtToken, "4111111111111111", "123")

        // WHEN
        lateinit var jwtSessionResult: PaymentTransactionManager.Response
        val jwtJob = launch(Dispatchers.Unconfined) {
            jwtSessionResult =
                paymentTransactionManager.executeSession(session)
        }

        jwtJob.join()

        val parsedResponse = ResponseParser.parse(jwtSessionResult.responseJwtList.first())
        val cacheToken = parsedResponse!!.responses.first().cacheToken

        assertEquals(1, parsedResponse.responses.size)
        assertFalse(TextUtils.isEmpty(cacheToken))

        // GIVEN
        lateinit var jsonWebResult: TrustPaymentsJsonApiService.Result
        val jsonJob = launch(Dispatchers.Unconfined) {
            val apiService = TrustPaymentsJsonApiService()
            jsonWebResult = apiService.performRefund(cacheToken!!, arrayOf("REFUND"), "GBP", "1100")
        }

        jsonJob.join()

        assertNotNull(jsonWebResult)

        val successJsonWebResult = jsonWebResult as TrustPaymentsJsonApiService.Result.Success

        assertEquals("Ok", successJsonWebResult.transactionResponses[0].errorMessage)
        assertEquals(ResponseErrorCode.Ok, successJsonWebResult.transactionResponses[0].errorCode)
        assertEquals("REFUND", successJsonWebResult.transactionResponses[0].requestTypeDescription)
    }

    @Test
    fun positive_cacheTokeniseWithAddressesAndMainAmount() = runBlocking {
        // GIVEN
        val jwtToken = jwtBuilder.getStandardWithMainAmountAndAddress(listOf(
            RequestType.CacheTokenise
        ), null, null, null,
        null,
        "Ian",
        null,
        "Hewertson",
        null,
        "102, Bridgwater Road",
        "Taunton",
        "Blackpool",
        "GB",
        "TA2 8BE",
        "test2@test.pl",
        "01823 335258",
        null,
        "Hollie",
        null,
        "Artist",
        null,
        "Merrick Cottage, 194",
        null,
        "Bedford",
        "GB",
        "MK42 9YD",
        "test@test.pl",
        "07810 307057")

        val session = paymentTransactionManager.createSessionFor(jwtToken, "4111111111111111", "123")

        // WHEN
        lateinit var jwtSessionResult: PaymentTransactionManager.Response
        val jwtJob = launch(Dispatchers.Unconfined) {
            jwtSessionResult =
                paymentTransactionManager.executeSession(session)
        }

        jwtJob.join()

        val parsedResponse = ResponseParser.parse(jwtSessionResult.responseJwtList.first())
        val cacheToken = parsedResponse!!.responses.first().cacheToken

        assertEquals(1, parsedResponse.responses.size)
        assertFalse(TextUtils.isEmpty(cacheToken))

        // GIVEN
        lateinit var jsonWebResult: TrustPaymentsJsonApiService.Result
        val jsonJob = launch(Dispatchers.Unconfined) {
            val apiService = TrustPaymentsJsonApiService()
            jsonWebResult = apiService.performRefund(cacheToken!!, arrayOf("REFUND"), "GBP", "1100")
        }

        jsonJob.join()

        assertNotNull(jsonWebResult)

        val successJsonWebResult = jsonWebResult as TrustPaymentsJsonApiService.Result.Success

        assertEquals("Ok", successJsonWebResult.transactionResponses[0].errorMessage)
        assertEquals(ResponseErrorCode.Ok, successJsonWebResult.transactionResponses[0].errorCode)
        assertEquals("REFUND", successJsonWebResult.transactionResponses[0].requestTypeDescription)
    }
}