package com.trustpayments.mobile.core.testcases.eu

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.rule.ActivityTestRule
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.testutils.createSessionFor
import com.trustpayments.mobile.core.testutils.createThreeDQueryResponse
import com.trustpayments.mobile.core.testutils.getAsFailure
import com.trustpayments.mobile.core.testutils.getAsSuccess
import com.trustpayments.mobile.core.util.JWTBuilder
import com.trustpayments.mobile.core.util.PaymentSessionResponse
import com.trustpayments.mobile.core.util.parse
import com.trustpayments.mobile.core.utils.ThreeDQueryTestActivity
import com.trustpayments.mobile.utils.core.testcardsdata.CardsDataWith3DSecureV1
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test

@Ignore("3DS V1 is no longer supported.")
/**
 * Test cases for the version 1 of 3-D Secure. Taken from the Cardinal Commerce wiki:
 * https://cardinaldocs.atlassian.net/wiki/spaces/CCen/pages/400654355/3DS+1.0+Test+Cases
 */
class CardinalCommerce3DSV1TestCasesIgnored: BaseTestCase() {

    private lateinit var paymentTransactionManager: PaymentTransactionManager

    private lateinit var activity: ThreeDQueryTestActivity
    
    private lateinit var jwtToken: String

    @get:Rule
    val initialActivityTestRule = ActivityTestRule(ThreeDQueryTestActivity::class.java)

    private val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    @Before
    override fun setUp() {
        super.setUp()

        val app = ApplicationProvider.getApplicationContext<Context>().applicationContext
        paymentTransactionManager =
            PaymentTransactionManager(app, getGatewayType(), false, BuildConfig.MERCHANT_USERNAME, isLocationDataConsentGiven = false)

        jwtToken = jwtBuilder.getFor3DSecure(listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        ))

        activity = initialActivityTestRule.activity
    }

    // region Test Case 1: Successful Authentication
    @Test
    fun test_positive_successfulAuthenticationForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000000007", "123")
        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = "Y",
            eciFlag = "02"
        )

        // WHEN
        lateinit var sessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }.parse()
        }

        handle3DSV1Popup(CardsDataWith3DSecureV1.passwordOnWebView)

        job.join()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNotNull(actualAuthTransactionResponse.xid)
        assertNotNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }

    @Test
    fun test_positive_successfulAuthenticationForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000003961", "1234")
        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = "Y",
            eciFlag = "05"
        )

        // WHEN
        lateinit var sessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }.parse()
        }

        handle3DSV1Popup(CardsDataWith3DSecureV1.passwordOnWebView)

        job.join()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNotNull(actualAuthTransactionResponse.xid)
        assertNotNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }
    // endregion

    // region Test Case 2: Failed Signature
    @Test
    fun test_negative_failedSignatureForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000000015", "123")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = null,
            eciFlag = null
        )

        // WHEN
        lateinit var sessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }.parse()
        }

        handle3DSV1Popup(CardsDataWith3DSecureV1.passwordOnWebView)

        job.join()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsFailure(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualTDQTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualTDQTransactionResponse.status)
        assertNull(actualTDQTransactionResponse.xid)
        assertNull(actualTDQTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualTDQTransactionResponse.eci)
    }

    @Test
    fun test_negative_failedSignatureForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000006022", "1234")
        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = null,
            eciFlag = null
        )

        // WHEN
        lateinit var sessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }.parse()
        }

        handle3DSV1Popup(CardsDataWith3DSecureV1.passwordOnWebView)

        job.join()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsFailure(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualTDQTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualTDQTransactionResponse.status)
        assertNull(actualTDQTransactionResponse.xid)
        assertNull(actualTDQTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualTDQTransactionResponse.eci)
    }
    // endregion

    // region Test Case 3: Failed Authentication
    @Test
    fun test_negative_failedAuthenticationForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000000023", "123")
        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = null,
            eciFlag = null
        )

        // WHEN
        lateinit var sessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }.parse()
        }

        handle3DSV1Popup(CardsDataWith3DSecureV1.passwordOnWebView)

        job.join()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsFailure(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualTDQTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualTDQTransactionResponse.status)
        assertNull(actualTDQTransactionResponse.xid)
        assertNull(actualTDQTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualTDQTransactionResponse.eci)
    }

    @Test
    fun test_negative_failedAuthenticationForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000000033", "1234")
        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = null,
            eciFlag = null
        )

        // WHEN
        lateinit var sessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }.parse()
        }

        handle3DSV1Popup(CardsDataWith3DSecureV1.passwordOnWebView)

        job.join()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsFailure(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualTDQTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualTDQTransactionResponse.status)
        assertNull(actualTDQTransactionResponse.xid)
        assertNull(actualTDQTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualTDQTransactionResponse.eci)
    }
    // endregion

    // region Test Case 4: Attempts/Non-Participating
    @Test
    fun test_positive_attemptsNonParticipatingForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000000908", "123")
        
        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = "A",
            eciFlag = "01"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNotNull(actualAuthTransactionResponse.xid)
        assertNotNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }

    @Test
    fun test_positive_attemptsNonParticipatingForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000003391", "1234")
        
        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = "A",
            eciFlag = "06"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNotNull(actualAuthTransactionResponse.xid)
        assertNotNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }
    // endregion

    // region Test Case 5: Timeout
    @Test
    fun test_negative_timeoutForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000000049", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()

        val actualTDQTransactionResponse = sessionResult.getAsFailure(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        // expecting no params
    }

    @Test
    fun test_negative_timeoutForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000008309", "1234")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()

        val actualTDQTransactionResponse = sessionResult.getAsFailure(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        // expecting no params
    }
    // endregion

    // region Test Case 6: Not Enrolled
    @Test
    fun test_negative_notEnrolledForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000000056", "123")
        
        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "N",
            paresStatus = null,
            eciFlag = "00"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNull(actualAuthTransactionResponse.xid)
        assertNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }

    @Test
    fun test_negative_notEnrolledForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000008135", "1234")
        
        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "N",
            paresStatus = null,
            eciFlag = "07"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNull(actualAuthTransactionResponse.xid)
        assertNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }
    // endregion

    // region Test Case 7: Unavailable
    @Test
    fun test_negative_unavailableForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000000064", "123")
        
        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "U",
            paresStatus = null,
            eciFlag = "00"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNull(actualAuthTransactionResponse.xid)
        assertNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }

    @Test
    fun test_negative_unavailableForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000007780", "1234")
        
        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "U",
            paresStatus = null,
            eciFlag = "07"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNull(actualAuthTransactionResponse.xid)
        assertNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }
    // endregion

    // region Test Case 8: Merchant Not Active
    @Test
    fun test_negative_merchantNotActiveForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000000072", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()

        val actualTDQTransactionResponse = sessionResult.getAsFailure(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        // expecting no params
    }

    @Test
    fun test_negative_merchantNotActiveForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000008416", "1234")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()

        val actualTDQTransactionResponse = sessionResult.getAsFailure(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        // expecting no params
    }
    // endregion

    // region Test Case 9: cmpi_lookup error
    @Test
    fun test_negative_cmpiLookupErrorForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000000080", "123")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()

        val actualTDQTransactionResponse = sessionResult.getAsFailure(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        // expecting no params
    }

    @Test
    fun test_negative_cmpiLookupErrorForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000006337", "1234")

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()

        val actualTDQTransactionResponse = sessionResult.getAsFailure(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        // expecting no params
    }
    // endregion

    // region Test Case 10: cmpi_authenticate error
    @Test
    fun test_negative_cmpiAuthenticateErrorForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000000098", "123")
        
        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = null,
            eciFlag = null
        )

        // WHEN
        lateinit var sessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }.parse()
        }

        handle3DSV1Popup(CardsDataWith3DSecureV1.passwordOnWebView)

        job.join()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsFailure(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualTDQTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualTDQTransactionResponse.status)
        assertNull(actualTDQTransactionResponse.xid)
        assertNull(actualTDQTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualTDQTransactionResponse.eci)
    }

    @Test
    fun test_negative_cmpiAuthenticateErrorForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000009299", "1234")
        
        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = null,
            eciFlag = null
        )

        // WHEN
        lateinit var sessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }.parse()
        }

        handle3DSV1Popup(CardsDataWith3DSecureV1.passwordOnWebView)

        job.join()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsFailure(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualTDQTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualTDQTransactionResponse.status)
        assertNull(actualTDQTransactionResponse.xid)
        assertNull(actualTDQTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualTDQTransactionResponse.eci)
    }
    // endregion

    // region Test Case 11: Authentication Unavailable
    @Test
    fun test_negative_authenticationUnavailableForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200000000000031", "123")
        
        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = "U",
            eciFlag = "00"
        )

        // WHEN
        lateinit var sessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }.parse()
        }

        handle3DSV1Popup(CardsDataWith3DSecureV1.passwordOnWebView)

        job.join()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNotNull(actualAuthTransactionResponse.xid)
        assertNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }

    @Test
    fun test_negative_authenticationUnavailableForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340000000000116", "1234")
        
        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "Y",
            paresStatus = "U",
            eciFlag = "07"
        )

        // WHEN
        lateinit var sessionResult: PaymentSessionResponse
        val job = launch(Dispatchers.Unconfined) {
            sessionResult =
                paymentTransactionManager.executeSession(session) { activity }.parse()
        }

        handle3DSV1Popup(CardsDataWith3DSecureV1.passwordOnWebView)

        job.join()
        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        // THEN
        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNotNull(actualAuthTransactionResponse.xid)
        assertNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }
    // endregion

    // region Test Case 12: Bypassed Authentication
    @Test
    fun test_positive_bypassedAuthenticationForMastercard() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "5200990000000009", "123")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "B",
            paresStatus = null,
            eciFlag = "00"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
            as PaymentSessionResponse.Success

        // THEN
        assertEquals(1, sessionResult.responseParts.size)
        assertEquals(2, sessionResult.allResponses.size)

        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNull(actualAuthTransactionResponse.xid)
        assertNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }

    @Test
    fun test_positive_bypassedAuthenticationForAmex() = runBlocking {
        // GIVEN
        val session =
            paymentTransactionManager.createSessionFor(jwtToken, "340099000000001", "1234")

        val expectedTransactionResponse = createThreeDQueryResponse(
            enrolled = "B",
            paresStatus = null,
            eciFlag = "07"
        )

        // WHEN
        val sessionResult =
            paymentTransactionManager.executeSession(session) { activity }.parse()
            as PaymentSessionResponse.Success

        // THEN
        assertEquals(1, sessionResult.responseParts.size)
        assertEquals(2, sessionResult.allResponses.size)

        val actualTDQTransactionResponse = sessionResult.getAsSuccess(0)
        val actualAuthTransactionResponse = sessionResult.getAsSuccess(1)

        assertEquals(expectedTransactionResponse.enrolled, actualAuthTransactionResponse.enrolled)
        assertEquals(expectedTransactionResponse.paresStatus, actualAuthTransactionResponse.status)
        assertNull(actualAuthTransactionResponse.xid)
        assertNull(actualAuthTransactionResponse.cavv)
        assertEquals(expectedTransactionResponse.eciFlag, actualAuthTransactionResponse.eci)
    }
    // endregion
}