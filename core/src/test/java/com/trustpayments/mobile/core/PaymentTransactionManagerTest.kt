package com.trustpayments.mobile.core

import android.app.Activity
import android.content.Context
import android.os.Build
import com.google.gson.Gson
import com.trustpayments.mobile.core.models.DeviceSafetyWarning
import com.trustpayments.mobile.core.models.api.jwt.JwtToken
import com.trustpayments.mobile.core.models.api.response.CustomerOutput
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.models.api.response.ResponseErrorCode
import com.trustpayments.mobile.core.models.api.response.ResponseLiveStatus
import com.trustpayments.mobile.core.models.api.response.ResponseSettlementStatus
import com.trustpayments.mobile.core.services.api.TrustPaymentsApiService
import com.trustpayments.mobile.core.services.api.TrustPaymentsGatewayType
import com.trustpayments.mobile.core.services.transaction.ActivityResultProvider
import com.trustpayments.mobile.core.services.transaction.Error
import com.trustpayments.mobile.core.services.transaction.LoggingManager
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.services.transaction.ThreeDSecureError
import com.trustpayments.mobile.core.services.transaction.ThreeDSecureManager
import com.trustpayments.mobile.core.util.BasePayload
import com.trustpayments.mobile.core.util.JWTBuilder
import io.mockk.MockKAnnotations
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.just
import io.mockk.mockkObject
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import kotlin.reflect.full.declaredMemberFunctions
import kotlin.reflect.jvm.isAccessible

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.M])
class PaymentTransactionManagerTest {
    private val gson = Gson()

    private lateinit var paymentTransactionManager: PaymentTransactionManager

    @MockK
    private lateinit var mockContext: Context

    @MockK
    private lateinit var mockApiService: TrustPaymentsApiService

    @MockK
    private lateinit var mockThreeDSecureManager: ThreeDSecureManager

    @MockK
    private lateinit var mockLoggingManager: LoggingManager

    @MockK
    private lateinit var mockActivity: Activity

    @MockK
    private lateinit var mockActivityResultProvider: ActivityResultProvider

    private val mockActivityProvider = { mockActivity }

    private val MERCHANT_USERNAME = "testUsername"

    private val REQUEST_CARD_PAN = "1111111111111111"
    private val REQUEST_CARD_EXPIRY_DATE = "12/23"
    private val REQUEST_CARD_CVV = "123"
    private val RESPONSE_ACS_URL = "acsUrl"
    private val RESPONSE_THREEDPAYLOAD = "threeDPayload"
    private val RESPONSE_THREEDVERSION = "threeDVersion"
    private val RESPONSE_THREEDV1_PARES = "pares"
    private val RESPONSE_THREEDV2_THREEDRESPONSE = "3dresponse"
    private val RESPONSE_ACQUIRERTRANSACTIONREFERENCE = "acquirerTransactionReference"

    private lateinit var successCardinalExecutionV1Response: ThreeDSecureManager.ExecutionResult.SuccessV1
    private lateinit var successCardinalExecutionV2Response: ThreeDSecureManager.ExecutionResult.SuccessV2
    private lateinit var errorCardinalExecutionResponse: ThreeDSecureManager.ExecutionResult.Failure

    private val jwtBuilder =
        JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    private val successCardinalInitializationResponse =
        ThreeDSecureManager.InitializationResult.Success("payload")
    private val errorCardinalInitializationResponse =
        ThreeDSecureManager.InitializationResult.Failure(ThreeDSecureError.Unknown)

    @Before
    fun setup() {
        MockKAnnotations.init(this)

        mockkObject(ThreeDSecureManager.Companion)
        every {
            ThreeDSecureManager.Companion.getInstance(
                any(),
                any(),
                any(),
                any(),
                any(),
                any()
            )
        } returns mockThreeDSecureManager

        mockkObject(TrustPaymentsApiService.Companion)
        every {
            TrustPaymentsApiService.Companion.getInstance(
                any(),
                any(),
                any()
            )
        } returns mockApiService

        mockkObject(LoggingManager.Companion)
        every { LoggingManager.Companion.getInstance(any()) } returns mockLoggingManager

        paymentTransactionManager = PaymentTransactionManager(
            mockContext,
            TrustPaymentsGatewayType.EU,
            false,
            MERCHANT_USERNAME,
            isLocationDataConsentGiven = false
        )

        successCardinalExecutionV1Response =
            ThreeDSecureManager.ExecutionResult.SuccessV1(RESPONSE_THREEDV1_PARES)
        successCardinalExecutionV2Response =
            ThreeDSecureManager.ExecutionResult.SuccessV2(RESPONSE_THREEDV2_THREEDRESPONSE)
        errorCardinalExecutionResponse =
            ThreeDSecureManager.ExecutionResult.Failure(ThreeDSecureError.Unknown)

        every { mockThreeDSecureManager.createSession() } just Runs
        every { mockThreeDSecureManager.deviceSafetyWarnings } returns emptyList()
        every { mockLoggingManager.sendErrorEvent(any(), any(), any(), any(), any()) } returns Unit
        every { mockLoggingManager.sendExceptionEvent(any()) } returns Unit
        every { mockLoggingManager.init() } returns Unit
        every { mockLoggingManager.release() } returns Unit
    }

    private fun getInitSuccessResponse(responseJwt: String, newJwt: String) =
        TrustPaymentsApiService.Result.Success(
            JwtToken(responseJwt), JwtToken(newJwt),
            listOf(
                com.trustpayments.mobile.core.models.api.response.TransactionResponse(
                    ResponseErrorCode.Ok,
                    "",
                    threeDInit = "SUCCESS",
                    requestTypeDescription = RequestType.JsInit
                )
            )
        )

    private fun getSuccessResponse(
        requestTypes: List<RequestType>,
        responseJwt: String,
        newJwt: String,
        customerOutput: Pair<RequestType, CustomerOutput>? = null
    ) = TrustPaymentsApiService.Result.Success(JwtToken(responseJwt), JwtToken(newJwt),
        requestTypes.map {
            com.trustpayments.mobile.core.models.api.response.TransactionResponse(
                ResponseErrorCode.Ok,
                "",
                requestTypeDescription = it,
                settleStatus = ResponseSettlementStatus.Pending,
                liveStatus = ResponseLiveStatus.NotLive,
                baseAmount = 1050,
                currencyISO3a = "GBP",
                threeDPayload = RESPONSE_THREEDPAYLOAD,
                threeDVersion = RESPONSE_THREEDVERSION,
                acsUrl = RESPONSE_ACS_URL,
                acquirerTransactionReference = RESPONSE_ACQUIRERTRANSACTIONREFERENCE,
                customerOutput = if (customerOutput?.first == it) customerOutput.second else null
            )
        }
    )

    //region Pre-checks
    @Test
    fun test_negative_executionFailsIfEmptyListOfRequests() = runBlocking {
        //GIVEN
        val session = createNormalSession(emptyList())

        //WHEN
        val result = paymentTransactionManager.executeSession(session)

        //THEN
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertTrue(result.error is Error.ParsingError)
    }

    @Test
    fun test_negative_executionFailsInvalidRequestType() = runBlocking {
        //GIVEN
        val session = createNormalSessionRaw(listOf("INVALID_REQUEST_TYPE"))

        //WHEN
        val result = paymentTransactionManager.executeSession(session)

        //THEN
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertTrue(result.error is Error.ParsingError)
    }

    @Test
    fun test_negative_executionFailsIfActivityProviderIsNotProvidedFor3DQRequest() = runBlocking {
        //GIVEN
        val session = createNormalSession(
            listOf(
                RequestType.ThreeDQuery,
                RequestType.Auth
            )
        )

        //WHEN
        val result = paymentTransactionManager.executeSession(session)

        //THEN
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertTrue(result.error is Error.DeveloperError.ActivityProviderRequired)
    }

    @Test
    fun test_negative_executionFailsIfActivityProviderIsNotProvidedFor3DQRequestWithActivityResultProvider() = runBlocking {
        //GIVEN
        val session = createNormalSession(
            listOf(
                RequestType.ThreeDQuery,
                RequestType.Auth
            )
        )

        //WHEN
        val result = paymentTransactionManager.executeSession(session, activityResultProvider = mockActivityResultProvider)

        //THEN
        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertTrue(result.error is Error.DeveloperError.ActivityProviderRequired)
    }

    @Test
    fun test_negative_initFails_apiFailure() = runBlocking {
        //GIVEN
        val session = createNormalSession(
            listOf(
                RequestType.ThreeDQuery,
                RequestType.Auth
            )
        )
        val jwt = session.jwtProvider()

        //WHEN
        coEvery {
            mockApiService.initializeSession(
                any(),
                any(),
                any()
            )
        } returns TrustPaymentsApiService.Result.Failure

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionNeverCalled()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationNeverCalled()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(jwt, session.jwtProvider())

        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertTrue(result.error is Error.InitializationError)
    }

    @Test
    fun test_negative_initFails_apiFailureWithActivityResultProvider() = runBlocking {
        //GIVEN
        val session = createNormalSession(
            listOf(
                RequestType.ThreeDQuery,
                RequestType.Auth
            )
        )
        val jwt = session.jwtProvider()

        //WHEN
        coEvery {
            mockApiService.initializeSession(
                any(),
                any(),
                any()
            )
        } returns TrustPaymentsApiService.Result.Failure

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider,
            mockActivityResultProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionNeverCalled()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationNeverCalled()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(jwt, session.jwtProvider())

        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertTrue(result.error is Error.InitializationError)
    }

    @Test
    fun test_negative_initFails_apiSuccess_errorCodeNotZero() = runBlocking {
        //GIVEN
        val session = createNormalSession(
            listOf(
                RequestType.ThreeDQuery,
                RequestType.Auth
            )
        )
        val jwt = session.jwtProvider()

        //WHEN
        coEvery {
            mockApiService.initializeSession(
                any(),
                any(),
                any()
            )
        } returns TrustPaymentsApiService.Result.Success(
            JwtToken(jwt), JwtToken(jwt), listOf(
                com.trustpayments.mobile.core.models.api.response.TransactionResponse(
                    ResponseErrorCode.InvalidField,
                    "",
                    threeDInit = "THREE_D_INIT"
                )
            )
        )

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionNeverCalled()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationNeverCalled()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(jwt, session.jwtProvider())

        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(jwt, (result.error as Error.InitializationFailure).jwt)
        assertNull((result.error as Error.InitializationFailure).initializationError)
    }


    @Test
    fun test_negative_initFails_apiSuccess_errorCodeNotZeroWithActivityResultProvider() = runBlocking {
        //GIVEN
        val session = createNormalSession(
            listOf(
                RequestType.ThreeDQuery,
                RequestType.Auth
            )
        )
        val jwt = session.jwtProvider()

        //WHEN
        coEvery {
            mockApiService.initializeSession(
                any(),
                any(),
                any()
            )
        } returns TrustPaymentsApiService.Result.Success(
            JwtToken(jwt), JwtToken(jwt), listOf(
                com.trustpayments.mobile.core.models.api.response.TransactionResponse(
                    ResponseErrorCode.InvalidField,
                    "",
                    threeDInit = "THREE_D_INIT"
                )
            )
        )

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider,
            mockActivityResultProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionNeverCalled()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationNeverCalled()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(jwt, session.jwtProvider())

        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(jwt, (result.error as Error.InitializationFailure).jwt)
        assertNull((result.error as Error.InitializationFailure).initializationError)
    }

    @Test
    fun test_negative_initFails_apiSuccess_threeDInitEmpty() = runBlocking {
        //GIVEN
        val session = createNormalSession(
            listOf(
                RequestType.ThreeDQuery,
                RequestType.Auth
            )
        )
        val jwt = session.jwtProvider()

        //WHEN
        coEvery {
            mockApiService.initializeSession(
                any(),
                any(),
                any()
            )
        } returns TrustPaymentsApiService.Result.Success(
            JwtToken(jwt), JwtToken(jwt), listOf(
                com.trustpayments.mobile.core.models.api.response.TransactionResponse(
                    ResponseErrorCode.Ok,
                    ""
                )
            )
        )

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionNeverCalled()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationNeverCalled()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(jwt, session.jwtProvider())

        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(jwt, (result.error as Error.InitializationFailure).jwt)
        assertNull((result.error as Error.InitializationFailure).initializationError)
    }

    @Test
    fun test_negative_initFails_apiSuccess_threeDInitEmptyWithActivityResultProvider() = runBlocking {
        //GIVEN
        val session = createNormalSession(
            listOf(
                RequestType.ThreeDQuery,
                RequestType.Auth
            )
        )
        val jwt = session.jwtProvider()

        //WHEN
        coEvery {
            mockApiService.initializeSession(
                any(),
                any(),
                any()
            )
        } returns TrustPaymentsApiService.Result.Success(
            JwtToken(jwt), JwtToken(jwt), listOf(
                com.trustpayments.mobile.core.models.api.response.TransactionResponse(
                    ResponseErrorCode.Ok,
                    ""
                )
            )
        )

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider,
            mockActivityResultProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionNeverCalled()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationNeverCalled()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(jwt, session.jwtProvider())

        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(jwt, (result.error as Error.InitializationFailure).jwt)
        assertNull((result.error as Error.InitializationFailure).initializationError)
    }

    @Test
    fun test_negative_InitSucceeds_CardinalInitializationFails() = runBlocking {
        //GIVEN
        val session = createNormalSession(
            listOf(
                RequestType.ThreeDQuery,
                RequestType.Auth
            )
        )

        val jwt = session.jwtProvider()

        //WHEN
        apiServiceReturnsSuccessfulInitResult(jwt, jwt)
        threeDSecureManagerReturnsErrorInitializationResult()

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionNeverCalled()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(jwt, session.jwtProvider())

        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(jwt, (result.error as Error.InitializationFailure).jwt)
        assertEquals(
            ThreeDSecureError.Unknown,
            (result.error as Error.InitializationFailure).initializationError
        )
    }

    @Test
    fun test_negative_InitSucceeds_CardinalInitializationFailsWithActivityResultProvider() = runBlocking {
        //GIVEN
        val session = createNormalSession(
            listOf(
                RequestType.ThreeDQuery,
                RequestType.Auth
            )
        )

        val jwt = session.jwtProvider()

        //WHEN
        apiServiceReturnsSuccessfulInitResult(jwt, jwt)
        threeDSecureManagerReturnsErrorInitializationResult()

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider,
            mockActivityResultProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionNeverCalled()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(jwt, session.jwtProvider())

        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(jwt, (result.error as Error.InitializationFailure).jwt)
        assertEquals(
            ThreeDSecureError.Unknown,
            (result.error as Error.InitializationFailure).initializationError
        )
    }

    @Test
    fun test_negative_initFails_apiSuccess_jwtParsingFailed() = runBlocking {
        //GIVEN
        val listOfRequests = listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        )

        val session = createNormalSession(listOfRequests)
        val responseJwt = session.jwtProvider()
        val newJwt = "INVALID_JWT"

        //WHEN
        apiServiceReturnsSuccessfulInitResult(responseJwt, newJwt)
        threeDSecureManagerReturnsSuccessfulInitializationResult()

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionNeverCalled()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(responseJwt, session.jwtProvider())
        assertEquals("", session.intermediateJwt)

        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertTrue(result.error is Error.ParsingError)
    }

    @Test
    fun test_negative_initFails_apiSuccess_jwtParsingFailedWithActivityResultProvider() = runBlocking {
        //GIVEN
        val listOfRequests = listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        )

        val session = createNormalSession(listOfRequests)
        val responseJwt = session.jwtProvider()
        val newJwt = "INVALID_JWT"

        //WHEN
        apiServiceReturnsSuccessfulInitResult(responseJwt, newJwt)
        threeDSecureManagerReturnsSuccessfulInitializationResult()

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider,
            mockActivityResultProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionNeverCalled()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(responseJwt, session.jwtProvider())
        assertEquals("", session.intermediateJwt)

        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertTrue(result.error is Error.ParsingError)
    }

    @Test
    fun test_negative_initFails_apiSuccess_emptyRequestsTypesList() = runBlocking {
        //GIVEN
        val listOfRequests = listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        )

        val session = createNormalSession(listOfRequests)
        val responseJwt = session.jwtProvider()
        val newJwt = jwtBuilder.getStandard(emptyList())

        //WHEN
        apiServiceReturnsSuccessfulInitResult(responseJwt, newJwt)
        threeDSecureManagerReturnsSuccessfulInitializationResult()

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionNeverCalled()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(responseJwt, session.jwtProvider())
        assertEquals("", session.intermediateJwt)

        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertTrue(result.error is Error.ParsingError)
    }

    @Test
    fun test_negative_initFails_apiSuccess_emptyRequestsTypesListWithActivityResultProvider() = runBlocking {
        //GIVEN
        val listOfRequests = listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        )

        val session = createNormalSession(listOfRequests)
        val responseJwt = session.jwtProvider()
        val newJwt = jwtBuilder.getStandard(emptyList())

        //WHEN
        apiServiceReturnsSuccessfulInitResult(responseJwt, newJwt)
        threeDSecureManagerReturnsSuccessfulInitializationResult()

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider,
            mockActivityResultProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionNeverCalled()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(responseJwt, session.jwtProvider())
        assertEquals("", session.intermediateJwt)

        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertTrue(result.error is Error.ParsingError)
    }

    @Test
    fun test_negative_3DQ_firstRequestFails() = runBlocking {
        //GIVEN
        val requests = listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        )

        val session = createNormalSession(requests)
        val jwt = session.jwtProvider()

        //WHEN
        apiServiceReturnsSuccessfulInitResult(jwt, jwt)
        threeDSecureManagerReturnsSuccessfulInitializationResult()
        apiServiceReturnsErrorResult(jwt)

        val result = paymentTransactionManager.executeSession(session, mockActivityProvider)

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionCalledOnce()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(jwt, session.jwtProvider())

        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(result.error, Error.HttpFailure)
    }

    @Test
    fun test_negative_3DQ_firstRequestFailsWithActivityResultProvider() = runBlocking {
        //GIVEN
        val requests = listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        )

        val session = createNormalSession(requests)
        val jwt = session.jwtProvider()

        //WHEN
        apiServiceReturnsSuccessfulInitResult(jwt, jwt)
        threeDSecureManagerReturnsSuccessfulInitializationResult()
        apiServiceReturnsErrorResult(jwt)

        val result = paymentTransactionManager.executeSession(session, mockActivityProvider, mockActivityResultProvider)

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionCalledOnce()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(jwt, session.jwtProvider())

        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(result.error, Error.HttpFailure)
    }

    @Test
    fun test_negative_no3DQ_firstRequestFails() = runBlocking {
        //GIVEN
        val requests = listOf(
            RequestType.Auth
        )

        val session = createNormalSession(requests)
        val jwt = session.jwtProvider()

        //WHEN
        apiServiceReturnsErrorResult(jwt)

        val result = paymentTransactionManager.executeSession(session)

        //THEN
        verifyApiServiceInitializationNeverCalled()
        verifyApiServiceTransactionCalledOnce()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationNeverCalled()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(jwt, session.jwtProvider())

        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(result.error, Error.HttpFailure)
    }

    @Test
    fun test_negative_no3DQ_firstRequestFailsWithActivityResultProvider() = runBlocking {
        //GIVEN
        val requests = listOf(
            RequestType.Auth
        )

        val session = createNormalSession(requests)
        val jwt = session.jwtProvider()

        //WHEN
        apiServiceReturnsErrorResult(jwt)

        val result = paymentTransactionManager.executeSession(session, activityResultProvider = mockActivityResultProvider)

        //THEN
        verifyApiServiceInitializationNeverCalled()
        verifyApiServiceTransactionCalledOnce()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationNeverCalled()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(jwt, session.jwtProvider())

        assertTrue(result.responseJwtList.isEmpty())
        assertNull(result.additionalTransactionResult)
        assertEquals(result.error, Error.HttpFailure)
    }

    @Test
    fun test_positive_AUTHSucceeds() = runBlocking {
        //GIVEN
        val requests = listOf(
            RequestType.Auth
        )

        val session = createNormalSession(requests)
        val responseJwt = session.jwtProvider()
        val newJwt = jwtBuilder.buildResponseJWT(BasePayload(emptyList(), ""))

        //WHEN
        apiServiceReturnsSuccessResult(
            responseJwt, newJwt, listOf(RequestType.Auth), Pair(
                RequestType.Auth, CustomerOutput.Result
            )
        )

        val result = paymentTransactionManager.executeSession(session)

        //THEN
        verifyApiServiceInitializationNeverCalled()
        verifyApiServiceTransactionCalledOnce()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationNeverCalled()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(responseJwt, session.jwtProvider())

        assertEquals(1, result.responseJwtList.size)
        assertEquals(responseJwt, result.responseJwtList[0])
        assertNull(result.additionalTransactionResult)
        assertNull(result.error)
    }

    @Test
    fun test_positive_AUTHSucceedsWithActivity() = runBlocking {
        //GIVEN
        val requests = listOf(
            RequestType.Auth
        )

        val session = createNormalSession(requests)
        val responseJwt = session.jwtProvider()
        val newJwt = jwtBuilder.buildResponseJWT(BasePayload(emptyList(), ""))

        //WHEN
        apiServiceReturnsSuccessResult(
            responseJwt, newJwt, listOf(RequestType.Auth), Pair(
                RequestType.Auth, CustomerOutput.Result
            )
        )

        val result = paymentTransactionManager.executeSession(session, mockActivity)

        //THEN
        verifyApiServiceInitializationNeverCalled()
        verifyApiServiceTransactionCalledOnce()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationNeverCalled()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(responseJwt, session.jwtProvider())

        assertEquals(1, result.responseJwtList.size)
        assertEquals(responseJwt, result.responseJwtList[0])
        assertNull(result.additionalTransactionResult)
        assertNull(result.error)
    }

    @Test
    fun test_positive_AUTHSucceedsWithActivityResultProvider() = runBlocking {
        //GIVEN
        val requests = listOf(
            RequestType.Auth
        )

        val session = createNormalSession(requests)
        val responseJwt = session.jwtProvider()
        val newJwt = jwtBuilder.buildResponseJWT(BasePayload(emptyList(), ""))

        //WHEN
        apiServiceReturnsSuccessResult(
            responseJwt, newJwt, listOf(RequestType.Auth), Pair(
                RequestType.Auth, CustomerOutput.Result
            )
        )

        val result = paymentTransactionManager.executeSession(session, activityResultProvider = mockActivityResultProvider)

        //THEN
        verifyApiServiceInitializationNeverCalled()
        verifyApiServiceTransactionCalledOnce()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationNeverCalled()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(responseJwt, session.jwtProvider())

        assertEquals(1, result.responseJwtList.size)
        assertEquals(responseJwt, result.responseJwtList[0])
        assertNull(result.additionalTransactionResult)
        assertNull(result.error)
    }

    @Test
    fun test_positive_AUTHSucceedsWithActivityResultProviderAndActivity() = runBlocking {
        //GIVEN
        val requests = listOf(
            RequestType.Auth
        )

        val session = createNormalSession(requests)
        val responseJwt = session.jwtProvider()
        val newJwt = jwtBuilder.buildResponseJWT(BasePayload(emptyList(), ""))

        //WHEN
        apiServiceReturnsSuccessResult(
            responseJwt, newJwt, listOf(RequestType.Auth), Pair(
                RequestType.Auth, CustomerOutput.Result
            )
        )

        val result = paymentTransactionManager.executeSession(session, mockActivity, mockActivityResultProvider)

        //THEN
        verifyApiServiceInitializationNeverCalled()
        verifyApiServiceTransactionCalledOnce()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationNeverCalled()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(responseJwt, session.jwtProvider())

        assertEquals(1, result.responseJwtList.size)
        assertEquals(responseJwt, result.responseJwtList[0])
        assertNull(result.additionalTransactionResult)
        assertNull(result.error)
    }

    @Test
    fun test_negative_firstResponseJWTParsingFailed() = runBlocking {
        //GIVEN
        val requests = listOf(
            RequestType.ThreeDQuery
        )

        val session = createNormalSession(requests)
        val jwt = session.jwtProvider()
        val newJwt = jwtBuilder.buildResponseJWT(BasePayload(listOf("ILLEGAL_VALUE"), ""))

        //WHEN
        apiServiceReturnsSuccessfulInitResult(jwt, jwt)
        threeDSecureManagerReturnsSuccessfulInitializationResult()
        apiServiceReturnsSuccessResult(
            jwt,
            newJwt,
            requests,
            Pair(RequestType.ThreeDQuery, CustomerOutput.ThreeDRedirect)
        )

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionCalledOnce()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(jwt, session.jwtProvider())

        assertEquals(1, result.responseJwtList.size)
        assertEquals(jwt, result.responseJwtList[0])
        assertNull(result.additionalTransactionResult)
        assertTrue(result.error is Error.ParsingError)
    }

    @Test
    fun test_negative_firstResponseJWTParsingFailedWithActivityResultProvider() = runBlocking {
        //GIVEN
        val requests = listOf(
            RequestType.ThreeDQuery
        )

        val session = createNormalSession(requests)
        val jwt = session.jwtProvider()
        val newJwt = jwtBuilder.buildResponseJWT(BasePayload(listOf("ILLEGAL_VALUE"), ""))

        //WHEN
        apiServiceReturnsSuccessfulInitResult(jwt, jwt)
        threeDSecureManagerReturnsSuccessfulInitializationResult()
        apiServiceReturnsSuccessResult(
            jwt,
            newJwt,
            requests,
            Pair(RequestType.ThreeDQuery, CustomerOutput.ThreeDRedirect)
        )

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider,
            mockActivityResultProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionCalledOnce()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionNeverCalled()

        assertEquals(jwt, session.jwtProvider())

        assertEquals(1, result.responseJwtList.size)
        assertEquals(jwt, result.responseJwtList[0])
        assertNull(result.additionalTransactionResult)
        assertTrue(result.error is Error.ParsingError)
    }

    @Test
    fun test_negative_challengeFails() = runBlocking {
        //GIVEN
        val requests = listOf(
            RequestType.ThreeDQuery
        )

        val session = createNormalSession(requests)
        val jwt = session.jwtProvider()
        val newJwt = jwtBuilder.buildResponseJWT(BasePayload(emptyList(), ""))

        //WHEN
        apiServiceReturnsSuccessfulInitResult(jwt, jwt)
        threeDSecureManagerReturnsSuccessfulInitializationResult()
        apiServiceReturnsSuccessResult(
            jwt,
            newJwt,
            requests,
            Pair(RequestType.ThreeDQuery, CustomerOutput.ThreeDRedirect)
        )
        threeDSecureManagerReturnsErrorCardinalResult()

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionCalledOnce()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionCalledOnce()

        assertEquals(jwt, session.jwtProvider())

        assertEquals(1, result.responseJwtList.size)
        assertEquals(jwt, result.responseJwtList[0])
        assertNull(result.additionalTransactionResult)
        assertEquals(ThreeDSecureError.Unknown, (result.error as Error.ThreeDSFailure).threeDSError)
    }

    @Test
    fun test_negative_challengeFailsWithActivityResultProvider() = runBlocking {
        //GIVEN
        val requests = listOf(
            RequestType.ThreeDQuery
        )

        val session = createNormalSession(requests)
        val jwt = session.jwtProvider()
        val newJwt = jwtBuilder.buildResponseJWT(BasePayload(emptyList(), ""))

        //WHEN
        apiServiceReturnsSuccessfulInitResult(jwt, jwt)
        threeDSecureManagerReturnsSuccessfulInitializationResult()
        apiServiceReturnsSuccessResult(
            jwt,
            newJwt,
            requests,
            Pair(RequestType.ThreeDQuery, CustomerOutput.ThreeDRedirect)
        )
        threeDSecureManagerReturnsErrorCardinalResult()

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider,
            mockActivityResultProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionCalledOnce()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionCalledOnce()

        assertEquals(jwt, session.jwtProvider())

        assertEquals(1, result.responseJwtList.size)
        assertEquals(jwt, result.responseJwtList[0])
        assertNull(result.additionalTransactionResult)
        assertEquals(ThreeDSecureError.Unknown, (result.error as Error.ThreeDSFailure).threeDSError)
    }

    @Test
    fun test_positive_threeDRedirect_noMoreRequests() = runBlocking {
        //GIVEN
        val requests = listOf(
            RequestType.ThreeDQuery
        )
        val session = createNormalSession(requests)
        val jwt = session.jwtProvider()
        val newJwt = jwtBuilder.getStandard(emptyList())

        //WHEN
        apiServiceReturnsSuccessfulInitResult(jwt, jwt)
        threeDSecureManagerReturnsSuccessfulInitializationResult()
        apiServiceReturnsSuccessResult(
            jwt,
            newJwt,
            requests,
            Pair(RequestType.ThreeDQuery, CustomerOutput.ThreeDRedirect)
        )
        threeDSecureManagerReturnsSuccessfulCardinalV1Result()

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionCalledOnce()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionCalledOnce()

        assertEquals(jwt, session.jwtProvider())

        assertEquals(1, result.responseJwtList.size)
        assertEquals(jwt, result.responseJwtList[0])
        assertEquals(RESPONSE_THREEDV1_PARES, result.additionalTransactionResult?.pares)
        assertNull(result.additionalTransactionResult?.threeDResponse)
        assertNull(result.error)
    }

    @Test
    fun test_positive_threeDRedirect_noMoreRequestsWithActivityResultProvider() = runBlocking {
        //GIVEN
        val requests = listOf(
            RequestType.ThreeDQuery
        )
        val session = createNormalSession(requests)
        val jwt = session.jwtProvider()
        val newJwt = jwtBuilder.getStandard(emptyList())

        //WHEN
        apiServiceReturnsSuccessfulInitResult(jwt, jwt)
        threeDSecureManagerReturnsSuccessfulInitializationResult()
        apiServiceReturnsSuccessResult(
            jwt,
            newJwt,
            requests,
            Pair(RequestType.ThreeDQuery, CustomerOutput.ThreeDRedirect)
        )
        threeDSecureManagerReturnsSuccessfulCardinalV1Result()

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider,
            mockActivityResultProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionCalledOnce()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionCalledOnce()

        assertEquals(jwt, session.jwtProvider())

        assertEquals(1, result.responseJwtList.size)
        assertEquals(jwt, result.responseJwtList[0])
        assertEquals(RESPONSE_THREEDV1_PARES, result.additionalTransactionResult?.pares)
        assertNull(result.additionalTransactionResult?.threeDResponse)
        assertNull(result.error)
    }

    @Test
    fun test_negative_challengeSuccessful_secondRequestFails() = runBlocking {
        //GIVEN
        val requests = listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        )
        val session = createNormalSession(requests)
        val jwt = session.jwtProvider()
        val newJwt = jwtBuilder.buildResponseJWT(
            BasePayload(
                listOf(RequestType.Auth).map { it.serializedName },
                ""
            )
        )

        //WHEN
        apiServiceReturnsSuccessfulInitResult(jwt, jwt)
        threeDSecureManagerReturnsSuccessfulInitializationResult()
        apiServiceReturnsSuccessResult(
            jwt, newJwt, listOf(RequestType.ThreeDQuery), Pair(
                RequestType.ThreeDQuery, CustomerOutput.ThreeDRedirect
            )
        )
        threeDSecureManagerReturnsSuccessfulCardinalV2Result()
        apiServiceReturnsErrorResult(newJwt)

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionCalledTwice()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionCalledOnce()

        assertEquals(jwt, session.jwtProvider())
        assertEquals("", session.intermediateJwt)

        assertEquals(1, result.responseJwtList.size)
        assertEquals(jwt, result.responseJwtList[0])
        assertEquals(
            RESPONSE_THREEDV2_THREEDRESPONSE,
            result.additionalTransactionResult?.threeDResponse
        )
        assertNull(result.additionalTransactionResult?.pares)
        assertTrue(result.error is Error.HttpFailure)
    }

    @Test
    fun test_negative_challengeSuccessful_secondRequestFailsWithActivityResultProvider() = runBlocking {
        //GIVEN
        val requests = listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        )
        val session = createNormalSession(requests)
        val jwt = session.jwtProvider()
        val newJwt = jwtBuilder.buildResponseJWT(
            BasePayload(
                listOf(RequestType.Auth).map { it.serializedName },
                ""
            )
        )

        //WHEN
        apiServiceReturnsSuccessfulInitResult(jwt, jwt)
        threeDSecureManagerReturnsSuccessfulInitializationResult()
        apiServiceReturnsSuccessResult(
            jwt, newJwt, listOf(RequestType.ThreeDQuery), Pair(
                RequestType.ThreeDQuery, CustomerOutput.ThreeDRedirect
            )
        )
        threeDSecureManagerReturnsSuccessfulCardinalV2Result()
        apiServiceReturnsErrorResult(newJwt)

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider,
            mockActivityResultProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionCalledTwice()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionCalledOnce()

        assertEquals(jwt, session.jwtProvider())
        assertEquals("", session.intermediateJwt)

        assertEquals(1, result.responseJwtList.size)
        assertEquals(jwt, result.responseJwtList[0])
        assertEquals(
            RESPONSE_THREEDV2_THREEDRESPONSE,
            result.additionalTransactionResult?.threeDResponse
        )
        assertNull(result.additionalTransactionResult?.pares)
        assertTrue(result.error is Error.HttpFailure)
    }

    @Test
    fun test_positive_3DSV1_secondRequestSucceeds() = runBlocking {
        //GIVEN
        val firstBatchRequests = listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        )
        val secondBatchRequests = listOf(
            RequestType.Auth
        )

        val session = createNormalSession(firstBatchRequests)

        val jwt = session.jwtProvider()
        val newJwt = jwtBuilder.getStandard(secondBatchRequests)
        val finalJwt = jwtBuilder.getStandard(emptyList())

        //WHEN
        apiServiceReturnsSuccessfulInitResult(jwt, jwt)
        apiServiceReturnsSuccessResult(
            jwt, newJwt, listOf(RequestType.ThreeDQuery),
            Pair(RequestType.ThreeDQuery, CustomerOutput.ThreeDRedirect)
        )
        apiServiceReturnsSuccessResult(newJwt, finalJwt, listOf(RequestType.Auth))
        threeDSecureManagerReturnsSuccessfulInitializationResult()
        threeDSecureManagerReturnsSuccessfulCardinalV1Result()

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionCalledTwice()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionCalledOnce()

        assertEquals(jwt, session.jwtProvider())
        assertEquals("", session.intermediateJwt)

        assertEquals(2, result.responseJwtList.size)
        assertEquals(jwt, result.responseJwtList[0])
        assertEquals(newJwt, result.responseJwtList[1])
        assertEquals(RESPONSE_THREEDV1_PARES, result.additionalTransactionResult?.pares)
        assertNull(result.additionalTransactionResult?.threeDResponse)
        assertNull(result.error)
    }

    @Test
    fun test_positive_3DSV1_secondRequestSucceedsWithActivityResultProvider() = runBlocking {
        //GIVEN
        val firstBatchRequests = listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        )
        val secondBatchRequests = listOf(
            RequestType.Auth
        )

        val session = createNormalSession(firstBatchRequests)

        val jwt = session.jwtProvider()
        val newJwt = jwtBuilder.getStandard(secondBatchRequests)
        val finalJwt = jwtBuilder.getStandard(emptyList())

        //WHEN
        apiServiceReturnsSuccessfulInitResult(jwt, jwt)
        apiServiceReturnsSuccessResult(
            jwt, newJwt, listOf(RequestType.ThreeDQuery),
            Pair(RequestType.ThreeDQuery, CustomerOutput.ThreeDRedirect)
        )
        apiServiceReturnsSuccessResult(newJwt, finalJwt, listOf(RequestType.Auth))
        threeDSecureManagerReturnsSuccessfulInitializationResult()
        threeDSecureManagerReturnsSuccessfulCardinalV1Result()

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider,
            mockActivityResultProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionCalledTwice()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionCalledOnce()

        assertEquals(jwt, session.jwtProvider())
        assertEquals("", session.intermediateJwt)

        assertEquals(2, result.responseJwtList.size)
        assertEquals(jwt, result.responseJwtList[0])
        assertEquals(newJwt, result.responseJwtList[1])
        assertEquals(RESPONSE_THREEDV1_PARES, result.additionalTransactionResult?.pares)
        assertNull(result.additionalTransactionResult?.threeDResponse)
        assertNull(result.error)
    }

    //tdq ok acsUrl ok cardinal ok v2 second part ok
    @Test
    fun test_positive_3DSV2_secondRequestSucceeds() = runBlocking {
        //GIVEN
        val firstBatchRequests = listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        )
        val secondBatchRequests = listOf(
            RequestType.Auth
        )

        val session = createNormalSession(firstBatchRequests)

        val jwt = session.jwtProvider()
        val newJwt = jwtBuilder.getStandard(secondBatchRequests)
        val finalJwt = jwtBuilder.getStandard(emptyList())

        //WHEN
        apiServiceReturnsSuccessfulInitResult(jwt, jwt)
        apiServiceReturnsSuccessResult(
            jwt, newJwt, listOf(RequestType.ThreeDQuery),
            Pair(RequestType.ThreeDQuery, CustomerOutput.ThreeDRedirect)
        )
        apiServiceReturnsSuccessResult(newJwt, finalJwt, listOf(RequestType.Auth))
        threeDSecureManagerReturnsSuccessfulInitializationResult()
        threeDSecureManagerReturnsSuccessfulCardinalV2Result()

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionCalledTwice()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionCalledOnce()

        assertEquals(jwt, session.jwtProvider())
        assertEquals("", session.intermediateJwt)

        assertEquals(2, result.responseJwtList.size)
        assertEquals(jwt, result.responseJwtList[0])
        assertEquals(newJwt, result.responseJwtList[1])
        assertEquals(
            RESPONSE_THREEDV2_THREEDRESPONSE,
            result.additionalTransactionResult?.threeDResponse
        )
        assertNull(result.additionalTransactionResult?.pares)
        assertNull(result.error)
    }

    @Test
    fun test_positive_3DSV2_secondRequestSucceedsWithActivityResultProvider() = runBlocking {
        //GIVEN
        val firstBatchRequests = listOf(
            RequestType.ThreeDQuery,
            RequestType.Auth
        )
        val secondBatchRequests = listOf(
            RequestType.Auth
        )

        val session = createNormalSession(firstBatchRequests)

        val jwt = session.jwtProvider()
        val newJwt = jwtBuilder.getStandard(secondBatchRequests)
        val finalJwt = jwtBuilder.getStandard(emptyList())

        //WHEN
        apiServiceReturnsSuccessfulInitResult(jwt, jwt)
        apiServiceReturnsSuccessResult(
            jwt, newJwt, listOf(RequestType.ThreeDQuery),
            Pair(RequestType.ThreeDQuery, CustomerOutput.ThreeDRedirect)
        )
        apiServiceReturnsSuccessResult(newJwt, finalJwt, listOf(RequestType.Auth))
        threeDSecureManagerReturnsSuccessfulInitializationResult()
        threeDSecureManagerReturnsSuccessfulCardinalV2Result()

        val result = paymentTransactionManager.executeSession(
            session,
            mockActivityProvider,
            mockActivityResultProvider
        )

        //THEN
        verifyApiServiceInitializationCalledOnce()
        verifyApiServiceTransactionCalledTwice()
        verifyThreeDSecureManagerCreationCalledOnce()
        verifyThreeDSecureManagerInitializationCalledOnce()
        verifyThreeDSecureManagerExecutionCalledOnce()

        assertEquals(jwt, session.jwtProvider())
        assertEquals("", session.intermediateJwt)

        assertEquals(2, result.responseJwtList.size)
        assertEquals(jwt, result.responseJwtList[0])
        assertEquals(newJwt, result.responseJwtList[1])
        assertEquals(
            RESPONSE_THREEDV2_THREEDRESPONSE,
            result.additionalTransactionResult?.threeDResponse
        )
        assertNull(result.additionalTransactionResult?.pares)
        assertNull(result.error)
    }

    @Test
    fun positive_executeSession_walletTokenAndWalletSourcePassedToApiService() = runBlocking {
        //GIVEN
        val expectedWalletToken = "expectedWalletToken"
        val expectedWalletSource = "expectedWalletSource"

        val requests = listOf(
            RequestType.Auth
        )

        val session = PaymentSession(
            { jwtBuilder.getStandard(requests) },
            REQUEST_CARD_PAN, REQUEST_CARD_EXPIRY_DATE, REQUEST_CARD_CVV,
            walletToken = expectedWalletToken, walletSource = expectedWalletSource
        )

        val responseJwt = session.jwtProvider()
        val newJwt = jwtBuilder.buildResponseJWT(BasePayload(emptyList(), ""))

        //WHEN
        apiServiceReturnsSuccessResult(
            responseJwt, newJwt, requests, Pair(
                RequestType.Auth, CustomerOutput.Result
            )
        )

        paymentTransactionManager.executeSession(session)

        //THEN
        verifyApiServiceTransactionCalledOnceWithParameters(
            expectedWalletToken,
            expectedWalletSource
        )
    }

    @Test
    fun positive_executeSession_walletTokenAndWalletSourcePassedToApiServiceWithActivityResultProvider() = runBlocking {
        //GIVEN
        val expectedWalletToken = "expectedWalletToken"
        val expectedWalletSource = "expectedWalletSource"

        val requests = listOf(
            RequestType.Auth
        )

        val session = PaymentSession(
            { jwtBuilder.getStandard(requests) },
            REQUEST_CARD_PAN, REQUEST_CARD_EXPIRY_DATE, REQUEST_CARD_CVV,
            walletToken = expectedWalletToken, walletSource = expectedWalletSource
        )

        val responseJwt = session.jwtProvider()
        val newJwt = jwtBuilder.buildResponseJWT(BasePayload(emptyList(), ""))

        //WHEN
        apiServiceReturnsSuccessResult(
            responseJwt, newJwt, requests, Pair(
                RequestType.Auth, CustomerOutput.Result
            )
        )

        paymentTransactionManager.executeSession(session, activityResultProvider = mockActivityResultProvider)

        //THEN
        verifyApiServiceTransactionCalledOnceWithParameters(
            expectedWalletToken,
            expectedWalletSource
        )
    }

    // endregion

    //GIVEN helpers

    private fun createNormalSession(requests: List<RequestType>): PaymentSession {
        val jwt = jwtBuilder.getStandard(requests)
        return PaymentSession({ jwt }, REQUEST_CARD_PAN, REQUEST_CARD_EXPIRY_DATE, REQUEST_CARD_CVV)
    }

    private fun createNormalSessionRaw(requests: List<String>): PaymentSession {
        val jwt = jwtBuilder.getForRawRequestTypes(requests)
        return PaymentSession({ jwt }, REQUEST_CARD_PAN, REQUEST_CARD_EXPIRY_DATE, REQUEST_CARD_CVV)
    }

    //WHEN helpers

    private fun apiServiceReturnsSuccessfulInitResult(responseJwt: String, newJwt: String) {
        coEvery {
            mockApiService.initializeSession(
                any(),
                any(),
                any()
            )
        } returns getInitSuccessResponse(responseJwt, newJwt)
    }

    private fun apiServiceReturnsSuccessResult(
        responseJwt: String, newJwt: String, responseRequestTypes: List<RequestType>,
        customerOutput: Pair<RequestType, CustomerOutput>? = null
    ) {
        coEvery {
            mockApiService.performTransaction(
                any(),
                any(),
                responseJwt,
                any(),
                any(),
                any(),
                any(),
                any(),
                any(),
                any(),
                any()
            )
        } returns getSuccessResponse(
            responseRequestTypes,
            responseJwt,
            newJwt,
            customerOutput = customerOutput
        )
    }

    private fun apiServiceReturnsErrorResult(requestJwt: String) {
        coEvery {
            mockApiService.performTransaction(
                any(),
                any(),
                requestJwt,
                any(),
                any(),
                any(),
                any(),
                any(),
                any()
            )
        } returns TrustPaymentsApiService.Result.Failure
    }

    private fun threeDSecureManagerReturnsSuccessfulCardinalV1Result() {
        coEvery {
            mockThreeDSecureManager.executeSession(
                any(),
                any(),
                any(),
                any()
            )
        } returns successCardinalExecutionV1Response
    }

    private fun threeDSecureManagerReturnsSuccessfulCardinalV2Result() {
        coEvery {
            mockThreeDSecureManager.executeSession(
                any(),
                any(),
                any(),
                any()
            )
        } returns successCardinalExecutionV2Response
    }

    private fun threeDSecureManagerReturnsErrorCardinalResult() {
        coEvery {
            mockThreeDSecureManager.executeSession(
                any(),
                any(),
                any(),
                any()
            )
        } returns errorCardinalExecutionResponse
    }

    private fun threeDSecureManagerReturnsSuccessfulInitializationResult() {
        coEvery { mockThreeDSecureManager.initSession(any()) } returns successCardinalInitializationResponse
    }

    private fun threeDSecureManagerReturnsErrorInitializationResult() {
        coEvery { mockThreeDSecureManager.initSession(any()) } returns errorCardinalInitializationResponse
    }

    //THEN helpers

    private fun verifyApiServiceInitializationNeverCalled() {
        coVerify(exactly = 0) { mockApiService.initializeSession(any(), any(), any()) }
    }

    private fun verifyApiServiceTransactionNeverCalled() {
        coVerify(exactly = 0) {
            mockApiService.performTransaction(
                any(),
                any(),
                any(),
                any(),
                any(),
                any(),
                any(),
                any(),
                any()
            )
        }
    }

    private fun verifyApiServiceInitializationCalledOnce() {
        coVerify(exactly = 1) { mockApiService.initializeSession(any(), any(), any()) }
    }

    private fun verifyApiServiceTransactionCalledOnce() {
        coVerify(exactly = 1) {
            mockApiService.performTransaction(
                any(),
                any(),
                any(),
                any(),
                any(),
                any(),
                any(),
                any(),
                any()
            )
        }
    }

    private fun verifyApiServiceTransactionCalledOnceWithParameters(
        walletToken: String, walletSource: String
    ) {
        coVerify(exactly = 1) {
            mockApiService.performTransaction(
                any(),
                any(),
                any(),
                any(),
                any(),
                any(),
                any(),
                any(),
                any(),
                eq(walletToken),
                eq(walletSource)
            )
        }
    }

    private fun verifyApiServiceTransactionCalledTwice() {
        coVerify(exactly = 2) {
            mockApiService.performTransaction(
                any(),
                any(),
                any(),
                any(),
                any(),
                any(),
                any(),
                any(),
                any()
            )
        }
    }

    private fun verifyThreeDSecureManagerExecutionCalledOnce() {
        coVerify(exactly = 1) { mockThreeDSecureManager.executeSession(any(), any(), any(), any()) }
    }

    private fun verifyThreeDSecureManagerExecutionNeverCalled() {
        coVerify(exactly = 0) { mockThreeDSecureManager.executeSession(any(), any(), any(), any()) }
    }

    private fun verifyThreeDSecureManagerInitializationCalledOnce() {
        coVerify(exactly = 1) { mockThreeDSecureManager.initSession(any()) }
    }

    private fun verifyThreeDSecureManagerInitializationNeverCalled() {
        coVerify(exactly = 0) { mockThreeDSecureManager.initSession(any()) }
    }

    private fun verifyThreeDSecureManagerCreationCalledOnce() {
        coVerify(exactly = 1) { mockThreeDSecureManager.createSession() }
    }

    @Test
    fun test_format_cardinal_warnings_message() = runBlocking {
        //GIVEN
        val errors: List<DeviceSafetyWarning> = listOf(DeviceSafetyWarning.SdkIntegrityTampered)
        val method =
            PaymentTransactionManager::class.declaredMemberFunctions.single {
                it.name == "formatCardinalWarningsMessage"
            }
        method.isAccessible = true

        //WHEN
        val result = method.call(paymentTransactionManager, errors) as? String
        //THEN
        assertNotNull(result)
        assertTrue(result!!.contains("SDK integrity tampered"))
    }

    @Test
    fun test_create_session_with_jwt_provider() = runBlocking {
        // Arrange
        val jwtProvider: () -> String = {
            "test"
        }
        val cardPan = "4111111111111111"
        val cardExpiryDate = "12/30"
        val cvv = "123"
        val walletToken = "TEST"
        val walletSource = "TEST"
        // Act
        val result = paymentTransactionManager.createSession(
            jwtProvider = jwtProvider,
            cardPan = cardPan,
            cardExpiryDate = cardExpiryDate,
            cardSecurityCode = cvv,
            walletToken = walletToken,
            walletSource = walletSource
        )
        // Assert
        assertEquals(cardPan, result.cardPan)
        assertEquals(cardExpiryDate, result.cardExpiryDate)
        assertEquals(cvv, result.cardSecurityCode)
        assertEquals(walletToken, result.walletToken)
        assertEquals(walletSource, result.walletSource)
    }

    @Test
    fun test_create_session_with_jwt_string() = runBlocking {
        // Arrange
        val jwtString = "111.111.111"
        val cardPan = "4111111111111111"
        val cardExpiryDate = "12/30"
        val cvv = "123"
        val walletToken = "TEST"
        val walletSource = "TEST"
        // Act
        val result = paymentTransactionManager.createSession(
            jwt = jwtString,
            cardPan = cardPan,
            cardExpiryDate = cardExpiryDate,
            cardSecurityCode = cvv,
            walletToken = walletToken,
            walletSource = walletSource
        )
        // Assert
        assertEquals(cardPan, result.cardPan)
        assertEquals(cardExpiryDate, result.cardExpiryDate)
        assertEquals(cvv, result.cardSecurityCode)
        assertEquals(walletToken, result.walletToken)
        assertEquals(walletSource, result.walletSource)
    }
}