package com.trustpayments.mobile.ui.utils

import android.os.SystemClock
import android.view.View

class OnSingleClickListener(
    private var defaultInterval: Int = 1000,
    private val onSingleClick: (View) -> Unit
) : View.OnClickListener {

    private var lastTimeClicked: Long = 0

    override fun onClick(v: View) {
        if (SystemClock.elapsedRealtime() - lastTimeClicked < defaultInterval) {
            return
        }
        lastTimeClicked = SystemClock.elapsedRealtime()
        onSingleClick(v)
    }
}

fun View.setOnSingleClickListener(defaultInterval: Int = 1000, onSingleClick: (View) -> Unit) {
    val safeClickListener = OnSingleClickListener(defaultInterval=defaultInterval) {
        onSingleClick(it)
    }
    setOnClickListener(safeClickListener)
}