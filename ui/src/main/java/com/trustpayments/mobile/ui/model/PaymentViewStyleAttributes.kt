package com.trustpayments.mobile.ui.model

/**
 * Class representing collection of attributes that can be specified to customize the drop-in
 * payment view
 */
data class PaymentViewStyleAttributes(
    val inputTextColor: Int,
    val inputTextSize: Float,
    val inputBackgroundColor: Int,
    val inputBorderColor: Int,
    val inputBorderWidth: Float,
    val inputCornerRadius: Float,
    val inputHintTextColor: Int,
    val inputSpacing: Int,

    val labelTextColor: Int,
    val labelTextSize: Float,
    val labelTextSpacing: Int,

    val errorTextColor: Int,

    val payButtonTextColor: Int,
    val payButtonTextSize: Float,
    val payButtonBackgroundColor: Int,
    val payButtonDisabledBackgroundColor: Int,
    val payButtonPressedBackgroundColor: Int,
    val payButtonBorderColor: Int,
    val payButtonBorderWidth: Float,
    val payButtonCornerRadius: Float,
    val payButtonPadding: Int
)