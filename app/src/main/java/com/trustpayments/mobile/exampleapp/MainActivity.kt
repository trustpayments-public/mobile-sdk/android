package com.trustpayments.mobile.exampleapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.trustpayments.mobile.exampleapp.common.privacyandpolicy.PrivacyPolicyActivity
import com.trustpayments.mobile.exampleapp.common.recyclerview.CommonAdapter
import com.trustpayments.mobile.exampleapp.common.recyclerview.ListItem
import com.trustpayments.mobile.exampleapp.databinding.ActivityMainBinding
import com.trustpayments.mobile.exampleapp.samples.advancedfeatures.AdvancedFeaturesActivity
import com.trustpayments.mobile.exampleapp.samples.cardPayment.CardPaymentActivity
import com.trustpayments.mobile.exampleapp.samples.digitalWalletPayment.DigitalWalletPaymentActivity
import com.trustpayments.mobile.exampleapp.samples.recurringinstallments.RecurringAndInstallmentsSampleActivity
import com.trustpayments.mobile.exampleapp.samples.tokenizedPayment.TokenizedPaymentActivity
import com.trustpayments.mobile.exampleapp.utils.Constants.SP_KEY_IS_PRIVACY_POLICY_ACCEPTED

class MainActivity : AppCompatActivity() {
    private val viewBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    private val items: List<ListItem> by lazy {
        listOf(
            ListItem.Header(R.string.drop_in_view_examples),
            ListItem.SampleItem(R.string.card_payments, CardPaymentActivity::class.java),
            ListItem.SampleItem(R.string.tokenized_payment, TokenizedPaymentActivity::class.java),
            ListItem.SampleItem(R.string.recurring_installments, RecurringAndInstallmentsSampleActivity::class.java),
            ListItem.Header(R.string.digital_wallets),
            ListItem.SampleItem(R.string.google_pay, DigitalWalletPaymentActivity::class.java),
            ListItem.Header(R.string.advanced_features),
            ListItem.SampleItem(R.string.advanced_features, AdvancedFeaturesActivity::class.java)
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!hasAcceptedTermsAndConditions()){
            PrivacyPolicyActivity.start(this)
        }

        setContentView(viewBinding.root)
        setSupportActionBar(viewBinding.toolbar)
        viewBinding.samplesList.apply {
            val linearLayoutManager = LinearLayoutManager(this@MainActivity)
            layoutManager = linearLayoutManager
            adapter = CommonAdapter(items)
            addItemDecoration(
                DividerItemDecoration(
                    this@MainActivity,
                    linearLayoutManager.orientation
                )
            )
        }
    }

    private fun hasAcceptedTermsAndConditions() =
        PreferenceManager.getDefaultSharedPreferences(this)
            .getBoolean(SP_KEY_IS_PRIVACY_POLICY_ACCEPTED, false)

}