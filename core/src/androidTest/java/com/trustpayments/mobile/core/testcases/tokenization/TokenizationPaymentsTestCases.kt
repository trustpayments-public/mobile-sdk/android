package com.trustpayments.mobile.core.testcases.tokenization

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.trustpayments.mobile.core.BuildConfig
import com.trustpayments.mobile.core.models.api.response.RequestType
import com.trustpayments.mobile.core.services.api.TrustPaymentsGatewayType
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.testutils.BaseTestCase
import com.trustpayments.mobile.core.testutils.createSessionFor
import com.trustpayments.mobile.core.testutils.getAsSuccess
import com.trustpayments.mobile.core.util.*
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class TokenizationPaymentsTestCases: BaseTestCase() {
    private lateinit var paymentTransactionManager: PaymentTransactionManager

    private val jwtBuilder = JWTBuilder(BuildConfig.MERCHANT_USERNAME, BuildConfig.SITE_REFERENCE, BuildConfig.JWT_KEY)

    @Before
    override fun setUp() {
        super.setUp()

        val app = ApplicationProvider.getApplicationContext<Context>().applicationContext
        paymentTransactionManager =
            PaymentTransactionManager(app, TrustPaymentsGatewayType.EU, false, BuildConfig.MERCHANT_USERNAME, isLocationDataConsentGiven = false)
    }

    @Test
    fun test_positive_visa() = runBlocking {
        // GIVEN
        val cardPan = "4000000000001026"
        val cvv = "123"

        val accountCheckResponse = runSuccessfulAccountCheck(cardPan, cvv)

        Thread.sleep(5000)

        val jwt = jwtBuilder.getStandard(listOf(RequestType.Auth),
            CredentialsOnFile.UsePreviouslySaved, accountCheckResponse.transactionReference)
        val session = paymentTransactionManager.createSessionFor(jwt, cvv = cvv)

        val result = paymentTransactionManager.executeSession(session).parse()

        Assert.assertTrue(result is PaymentSessionResponse.Success)
        Assert.assertEquals(1, (result as PaymentSessionResponse.Success).allResponses.size)
        Assert.assertEquals(1, result.responseParts.size)
    }

    @Test
    fun test_positive_amex() = runBlocking {
        // GIVEN
        val cardPan = "340000000000611"
        val cvv = "1234"

        val accountCheckResponse = runSuccessfulAccountCheck(cardPan, cvv)

        Thread.sleep(5000)

        val jwt = jwtBuilder.getStandard(listOf(RequestType.Auth),
            CredentialsOnFile.UsePreviouslySaved, accountCheckResponse.transactionReference)
        val session = paymentTransactionManager.createSessionFor(jwt, cvv = cvv)

        val result = paymentTransactionManager.executeSession(session).parse()

        Assert.assertTrue(result is PaymentSessionResponse.Success)
        Assert.assertEquals(1, (result as PaymentSessionResponse.Success).allResponses.size)
        Assert.assertEquals(1, result.responseParts.size)
    }

    @Test
    fun test_negative_invalidTransactionReference() = runBlocking {
        // GIVEN
        val cvv = "123"

        val jwt = jwtBuilder.getStandard(listOf(RequestType.Auth),
            CredentialsOnFile.UsePreviouslySaved, "Invalid transaction reference")
        val session = paymentTransactionManager.createSessionFor(jwt, cvv = cvv)

        val result = paymentTransactionManager.executeSession(session).parse()

        Assert.assertTrue(result is PaymentSessionResponse.Failure.TransactionFailure)
        result as PaymentSessionResponse.Failure.TransactionFailure

        Assert.assertEquals(1, result.allResponses.size)
        Assert.assertEquals(1, result.responseParts.size)

        Assert.assertTrue(result.allResponses.first() is OldTransactionResponse.Error.TransactionError)
    }

    //TODO this test will fail as long as backend will not validate cvv, which is currently the case
//    @Test
//    fun test_negative_invalidCvv_visa() = runBlocking {
//        // GIVEN
//        val cardPan = "4000000000001026"
//
//        val accountCheckResponse = runSuccessfulAccountCheck(cardPan, "123")
//
//        val jwt = jwtBuilder.getStandard(listOf(RequestType.Auth),
//            CredentialsOnFile.UsePreviouslySaved, accountCheckResponse.transactionReference)
//        val session = paymentTransactionManager.createSessionFor(jwt, cvv = "321")
//
//        val result = paymentTransactionManager.executeSession(session)
//
//        Assert.assertTrue(result is PaymentSessionResponse.Failure.TransactionFailure)
//    }

    //This is a setup step required to get valid transaction reference
    private fun runSuccessfulAccountCheck(cardPan: String, cvv: String) = runBlocking {
        val firstToken = jwtBuilder.getStandard(listOf(RequestType.AccountCheck), CredentialsOnFile.SaveForFutureUse)
        val firstSession =
            paymentTransactionManager.createSessionFor(firstToken, cardPan, cvv)

        // WHEN
        val result = paymentTransactionManager.executeSession(firstSession).parse()

        Assert.assertTrue(result is PaymentSessionResponse.Success)

        result.getAsSuccess(0)
    }
}