#jwt token
-keepclassmembers class com.trustpayments.mobile.ui.utils.JWTTokenBody { <fields>; }
-keepclassmembers class com.trustpayments.mobile.ui.utils.JWTTokenBody$Payload { <fields>; }
#views
-keepnames class * extends android.view.View
