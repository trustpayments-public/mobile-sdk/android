package com.trustpayments.mobile.exampleapp.functionaltests

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.trustpayments.mobile.exampleapp.MainActivity
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.samples.advancedfeatures.AdvancedFeaturesActivity
import com.trustpayments.mobile.exampleapp.samples.cardPayment.CardPaymentActivity
import com.trustpayments.mobile.exampleapp.samples.digitalWalletPayment.DigitalWalletPaymentActivity
import com.trustpayments.mobile.exampleapp.samples.recurringinstallments.RecurringAndInstallmentsSampleActivity
import com.trustpayments.mobile.exampleapp.samples.tokenizedPayment.TokenizedPaymentActivity
import com.trustpayments.mobile.exampleapp.utils.TestUtils
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MainActivityNavigationUITestSuite {

    @get:Rule
    val activityScenarioRule = ActivityScenarioRule(MainActivity::class.java)
    private lateinit var context: Context

    @Before
    fun setUp() {
        context = InstrumentationRegistry.getInstrumentation().targetContext
        TestUtils.clearSharedPreferences(context)
    }

    @After
    fun after() {
        TestUtils.clearSharedPreferences(context)
    }

    private fun performPrivacyPolicyAcceptance() {
        Espresso.onView(ViewMatchers.withId(R.id.privacy_policy_button_accept))
            .perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withText("YES"))
            .inRoot(RootMatchers.isDialog())
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(ViewActions.click())
    }

    @Test
    fun testPrivacyPolicyAcceptanceFlow() {
        performPrivacyPolicyAcceptance()
        Espresso.onView(ViewMatchers.withId(R.id.samples_list))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun testCardPaymentsItemClickAction() = runBlocking {
        Intents.init()
        delay(100)
        TestUtils.waitForVisibleView(R.id.privacy_policy_button_accept, 500)
        performPrivacyPolicyAcceptance()
        Espresso.onView(ViewMatchers.withId(R.id.samples_list))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    1,
                    ViewActions.click()
                )
            )

        Intents.intended(IntentMatchers.hasComponent(CardPaymentActivity::class.java.name))
        Intents.release()
    }

    @Test
    fun testTokenizedPaymentItemClickAction() {
        Intents.init()
        performPrivacyPolicyAcceptance()
        Espresso.onView(ViewMatchers.withId(R.id.samples_list))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    2,
                    ViewActions.click()
                )
            )

        Intents.intended(IntentMatchers.hasComponent(TokenizedPaymentActivity::class.java.name))
        Intents.release()
    }


    @Test
    fun testRecurringInstallmentItemClickAction() {
        Intents.init()
        performPrivacyPolicyAcceptance()
        Espresso.onView(ViewMatchers.withId(R.id.samples_list))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    3,
                    ViewActions.click()
                )
            )

        Intents.intended(IntentMatchers.hasComponent(RecurringAndInstallmentsSampleActivity::class.java.name))
        Intents.release()
    }


    @Test
    fun testGooglePayItemClickAction() {
        Intents.init()
        performPrivacyPolicyAcceptance()
        Espresso.onView(ViewMatchers.withId(R.id.samples_list))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    5,
                    ViewActions.click()
                )
            )

        Intents.intended(IntentMatchers.hasComponent(DigitalWalletPaymentActivity::class.java.name))
        Intents.release()
    }


    @Test
    fun testAdvancedFeaturesItemClickAction() {
        Intents.init()
        performPrivacyPolicyAcceptance()
        Espresso.onView(ViewMatchers.withId(R.id.samples_list))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    7,
                    ViewActions.click()
                )
            )

        Intents.intended(IntentMatchers.hasComponent(AdvancedFeaturesActivity::class.java.name))
        Intents.release()
    }

    @Test
    fun testClickHeaderItem() {
        performPrivacyPolicyAcceptance()
        Espresso.onView(ViewMatchers.withId(R.id.samples_list))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0,
                    ViewActions.click()
                )
            )
        Espresso.onView(ViewMatchers.withId(R.id.samples_list))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}
