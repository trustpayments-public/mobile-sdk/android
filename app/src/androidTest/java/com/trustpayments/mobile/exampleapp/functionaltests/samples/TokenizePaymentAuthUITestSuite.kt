package com.trustpayments.mobile.exampleapp.functionaltests.samples

import android.content.Context
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.isEnabled
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.samples.tokenizedPayment.TokenizedPaymentActivity
import com.trustpayments.mobile.ui.model.PaymentInputType
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4::class)
@LargeTest
class TokenizePaymentAuthUITestSuite {

    @get:Rule
    val activityScenarioRule = ActivityScenarioRule(TokenizedPaymentActivity::class.java)
    private lateinit var context: Context

    @Before
    fun setUp() {
        context = InstrumentationRegistry.getInstrumentation().targetContext
    }

    @After
    fun after() {
        activityScenarioRule.scenario.close()
    }

    /**
     * Checks if Add Card button is enabled and shows correct status initially.
     * */
    @Test
    fun testA_AddCardButtonInitialBehaviour() {
        Espresso.onView(withId(R.id.btnAddCard))
            .check(matches(isEnabled()))
        Espresso.onView(withId(R.id.tvStatus))
            .check(matches(withText(context.getString(R.string.status_token_na))))
    }

    /**
     * Checks if Authorize button is disabled initially.
     * */
    @Test
    fun testB_AuthorizeButtonInitialBehaviour() {
        Espresso.onView(withId(R.id.btnAuthorize)).check(matches(not(isEnabled())))
    }

    /**
     * Verify that Add Card button cannot continue without user input.
     * */
    @Test
    fun testC_TapAddCardButtonBehaviourWithoutCardInfoInput() {
        Espresso.onView(withId(R.id.btnAddCard))
            .perform(ViewActions.click())
        Espresso.onView(withText(context.getString(R.string.error))).check(matches(isDisplayed()))
    }

    /**
     * Verify that Add Card button flow can continue successfully with user input.
     * */
    @Test
    fun testD_TapAddCardButtonBehaviourWithCardInfoInput(): Unit = runBlocking {
        val activityScenario = ActivityScenario.launch(TokenizedPaymentActivity::class.java)
        var activity: TokenizedPaymentActivity? = null
        activityScenario.onActivity { activityInstance ->
            activity = activityInstance
        }
        // Provide card details for authorization to be proceeded.
        activity?.onInputValid(PaymentInputType.PAN, "4111111111111111")
        activity?.onInputValid(PaymentInputType.ExpiryDate, "12/2028")
        activity?.onInputValid(PaymentInputType.CVV, "123")
        // Trigger Add card button click.
        Espresso.onView(withId(R.id.btnAddCard))
            .perform(ViewActions.click())
        // Wait for authorization to be executed.
        delay(5000)
        // Verify Authorization success message is displayed in dialog.
        Espresso.onView(withText(context.getString(R.string.hs_transaction_success)))
            .check(matches(isDisplayed()))
        // Dismiss success dialog.
        Espresso.onView(withText(context.getString(R.string.ok_text))).perform(ViewActions.click())
        delay(100)
        // Verify Authorize button is enabled and can proceed to next screen.
        Espresso.onView(withId(R.id.btnAuthorize)).check(matches(isEnabled()))
        activityScenario.close()
    }

    /**
     * Verify that Add Card button flow cannot continue with wrong user input.
     * */
    @Test
    fun testD_TapAddCardButtonBehaviourWithWrongCardInfoInput(): Unit = runBlocking {
        val activityScenario = ActivityScenario.launch(TokenizedPaymentActivity::class.java)
        var activity: TokenizedPaymentActivity? = null
        activityScenario.onActivity { activityInstance ->
            activity = activityInstance
        }
        // Provide card details for authorization to be proceeded.
        activity?.onInputValid(PaymentInputType.PAN, "4111111111111111")
        activity?.onInputValid(PaymentInputType.ExpiryDate, "12/2020")
        activity?.onInputValid(PaymentInputType.CVV, "123")
        // Trigger Add card button click.
        Espresso.onView(withId(R.id.btnAddCard))
            .perform(ViewActions.click())
        // Wait for authorization to be executed.
        delay(5000)
        // Verify Authorization failure message is displayed in dialog.
        Espresso.onView(withText(context.getString(R.string.error)))
            .check(matches(isDisplayed()))
        // Dismiss failure dialog.
        Espresso.onView(withText(context.getString(R.string.ok_text))).perform(ViewActions.click())
        delay(100)
        // Verify Authorize button is disabled.
        Espresso.onView(withId(R.id.btnAuthorize)).check(matches(not(isEnabled())))
        activityScenario.close()
    }
}
