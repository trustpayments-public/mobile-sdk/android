package com.trustpayments.mobile.ui.utils

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.res.Resources
import android.os.Parcelable
import android.util.SparseArray
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.view.children


fun ViewGroup.saveChildViewStates(): SparseArray<Parcelable> {
    val childViewStates = SparseArray<Parcelable>()
    children.forEach { child -> child.saveHierarchyState(childViewStates) }
    return childViewStates
}

fun ViewGroup.restoreChildViewStates(childViewStates: SparseArray<Parcelable>) {
    children.forEach { child -> child.restoreHierarchyState(childViewStates) }
}

/**
 * Converts dp size into pixels.
 *
 * @param dp size to get converted
 * @return Pixel size
 */
fun Int.fromDpToPx(): Int {
    return (this * Resources.getSystem().displayMetrics.density).toInt()
}

fun Float.fromDpToPx(): Float {
    return (this * Resources.getSystem().displayMetrics.density)
}

/**
 * Converts sp size into pixels.
 *
 * @param sp size to get converted
 * @return Pixel size
 */
fun Float.fromSpToPx(): Float {
    return (this * Resources.getSystem().displayMetrics.scaledDensity)
}

/**
 * Converts pixels into dp size.
 *
 * @param pixels to get converted
 * @return dp size
 */
fun Float.fromPxToDp(): Float {
    return (this / Resources.getSystem().displayMetrics.density)
}

fun String.unmask(): String {
    return replace("[./() \\-+]".toRegex(), "")
}

fun Boolean.toInt(): Int =
    if (this) 1 else 0

/**
 * @return Activity using the Context.
 * */
fun Context.findActivity(): Activity? {
    var context = this
    while (context is ContextWrapper) {
        if (context is Activity) return context
        context = context.baseContext
    }
    return null
}

/**
 * Set Activity as a Secure Content Screen preventing it from appearing in screenshots or
 * from being viewed on non-secure,secondary & virtual displays.
 * */
fun Activity.setSecureWindowFlags(){
    this.window?.setFlags(
        WindowManager.LayoutParams.FLAG_SECURE,
        WindowManager.LayoutParams.FLAG_SECURE
    )
}

/**
 * Used to clear previously set Secure Window Flags to current Activity.
 * */
fun Activity.clearSecureWindowFlags(){
    this.window?.clearFlags(
        WindowManager.LayoutParams.FLAG_SECURE
    )
}