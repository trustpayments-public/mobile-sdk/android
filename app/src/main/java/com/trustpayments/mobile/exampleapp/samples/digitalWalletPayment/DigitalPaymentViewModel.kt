package com.trustpayments.mobile.exampleapp.samples.digitalWalletPayment

import android.app.Activity
import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.trustpayments.mobile.core.PaymentSession
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.core.util.ResponseParser
import com.trustpayments.mobile.exampleapp.BuildConfig
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.common.jwt.CredentialsOnFile
import com.trustpayments.mobile.exampleapp.common.jwt.StandardPayload
import com.trustpayments.mobile.exampleapp.common.jwt.TokenizationPayload
import com.trustpayments.mobile.exampleapp.common.jwtexplorer.JWTEditorToken
import com.trustpayments.mobile.exampleapp.domain.RequestType
import com.trustpayments.mobile.exampleapp.utils.Utils
import com.trustpayments.mobile.exampleapp.utils.Utils.buildRequestPayload
import com.trustpayments.mobile.ui.model.PaymentInputType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json

/**
 * View-model associated with Tokenize Payment AUTH Screen.
 * */
class DigitalPaymentViewModel : ViewModel() {

    // Notify activity regarding different UI use cases.
    internal var uiState: MutableLiveData<UIState> = MutableLiveData()

    // Used to notify JWT Explorer view regarding the request payload in use.
    internal var requestPayload: MutableLiveData<String> = MutableLiveData()

    // Used to notify JWT Explorer view regarding the response payload received by SDK.
    internal var responsePayload: MutableLiveData<String> = MutableLiveData()

    // Card payment request payload that is allowed to be edited for the current use case.
    private var editableCardPayload: TokenizationPayload

    // Google payment request payload that is allowed to be edited for the current use case.
    private var editableGooglePayload: StandardPayload

    // Credential details of the card.
    private var pan = ""
    private var expiryDate = ""
    private var cvv = ""

    // Initial transaction details for two use cases.(Card payment & Google payment).
    var googlePaymentAmount: Long = 39990L
    private var cardPaymentAmount: Long = 84999L
    private val currency = "GBP"

    internal var isShowingGooglePayload: MutableLiveData<Boolean> = MutableLiveData()

    init {
        uiState.postValue(UIState.Idle)
        // Builds payload request. Contains site-reference and payment information.
        editableCardPayload = TokenizationPayload(
            siteReference = BuildConfig.SITE_REFERENCE,
            currencyISO3a = currency,
            baseAmount = cardPaymentAmount.toInt(),
            requestTypeDescriptions = listOf(
                RequestType.ThreeDQuery.serializedName,
                RequestType.Auth.serializedName,
            ),
            credentialsOnFile = CredentialsOnFile.SaveForFutureUse,
            parentTransactionReference = null,
        )
        editableGooglePayload = StandardPayload(
            siteReference = BuildConfig.SITE_REFERENCE,
            currencyISO3a = currency,
            baseAmount = googlePaymentAmount.toInt(),
            requestTypeDescriptions = listOf(RequestType.Auth.serializedName),
        ).apply {
            isShowingGooglePayload.postValue(true)
            updateRequestPayloadInfoUI(
                Json.encodeToString(StandardPayload.serializer(), this)
            )
        }
    }

    /**
     * Triggered by user when clicked on Add Card Button in UI.
     * */
    fun onPayButtonClicked(
        context: Context,
        paymentTransactionManager: PaymentTransactionManager,
        activityProvider: (() -> Activity)? = null,
    ) {
        uiState.postValue(UIState.Loading)
        if (pan.isBlank() || expiryDate.isBlank() || cvv.isBlank()) {
            uiState.postValue(UIState.Error(context.getString(R.string.provide_card_data)))
            return
        }
        isShowingGooglePayload.postValue(false)
        // Builds JWT Token using transaction Payload.
        val jwtToken = Utils.buildJWT(
            merchantUsername = BuildConfig.MERCHANT_USERNAME,
            payload = editableCardPayload.buildRequestPayload(),
        )
        updateRequestPayloadInfoUI(
            Json.encodeToString(TokenizationPayload.serializer(), editableCardPayload)
        )
        // Create payment session for card payment.
        val session = paymentTransactionManager.createSession(
            jwtProvider = { jwtToken },
            cardPan = pan,
            cardExpiryDate = expiryDate,
            cardSecurityCode = cvv,
        )
        executeSdkTransaction(
            context = context,
            isGooglePayment = false,
            paymentSession = session,
            paymentTransactionManager = paymentTransactionManager,
            activityProvider = activityProvider,
        )
    }

    /**
     * Triggered when Google Payment SDK notifies activity regarding the payment executed through
     * Google Wallet.
     * */
    fun onGoogleWalletPayment(
        context: Context,
        walletToken: String,
        paymentTransactionManager: PaymentTransactionManager,
        activityProvider: (() -> Activity)? = null,
    ) {
        Log.d(TAG, "GoogleWalletToken $walletToken")
        uiState.postValue(UIState.Loading)
        isShowingGooglePayload.postValue(true)
        val jwtToken = Utils.buildJWT(
            merchantUsername = BuildConfig.MERCHANT_USERNAME,
            payload = editableGooglePayload.buildRequestPayload(),
        )
        updateRequestPayloadInfoUI(
            Json.encodeToString(StandardPayload.serializer(), editableGooglePayload)
        )
        // Create payment session for google payment.
        val session = paymentTransactionManager.createSession(
            jwtProvider = { jwtToken },
            cardPan = null,
            cardExpiryDate = null,
            cardSecurityCode = null,
            walletToken = walletToken,
            walletSource = "GOOGLEPAY"
        )
        // Executes payment request
        executeSdkTransaction(
            context = context,
            isGooglePayment = true,
            paymentSession = session,
            paymentTransactionManager = paymentTransactionManager,
            activityProvider = activityProvider,
        )
    }

    private fun executeSdkTransaction(
        context: Context,
        isGooglePayment: Boolean,
        paymentSession: PaymentSession,
        paymentTransactionManager: PaymentTransactionManager,
        activityProvider: (() -> Activity)? = null,
    ) {
        updateResponsePayloadInfoUI(emptyList())
        viewModelScope.launch(Dispatchers.IO) {
            // Executes payment request using PaymentTransactionManager.
            val result = paymentTransactionManager.executeSession(
                session = paymentSession,
                activityProvider = activityProvider
            )

            updateResponsePayloadInfoUI(result.responseJwtList)
            // Process response received by SDK for executed transaction.
            val parsedResult = ResponseParser.parse(result.responseJwtList)

            val errorMessage = Utils.findExecuteSessionErrorMessage(result, context)
            val transactionReference = parsedResult?.firstOrNull()
                ?.customerOutput?.transactionReference
            if (transactionReference.isNullOrBlank() || errorMessage.isNullOrBlank().not()) {
                uiState.postValue(
                    UIState.Error(
                        errorMessage ?: context.getString(R.string.hs_transaction_failed),
                    ),
                )
            } else {
                uiState.postValue(
                    if (isGooglePayment) {
                        UIState.GooglePaymentSuccess
                    } else {
                        UIState.CardPaymentSuccess
                    }
                )
            }
        }
    }

    /**
     * Update view-model in case user modifies Request payload using JWT Explorer View.
     * @param token contains modified request payload.
     * Since this digital payment screen has two different options (card payment & Google Pay),
     * this method will attempt to differentiate each request and update
     * the desired payload accordingly.
     * */
    fun onPayloadChanged(token: JWTEditorToken) {
        val json = Json { encodeDefaults = false }
        val tokenizePayload: TokenizationPayload? = try {
            json.decodeFromString(TokenizationPayload.serializer(), token.token)
        } catch (e: Exception) {
            Log.d(TAG, "PayloadChanged(Tokenization) Exception: ${e.localizedMessage}")
            null
        }
        tokenizePayload?.also {
            editableCardPayload = it
        } ?: run {
            val googlePayload: StandardPayload? = try {
                json.decodeFromString(StandardPayload.serializer(), token.token)
            } catch (e: Exception) {
                Log.d(TAG, "PayloadChanged(StandardPayload) Exception: ${e.localizedMessage}")
                null
            }
            googlePayload?.also {
                googlePaymentAmount = it.baseAmount.toLong()
                editableGooglePayload = it
            }
        }
    }

    /**
     * Updates view-model when user enter card details using Drop-In-Payment View.
     * */
    fun onUpdateCardData(
        paymentInputType: PaymentInputType,
        input: String,
    ) {
        when (paymentInputType) {
            PaymentInputType.PAN -> pan = input
            PaymentInputType.ExpiryDate -> expiryDate = input
            PaymentInputType.CVV -> cvv = input
        }
    }

    /**
     * Notify JWT Explorer view in UI regarding the request payload in use.
     * */
    private fun updateRequestPayloadInfoUI(payload: String) {
        requestPayload.postValue(payload)
    }

    /**
     * Notify JWT Explorer view regarding the response payload received by SDK.
     * */
    private fun updateResponsePayloadInfoUI(jwtResponseList: List<String>) {
        val payload = jwtResponseList.takeIf { it.isNotEmpty() }?.let {
            Utils.jwtStringListToDecodedJsonArrayString(jwtResponseList)
        } ?: ""
        responsePayload.postValue(payload)
    }

    companion object {
        private const val TAG = "DigitalPayVM"
    }

    /**
     * Represents different UI states associated with Tokenize Payment AUTH Screen.
     * */
    sealed class UIState {
        data object Idle : UIState()
        data object Loading : UIState()
        data object CardPaymentSuccess : UIState()
        data object GooglePaymentSuccess : UIState()
        class Error(val message: String) : UIState()
    }
}
