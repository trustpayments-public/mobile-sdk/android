#!/usr/bin/env bash
source .cicd_scripts/other/utils.sh

execute_unit_tests_in_parallel() {
    set -u -o pipefail -E

    local unit_tests_resource=$1
    local unit_tests_path=$2

    log_this INFO "Running Unit parallel tests: ${unit_tests_path}. Actual node: ${CI_NODE_INDEX}/${CI_NODE_TOTAL}"

    TESTS=($(find ${unit_tests_path} -type f | grep Test.kt\$ | sed s~${unit_tests_path}~~g | sed s~/~.~g | sort))
    len=${#TESTS[@]}

    if [ $CI_NODE_TOTAL -ne $len ]; then
      log_this ERROR "Number of nodes must be equal to the number of files that should be run in parallel. Nodes: ${CI_NODE_TOTAL}. Feature files: ${len}"
      exit 1
    fi

    index=$(($CI_NODE_INDEX-1))
    TEST=${TESTS[$index]%.kt}

    log_this INFO "Executing Test: ${TEST}"
    /bin/bash gradlew ${unit_tests_resource} ${CARDINAL_GRADLE_PARAMETERS} ${GRADLE_PARAMETERS} --tests ${TEST}

    log_this INFO "Rename JACOCO file, each file output for test must be unique"
    cd ${ARTIFACTS_BUILD_JACOCO_FROM}
    ls | xargs -I {} mv {}  ${TEST##*.}_{}
}
