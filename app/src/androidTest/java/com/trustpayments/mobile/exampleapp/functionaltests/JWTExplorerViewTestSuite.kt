package com.trustpayments.mobile.exampleapp.functionaltests

import android.view.View
import android.webkit.WebView
import androidx.lifecycle.Lifecycle
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.google.android.material.tabs.TabLayout
import com.trustpayments.mobile.exampleapp.MockActivity
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.common.jwtexplorer.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.*

class JWTExplorerViewTestSuite {
    private val stepsList = listOf(
        JWTEditorStep(
            "normal",
            "TEST",
            listOf(
                JWTEditorToken(
                    "request",
                    "normal",
                    "REQUEST",
                    "{\n" +
                            "  \"payload\":{\n" +
                            "    \"accounttypedescription\":\"ECOM\",\n" +
                            "    \"baseamount\":\"1050\",\n" +
                            "    \"currencyiso3a\":\"GBP\",\n" +
                            "    \"sitereference\":\"test_site12345\",\n" +
                            "    \"requesttypedescriptions\":[\"THREEDQUERY\",\"AUTH\"]\n" +
                            "  },\n" +
                            "  \"iat\":1559033849,\n" +
                            "  \"iss\":\"jwt.user\"\n" +
                            "}",
                    true
                ),
                JWTEditorToken(
                    "response",
                    "normal",
                    "RESPONSE",
                    "['TEST']",
                    false
                )
            )
        ),
        JWTEditorStep(
            "normal",
            "TEST 1",
            listOf(
                JWTEditorToken(
                    "request",
                    "normal",
                    "REQUEST",
                    "{\n" +
                            "  \"payload\":{\n" +
                            "    \"accounttypedescription\":\"ECOM\",\n" +
                            "    \"baseamount\":\"1050\",\n" +
                            "    \"currencyiso3a\":\"GBP\",\n" +
                            "    \"sitereference\":\"test_site12345\",\n" +
                            "    \"requesttypedescriptions\":[\"THREEDQUERY\",\"AUTH\"]\n" +
                            "  },\n" +
                            "  \"iat\":1,\n" +
                            "  \"iss\":\"jwt.user\"\n" +
                            "}",
                    true
                ),
                JWTEditorToken(
                    "response",
                    "normal",
                    "RESPONSE",
                    "['TEST 1 ']",
                    false
                )
            )
        )
    )
    @get:Rule
    val activityScenarioRule =
        ActivityScenarioRule(MockActivity::class.java)

    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
        activityScenarioRule.scenario?.close()
    }

    @Test
    fun testDefaultMode() {
        activityScenarioRule.scenario.moveToState(Lifecycle.State.RESUMED)
        activityScenarioRule.scenario?.onActivity { activity ->
            val expectedStepsTabs = 1
            val expectedTokenTabs = 2
            val jwtExplorerView = activity.findViewById<JWTExplorerView>(R.id.jwt_explorer_view)
            val stepsTabLayout = jwtExplorerView.findViewById<TabLayout>(R.id.steps_tabs_layout)
            val tokenTabLayout = jwtExplorerView.findViewById<TabLayout>(R.id.tokens_tabs_layout)
            val actualStepsTabsCount = stepsTabLayout.tabCount
            val actualTokenTabsCount = tokenTabLayout.tabCount
            Assert.assertEquals(jwtExplorerView?.mode, JWTEditorMode.DEFAULT)
            Assert.assertEquals(actualStepsTabsCount, expectedStepsTabs)
            Assert.assertEquals(actualTokenTabsCount, expectedTokenTabs)
            Assert.assertEquals(stepsTabLayout.visibility, View.GONE)
            Assert.assertEquals(tokenTabLayout.visibility, View.VISIBLE)
        }
    }

    @Test
    fun testStepsMode() {
        activityScenarioRule.scenario.moveToState(Lifecycle.State.RESUMED)
        activityScenarioRule.scenario?.onActivity { activity ->
            val expectedStepsTabs = 2
            val expectedTokenTabs = 2
            val jwtExplorerView = activity.findViewById<JWTExplorerView>(R.id.jwt_explorer_view)
            jwtExplorerView.setMode(JWTEditorMode.STEPS)
            jwtExplorerView.setSteps(stepsList)
            val stepsTabLayout = jwtExplorerView.findViewById<TabLayout>(R.id.steps_tabs_layout)
            val tokenTabLayout = jwtExplorerView.findViewById<TabLayout>(R.id.tokens_tabs_layout)
            val actualStepsTabsCount = stepsTabLayout.tabCount
            val actualTokenTabsCount = tokenTabLayout.tabCount
            Assert.assertEquals(jwtExplorerView?.mode, JWTEditorMode.STEPS)
            Assert.assertEquals(actualStepsTabsCount, expectedStepsTabs)
            Assert.assertEquals(actualTokenTabsCount, expectedTokenTabs)
            Assert.assertEquals(stepsTabLayout.visibility, View.VISIBLE)
            Assert.assertEquals(tokenTabLayout.visibility, View.VISIBLE)
        }
    }

    @Test(expected = Exception::class)
    fun testExceptionAddingStepsWithDefaultMode() {
        activityScenarioRule.scenario.moveToState(Lifecycle.State.RESUMED)
        activityScenarioRule.scenario?.onActivity { activity ->
            val jwtExplorerView = activity.findViewById<JWTExplorerView>(R.id.jwt_explorer_view)
            Assert.assertEquals(jwtExplorerView?.mode, JWTEditorMode.DEFAULT)
            jwtExplorerView.setSteps(emptyList())
        }
    }

    @Test(expected = Exception::class)
    fun testExceptionAddingRequestTokenWithStepstMode() {
        activityScenarioRule.scenario.moveToState(Lifecycle.State.RESUMED)
        activityScenarioRule.scenario?.onActivity { activity ->
            val jwtExplorerView = activity.findViewById<JWTExplorerView>(R.id.jwt_explorer_view)
            jwtExplorerView.setMode(JWTEditorMode.STEPS)
            Assert.assertEquals(jwtExplorerView?.mode, JWTEditorMode.STEPS)
            jwtExplorerView.setRequestJwtToken("")
        }
    }

    @Test(expected = Exception::class)
    fun testExceptionAddingResponseTokenWithStepstMode() {
        activityScenarioRule.scenario.moveToState(Lifecycle.State.RESUMED)
        activityScenarioRule.scenario?.onActivity { activity ->
            val jwtExplorerView = activity.findViewById<JWTExplorerView>(R.id.jwt_explorer_view)
            jwtExplorerView.setMode(JWTEditorMode.STEPS)
            Assert.assertEquals(jwtExplorerView?.mode, JWTEditorMode.STEPS)
            jwtExplorerView.setResponseJwtToken("")
        }
    }

    @Test
    fun testDefaultModeNavigation() {
        activityScenarioRule.scenario.moveToState(Lifecycle.State.RESUMED)
        activityScenarioRule.scenario?.onActivity { activity ->
            val jwtExplorerView = activity.findViewById<JWTExplorerView>(R.id.jwt_explorer_view)
            val tokenTabLayout = jwtExplorerView.findViewById<TabLayout>(R.id.tokens_tabs_layout)
            tokenTabLayout.getTabAt(1)?.select()
            Assert.assertEquals(jwtExplorerView?.mode, JWTEditorMode.DEFAULT)
            Assert.assertEquals(tokenTabLayout.selectedTabPosition, 1)
        }
    }

    @Test
    fun testStepsModeNavigation() {
        activityScenarioRule.scenario.moveToState(Lifecycle.State.RESUMED)
        activityScenarioRule.scenario?.onActivity { activity ->
            val jwtExplorerView = activity.findViewById<JWTExplorerView>(R.id.jwt_explorer_view)
            jwtExplorerView.setMode(JWTEditorMode.STEPS)
            jwtExplorerView.setSteps(stepsList)
            val stepsTabLayout = jwtExplorerView.findViewById<TabLayout>(R.id.steps_tabs_layout)
            val tokenTabLayout = jwtExplorerView.findViewById<TabLayout>(R.id.tokens_tabs_layout)
            tokenTabLayout.getTabAt(1)?.select()
            stepsTabLayout.getTabAt(1)?.select()
            Assert.assertEquals(jwtExplorerView?.mode, JWTEditorMode.STEPS)
            Assert.assertEquals(0, tokenTabLayout.selectedTabPosition)
            Assert.assertEquals(1, stepsTabLayout.selectedTabPosition)
        }
    }

    @Test
    fun testTokenChange(): Unit = runBlocking{
        activityScenarioRule.scenario.moveToState(Lifecycle.State.RESUMED)
        activityScenarioRule.scenario?.onActivity { activity ->
            var editedToken: JWTEditorToken? = null
            val expectedToken = "['TEST']"
            val jwtExplorerView = activity.findViewById<JWTExplorerView>(R.id.jwt_explorer_view)
            val webView = jwtExplorerView.findViewById<WebView>(R.id.webContainer)
            jwtExplorerView.setOnTokenChangedListener(object : OnTokenChangedListener{
                override fun onTokenChanged(token: JWTEditorToken) {
                    editedToken = token
                }

            })
            webView.evaluateJavascript("Android.tokenChanged(\"${expectedToken}\")", null)
            launch {
                delay(200)
                Assert.assertEquals(expectedToken, editedToken?.token)
            }
        }
    }
}