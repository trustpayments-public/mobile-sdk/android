package com.trustpayments.mobile.exampleapp.pageobjects

import android.view.View
import androidx.test.espresso.Espresso
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.utils.core.waitForCondition
import org.hamcrest.Matcher

open class BaseAppPage {


    val uiDevice: UiDevice by lazy {
        UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    }


    fun performPrivacyPolicyAcceptance() {
        Espresso.onView(ViewMatchers.withId(R.id.privacy_policy_button_accept))
            .perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withText("YES"))
            .inRoot(RootMatchers.isDialog())
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(ViewActions.click())
    }
    fun waitUntilElementIsDisplayed(
        viewMatcher: Matcher<View>,
        timeout: Int = 8000
    ) {
        waitForCondition({
            try {
                Espresso.onView(viewMatcher)
                    .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
                true
            } catch (ex: NoMatchingViewException) {
                false
            }
        }, timeout.toLong())
    }
}
