package com.trustpayments.mobile.exampleapp.samples.tokenizedPayment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.trustpayments.mobile.core.services.api.TrustPaymentsGatewayType
import com.trustpayments.mobile.core.services.transaction.PaymentTransactionManager
import com.trustpayments.mobile.exampleapp.BuildConfig
import com.trustpayments.mobile.exampleapp.R
import com.trustpayments.mobile.exampleapp.common.jwtexplorer.JWTEditorToken
import com.trustpayments.mobile.exampleapp.common.jwtexplorer.OnTokenChangedListener
import com.trustpayments.mobile.exampleapp.databinding.ActivityTokenizedPaymentLaunchBinding
import com.trustpayments.mobile.exampleapp.utils.Utils
import com.trustpayments.mobile.exampleapp.utils.Utils.hideKeyboard

class TokenizedPaymentTriggerActivity : AppCompatActivity() {

    private val viewBinding by lazy {
        ActivityTokenizedPaymentLaunchBinding.inflate(layoutInflater)
    }

    // PaymentTransactionManager responsible for executing the transaction.
    private val paymentTransactionManager by lazy {
        PaymentTransactionManager(
            context = this@TokenizedPaymentTriggerActivity,
            gatewayType = TrustPaymentsGatewayType.EU,
            isCardinalLive = false,
            merchantUsername = BuildConfig.MERCHANT_USERNAME,
            isLocationDataConsentGiven = true,
        )
    }
    private val viewModel: TokenizedPaymentLaunchViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(viewBinding.root)
        setSupportActionBar(viewBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewBinding.toolbar.setNavigationOnClickListener { finish() }
        setUiElements()
        setObservers()
    }

    private fun setUiElements() {
        viewBinding.apply {
            intent.extras?.getString(TOKEN)?.takeIf { it.isNotBlank() }?.let {
                viewModel.setParentTransactionReference(it)
                btnAuthorize.isEnabled = true
            } ?: run {
                btnAuthorize.isEnabled = false
            }
            intent.extras?.getString(CVV)?.takeIf { it.isNotBlank() }?.let {
                viewModel.setCardCVV(it)
            }
            btnAuthorize.setOnClickListener {
                hideKeyboard(it)
                viewModel.onAuthorizedButtonClicked(
                    context = this@TokenizedPaymentTriggerActivity,
                    paymentTransactionManager = paymentTransactionManager,
                    activityProvider = {
                        this@TokenizedPaymentTriggerActivity
                    },
                )
            }
            jwtExplorer.apply {
                setOnTokenChangedListener(
                    object : OnTokenChangedListener {
                        override fun onTokenChanged(token: JWTEditorToken) {
                            viewModel.onPayloadChanged(token)
                        }
                    },
                )
            }
        }
    }

    private fun setObservers() {
        viewBinding.apply {
            viewModel.transactionReference.observe(this@TokenizedPaymentTriggerActivity) {
                tvStatus.text = it
            }
            viewModel.requestPayload.observe(this@TokenizedPaymentTriggerActivity) {
                jwtExplorer.setRequestJwtToken(it)
            }
            viewModel.responsePayload.observe(this@TokenizedPaymentTriggerActivity) {
                jwtExplorer.apply {
                    setResponseJwtToken(it)
                    it.takeIf { it.isNotBlank() }?.also {
                        moveTokenTab(1)
                    }
                }
            }
            viewModel.uiState.observe(this@TokenizedPaymentTriggerActivity) {
                when (it) {
                    is TokenizedPaymentLaunchViewModel.UIState.Loading -> {
                        progressBar.isVisible = true
                        btnAuthorize.isEnabled = false
                    }

                    is TokenizedPaymentLaunchViewModel.UIState.Idle -> {
                        progressBar.isVisible = false
                        btnAuthorize.isEnabled = true
                    }

                    is TokenizedPaymentLaunchViewModel.UIState.Success -> {
                        progressBar.isVisible = false
                        btnAuthorize.isEnabled = true
                        Utils.showDialog(
                            context = this@TokenizedPaymentTriggerActivity,
                            title = getString(R.string.hs_transaction_success),
                            message = getString(R.string.request_success)
                        )
                    }

                    is TokenizedPaymentLaunchViewModel.UIState.Error -> {
                        progressBar.isVisible = false
                        btnAuthorize.isEnabled = true
                        Utils.showDialog(
                            context = this@TokenizedPaymentTriggerActivity,
                            title = getString(R.string.error),
                            message = it.message
                        )
                    }
                }
            }
        }
    }

    companion object {
        private const val TOKEN = "token"
        private const val CVV = "cvv"

        fun launchActivity(
            context: Context,
            token: String,
            cvv: String,
        ) {
            context.run {
                val intent = Intent(context, TokenizedPaymentTriggerActivity::class.java).apply {
                    putExtra(TOKEN, token)
                    putExtra(CVV, cvv)
                }
                startActivity(intent)
            }
        }
    }
}
