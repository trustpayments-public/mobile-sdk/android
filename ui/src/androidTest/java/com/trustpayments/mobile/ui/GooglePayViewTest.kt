package com.trustpayments.mobile.ui

import android.app.Activity
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.trustpayments.mobile.ui.dropin.DropInPaymentView
import com.trustpayments.mobile.ui.model.GooglePayButtonColor
import com.trustpayments.mobile.ui.testutils.GooglePayViewTestActivity
import org.junit.Assert
import org.junit.Rule
import org.junit.Test

class GooglePayViewTest {
    @get:Rule
    val activityScenarioRule =
        ActivityScenarioRule(GooglePayViewTestActivity::class.java)

    private fun findGooglePayButton(activity: Activity): GooglePayButton? {
        return activity.findViewById(R.id.googlePayButton)
    }

    private fun findGooglePayButtonBackground(activity: Activity): View? {
        val googlePayButton = findGooglePayButton(activity)
        return googlePayButton?.findViewById(R.id.buttonBackground)
    }

    private fun findGooglePayButtonImage(activity: Activity): View? {
        val googlePayButton = findGooglePayButton(activity)
        return googlePayButton?.findViewById(R.id.googlePayImage)
    }

    @Test
    fun test_defaultConfig() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            Assert.assertEquals(
                findGooglePayButtonBackground(activity)?.tag,
                R.drawable.googlepay_button_background
            )
            Assert.assertEquals(
                findGooglePayButtonImage(activity)?.tag,
                R.drawable.buy_with_googlepay_button_content
            )
        }
    }

    @Test
    fun test_changeDefaultColor() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val googlePayButton = activity.findViewById<GooglePayButton>(R.id.googlePayButton)
            googlePayButton.setupButtonView(GooglePayButtonColor.WHITE)

            Assert.assertEquals(
                findGooglePayButtonBackground(activity)?.tag,
                R.drawable.googlepay_button_background_white
            )
            Assert.assertEquals(
                findGooglePayButtonImage(activity)?.tag,
                R.drawable.buy_with_googlepay_button_content_white
            )
        }
    }

    @Test
    fun test_changeDefaultText() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val googlePayButton = activity.findViewById<GooglePayButton>(R.id.googlePayButton)
            googlePayButton.setupButtonView(withText = false)

            Assert.assertEquals(
                findGooglePayButtonBackground(activity)?.tag,
                R.drawable.googlepay_button_background
            )
            Assert.assertEquals(
                findGooglePayButtonImage(activity)?.tag,
                R.drawable.googlepay_button_content
            )
        }
    }

    @Test
    fun test_changeDefaultShadow() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val googlePayButton = activity.findViewById<GooglePayButton>(R.id.googlePayButton)
            googlePayButton.setupButtonView(withShadow = false)

            Assert.assertEquals(
                findGooglePayButtonBackground(activity)?.tag,
                R.drawable.googlepay_button_no_shadow_background
            )
            Assert.assertEquals(
                findGooglePayButtonImage(activity)?.tag,
                R.drawable.buy_with_googlepay_button_content
            )
        }
    }

    @Test
    fun test_whiteWithTextWithShadow() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val googlePayButton = activity.findViewById<GooglePayButton>(R.id.googlePayButton)
            googlePayButton.setupButtonView(
                GooglePayButtonColor.WHITE,
                withText = true,
                withShadow = true
            )

            Assert.assertEquals(
                findGooglePayButtonBackground(activity)?.tag,
                R.drawable.googlepay_button_background_white
            )
            Assert.assertEquals(
                findGooglePayButtonImage(activity)?.tag,
                R.drawable.buy_with_googlepay_button_content_white
            )
        }
    }

    @Test
    fun test_whiteWithTextNoShadow() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val googlePayButton = activity.findViewById<GooglePayButton>(R.id.googlePayButton)
            googlePayButton.setupButtonView(
                GooglePayButtonColor.WHITE,
                withText = true,
                withShadow = false
            )

            Assert.assertEquals(
                findGooglePayButtonBackground(activity)?.tag,
                R.drawable.googlepay_button_no_shadow_background_white
            )
            Assert.assertEquals(
                findGooglePayButtonImage(activity)?.tag,
                R.drawable.buy_with_googlepay_button_content_white
            )
        }
    }

    @Test
    fun test_whiteNoTextNoShadow() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val googlePayButton = activity.findViewById<GooglePayButton>(R.id.googlePayButton)
            googlePayButton.setupButtonView(
                GooglePayButtonColor.WHITE,
                withText = false,
                withShadow = false
            )

            Assert.assertEquals(
                findGooglePayButtonBackground(activity)?.tag,
                R.drawable.googlepay_button_no_shadow_background_white
            )
            Assert.assertEquals(
                findGooglePayButtonImage(activity)?.tag,
                R.drawable.googlepay_button_content_white
            )
        }
    }

    @Test
    fun test_blackWithTextWithShadow() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val googlePayButton = activity.findViewById<GooglePayButton>(R.id.googlePayButton)
            googlePayButton.setupButtonView(
                GooglePayButtonColor.BLACK,
                withText = true,
                withShadow = true
            )

            Assert.assertEquals(
                findGooglePayButtonBackground(activity)?.tag,
                R.drawable.googlepay_button_background
            )
            Assert.assertEquals(
                findGooglePayButtonImage(activity)?.tag,
                R.drawable.buy_with_googlepay_button_content
            )
        }
    }

    @Test
    fun test_blackWithTextNoShadow() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val googlePayButton = activity.findViewById<GooglePayButton>(R.id.googlePayButton)
            googlePayButton.setupButtonView(
                GooglePayButtonColor.BLACK,
                withText = true,
                withShadow = false
            )

            Assert.assertEquals(
                findGooglePayButtonBackground(activity)?.tag,
                R.drawable.googlepay_button_no_shadow_background
            )
            Assert.assertEquals(
                findGooglePayButtonImage(activity)?.tag,
                R.drawable.buy_with_googlepay_button_content
            )
        }
    }

    @Test
    fun test_blackNoTextNoShadow() {
        // GIVEN
        val scenario = activityScenarioRule.scenario

        // WHEN
        scenario.moveToState(Lifecycle.State.RESUMED)

        // THEN
        scenario.onActivity { activity ->
            val googlePayButton = activity.findViewById<GooglePayButton>(R.id.googlePayButton)
            googlePayButton.setupButtonView(
                GooglePayButtonColor.BLACK,
                withText = false,
                withShadow = false
            )

            Assert.assertEquals(
                findGooglePayButtonBackground(activity)?.tag,
                R.drawable.googlepay_button_no_shadow_background
            )
            Assert.assertEquals(
                findGooglePayButtonImage(activity)?.tag,
                R.drawable.googlepay_button_content
            )
        }
    }

}