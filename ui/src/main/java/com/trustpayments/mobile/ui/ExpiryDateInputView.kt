package com.trustpayments.mobile.ui

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import com.trustpayments.mobile.ui.card.CardValidator
import com.trustpayments.mobile.ui.common.BaseInputView
import com.trustpayments.mobile.ui.model.PaymentInputType
import com.trustpayments.mobile.ui.utils.toInt

/**
 * UI component for handling card expiry date
 */
class ExpiryDateInputView(context: Context, attrs: AttributeSet?) : BaseInputView(context, attrs),
    ExpiryDateInputViewModel.ExpiryDateInputCallbacks {

    private val viewModel = ExpiryDateInputViewModel(CardValidator(), this)

    init {
        initView()
    }

    private var beforeText = ""

    private fun initView() {
        setInputMaxLength(5)
        binding.apply {
            input.inputType = InputType.TYPE_CLASS_NUMBER or
                    InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD or
                    InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS

            input.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {}

                override fun beforeTextChanged(
                    text: CharSequence?, start: Int, count: Int, after: Int
                ) {
                    beforeText = text.toString()
                }

                override fun onTextChanged(
                    text: CharSequence?, start: Int, before: Int, count: Int
                ) {
                    val expiryDate = viewModel.formatExpiryDate(text.toString())

                    // get cursor position
                    val initialCursorPosition = start + before
                    // check if length before change and after masking is the same - this will be
                    // true only when a separator sign has been deleted
                    val separatorDeleted =
                        beforeText.isNotEmpty() && beforeText.length == expiryDate.length
                    // get digit count after cursor position
                    val numOfCharsToRightOfCursor = getNumberOfChars(
                        beforeText.substring(
                            initialCursorPosition,
                            beforeText.length
                        )
                    )
                    input.removeTextChangedListener(this)
                    input.setText(expiryDate)
                    // set new cursor position
                    input.setSelection(
                        getNewCursorPosition(
                            numOfCharsToRightOfCursor, expiryDate, separatorDeleted
                        )
                    )
                    input.addTextChangedListener(this)
                }

            })

            input.setOnFocusChangeListener { _, hasFocus ->
                if (!hasFocus) {
                    viewModel.validateExpiryDate(input.text.toString())
                }
            }
        }

    }

    override fun getLabelText(): String =
        resources.getString(R.string.pvc_expiry_date_label)

    override fun getHintText(): String =
        resources.getString(R.string.pvc_expiry_date_hint)

    override fun getStartIcon(): Drawable? =
        ContextCompat.getDrawable(context, R.drawable.ic_calendar)

    override fun onExpiryDateValid(input: String) {
        resetError()
        // pass valid input data to the drop-in view
        inputValidationListener?.onInputValid(PaymentInputType.ExpiryDate, input)
    }

    override fun onExpiryDateInvalid(showError: Boolean) {
        if (showError) {
            setError(R.string.pvc_input_error_text)
        } else {
            resetError()
        }
        // notify a drop-in view about validation state change
        inputValidationListener?.onInputInvalid(PaymentInputType.ExpiryDate)
    }

    private fun getNewCursorPosition(
        charsCountToRightOfCursor: Int,
        numberString: String,
        separatorDeleted: Boolean
    ): Int {
        var position = 0
        var c = charsCountToRightOfCursor
        for (i in numberString.reversed()) {
            if (c == 0)
                break

            if (i.isDigit() || i == '/')
                c--
            position++
        }
        return numberString.length - position - separatorDeleted.toInt()
    }

    private fun getNumberOfChars(text: String): Int {
        var count = 0
        if (text.length == 1 && text == "/")
            return 0
        for (i in text)
            if (i.isDigit() || i == '/')
                count++
        return count
    }
}